<?php

declare(strict_types = 1);

namespace Orangear\Common\Identifier;

/**
 * Interface IdentifierInterface
 * @package Orangear\Common\Identifier
 */
interface IdentifierInterface
{
    /**
     * @return string
     */
    public function __toString(): string;
}
