<?php

declare(strict_types = 1);

namespace Orangear\Common\Identifier;

/**
 * Class Uuid4IdentifiableEntity
 * @package Orangear\Common\Identifier
 */
abstract class Uuid4IdentifiableEntity
{
    /** @var IdentifierInterface */
    protected $id;

    /**
     * @return IdentifierInterface
     */
    protected function getId(): IdentifierInterface
    {
        return $this->id;
    }

    /**
     * @param IdentifierInterface $identifier
     * @return Uuid4IdentifiableEntity
     */
    protected function setId(IdentifierInterface $identifier): self
    {
        $this->id = $identifier;

        return $this;
    }
}
