<?php

declare(strict_types = 1);

namespace Orangear\Common\Identifier;

/**
 * Interface StringIdentifierInterfaces
 * @package Orangear\Common\Identifier
 */
interface StringIdentifierInterfaces extends IdentifierInterface
{
    /**
     * @param string $identifier
     * @return IdentifierInterface
     */
    public static function fromString(string $identifier): IdentifierInterface;
}
