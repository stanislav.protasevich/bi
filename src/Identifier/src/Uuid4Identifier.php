<?php

declare(strict_types = 1);

namespace Orangear\Common\Identifier;

use InvalidArgumentException;

/**
 * Class Uuid4Identifier
 * @package Orangear\Common\Identifier
 */
abstract class Uuid4Identifier implements StringIdentifierInterfaces
{
    const FORMAT = '/^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$/';

    /** @var string */
    protected $identifier;

    /**
     * Uuid4Identifier constructor.
     * @param $identifier
     */
    protected function __construct(string $identifier)
    {
        $this->setIdentifier($identifier);
    }

    /**
     * @param string $identifier
     */
    protected function setIdentifier(string $identifier)
    {
        $this->assetValidFormat($identifier);

        $this->identifier = $identifier;
    }

    /**
     * @param string $identifier
     */
    protected function assetValidFormat(string $identifier)
    {
        if (preg_match(self::FORMAT, $identifier) !== 1) {
            throw new InvalidArgumentException('Invalid uuid4 identifier: "' . $identifier . '"');
        }
    }

    /**
     * @inheritdoc
     */
    public function __toString(): string
    {
        return (string) $this->identifier;
    }

    /**
     * @inheritdoc
     */
    public static function fromString(string $identifier): IdentifierInterface
    {
        return new static($identifier);
    }
}
