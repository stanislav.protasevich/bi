<?php

declare(strict_types = 1);

namespace Orangear\Common\CQRS;

use InvalidArgumentException;
use Throwable;

/**
 * Class InvalidCommandException
 * @package Orangear\Common\CQRS
 */
class InvalidCommandException extends InvalidArgumentException
{
    /** @var array */
    protected $messages = [];

    /**
     * InvalidCommandException constructor.
     * @param string $message
     * @param array $errors
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "", array $errors, int $code = 0, Throwable $previous = null)
    {
        $this->messages = $errors;

        parent::__construct($message, $code, $previous);
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }
}
