<?php

declare(strict_types = 1);

namespace Orangear\Common\CQRS;

/**
 * Interface CommandInterface
 * @package Orangear\Common\CQRS
 */
interface CommandInterface {}
