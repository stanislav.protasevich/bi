<?php

declare(strict_types=1);

namespace Orangear\Admin\Infrastructure\Repository;

use Orangear\Admin\Model\Admin\Admin;
use Orangear\Admin\Model\Admin\AdminCollection;
use Prooph\EventSourcing\Aggregate\AggregateRepository;

/**
 * Class EventStoreAdminCollection
 *
 * @package Orangear\Admin\Infrastructure\Repository
 */
class EventStoreAdminCollection extends AggregateRepository implements AdminCollection
{
    /**
     * @param Admin $admin
     */
    public function save(Admin $admin): void
    {
        $this->saveAggregateRoot($admin);
    }
}