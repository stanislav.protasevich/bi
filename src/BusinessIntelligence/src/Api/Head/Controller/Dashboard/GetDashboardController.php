<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Api\Head\Controller\Dashboard;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\Query\GetDashboardQuery;
use Orangear\BusinessIntelligence\Infrastructure\QueryHandler\GetDashboardQueryHandler;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GetDashboardController
 * @package Orangear\BusinessIntelligence\Api\Head\Controller\Panel
 */
final class GetDashboardController
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var GetDashboardQueryHandler */
    private $queryHandler;

    /**
     * GetDashboardController constructor
     *
     * @param TokenStorageInterface $tokenStorage
     * @param GetDashboardQueryHandler $queryHandler
     */
    public function __construct(TokenStorageInterface $tokenStorage, GetDashboardQueryHandler $queryHandler)
    {
        $this->tokenStorage = $tokenStorage;
        $this->queryHandler = $queryHandler;
    }

    /**
     * @ApiDoc(
     *   section = "Dashboard",
     *   description = "Get dashboard for head",
     *   requirements = {
     *     {
     *       "name"        = "id",
     *       "dataType"    = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Project identifier"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Throwable $error) {
            return new JsonResponse(
                [
                    'message' => $error->getMessage()
                ],
                $error->getCode()
            );
        }

        try {
            $data = $this->queryHandler->__invoke(
                GetDashboardQuery::forHead(
                    (string) $this->tokenStorage->getToken()->getUser()->id(),
                    $payload['project_id']
                )
            );

            return new JsonResponse($data);
        } catch (\Throwable $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    public function getPayloadFromRequest(Request $request)
    {
        $payload = [];

        $payload['project_id'] = $request->get('id', null);

        return $payload;
    }
}