<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Api\Head\Controller\Panel;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\BusinessIntelligence\Domain\Model\Panel\Query\GetStaffDataPanelQuery;
use Orangear\BusinessIntelligence\Infrastructure\QueryHandler\GetStaffDataPanelQueryHandler;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class StaffDataController
 * @package Orangear\BusinessIntelligence\Api\Head\Controller\Panel
 */
final class StaffDataController
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var GetStaffDataPanelQueryHandler */
    private $queryHandler;

    /**
     * StaffDataController constructor
     *
     * @param TokenStorageInterface $tokenStorage
     * @param GetStaffDataPanelQueryHandler $queryHandler
     */
    public function __construct(TokenStorageInterface $tokenStorage, GetStaffDataPanelQueryHandler $queryHandler)
    {
        $this->tokenStorage = $tokenStorage;
        $this->queryHandler = $queryHandler;
    }

    /**
     * @ApiDoc(
     *   section = "Panels",
     *   description = "Get dashboard panel for head",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        $memberId = (string) $this->tokenStorage->getToken()->getUser()->id();

        try {
            $data = $this->queryHandler->__invoke(GetStaffDataPanelQuery::forHead($memberId));

            return new JsonResponse(['data' => $data]);
        } catch (\Throwable $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}