<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Api\Head\Controller\Panel;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\Query\GetDashboardPanelQuery;
use Orangear\BusinessIntelligence\Infrastructure\QueryHandler\GetDashboardPanelQueryHandler;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DashboardController
 * @package Orangear\BusinessIntelligence\Api\Head\Controller\Panel
 */
final class DashboardController
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var GetDashboardPanelQueryHandler */
    private $queryHandler;

    /**
     * DashboardController constructor
     *
     * @param TokenStorageInterface $tokenStorage
     * @param GetDashboardPanelQueryHandler $queryHandler
     */
    public function __construct(TokenStorageInterface $tokenStorage, GetDashboardPanelQueryHandler $queryHandler)
    {
        $this->tokenStorage = $tokenStorage;
        $this->queryHandler = $queryHandler;
    }

    /**
     * @ApiDoc(
     *   section = "Panels",
     *   description = "Get dashboard panel for head",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        $memberId = (string) $this->tokenStorage->getToken()->getUser()->id();

        try {
            $data = $this->queryHandler->__invoke(GetDashboardPanelQuery::forHead($memberId));

            return new JsonResponse(['data' => $data]);
        } catch (\Throwable $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}