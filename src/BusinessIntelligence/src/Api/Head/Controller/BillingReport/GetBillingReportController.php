<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Api\Head\Controller\BillingReport;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\BusinessIntelligence\Domain\Model\BillingReport\Query\GetBillingReportQuery;
use Orangear\BusinessIntelligence\Infrastructure\QueryHandler\GetBillingReportQueryHandler;
use Orangear\BusinessIntelligence\Infrastructure\QueryHandler\GetDashboardQueryHandler;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GetBillingReportController
 * @package Orangear\BusinessIntelligence\Api\Head\Controller\Panel
 */
final class GetBillingReportController
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var GetBillingReportQueryHandler */
    private $queryHandler;

    /**
     * GetBillingReportController constructor
     *
     * @param TokenStorageInterface $tokenStorage
     * @param GetBillingReportQueryHandler $queryHandler
     */
    public function __construct(TokenStorageInterface $tokenStorage, GetBillingReportQueryHandler $queryHandler)
    {
        $this->tokenStorage = $tokenStorage;
        $this->queryHandler = $queryHandler;
    }

    /**
     * @ApiDoc(
     *   section = "Billing Report",
     *   description = "Get billing report for head",
     *   requirements = {
     *     {
     *       "name"        = "id",
     *       "dataType"    = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Project identifier"
     *     }
     *   },
     *   parameters = {
     *     {"name" = "start_date", "dataType" = "string", "required" = true, "description" = "Start date"},
     *     {"name" = "end_date", "dataType" = "string", "required" = true, "description" = "End date"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Throwable $error) {
            return new JsonResponse(
                [
                    'message' => $error->getMessage()
                ],
                $error->getCode()
            );
        }

        try {
            $data = $this->queryHandler->__invoke(
                GetBillingReportQuery::forHead(
                    (string) $this->tokenStorage->getToken()->getUser()->id(),
                    $payload['project_id'],
                    $payload['start_date'],
                    $payload['end_date']
                )
            );

            return new JsonResponse(['data' => $data]);
        } catch (\Throwable $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    public function getPayloadFromRequest(Request $request)
    {
        $payload = [];

        $payload['project_id'] = $request->get('id', null);
        $payload['start_date'] = $request->get('start_date', null);
        $payload['end_date'] = $request->get('end_date', null);

        return $payload;
    }
}
