<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Api\Head\Controller\BillingReport;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\BusinessIntelligence\Domain\Model\BillingReport\Query\GetBillingReportQuery;
use Orangear\BusinessIntelligence\Infrastructure\QueryHandler\GetBillingReportQueryHandler;
use Orangear\BusinessIntelligence\Infrastructure\QueryHandler\GetDashboardQueryHandler;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * Class ExportBillingReportController
 * @package Orangear\BusinessIntelligence\Api\Head\Controller\Panel
 */
final class ExportBillingReportController
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var GetBillingReportQueryHandler */
    private $queryHandler;

    /**
     * ExportBillingReportController constructor
     *
     * @param TokenStorageInterface $tokenStorage
     * @param GetBillingReportQueryHandler $queryHandler
     */
    public function __construct(TokenStorageInterface $tokenStorage, GetBillingReportQueryHandler $queryHandler)
    {
        $this->tokenStorage = $tokenStorage;
        $this->queryHandler = $queryHandler;
    }

    /**
     * @ApiDoc(
     *   section = "Billing Report",
     *   description = "Get billing report for head",
     *   requirements = {
     *     {
     *       "name"        = "id",
     *       "dataType"    = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Project identifier"
     *     }
     *   },
     *   parameters = {
     *     {"name" = "start_date", "dataType" = "string", "required" = true, "description" = "Start date"},
     *     {"name" = "end_date", "dataType" = "string", "required" = true, "description" = "End date"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Throwable $error) {
            return new JsonResponse(
                [
                    'message' => $error->getMessage()
                ],
                $error->getCode()
            );
        }

        try {
            $data = $this->queryHandler->__invoke(
                GetBillingReportQuery::forHead(
                    (string) $this->tokenStorage->getToken()->getUser()->id(),
                    $payload['project_id'],
                    $payload['start_date'],
                    $payload['end_date']
                )
            );

            $arrayData = [
                ['Project', 'Date', 'Revenue', 'Deduction', 'Cross Payment', 'Net Revenue', 'Revenue Received', 'Revenue Unpaid', 'Pub Amount', 'Pub paid', 'Pub unpaid', 'Gross Income', 'Net Income', 'Total Expenses', 'Net Profit']
            ];

            foreach ($data as $d) {
                $arrayData[] = [
                    $d['project']['name'],
                    $d['date'],
                    number_format($d['revenue'], 2, '.', ''),
                    number_format($d['deduction'],2, '.', ''),
                    number_format($d['crossPayment'],2, '.', ''),

                    number_format($d['revenue'],2, '.', ''),
                    number_format($d['revenueReceived'],2, '.', ''),
                    number_format($d['revenueUnpaid'],2, '.', ''),
                    number_format($d['publisherAmount'],2, '.', ''),
                    number_format($d['publisherPaid'],2, '.', ''),

                    number_format($d['publisherUnpaid'],2, '.', ''),
                    number_format($d['grossIncome'],2, '.', ''),
                    number_format($d['netIncome'],2, '.', ''),
                    number_format($d['totalExpenses'],2, '.', ''),
                    number_format($d['netProfit'],2, '.', ''),
                ];
            }

            if(count($arrayData) < 0) {
                return new JsonResponse(['errors' => 'No data'], 400);
            }

            $columns = array_slice(range('A', 'Z'), 0, count($arrayData[0]));

            $spreadsheet = new Spreadsheet();
            $spreadsheet->getActiveSheet()->freezePane('A2');
            $spreadsheet->getActiveSheet()->fromArray($arrayData, null, 'A1');

            foreach ($columns as $column) {
                $spreadsheet->getActiveSheet()->getColumnDimension($column)->setAutoSize(true);
            }

            $spreadsheet->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $count = count($data);
            foreach ($columns as $column) {
                $spreadsheet->getActiveSheet()
                    ->setCellValueExplicit(
                        $column . ($count + 2),
                        '=SUM(' . $column . '2:' . $column . ($count + 1) .')',
                        \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_FORMULA
                    );
            }

            $spreadsheet->getActiveSheet()->getStyle('A' . ($count + 2) . ':O' . ($count + 2))->getFont()->setBold(true);

            $spreadsheet->getActiveSheet()->mergeCells('A' . ($count + 2) . ':B' . ($count + 2));
            $spreadsheet->getActiveSheet()->getCell('A' . ($count + 2))->setValue('TOTAL');
            $spreadsheet->getActiveSheet()->getStyle('A' . ($count + 2))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $spreadsheet->getActiveSheet()->getStyle('A1:O' . ($count + 2))->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $spreadsheet->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

            $spreadsheet->getProperties()
                ->setCreator('Tapmedia Business Intelligence')
                ->setTitle('Billing Report');

            header('Access-Control-Allow-Origin: *');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8');
            header('Content-Disposition: attachment;filename="billing-report.xlsx"');
            header('Cache-Control: max-age=0');

            $writer = new Xlsx($spreadsheet);
//            $writer->save('report_admin.xlsx');
            $writer->save('php://output');

            return new JsonResponse('', 200);
        } catch (\Throwable $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    public function getPayloadFromRequest(Request $request)
    {
        $payload = [];

        $payload['project_id'] = $request->get('id', null);
        $payload['start_date'] = $request->get('start_date', null);
        $payload['end_date'] = $request->get('end_date', null);

        return $payload;
    }
}
