<?php

namespace Orangear\BusinessIntelligence\Api\Head\Controller\ClientData;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ModelController
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * BestPublisherChartController constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @ApiDoc(
     *   section = "Client Data",
     *   description = "Get models",
     *   statusCodes = {
     *     202 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        $data = [
            [
                'model' => 'CPA',
                'description' => 'cost per action'
            ],
            [
                'model' => 'CPI',
                'description' => 'cost per install'
            ],
        ];

        return new JsonResponse( $data );
    }
}
