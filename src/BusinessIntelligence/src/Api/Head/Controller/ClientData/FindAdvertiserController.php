<?php

namespace Orangear\BusinessIntelligence\Api\Head\Controller\ClientData;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\BusinessIntelligence\Infrastructure\Query\ClientData\FindAdvertiserQuery;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use DateTime;

class FindAdvertiserController
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var FindAdvertiserQuery */
    private $query;

    /**
     * BestPublisherChartController constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param FindAdvertiserQuery $query
     */
    public function __construct(TokenStorageInterface $tokenStorage, FindAdvertiserQuery $query)
    {
        $this->tokenStorage = $tokenStorage;
        $this->query = $query;
    }

    /**
     * @ApiDoc(
     *   section = "Client Data",
     *   description = "Find advertiser",
     *   parameters = {
     *     {"name" = "company_id", "dataType" = "string", "required" = false, "description" = "Company identifier"},
     *     {"name" = "start_date", "dataType" = "string", "required" = true, "description" = "Start date"},
     *     {"name" = "end_date", "dataType" = "string", "required" = true, "description" = "End date"},
     *     {"name" = "country_iso", "dataType" = "string", "required" = false, "description" = "Country ISO code"},
     *     {"name" = "model", "dataType" = "string", "required" = false, "description" = "Business model"},
     *     {"name" = "amount_from", "dataType" = "int", "required" = false, "description" = "Minimum amount"},
     *     {"name" = "amount_to", "dataType" = "int", "required" = false, "description" = "Maximum amount"},
     *     {"name" = "name", "dataType" = "string", "required" = true, "description" = "Business model"},
     *     {"name" = "exclude_ids", "dataType" = "array", "required" = true, "description" = "Exclude ids"},
     *   },
     *   statusCodes = {
     *     202 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        $companyId = $request->request->get( 'company_id', null );
        $from = $request->request->get( 'start_date', null );
        $to = $request->request->get( 'end_date', null );
        $country = $request->request->get( 'country_iso', null );
        $amountFrom = $request->request->get( 'amount_from', null );
        $amountTo = $request->request->get( 'amount_to', null );
        $name = $request->request->get( 'name', null );
        $type = $request->request->get( 'type', null );
        $ids = $request->request->get( 'exclude_ids', [] );

        if ($from == null || $to == null) {
            return new JsonResponse( [
                'message' => 'Dates cannot be null.'
            ], 422 );
        }

        if ($name == null || empty( $name )) {
            return new JsonResponse( [
                'message' => 'Advertiser name cannot be null.'
            ], 422 );
        }

        if ($country == 'Choose Country') {
            $country = null;
        }

        try {
            $data = ($this->query)( $companyId, $type, $name, new DateTime( $from ), new DateTime( $to ), $amountFrom, $amountTo, $country, $ids, (string) $this->tokenStorage->getToken()->getUser()->id());
        } catch (\Exception $e) {
            return new JsonResponse( [
                'message' => 'Error. Contact developer',
            ], 422 );
        }

        return new JsonResponse( $data );
    }
}
