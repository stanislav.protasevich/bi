<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Api\Head\Controller\StaffData;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query\GetStaffDataByManagerQuery;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query\GetStaffDataByProjectQuery;
use Orangear\BusinessIntelligence\Domain\Model\Project\Command\CreateProjectCommand;
use Orangear\BusinessIntelligence\Infrastructure\QueryHandler\GetProjectStaffDataQueryHandler;
use Prooph\ServiceBus\Exception\CommandDispatchException;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Prooph\ServiceBus\QueryBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ProjectDataController
 *
 * @package Orangear\BusinessIntelligence\Api\Controller\StaffData
 */
final class ProjectDataController
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var GetProjectStaffDataQueryHandler */
    private $queryHandler;

    /**
     * ProjectDataController constructor
     *
     * @param TokenStorageInterface $tokenStorage
     * @param GetProjectStaffDataQueryHandler $queryHandler
     */
    public function __construct(TokenStorageInterface $tokenStorage, GetProjectStaffDataQueryHandler $queryHandler)
    {
        $this->tokenStorage = $tokenStorage;
        $this->queryHandler = $queryHandler;
    }

    /**
     * @ApiDoc(
     *   section = "Staff Data",
     *   description = "Get staff data by project",
     *   requirements = {
     *     {
     *       "name"        = "id",
     *       "dataType"    = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Project identifier"
     *     }
     *   },
     *   parameters = {
     *     {"name" = "start_date", "dataType" = "string", "required" = true, "description" = "Start date"},
     *     {"name" = "end_date", "dataType" = "string", "required" = false, "description" = "End date"},
     *   },
     *   statusCodes = {
     *     202 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Throwable $error) {
            return new JsonResponse(
                [
                    'message' => $error->getMessage()
                ],
                $error->getCode()
            );
        }

        $query = GetStaffDataByProjectQuery::forMember(
            (string) $this->tokenStorage->getToken()->getUser()->id()->toString(),
            $payload['project_id'],
            $payload['start_date'],
            $payload['end_date']
        );

        try {
            $response = $this->queryHandler->__invoke($query);

            return new JsonResponse(['data' => $response]);
        } catch (\Throwable $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }

    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    public function getPayloadFromRequest(Request $request)
    {
        $payload = [];

        $payload['project_id'] = $request->attributes->get('id', null);
        $payload['start_date'] = $request->query->get('start_date', null);
        $payload['end_date'] = $request->query->get('end_date', null);

        return $payload;
    }
}
