<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Api\Head\Controller\StaffData;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query\GetManagerDataSummaryQuery;
use Orangear\BusinessIntelligence\Infrastructure\QueryHandler\GetHeadManagerDataSummaryQueryHandler;
use Prooph\ServiceBus\QueryBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ManagerDataController
 *
 * @package Orangear\BusinessIntelligence\Api\Head\Controller\StaffData
 */
final class ManagerSummaryController
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var GetHeadManagerDataSummaryQueryHandler */
    private $queryHandler;

    /**
     * ManagerDataController constructor
     *
     * @param TokenStorageInterface $tokenStorage
     * @param GetHeadManagerDataSummaryQueryHandler $queryHandler
     */
    public function __construct(TokenStorageInterface $tokenStorage, GetHeadManagerDataSummaryQueryHandler $queryHandler)
    {
        $this->tokenStorage = $tokenStorage;
        $this->queryHandler = $queryHandler;
    }

    /**
     * @ApiDoc(
     *   section = "Staff Data",
     *   description = "Get staff data by manager",
     *   statusCodes = {
     *     202 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Throwable $error) {
            return new JsonResponse(
                [
                    'message' => $error->getMessage()
                ],
                $error->getCode()
            );
        }

        $query = GetManagerDataSummaryQuery::forHead(
            (string) $this->tokenStorage->getToken()->getUser()->id(),
            $payload['employee_id']
        );

        try {
            $response = $this->queryHandler->__invoke($query);

            return new JsonResponse(['data' => $response]);
        } catch (\Throwable $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    public function getPayloadFromRequest(Request $request)
    {
        $payload = [];

        $payload['employee_id'] = $request->attributes->get('id', null);

        return $payload;
    }
}
