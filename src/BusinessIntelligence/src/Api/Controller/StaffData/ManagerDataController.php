<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Api\Controller\StaffData;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query\GetStaffDataByManagerQuery;
use Orangear\BusinessIntelligence\Domain\Model\Project\Command\CreateProjectCommand;
use Prooph\ServiceBus\Exception\CommandDispatchException;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Prooph\ServiceBus\QueryBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


/**
 * Class ManagerDataController
 *
 * @package Orangear\BusinessIntelligence\Api\Controller\StaffData
 */
final class ManagerDataController
{
    /** @var QueryBus */
    private $queryBus;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * CreateProjectController constructor
     *
     * @param QueryBus $queryBus
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(QueryBus $queryBus, TokenStorageInterface $tokenStorage)
    {
        $this->queryBus = $queryBus;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @ApiDoc(
     *   section = "Staff Data",
     *   description = "Get staff data by manager",
     *   parameters = {
     *     {"name" = "start_date", "dataType" = "string", "required" = true, "description" = "Start date"},
     *     {"name" = "end_date", "dataType" = "string", "required" = false, "description" = "End date"},
     *   },
     *   statusCodes = {
     *     202 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Throwable $error) {
            return new JsonResponse(
                [
                    'message' => $error->getMessage()
                ],
                $error->getCode()
            );
        }

        $query = GetStaffDataByManagerQuery::forMember(
            null, //$this->tokenStorage->getToken()->getUser()->id()->toString(),
            $payload['employee_id'],
            $payload['start_date'],
            $payload['end_date']
        );

        try {
            $response = $this->queryBus->dispatch($query);

            $response->then(function($result) use (&$data) {
                $data = $result;
            })->otherwise(function(\Exception $exception) use (&$data) {
                $data = ['errors' => $exception->getPrevious()->getMessage()];
            });

            return new JsonResponse(['data' => $data]);
        } catch (CommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }

    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    public function getPayloadFromRequest(Request $request)
    {
        $payload = [];

        $payload['employee_id'] = $request->attributes->get('id', null);
        $payload['start_date'] = $request->query->get('start_date', null);
        $payload['end_date'] = $request->query->get('end_date', null);

        return $payload;
    }
}
