<?php

namespace Orangear\BusinessIntelligence\Api\Controller\StaffData;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\BusinessIntelligence\Domain\Model\StaffData\Query\CompanyKpiChartQuery;
use Orangear\BusinessIntelligence\Domain\Model\StaffData\Query\ManagerKpiChartQuery;
use Prooph\ServiceBus\QueryBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class KpiChartController
 * @package Orangear\BusinessIntelligence\Api\Controller\StaffData
 */
final class KpiChartController
{
    const CHART_TYPE_BEST_MANAGER   = 'top-best-managers';
    const CHART_TYPE_WORST_MANAGER  = 'top-worst-managers';
    const CHART_TYPE_BEST_COMPANY   = 'top-best-companies';
    const CHART_TYPE_WORST_COMPANY  = 'top-worst-companies';

    /** @var QueryBus */
    private $queryBus;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * KpiChartController constructor
     *
     * @param QueryBus $queryBus
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(QueryBus $queryBus, TokenStorageInterface $tokenStorage)
    {
        $this->queryBus = $queryBus;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @ApiDoc(
     *   section = "Staff Data",
     *   description = "Get staff data KPI chart",
     *   parameters = {
     *     {"name" = "start_date", "dataType" = "string", "required" = true, "description" = "Start date"},
     *     {"name" = "end_date", "dataType" = "string", "required" = false, "description" = "End date"},
     *   },
     *   statusCodes = {
     *     202 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Exception $exception) {
            return new JsonResponse(
                [
                    'message' => $exception->getMessage()
                ],
                $exception->getCode()
            );
        }

        $query = ManagerKpiChartQuery::forAdmin(null, $payload['start_date'], $payload['end_date'], ManagerKpiChartQuery::DIRECTION_BEST);
        switch ($payload['type']) {
            case self::CHART_TYPE_WORST_MANAGER:
                $query = ManagerKpiChartQuery::forAdmin(null, $payload['start_date'], $payload['end_date'], ManagerKpiChartQuery::DIRECTION_WORST);
                break;
            case self::CHART_TYPE_BEST_COMPANY:
                $query = CompanyKpiChartQuery::forAdmin(null, $payload['start_date'], $payload['end_date'], CompanyKpiChartQuery::DIRECTION_BEST);
                break;
            case self::CHART_TYPE_WORST_COMPANY:
                $query = CompanyKpiChartQuery::forAdmin(null, $payload['start_date'], $payload['end_date'], CompanyKpiChartQuery::DIRECTION_WORST);
                break;
        }

        try {
            $response = $this->queryBus->dispatch($query);

            $data = [];
            $response->then(function($result) use (&$data) { $data = $result; })
            ->otherwise(function ($exception){
                var_dump($exception->getPrevious());
            });

            return new JsonResponse(['data' => $data]);
        } catch (\Exception $exception) {
            return new JsonResponse(['message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST, []);
        }
    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    public function getPayloadFromRequest(Request $request)
    {
        $payload = [];

        $payload['start_date'] = $request->query->get('start_date', null);
        $payload['end_date'] = $request->query->get('end_date', null);

        $type = $request->attributes->get('type', self::CHART_TYPE_BEST_MANAGER);

        if (!in_array($type, [self::CHART_TYPE_BEST_MANAGER, self::CHART_TYPE_WORST_MANAGER, self::CHART_TYPE_BEST_COMPANY, self::CHART_TYPE_WORST_COMPANY])) {
            throw new \InvalidArgumentException('Invalid chart type');
        }

        $payload['type'] = $type;

        return $payload;
    }
}
