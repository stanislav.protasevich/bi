<?php

namespace Orangear\BusinessIntelligence\Api\Controller\ClientData;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\BusinessIntelligence\Infrastructure\Query\ClientData\GetAdvertiserQuery;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use DateTime;

class AdvertiserStatisticsController
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var GetAdvertiserQuery */
    private $query;

    /**
     * BestPublisherChartController constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param GetAdvertiserQuery $query
     */
    public function __construct(TokenStorageInterface $tokenStorage, GetAdvertiserQuery $query)
    {
        $this->tokenStorage = $tokenStorage;
        $this->query = $query;
    }

    /**
     * @ApiDoc(
     *   section = "Client Data",
     *   description = "Get advertiser statistics",
     *   requirements = {
     *     {
     *       "name"        = "id",
     *       "dataType"    = "string",
     *       "requirement" = "\d+",
     *       "description" = "Advertiser identifier"
     *     }
     *   },
     *   parameters = {
     *     {"name" = "company_id", "dataType" = "string", "required" = false, "description" = "Company identifier"},
     *     {"name" = "start_date", "dataType" = "string", "required" = true, "description" = "Start date"},
     *     {"name" = "end_date", "dataType" = "string", "required" = true, "description" = "End date"},
     *     {"name" = "country_iso", "dataType" = "string", "required" = false, "description" = "Country ISO code"},
     *     {"name" = "model", "dataType" = "string", "required" = false, "description" = "Business model"},
     *     {"name" = "amount_from", "dataType" = "int", "required" = false, "description" = "Minimum amount"},
     *     {"name" = "amount_to", "dataType" = "int", "required" = false, "description" = "Maximum amount"},
     *   },
     *   statusCodes = {
     *     202 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        $id = $request->attributes->get( 'id', null );
        $from = $request->get( 'start_date', null );
        $to = $request->get( 'end_date', null );
        $companyId = $request->get( 'company_id', null );
        $amountFrom = $request->get( 'amount_from', null );
        $amountTo = $request->get( 'amount_to', null );
        $country = $request->get( 'country_iso', null );

        if ($from == null || $to == null) {
            return new JsonResponse( [
                'message' => 'Dates cannot be null.'
            ], 422 );
        }

        if ($id == null) {
            return new JsonResponse( [
                'message' => 'Advertiser identifier cannot be null.'
            ], 422 );
        }

//        if ($companyId == null) {
//            return new JsonResponse( [
//                'message' => 'Company identifier cannot be null.'
//            ], 422 );
//        }

        if ($country == 'Choose Country') {
            $country = null;
        }

        try {
            $data = ($this->query)( $companyId, $id, new DateTime( $from ), new DateTime( $to ), $country, $amountFrom, $amountTo );
        } catch (\Exception $e) {
            return new JsonResponse( [
                'message' => 'Error. Contact developer',
            ], 422 );
        }

        return new JsonResponse( $data );
    }
}
