<?php

namespace Orangear\BusinessIntelligence\Api\Controller\ClientData;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\BusinessIntelligence\Infrastructure\Query\ClientData\WorstAdvertiserChartQuery;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ClickHouse\Client as ClickHouseClient;
use DateTime;

class WorstAdvertiserChartController
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var WorstAdvertiserChartQuery */
    private $query;

    /**
     * BestPublisherChartController constructor.
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage, WorstAdvertiserChartQuery $query)
    {
        $this->tokenStorage = $tokenStorage;
        $this->query = $query;
    }

    /**
     * @ApiDoc(
     *   section = "Client Data",
     *   description = "Worst advertisers",
     *   parameters = {
     *     {"name" = "company_id", "dataType" = "string", "required" = false, "description" = "Company identifier"},
     *     {"name" = "start_date", "dataType" = "string", "required" = true, "description" = "Start date"},
     *     {"name" = "end_date", "dataType" = "string", "required" = true, "description" = "End date"},
     *     {"name" = "type", "dataType" = "string", "required" = true, "description" = "End date"},
     *     {"name" = "country_iso", "dataType" = "string", "required" = false, "description" = "Country ISO code"},
     *     {"name" = "model", "dataType" = "string", "required" = false, "description" = "Business model"},
     *     {"name" = "amount_from", "dataType" = "int", "required" = false, "description" = "Minimum amount"},
     *     {"name" = "amount_to", "dataType" = "int", "required" = false, "description" = "Maximum amount"},
     *   },
     *   statusCodes = {
     *     202 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        $from = $request->get( 'start_date', null );
        $to = $request->get( 'end_date', null );
        $type = $request->get( 'type', null );
        $companyId = $request->get( 'company_id', null );
        $amountFrom = $request->get( 'amount_from', 1 );
        $amountTo = $request->get( 'amount_to', null );
        $country = $request->get( 'country_iso', null );

        if ($from == null || $to == null) {
            return new JsonResponse( [
                'message' => 'Dates cannot be null.'
            ], 422 );
        }

        if ($country == 'Choose Country') {
            $country = null;
        }

        try {
            $data = ($this->query)( new DateTime( $from ), new DateTime( $to ), $type, $companyId, $amountFrom, $amountTo, $country );
        } catch (\Exception $e) {
            return new JsonResponse( [
                'message' => 'Error. Contact developer',
            ], 422 );
        }

        return new JsonResponse( $data );
    }
}
