<?php

namespace Orangear\BusinessIntelligence\Api\Controller\Platform;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\CommandDispatchException;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Orangear\BusinessIntelligence\Domain\Model\Platform\Command\CreatePlatformCommand;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class CreatePlatformController
 *
 * @package Orangear\BusinessIntelligence\Api\Controller\Platform
 */
final class CreatePlatformController
{
    /** @var CommandBus */
    private $commandBus;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * CreatePlatformController constructor
     *
     * @param CommandBus $commandBus
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(CommandBus $commandBus, TokenStorageInterface $tokenStorage)
    {
        $this->commandBus = $commandBus;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @ApiDoc(
     *   section = "Platform",
     *   description = "Create new platform",
     *   parameters = {
     *     {"name" = "name", "dataType" = "string", "required" = true, "description" = "Platform name"},
     *     {"name" = "logo", "dataType" = "string", "required" = false, "description" = "Platform logotype image"},
     *   },
     *   statusCodes = {
     *     202 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Throwable $error) {
            return new JsonResponse(
                [
                    'message' => $error->getMessage()
                ],
                $error->getCode()
            );
        }

        $command = CreatePlatformCommand::forAdmin(
            $this->tokenStorage->getToken()->getUser()->id()->toString(),
            $payload['platform_name'],
            $payload['platform_logo']
        );

        try {
            $this->commandBus->dispatch($command);
        } catch (CommandDispatchException $exception) {
            $exception = $exception->getPrevious();

            if ($exception instanceof MessageDispatchException) {
                $exception = $exception->getPrevious();
            }

            return new JsonResponse(['message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST, []);
        }

        return new JsonResponse(null, Response::HTTP_ACCEPTED);
    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    public function getPayloadFromRequest(Request $request)
    {
        $payload = [];

        $payload['platform_name'] = $request->request->get('name', null);
        $payload['platform_logo'] = $request->request->get('logo', null);

        return $payload;
    }
}
