<?php

namespace Orangear\BusinessIntelligence\Api\Controller\Project;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\BusinessIntelligence\Domain\Model\Project\Command\CreateProjectCommand;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\CommandDispatchException;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class CreateProjectController
 *
 * @package Orangear\BusinessIntelligence\Api\Controller\Project
 */
final class CreateProjectController
{
    /** @var CommandBus */
    private $commandBus;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * CreateProjectController constructor
     *
     * @param CommandBus $commandBus
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(CommandBus $commandBus, TokenStorageInterface $tokenStorage)
    {
        $this->commandBus = $commandBus;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @ApiDoc(
     *   section = "Project",
     *   description = "Create new project",
     *   parameters = {
     *     {"name" = "platform_id", "dataType" = "string", "required" = true, "description" = "Project platform identifier"},
     *     {"name" = "name", "dataType" = "string", "required" = true, "description" = "Project name"},
     *     {"name" = "logo", "dataType" = "string", "required" = false, "description" = "Project logotype image"},
     *     {"name" = "api_url", "dataType" = "string", "required" = true, "description" = "Project's API URL"},
     *   },
     *   statusCodes = {
     *     202 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Throwable $error) {
            return new JsonResponse(
                [
                    'message' => $error->getMessage()
                ],
                $error->getCode()
            );
        }

        $command = CreateProjectCommand::forAdmin(
            $this->tokenStorage->getToken()->getUser()->id()->toString(),
            $payload['platform_id'],
            $payload['project_name'],
            $payload['project_logo'],
            $payload['project_api_url']
        );

        try {
            $this->commandBus->dispatch($command);
        } catch (CommandDispatchException $exception) {
            $exception = $exception->getPrevious();

            if ($exception instanceof MessageDispatchException) {
                $exception = $exception->getPrevious();
            }

            return new JsonResponse(['message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST, []);
        }

        return new JsonResponse(null, Response::HTTP_ACCEPTED);
    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    public function getPayloadFromRequest(Request $request)
    {
        $payload = [];

        $payload['platform_id'] = $request->request->get('platform_id', null);
        $payload['project_name'] = $request->request->get('name', null);
        $payload['project_logo'] = $request->request->get('logo', null);
        $payload['project_api_url'] = $request->request->get('api_url', null);

        return $payload;
    }
}
