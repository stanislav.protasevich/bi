<?php

namespace Orangear\BusinessIntelligence\Api\Controller\ChargeType;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\Query\ListChargeTypeQuery;
use Prooph\ServiceBus\QueryBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ListChargeTypeController
 *
 * @package Orangear\BusinessIntelligence\Api\Controller\ChargeType
 */
final class ListChargeTypeController
{
    /** @var QueryBus */
    private $queryBus;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * ListChargeTypeController constructor
     *
     * @param QueryBus $queryBus
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(QueryBus $queryBus, TokenStorageInterface $tokenStorage)
    {
        $this->queryBus = $queryBus;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @ApiDoc(
     *   section = "Charge types",
     *   description = "Get list of charge types",
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $query = ListChargeTypeQuery::forMember(
                $this->tokenStorage->getToken()->getUser()->id()->toString()
            );
        } catch (\Throwable $error) {
            return new JsonResponse(
                [
                    'message' => $error->getMessage()
                ],
                $error->getCode()
            );
        }

        try {
            $data = [];

            $response = $this->queryBus->dispatch($query);

            $response
                ->then(function($result) use (&$data) {
                    $data = $result;
                })
                ->otherwise(function(\Exception $exception) use (&$data) {
                    $data = ['errors' => $exception->getPrevious()->getMessage()];
                });

            return new JsonResponse(['data' => $data]);
        } catch (CommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        } catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}
