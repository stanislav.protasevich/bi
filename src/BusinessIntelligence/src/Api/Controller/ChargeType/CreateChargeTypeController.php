<?php

namespace Orangear\BusinessIntelligence\Api\Controller\ChargeType;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\Command\CreateChargeTypeCommand;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\CommandDispatchException;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class CreateChargeTypeController
 *
 * @package Orangear\BusinessIntelligence\Api\Controller\ChargeType
 */
final class CreateChargeTypeController
{
    /** @var CommandBus */
    private $commandBus;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * CreateChargeTypeController constructor
     *
     * @param CommandBus $commandBus
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(CommandBus $commandBus, TokenStorageInterface $tokenStorage)
    {
        $this->commandBus = $commandBus;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @ApiDoc(
     *   section = "Charge types",
     *   description = "Create charge type",
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   parameters = {
     *     {"name" = "name", "dataType" = "string", "required" = true, "description" = "Charge type name"},
     *     {"name" = "description", "dataType" = "string", "required" = false, "description" = "Charge type description"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Throwable $error) {
            return new JsonResponse(
                [
                    'message' => $error->getMessage()
                ],
                $error->getCode()
            );
        }

        $command = CreateChargeTypeCommand::forMember(
            $this->tokenStorage->getToken()->getUser()->id()->toString(),
            $payload['charge_type_name'],
            $payload['charge_type_description']
        );

        try {
            $this->commandBus->dispatch($command);
        } catch (CommandDispatchException $exception) {
            $exception = $exception->getPrevious();

            if ($exception instanceof MessageDispatchException) {
                $exception = $exception->getPrevious();
            }

            return new JsonResponse(['message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST, []);
        }

        return new JsonResponse(null, Response::HTTP_ACCEPTED);
    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    public function getPayloadFromRequest(Request $request)
    {
        $payload = [];

        $payload['charge_type_name'] = $request->get('name', null);
        $payload['charge_type_description'] = $request->get('description', null);

        return $payload;
    }
}
