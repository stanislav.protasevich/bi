<?php

namespace Orangear\BusinessIntelligence\Api\Controller\Synchronization;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\Command\SyncDashboardCommand;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\CommandDispatchException;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class SyncDashboardController
 *
 * @package Orangear\BusinessIntelligence\Api\Controller\Dashborad
 */
final class SyncDashboardController
{
    /** @var CommandBus */
    private $commandBus;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * SyncDashboardController constructor
     *
     * @param CommandBus $commandBus
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(CommandBus $commandBus, TokenStorageInterface $tokenStorage)
    {
        $this->commandBus = $commandBus;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @ApiDoc(
     *   section = "Synchronization",
     *   description = "Sync project's dashboard",
     *   parameters = {
     *     {"name" = "dashboard", "dataType" = "string", "required" = true, "description" = "Dashboard data"},
     *   },
     *   statusCodes = {
     *     202 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Throwable $error) {
            return new JsonResponse(
                [
                    'message' => $error->getMessage()
                ],
                $error->getCode()
            );
        }

        $command = SyncDashboardCommand::forProject(
            $request->headers->get('X-Auth-Token'),
            $payload['period'],
            $payload['timezone'],
            $payload['incomeToday'],
            $payload['incomeYesterday'],
            $payload['incomeLast7Days'],
            $payload['incomeThisMonth'],
            $payload['incomeLastMonth'],
            $payload['incomeAllTime'],
            $payload['incomeSuperlinkToday'],
            $payload['incomeSuperlinkYesterday'],
            $payload['incomeSuperlinkLast7Days'],
            $payload['incomeSuperlinkThisMonth'],
            $payload['incomeSuperlinkLastMonth'],
            $payload['incomeSuperlinkAllTime'],
            $payload['revenueSuperlinkToday'],
            $payload['revenueSuperlinkYesterday'],
            $payload['revenueSuperlinkLast7Days'],
            $payload['revenueSuperlinkThisMonth'],
            $payload['revenueSuperlinkLastMonth'],
            $payload['revenueSuperlinkAllTime'],
            $payload['payoutToday'],
            $payload['payoutYesterday'],
            $payload['payoutLast7Days'],
            $payload['payoutThisMonth'],
            $payload['payoutLastMonth'],
            $payload['payoutAllTime'],
            $payload['offersActive'],
            $payload['offersPrivate'],
            $payload['offersSuspended'],
            $payload['offersIncent'],
            $payload['offersNonIncent'],
            $payload['offersTotal'],
            $payload['offersPerDay']
        );

        try {
            $this->commandBus->dispatch($command);
        } catch (CommandDispatchException $exception) {
            $exception = $exception->getPrevious();

            if ($exception instanceof MessageDispatchException) {
                $exception = $exception->getPrevious();
            }

            return new JsonResponse(['message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST, []);
        }

        return new JsonResponse(null, Response::HTTP_ACCEPTED);
    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    public function getPayloadFromRequest(Request $request)
    {
        $payload = [];

        $dashboard = json_decode($request->get('dashboard') ?? [], true);

        $payload['period']                    = $dashboard['period'] ?? null;
        $payload['timezone']                  = $dashboard['timezone'] ?? null;
        $payload['incomeToday']               = $dashboard['incomeToday'] ?? null;
        $payload['incomeYesterday']           = $dashboard['incomeYesterday'] ?? null;
        $payload['incomeLast7Days']           = $dashboard['incomeLast7Days'] ?? null;
        $payload['incomeThisMonth']           = $dashboard['incomeThisMonth'] ?? null;
        $payload['incomeLastMonth']           = $dashboard['incomeLastMonth'] ?? null;
        $payload['incomeAllTime']             = $dashboard['incomeAllTime'] ?? null;
        $payload['incomeSuperlinkToday']      = $dashboard['incomeSuperlinkToday'] ?? null;
        $payload['incomeSuperlinkYesterday']  = $dashboard['incomeSuperlinkYesterday'] ?? null;
        $payload['incomeSuperlinkLast7Days']  = $dashboard['incomeSuperlinkLast7Days'] ?? null;
        $payload['incomeSuperlinkThisMonth']  = $dashboard['incomeSuperlinkThisMonth'] ?? null;
        $payload['incomeSuperlinkLastMonth']  = $dashboard['incomeSuperlinkLastMonth'] ?? null;
        $payload['incomeSuperlinkAllTime']    = $dashboard['incomeSuperlinkAllTime'] ?? null;
        $payload['revenueSuperlinkToday']     = $dashboard['revenueSuperlinkToday'] ?? null;
        $payload['revenueSuperlinkYesterday'] = $dashboard['revenueSuperlinkYesterday'] ?? null;
        $payload['revenueSuperlinkLast7Days'] = $dashboard['revenueSuperlinkLast7Days'] ?? null;
        $payload['revenueSuperlinkThisMonth'] = $dashboard['revenueSuperlinkThisMonth'] ?? null;
        $payload['revenueSuperlinkLastMonth'] = $dashboard['revenueSuperlinkLastMonth'] ?? null;
        $payload['revenueSuperlinkAllTime']   = $dashboard['revenueSuperlinkAllTime'] ?? null;
        $payload['payoutToday']               = $dashboard['payoutToday'] ?? null;
        $payload['payoutYesterday']           = $dashboard['payoutYesterday'] ?? null;
        $payload['payoutLast7Days']           = $dashboard['payoutLast7Days'] ?? null;
        $payload['payoutThisMonth']           = $dashboard['payoutThisMonth'] ?? null;
        $payload['payoutLastMonth']           = $dashboard['payoutLastMonth'] ?? null;
        $payload['payoutAllTime']             = $dashboard['payoutAllTime'] ?? null;
        $payload['offersActive']              = $dashboard['offersActive'] ?? null;
        $payload['offersPrivate']             = $dashboard['offersPrivate'] ?? null;
        $payload['offersSuspended']           = $dashboard['offersSuspended'] ?? null;
        $payload['offersIncent']              = $dashboard['offersIncent'] ?? null;
        $payload['offersNonIncent']           = $dashboard['offersNonIncent'] ?? null;
        $payload['offersTotal']               = $dashboard['offersTotal'] ?? null;
        $payload['offersPerDay']              = $dashboard['offersPerDay'] ?? null;

        return $payload;
    }
}
