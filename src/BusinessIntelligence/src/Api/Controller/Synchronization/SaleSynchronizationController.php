<?php

namespace Orangear\BusinessIntelligence\Api\Controller\Synchronization;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Command\SaleSynchronizationCommand;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\CommandDispatchException;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SaleSynchronizationController
 * @package Orangear\BusinessIntelligence\Api\Controller\Synchronization
 */
class SaleSynchronizationController
{
    /** @var CommandBus */
    private $commandBus;

    /**
     * ManagerSaleSynchronizationController constructor
     *
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @ApiDoc(
     *   section = "Synchronization",
     *   description = "Sales synchronization",
     *   parameters = {
     *     {"name" = "data", "dataType" = "string", "required" = true, "description" = "Sales data"},
     *   },
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Throwable $error) {
            return new JsonResponse(
                [
                    'message' => $error->getMessage()
                ],
                $error->getCode()
            );
        }

        $command = SaleSynchronizationCommand::forToken(
            $payload['token'],
            $payload['data']
        );

        try {
            $this->commandBus->dispatch($command);
        } catch (CommandDispatchException $exception) {
            $exception = $exception->getPrevious();

            if ($exception instanceof MessageDispatchException) {
                $exception = $exception->getPrevious();
            }

            return new JsonResponse(['message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST, []);
        }

        return new JsonResponse(null, Response::HTTP_ACCEPTED);
    }

    private function getPayloadFromRequest($request)
    {
        $payload = [];

        $payload['token'] = $request->headers->get('X-AUTH-TOKEN', null);
        $payload['data'] = json_decode($request->get('data', null), true);

        return $payload;
    }
}