<?php

namespace Orangear\BusinessIntelligence\Api\Controller\Synchronization;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\Command\SyncDashboardCommand;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Command\SyncManagerSaleCommand;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\Command\SyncEmployeeCommand;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\CommandDispatchException;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ManagerSaleSynchronizationController
 *
 * @package Orangear\BusinessIntelligence\Api\Controller\Dashborad
 */
final class ManagerSaleSynchronizationController
{
    /** @var CommandBus */
    private $commandBus;

    /**
     * ManagerSaleSynchronizationController constructor
     *
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @ApiDoc(
     *   section = "Synchronization",
     *   description = "Manager sales synchronization",
     *   requirements = {
     *     {"name" = "project_id", "dataType" = "string", "required" = true, "description" = "Project identifier"},
     *   },
     *   parameters = {
     *     {"name" = "start_date", "dataType" = "string", "required" = true, "description" = "Start date"},
     *     {"name" = "end_date", "dataType" = "string", "required" = true, "description" = "End date"}
     *   },
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Throwable $error) {
            return new JsonResponse(
                [
                    'message' => $error->getMessage()
                ],
                $error->getCode()
            );
        }

        $command = SyncManagerSaleCommand::forProject(
            $payload['project_id'],
            $payload['start_date'],
            $payload['end_date']
        );

        try {
            $this->commandBus->dispatch($command);
        } catch (CommandDispatchException $exception) {
            $exception = $exception->getPrevious();

            if ($exception instanceof MessageDispatchException) {
                $exception = $exception->getPrevious();
            }

            return new JsonResponse(['message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST, []);
        }

        return new JsonResponse(null, Response::HTTP_ACCEPTED);
    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    public function getPayloadFromRequest(Request $request)
    {
        $payload = [];

        $payload['project_id'] = $request->get('project_id', null);
        $payload['start_date'] = $request->get('start_date', null);
        $payload['end_date'] = $request->get('end_date', null);

        return $payload;
    }
}
