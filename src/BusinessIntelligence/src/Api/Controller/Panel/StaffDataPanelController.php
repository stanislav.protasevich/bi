<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Api\Controller\Panel;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\BusinessIntelligence\Domain\Model\Panel\Query\GetStaffDataPanelQuery;
use Prooph\ServiceBus\QueryBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class StaffDataPanelController
 *
 * @package Orangear\BusinessIntelligence\Api\Controller\Panel
 */
final class StaffDataPanelController
{
    /** @var  QueryBus */
    private $queryBus;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * StaffDataPanelController constructor
     *
     * @param QueryBus $queryBus
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(QueryBus $queryBus, TokenStorageInterface $tokenStorage)
    {
        $this->queryBus = $queryBus;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @ApiDoc(
     *   section = "Panels",
     *   description = "Get panel for panel data",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        $memberId = (string) $this->tokenStorage->getToken()->getUser()->id();

        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Throwable $error) {
            return new JsonResponse(
                [
                    'message' => $error->getMessage()
                ],
                $error->getCode()
            );
        }

        $query = GetStaffDataPanelQuery::forMember($memberId);

        try {
            $response = $this->queryBus->dispatch($query);

            $response->then(function($result) use (&$data) {
                $data = $result;
            })->otherwise(function(\Exception $exception) use (&$data) {
                $data = ['errors' => $exception->getPrevious()->getMessage()];
            });

            return new JsonResponse(['data' => $data]);
        } catch (CommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    public function getPayloadFromRequest(Request $request)
    {
        $payload = [];

        return $payload;
    }
}
