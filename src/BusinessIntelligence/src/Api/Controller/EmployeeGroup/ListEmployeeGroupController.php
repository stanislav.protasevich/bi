<?php

namespace Orangear\BusinessIntelligence\Api\Controller\EmployeeGroup;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\Command\CreateEmployeeGroupCommand;
use Prooph\ServiceBus\Exception\CommandDispatchException;
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Prooph\ServiceBus\QueryBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ListChargeTypeController
 *
 * @package Orangear\BusinessIntelligence\Api\Controller\ChargeType
 */
final class ListEmployeeGroupController
{
    /** @var QueryBus */
    private $queryBus;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * ListChargeTypeController constructor
     *
     * @param QueryBus $queryBus
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(QueryBus $queryBus, TokenStorageInterface $tokenStorage)
    {
        $this->queryBus = $queryBus;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @ApiDoc(
     *   section = "Charge types",
     *   description = "Get list of charge types",
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Throwable $error) {
            return new JsonResponse(
                [
                    'message' => $error->getMessage()
                ],
                $error->getCode()
            );
        }

        $command = CreateEmployeeGroupCommand::forMember(
            $this->tokenStorage->getToken()->getUser()->id()->toString(),
            $payload['charge_type_name'],
            $payload['charge_type_description']
        );

        try {
            $this->queryBus->dispatch($command);
        } catch (CommandDispatchException $exception) {
            $exception = $exception->getPrevious();

            if ($exception instanceof MessageDispatchException) {
                $exception = $exception->getPrevious();
            }

            return new JsonResponse(['message' => $exception->getMessage()], Response::HTTP_BAD_REQUEST, []);
        }

        return new JsonResponse(null, Response::HTTP_ACCEPTED);
    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    public function getPayloadFromRequest(Request $request)
    {
        $payload = [];

        $payload['charge_type_name'] = $request->get('name', null);
        $payload['charge_type_description'] = $request->get('description', null);

        return $payload;
    }
}
