<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Api\Admin\Controller\Platform;

use Orangear\BusinessIntelligence\Domain\Model\Platform\Command\CreatePlatformCommand;
use Orangear\BusinessIntelligence\Domain\Model\Platform\Command\Handler\CreatePlatformCommandHandlerInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformRepositoryInterface;
use Orangear\Common\CQRS\InvalidCommandException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CreatePlatformController
 * @package Orangear\BusinessIntelligence\Api\Admin\Controller\Platform
 */
final class CreatePlatformController
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var PlatformRepositoryInterface */
    private $platformRepository;

    /** @var CreatePlatformCommandHandlerInterface */
    private $commandHandler;

    /**
     * CreatePlatformController constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param PlatformRepositoryInterface $platformRepository
     * @param CreatePlatformCommandHandlerInterface $commandHandler
     */
    public function __construct(TokenStorageInterface $tokenStorage, PlatformRepositoryInterface $platformRepository, CreatePlatformCommandHandlerInterface $commandHandler)
    {
        $this->tokenStorage = $tokenStorage;
        $this->platformRepository = $platformRepository;
        $this->commandHandler = $commandHandler;
    }

    /**
     * @ApiDoc(
     *   section = "Platform",
     *   description = "Create new platform",
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   parameters = {
     *     {"name" = "name", "dataType" = "string", "required" = true, "description" = "Platform name"},
     *     {"name" = "logo", "dataType" = "string", "required" = false, "description" = "Platform logotype image"},
     *   },
     *   statusCodes = {
     *     202 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        try {
            $payload = $this->getPayloadFromRequest($request);

            $platformId = $this->platformRepository->nextIdentifier();

            $command = CreatePlatformCommand::forAdmin(
                $this->tokenStorage->getToken()->getUser()->id(),
                $platformId,
                $payload['name'],
                $payload['logo']
            );

            ($this->commandHandler)($command);

            return new JsonResponse(['id' => (string) $platformId], Response::HTTP_ACCEPTED);
        } catch (InvalidCommandException $throwable) {
            return new JsonResponse($throwable->getMessages(), Response::HTTP_BAD_REQUEST);
        } catch (\Throwable $throwable) {
            return new JsonResponse($throwable->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    public function getPayloadFromRequest(Request $request)
    {
        $payload = [];

        $payload['name'] = $request->request->get('name', null);
        $payload['logo'] = $request->request->get('logo', null);

        return $payload;
    }
}
