<?php

namespace Orangear\BusinessIntelligence\Api;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class OrangearBusinessIntelligenceApiBundle
 *
 * @package Orangear\BusinessIntelligencePanel\Api
 */
final class OrangearBusinessIntelligenceApiBundle extends Bundle {}
