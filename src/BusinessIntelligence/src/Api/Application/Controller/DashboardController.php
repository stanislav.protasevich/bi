<?php

namespace Orangear\BusinessIntelligence\Api\Application\Controller;

use Orangear\BusinessIntelligence\Domain\Model\Application\ApplicationInterface;
use Orangear\BusinessIntelligence\Domain\Model\Application\Query\FindTokenQueryInterface;
use Orangear\BusinessIntelligence\Infrastructure\Query\Dashboard\GetDashboardQuery;
use Orangear\BusinessIntelligence\Infrastructure\QueryHandler\GetDashboardQueryHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class DashboardController
 * @package Orangear\BusinessIntelligence\Api\Application\Controller
 */
final class DashboardController
{
    /** FindTokenQueryInterface */
    private $findTokenQuery;

    /** @var GetDashboardQuery */
    private $queryHandler;

    /**
     * DashboardController constructor.
     * @param FindTokenQueryInterface $findTokenQuery
     * @param GetDashboardQuery $queryHandler
     */
    public function __construct(FindTokenQueryInterface $findTokenQuery, GetDashboardQuery $queryHandler)
    {
        $this->findTokenQuery = $findTokenQuery;
        $this->queryHandler = $queryHandler;
    }

    public function __invoke(Request $request)
    {
        $token = $request->headers->get('Authorization', null);
        $token = str_replace('Bearer ', '', $token);

        if (!$token) {
            return new JsonResponse(json_encode([
                'error' => 'OAuthException',
                'status' => 401,
                'message' => 'The OAuth token was provided but was invalid.'
            ]), 401, [], true);
        }

        try {
            /** @var ApplicationInterface $application */
            $application = ($this->findTokenQuery)($token);
        } catch (\Exception $exception) {
            return new JsonResponse(json_encode([
                'error' => 'OAuthException',
                'status' => 401,
                'message' => 'The OAuth token was provided but was invalid.'
            ]), 401, [], true);
        }

        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Throwable $error) {
            return new JsonResponse(
                [
                    'error' => 'Exception',
                    'status' => 400,
                    'message' => 'Bad request'
                ],
                400
            );
        }

        if (!is_null($payload['company_id'])) {
            if (!in_array($payload['company_id'], $application->access())) {
                return new JsonResponse(
                    [
                        'error' => 'Exception',
                        'status' => 403,
                        'message' => 'The current application is forbidden from accessing this data.'
                    ],
                    403
                );
            }
        }

        $data = ($this->queryHandler)(!is_null($payload['company_id']) ? [$payload['company_id']] : $application->access());

        return new JsonResponse(json_encode($data), 200, [], true);
    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    public function getPayloadFromRequest(Request $request)
    {
        $payload = [];

        $payload['company_id'] = $request->get('id', null);

        return $payload;
    }
}
