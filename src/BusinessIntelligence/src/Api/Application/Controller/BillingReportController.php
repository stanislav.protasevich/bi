<?php

namespace Orangear\BusinessIntelligence\Api\Application\Controller;

use Orangear\BusinessIntelligence\Domain\Model\Application\ApplicationInterface;
use Orangear\BusinessIntelligence\Domain\Model\Application\Query\FindTokenQueryInterface;
use Orangear\BusinessIntelligence\Infrastructure\Query\BillingReport\GetBillingReportQuery;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class BillingReportController
 * @package Orangear\BusinessIntelligence\Api\Application\Controller
 */
final class BillingReportController
{
    /** FindTokenQueryInterface */
    private $findTokenQuery;

    /** @var GetBillingReportQuery */
    private $queryHandler;

    /**
     * BillingReportController constructor.
     * @param FindTokenQueryInterface $findTokenQuery
     * @param GetBillingReportQuery $queryHandler
     */
    public function __construct(FindTokenQueryInterface $findTokenQuery, GetBillingReportQuery $queryHandler)
    {
        $this->findTokenQuery = $findTokenQuery;
        $this->queryHandler = $queryHandler;
    }

    public function __invoke(Request $request)
    {
        $token = $request->headers->get('Authorization', null);
        $token = str_replace('Bearer ', '', $token);

        if (!$token) {
            return new JsonResponse(json_encode([
                'error' => 'OAuthException',
                'status' => 401,
                'message' => 'The OAuth token was provided but was invalid.'
            ]), 401, [], true);
        }

        try {
            /** @var ApplicationInterface $application */
            $application = ($this->findTokenQuery)($token);
        } catch (\Exception $exception) {
            return new JsonResponse(json_encode([
                'error' => 'OAuthException',
                'status' => 401,
                'message' => 'The OAuth token was provided but was invalid.'
            ]), 401, [], true);
        }

        try {
            $payload = $this->getPayloadFromRequest($request);
        } catch (\Throwable $error) {
            return new JsonResponse(
                [
                    'error' => 'Exception',
                    'status' => 400,
                    'message' => 'Bad request'
                ],
                400
            );
        }

        if (!is_null($payload['company_id'])) {
            if (!in_array($payload['company_id'], $application->access())) {
                return new JsonResponse(
                    [
                        'error' => 'Exception',
                        'status' => 403,
                        'message' => 'The current application is forbidden from accessing this data.'
                    ],
                    403
                );
            }
        }

        $data = ($this->queryHandler)(!is_null($payload['company_id']) ? [$payload['company_id']] : $application->access(), $payload['start_date'], $payload['end_date']);

        return new JsonResponse(json_encode($data), 200, [], true);
    }

    /**
     * @param Request $request
     *
     * @return array|null
     */
    public function getPayloadFromRequest(Request $request)
    {
        $payload = [];

        $payload['company_id'] = $request->get('id', null);
        $payload['start_date'] = $request->get('start_date', null);
        $payload['end_date'] = $request->get('end_date', null);

        return $payload;
    }
}
