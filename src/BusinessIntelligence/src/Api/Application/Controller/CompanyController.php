<?php

namespace Orangear\BusinessIntelligence\Api\Application\Controller;

use Orangear\BusinessIntelligence\Domain\Model\Application\ApplicationInterface;
use Orangear\BusinessIntelligence\Domain\Model\Application\Query\FindTokenQueryInterface;
use Orangear\BusinessIntelligence\Infrastructure\Query\Dashboard\GetDashboardQuery;
use Orangear\BusinessIntelligence\Infrastructure\QueryHandler\GetDashboardQueryHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CompanyController
 * @package Orangear\BusinessIntelligence\Api\Application\Controller
 */
final class CompanyController
{
    /** FindTokenQueryInterface */
    private $findTokenQuery;

    private $queryHandler;

    /**
     * CompanyController constructor.
     */
    public function __construct(FindTokenQueryInterface $findTokenQuery, $queryHandler)
    {
        $this->findTokenQuery = $findTokenQuery;
        $this->queryHandler = $queryHandler;
    }

    public function __invoke(Request $request)
    {
        $token = $request->headers->get('Authorization', null);
        $token = str_replace('Bearer ', '', $token);

        if (!$token) {
            return new JsonResponse(json_encode([
                'error' => 'OAuthException',
                'status' => 401,
                'message' => 'The OAuth token was provided but was invalid.'
            ]), 401, [], true);
        }

        try {
            /** @var ApplicationInterface $application */
            $application = ($this->findTokenQuery)($token);
        } catch (\Exception $exception) {
            return new JsonResponse(json_encode([
                'error' => 'OAuthException',
                'status' => 401,
                'message' => 'The OAuth token was provided but was invalid.'
            ]), 401, [], true);
        }

        $data = ($this->queryHandler)($application->access());

        return new JsonResponse(json_encode($data), 200, [], true);
    }
}
