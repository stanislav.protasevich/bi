<?php

namespace Orangear\BusinessIntelligence\Domain\Model;

/**
 * Interface IdentifierInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model
 */
interface IdentifierInterface
{
    /**
     * @return string
     */
    public function toString(): string;
}
