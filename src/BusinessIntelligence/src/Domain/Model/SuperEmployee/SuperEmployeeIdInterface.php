<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\SuperEmployee;

use Orangear\BusinessIntelligence\Domain\Model\IdentifierInterface;

/**
 * Interface DashboardIdInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Employee
 */
interface SuperEmployeeIdInterface extends IdentifierInterface {}
