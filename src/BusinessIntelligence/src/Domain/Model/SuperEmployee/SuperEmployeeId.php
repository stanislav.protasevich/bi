<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\SuperEmployee;

use Ramsey\Uuid\Uuid;

/**
 * Class DashboardId
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Dashboard
 */
class SuperEmployeeId implements SuperEmployeeIdInterface
{
    /** @var string */
    private $employeeId;

    /**
     * EmployeeId constructor
     *
     * @param null $employeeId
     */
    private function __construct($employeeId = null)
    {
        $this->employeeId = $employeeId ?: Uuid::uuid4()->toString();
    }

    /**
     * {@inheritdoc}
     */
    public static function fromString($employeeId = null) : self
    {
        return new static($employeeId);
    }

    /**
     * {@inheritdoc}
     */
    public function toString() : string
    {
        return $this->employeeId;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->employeeId;
    }
}
