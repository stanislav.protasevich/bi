<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\SuperEmployee;

use Orangear\Admin\Core\Domain\Model\Employee\EmployeeInterface;

/**
 * Class SuperEmployee
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\SuperEmployee
 */
class SuperEmployee implements SuperEmployeeInterface
{
    /** @var SuperEmployeeIdInterface */
    private $id;

    /** @var string */
    private $remoteId;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var EmployeeInterface */
    private $employee;

    /**
     * Employee constructor
     *
     * @param SuperEmployeeIdInterface $id
     * @param string $remoteId
     * @param string $firstName
     * @param string $lastName
     * @param EmployeeInterface $employee
     */
    private function __construct(SuperEmployeeIdInterface $id, $remoteId = null, $firstName, $lastName, EmployeeInterface $employee = null)
    {
        $this->id = $id;
        $this->remoteId = $remoteId;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->employee = $employee;
    }

    /**
     * @param SuperEmployeeIdInterface $id
     * @param null $remoteId
     * @param $firstName
     * @param $lastName
     * @param EmployeeInterface|null $employee
     *
     * @return SuperEmployee
     */
    public function withData(SuperEmployeeIdInterface $id, $remoteId = null, $firstName, $lastName, EmployeeInterface $employee = null)
    {
        return new self($id, $remoteId, $firstName, $lastName, $employee);
    }

    /**
     * @return SuperEmployeeIdInterface
     */
    public function SuperEmployeeId() : SuperEmployeeIdInterface
    {
        return $this->id;
    }

    /**
     * @param SuperEmployeeId $id
     */
    public function setSuperEmployeeId(SuperEmployeeId $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function firstName() : string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function lastName() : string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string|null
     */
    public function remoteId() :? string
    {
        return $this->remoteId;
    }

    /**
     * @param string $remoteId
     */
    public function setRemoteId(string $remoteId)
    {
        $this->remoteId = $remoteId;
    }


    /**
     * @return EmployeeInterface
     */
    public function employee() :? EmployeeInterface
    {
        return $this->employee;
    }

    /**
     * @param EmployeeInterface $employee
     */
    public function setEmployee(EmployeeInterface $employee)
    {
        $this->employee = $employee;
    }
}
