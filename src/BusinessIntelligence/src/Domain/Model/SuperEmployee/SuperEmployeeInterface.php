<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\SuperEmployee;

/**
 * Interface SuperEmployeeInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\SuperEmployee
 */
interface SuperEmployeeInterface {}
