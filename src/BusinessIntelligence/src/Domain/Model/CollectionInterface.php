<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model;

/**
 * Interface CollectionInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model
 */
interface CollectionInterface {}
