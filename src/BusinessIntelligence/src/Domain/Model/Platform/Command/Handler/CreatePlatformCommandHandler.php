<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform\Command\Handler;

use Orangear\BusinessIntelligence\Domain\Model\Platform\Command\CreatePlatformCommandInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\Command\Exception\InvalidCreatePlatformCommandException;
use Orangear\BusinessIntelligence\Domain\Model\Platform\Exception\PlatformAlreadyExists;
use Orangear\BusinessIntelligence\Domain\Model\Platform\Platform;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformRepositoryInterface;
use Orangear\Common\Validator\CommandValidatorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class CreatePlatformCommandHandler
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform\Command\Handler
 */
final class CreatePlatformCommandHandler implements CreatePlatformCommandHandlerInterface
{
    /** @var PlatformRepositoryInterface */
    private $platformRepository;

    /** @var CommandValidatorInterface */
    private $commandValidator;

    /**
     * CreatePlatformCommandHandler constructor.
     * @param PlatformRepositoryInterface $platformRepository
     * @param CommandValidatorInterface $validator
     */
    public function __construct(PlatformRepositoryInterface $platformRepository, CommandValidatorInterface $validator)
    {
        $this->platformRepository = $platformRepository;
        $this->commandValidator = $validator;
    }

    /**
     * @param CreatePlatformCommandInterface $command
     */
    public function __invoke(CreatePlatformCommandInterface $command)
    {
        if ($errors = $this->commandValidator->validate($command)){
            throw InvalidCreatePlatformCommandException::withErrors($errors);
        }

        $memberId = $command->memberId();
        $platformId = $command->platformId();
        $platformName = $command->platformName();
        $platformLogo = $command->platformLogo();

        $platform = Platform::withData($platformId, $platformName, $platformLogo);

        $this->platformRepository->create($platform);
    }
}
