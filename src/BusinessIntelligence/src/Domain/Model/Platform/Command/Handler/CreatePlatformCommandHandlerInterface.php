<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform\Command\Handler;

/**
 * Interface CreatePlatformCommandHandlerInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform\Command\Handler
 */
interface CreatePlatformCommandHandlerInterface {}
