<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform\Command;

use Orangear\BusinessIntelligence\Domain\Model\CommandInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberIdentifierInterface;
use Orangear\Common\Identifier\IdentifierInterface;

/**
 * Interface CreatePlatformCommandInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\Project\Command
 */
interface CreatePlatformCommandInterface extends CommandInterface
{
    /**
     * @return MemberIdentifierInterface
     */
    public function memberId(): MemberIdentifierInterface;

    /**
     * @return IdentifierInterface
     */
    public function platformId(): IdentifierInterface;

    /**
     * @return string
     */
    public function platformName(): string;

    /**
     * @return null|string
     */
    public function platformLogo(): ?string;

    /**
     * @param MemberIdentifierInterface $adminId
     * @param IdentifierInterface $platformId
     * @param null|string $name
     * @param null|string $logo
     * @return CreatePlatformCommandInterface
     */
    public static function forAdmin(MemberIdentifierInterface $adminId, IdentifierInterface $platformId, ?string $name, ?string $logo): CreatePlatformCommandInterface;
}
