<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform\Command;

use Orangear\Common\Identifier\IdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberIdentifierInterface;

/**
 * Class CreatePlatformCommand
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform\Command
 */
final class CreatePlatformCommand implements CreatePlatformCommandInterface
{
    /** @var string */
    private $memberId;

    /** @var string */
    private $platformId;

    /** @var string */
    private $name;

    /** @var string */
    private $logo;

    /**
     * CreatePlatformCommand constructor
     *
     * @param MemberIdentifierInterface $memberId
     * @param IdentifierInterface $platformId
     * @param string $name
     * @param string $logo
     */
    private function __construct(MemberIdentifierInterface $memberId, IdentifierInterface $platformId, ?string $name, ?string $logo)
    {
        $this->memberId = $memberId;
        $this->platformId = $platformId;
        $this->name = $name;
        $this->logo = $logo;
    }

    /**
     * @inheritdoc
     */
    public static function forAdmin(MemberIdentifierInterface $adminId, IdentifierInterface $platformId, ?string $name, ?string $logo): CreatePlatformCommandInterface
    {
        return new self($adminId, $platformId, $name, $logo);
    }

    /**
     * @inheritdoc
     */
    public function memberId(): MemberIdentifierInterface
    {
        return $this->memberId;
    }

    /**
     * @inheritdoc
     */
    public function platformId(): IdentifierInterface
    {
        return $this->platformId;
    }

    /**
     * @inheritdoc
     */
    public function platformName(): string
    {
        return (string) $this->name;
    }

    /**
     * @inheritdoc
     */
    public function platformLogo(): ?string
    {
        if ($this->logo === null) {
            return null;
        }

        return (string) $this->logo;
    }
}
