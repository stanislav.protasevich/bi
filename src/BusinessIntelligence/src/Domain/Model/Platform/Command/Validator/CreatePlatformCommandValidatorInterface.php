<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform\Command\Validator;

/**
 * Class CreatePlatformCommandValidator
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform\Command\Validator
 */
interface CreatePlatformCommandValidatorInterface {};
