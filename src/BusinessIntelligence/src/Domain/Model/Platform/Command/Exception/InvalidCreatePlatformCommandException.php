<?php

//declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform\Command\Exception;

use Orangear\Common\CQRS\InvalidCommandException;

/**
 * Class InvalidCreatePlatformCommandException
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform\Command\Exception
 */
class InvalidCreatePlatformCommandException extends InvalidCommandException
{
    /**
     * @param array $violations
     * @return InvalidCreatePlatformCommandException
     */
    public static function withErrors(array $violations)
    {
        return new static('Invalid create platform command.', $violations);
    }
}
