<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform\Handler;

use Orangear\BusinessIntelligence\Domain\Model\Admin\AdminRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\Command\CreatePlatformCommandInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\Event\PlatformWasCreated;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformRepositoryInterface;
use Prooph\ServiceBus\EventBus;

/**
 * Class CreateUserHandler
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform\Handler
 */
final class CreatePlatformHandler
{
    /** @var AdminRepositoryInterface */
    private $adminRepository;

    /** @var PlatformRepositoryInterface */
    private $platformRepository;

    /** @var EventBus */
    private $eventBus;

    /**
     * CreatePlatformHandler constructor
     *
     * @param AdminRepositoryInterface $adminRepository
     * @param PlatformRepositoryInterface $platformRepository
     * @param EventBus $eventBus
     */
    public function __construct(AdminRepositoryInterface $adminRepository, PlatformRepositoryInterface $platformRepository, EventBus $eventBus)
    {
        $this->adminRepository = $adminRepository;
        $this->platformRepository = $platformRepository;
        $this->eventBus = $eventBus;
    }

    /**
     * @param CreatePlatformCommandInterface $command
     */
    public function __invoke(CreatePlatformCommandInterface $command)
    {
        $admin = $this->adminRepository->findAdminByIdOrFail($command->memberId());

        $this->platformRepository->findPlatformByNameAndFail($command->platformName());

        $platform = $admin->createPlatform(
            $command->platformId(),
            $command->platformName(),
            $command->platformLogo()
        );

        $this->platformRepository->create($platform);

        $this->eventBus->dispatch(
            PlatformWasCreated::byAdmin($admin->id(), $platform->platformId(), $platform->name(), $platform->logo())
        );
    }
}
