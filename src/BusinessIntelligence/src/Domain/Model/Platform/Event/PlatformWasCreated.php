<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Platform\Event;

use Orangear\BusinessIntelligence\Domain\Model\EventInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformIdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;

/**
 * Class PlatformWasCreated
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform\Event
 */
final class PlatformWasCreated implements EventInterface
{
    /** @var IdentifierInterface */
    private $userId;

    /** @var PlatformIdentifierInterface */
    private $platformId;

    /** @var string */
    private $name;

    /** @var string */
    private $logo;

    /**
     * PlatformWasCreated constructor
     *
     * @param IdentifierInterface $userId
     * @param PlatformIdentifierInterface $platformId
     * @param string $name
     * @param string $logo
     */
    private function __construct(IdentifierInterface $userId, PlatformIdentifierInterface $platformId, string $name, string $logo)
    {
        $this->userId = $userId;
        $this->platformId = $platformId;
        $this->name = $name;
        $this->logo = $logo;
    }

    /**
     * @param IdentifierInterface $userId
     * @param PlatformIdentifierInterface $platformId
     * @param string $name
     * @param string $logo
     *
     * @return PlatformWasCreated
     */
    public static function byAdmin(IdentifierInterface $userId, PlatformIdentifierInterface $platformId, string $name, string $logo) : self
    {
        return new self($userId, $platformId, $name, $logo);
    }

    /**
     * @return IdentifierInterface
     */
    public function userId(): IdentifierInterface
    {
        return $this->userId;
    }

    /**
     * @return PlatformIdentifierInterface
     */
    public function platformId(): PlatformIdentifierInterface
    {
        return $this->platformId;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function logo(): string
    {
        return $this->logo;
    }
}