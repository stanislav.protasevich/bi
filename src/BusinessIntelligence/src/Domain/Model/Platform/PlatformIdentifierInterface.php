<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform;

use Orangear\Common\Identifier\IdentifierInterface;

/**
 * Interface PlatformIdentifierInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform
 */
interface PlatformIdentifierInterface extends IdentifierInterface {}
