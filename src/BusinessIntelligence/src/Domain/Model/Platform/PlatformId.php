<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform;

use Orangear\Common\Identifier\Uuid4Identifier;

/**
 * Class PlatformId
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform
 */
final class PlatformId extends Uuid4Identifier implements PlatformIdentifierInterface {}
