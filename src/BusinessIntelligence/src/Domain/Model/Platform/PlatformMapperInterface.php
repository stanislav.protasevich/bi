<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform;

/**
 * Class PlatformMapperInterface
 *
 * @package BusinessIntelligence\Domain\Model\Platform
 */
interface PlatformMapperInterface
{
    /**
     * @param PlatformIdentifierInterface $id
     *
     * @return mixed
     */
    public function fetchById(PlatformIdentifierInterface $id) :? PlatformInterface;

    /**
     * @param array $conditions
     *
     * @return mixed
     */
    public function fetchAll(array $conditions = array());

    /**
     * @param array $conditions
     *
     * @return mixed
     */
    public function fetchOne(array $conditions);

    /**
     * @param PlatformInterface $platform
     */
    public function create(PlatformInterface $platform);
}
