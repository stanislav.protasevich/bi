<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform;

use Orangear\BusinessIntelligence\Domain\Model\Collection;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformCollectionInterface;

/**
 * Class PlatformCollection
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform
 */
final class PlatformCollection extends Collection implements PlatformCollectionInterface
{
    /**
     * @param PlatformIdentifierInterface $identifier
     *
     * @return null|PlatformInterface
     */
    public function get($identifier) :? PlatformInterface
    {
        return $this->repository->findById($identifier);
    }
}
