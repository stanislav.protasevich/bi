<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform;

use Orangear\Common\Identifier\IdentifierInterface;

/**
 * Interface PlatformInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform
 */
interface PlatformInterface
{
    /**
     * @return IdentifierInterface
     */
    public function platformId(): IdentifierInterface;

    /**
     * @return string
     */
    public function name(): string;

    /**
     * @return null|string
     */
    public function logo(): ?string;

    /**
     * @param IdentifierInterface $platformId
     * @param string $name
     * @param null|string $logo
     * @return PlatformInterface
     */
    public static function withData(IdentifierInterface $platformId, string $name, ?string $logo): PlatformInterface;
}