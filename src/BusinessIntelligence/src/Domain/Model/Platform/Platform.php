<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform;

use Assert\Assertion;
use Orangear\Common\Identifier\IdentifierInterface;
use Orangear\Common\Identifier\Uuid4IdentifiableEntity;

/**
 * Class Platform
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform
 */
class Platform extends Uuid4IdentifiableEntity implements PlatformInterface
{
    /** @var string */
    private $name;

    /** @var string */
    private $logo;

    /**
     * Platform constructor.
     * @param IdentifierInterface $platformId
     * @param string $name
     * @param null|string $logo
     */
    private function __construct(IdentifierInterface $platformId, string $name, ?string $logo)
    {
        $this->setPlatformId($platformId);
        $this->setName($name);
        $this->setLogo($logo);
    }

    /**
     * @inheritdoc
     */
    public static function withData(IdentifierInterface $platformId, string $name, ?string $logo): PlatformInterface
    {
        return new self($platformId, $name, $logo);
    }

    /**
     * @inheritdoc
     */
    public function platformId(): IdentifierInterface
    {
        return $this->getId();
    }

    /**
     * @inheritdoc
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @inheritdoc
     */
    public function logo(): ?string
    {
        return $this->logo;
    }

    /**
     * @param IdentifierInterface $platformId
     *
     * @throws Exception\InvalidPlatformId
     */
    private function setPlatformId(IdentifierInterface $platformId)
    {
        try {
            Assertion::notNull($platformId);
            Assertion::notEmpty($platformId);
            Assertion::implementsInterface(PlatformIdentifierInterface::class, PlatformIdentifierInterface::class, $platformId);
        } catch (\Exception $exception) {
            throw new Exception\InvalidPlatformId($platformId);
        }

        $this->setId($platformId);
    }

    /**
     * @param string $platformName
     *
     * @throws Exception\InvalidPlatformName
     */
    private function setName(string $platformName)
    {
        try {
            Assertion::notNull($platformName);
            Assertion::notEmpty($platformName);
            Assertion::string($platformName);
        } catch (\Exception $exception) {
            throw Exception\InvalidPlatformName::withPlatformName($platformName);
        }

        $this->name = $platformName;
    }

    /**
     * @param null|string $platformLogo
     *
     * @throws Exception\InvalidPlatformLogo
     */
    private function setLogo(?string $platformLogo)
    {
        if ($platformLogo !== null) {
            try {
                Assertion::string($platformLogo);
            } catch (\Exception $exception) {
                throw Exception\InvalidPlatformLogo::withPlatformLogo($platformLogo);
            }

            $this->logo = $platformLogo;
        }
    }
}
