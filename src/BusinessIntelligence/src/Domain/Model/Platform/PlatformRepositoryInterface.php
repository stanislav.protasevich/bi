<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Platform;

use Orangear\BusinessIntelligence\Domain\Model\Platform\Exception\PlatformAlreadyExists;

/**
 * Interface PlatformRepositoryInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform
 */
interface PlatformRepositoryInterface
{
    /**
     * @param PlatformId $identifier
     *
     * @return PlatformInterface
     */
    public function findById(PlatformId $identifier) :? PlatformInterface;

    /**
     * @param string $name
     *
     * @return array
     */
    public function findByName(string $name) : array;

    /**
     * @param PlatformInterface $platform
     */
    public function create(PlatformInterface $platform);

    /**
     * @param string $name
     *
     * @throws PlatformAlreadyExists
     */
    public function findPlatformByNameAndFail(string $name);

    /**
     * @param array $conditions
     * @return null|PlatformInterface
     */
    public function findBy(array $conditions): ?PlatformInterface;

    /**
     * @return mixed
     */
    public function nextIdentifier();
}
