<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform;

/**
 * Class PlatformCollectionInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform
 */
interface PlatformCollectionInterface
{
    /**
     * @param $platform
     */
    public function add($platform);
}
