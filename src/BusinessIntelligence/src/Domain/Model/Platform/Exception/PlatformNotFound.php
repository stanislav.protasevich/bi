<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform\Exception;

use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformIdentifierInterface;

/**
 * Class PlatformNotFound
 *
 * @package BusinessIntelligence\Domain\Model\Platform\Exception
 */
final class PlatformNotFound extends \InvalidArgumentException
{
    /**
     * @param PlatformIdentifierInterface $platformId
     *
     * @return PlatformNotFound
     */
    public static function withPlatformId(PlatformIdentifierInterface $platformId)
    {
        return new self(sprintf('Platform with id "%s" cannot be found.', $platformId->toString()));
    }
}
