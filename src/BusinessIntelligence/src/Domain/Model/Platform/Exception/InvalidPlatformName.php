<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Platform\Exception;

/**
 * Class InvalidPlatformName
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform\Exception
 */
final class InvalidPlatformName extends \InvalidArgumentException
{
    /**
     * @param string $platformName
     *
     * @return InvalidPlatformName
     */
    public static function withPlatformName(string $platformName)
    {
        return new self(sprintf('Invalid platform name "%s".',$platformName));
    }
}
