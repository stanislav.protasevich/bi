<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Platform\Exception;

/**
 * Class PlatformAlreadyExists
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform\Exception
 */
final class PlatformAlreadyExists extends \InvalidArgumentException
{
    /**
     * @param string $name
     *
     * @return PlatformAlreadyExists
     */
    public static function withPlatformName(string $name)
    {
        return new self(sprintf('Platform with name "%s" already exists.', $name));
    }
}
