<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Platform\Exception;

/**
 * Class InvalidPlatformLogo
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform\Exception
 */
final class InvalidPlatformLogo extends \InvalidArgumentException
{
    /**
     * @param string $platformLogo
     *
     * @return InvalidPlatformLogo
     */
    public static function withPlatformLogo(string $platformLogo)
    {
        return new self(sprintf('Invalid platform logo "%s".',$platformLogo));
    }
}
