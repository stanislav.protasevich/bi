<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Platform\Exception;

use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;

/**
 * Class InvalidPlatformId
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform\Exception
 */
final class InvalidPlatformId extends \InvalidArgumentException
{
    /**
     * @param IdentifierInterface $platformId
     *
     * @return InvalidPlatformId
     */
    public static function withPlatformName(IdentifierInterface $platformId)
    {
        return new self(sprintf('Invalid platform id "%s".', $platformId->toString()));
    }
}
