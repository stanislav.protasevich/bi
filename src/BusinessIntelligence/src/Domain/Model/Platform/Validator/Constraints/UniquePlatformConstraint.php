<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

final class UniquePlatformConstraint extends Constraint
{
    /**
     * @var string
     */
    public $message = 'Platform with name {{ value }} already exists.';

    /**
     * @return array|string
     */
    public function getTargets()
    {
        return array(self::PROPERTY_CONSTRAINT, self::CLASS_CONSTRAINT);
    }

    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'cf.validator.unique.in.system';
    }
}
