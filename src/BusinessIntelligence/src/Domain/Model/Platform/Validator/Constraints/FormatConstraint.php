<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform\Validator\Constraints;

use Symfony\Component\Validator\Constraints\Regex;

/**
 * Class FormatConstraint
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform\Validator\Constraints
 */
final class FormatConstraint extends Regex
{
    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'cf.validator.unique.in';
    }
}
