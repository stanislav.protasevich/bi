<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\RegexValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class FormatValidator
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform\Validator
 */
class FormatValidator extends RegexValidator
{
    /**
     * @inheritdoc
     */
    public function validate($value, Constraint $constraint)
    {
        if (null === $value || '' === $value) {
            return;
        }

        if (!is_scalar($value) && !(is_object($value) && method_exists($value, '__toString'))) {
            throw new UnexpectedTypeException($value, 'string');
        }

        $value = (string) $value;

        $valueWithoutNamespaces = array_map('trim', explode(' ', $value));

        $value = implode('',$valueWithoutNamespaces);

        if ($constraint->match xor preg_match($constraint->pattern, $value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->setCode(Regex::REGEX_FAILED_ERROR)
                ->addViolation();
        }
    }
}
