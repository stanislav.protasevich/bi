<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Platform\Validator;

use Doctrine\ORM\EntityManagerInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\Command\CreatePlatformCommandInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\Platform;
use Orangear\BusinessIntelligence\Domain\Model\Platform\Validator\Constraints\UniquePlatformConstraint;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class UniqueInSystem
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform\Validator
 */
final class UniquePlatformValidator extends ConstraintValidator
{
    /** @var EntityManagerInterface */
    private $repository;

    /**
     * UniqueInSystem constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Platform::class);
    }

    /**
     * @inheritdoc
     */
    public function validate($value, Constraint $constraint)
    {
        /** @var CreatePlatformCommandInterface $value */
        /** @var UniquePlatformConstraint $constraint */

        $platform = $this->repository->findBy(['name' => $value]);

        if ($platform) {
            $this->context
                ->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->addViolation();
        }
    }
}
