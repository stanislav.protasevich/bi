<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model;

/**
 * Class AggregateRootTrait
 *
 * @package Orangear\BusinessIntelligence\Domain\Model
 */
trait AggregateRootTrait
{
    /**
     * @var array
     */
    protected $events = [];

    /**
     * @param EventInterface $event
     */
    public function record(EventInterface $event)
    {
        $this->events[] = $event;
    }

    /**
     * @return array
     */
    public function getRecordedEvents() : array
    {
        return $this->events;
    }
}
