<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee;

use DateTime;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;

/**
 * Class RemoteEmployee
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee
 */
class RemoteEmployee implements RemoteEmployeeInterface
{
    /** @var RemoteEmployeeIdentifierInterface */
    private $id;

    /** @var DateTime */
    private $employedSince;

    /** @var string */
    private $remoteId;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var string */
    private $position;

    /** @var string */
    private $notes;

    /** @var ProjectInterface */
    private $project;

    /** @var EmployeeInterface */
    private $employee;

    /** @var bool */
    private $activated;

    /**
     * RemoteEmployee constructor
     *
     * @param RemoteEmployeeIdentifierInterface $id
     * @param DateTime $employedSince
     * @param string $remoteId
     * @param string $firstName
     * @param string $lastName
     * @param string $position
     * @param string $notes
     * @param ProjectInterface $project
     * @param EmployeeInterface $employee
     * @param bool $activated
     */
    private function __construct(RemoteEmployeeIdentifierInterface $id, DateTime $employedSince = null, $remoteId, $firstName, $lastName, $position, $notes, ProjectInterface $project, EmployeeInterface $employee = null, $activated)
    {
        $this->id = $id;
        $this->employedSince = $employedSince;
        $this->remoteId = $remoteId;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->position = $position;
        $this->notes = $notes;
        $this->project = $project;
        $this->employee = $employee;
        $this->activated = $activated;
    }

    /**
     * @param RemoteEmployeeIdentifierInterface $id
     * @param DateTime $employedSince
     * @param $remoteId
     * @param $firstName
     * @param $lastName
     * @param $position
     * @param $notes
     * @param ProjectInterface $project
     * @param EmployeeInterface $employee
     * @param bool $activated
     *
     * @return RemoteEmployee
     */
    public static function create(RemoteEmployeeIdentifierInterface $id, DateTime $employedSince = null, $remoteId, $firstName, $lastName, $position, $notes, ProjectInterface $project, EmployeeInterface $employee = null, $activated)
    {
        return new self($id, $employedSince, $remoteId, $firstName, $lastName, $position, $notes, $project, $employee, $activated);
    }

    /**
     * {@inheritdoc}
     */
    public function id(): RemoteEmployeeIdentifierInterface
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function employedSince(): DateTime
    {
        return $this->employedSince;
    }

    /**
     * {@inheritdoc}
     */
    public function remoteId():? string
    {
        return $this->remoteId;
    }

    /**
     * {@inheritdoc}
     */
    public function firstName():? string
    {
        return $this->firstName;
    }

    /**
     * {@inheritdoc}
     */
    public function lastName():? string
    {
        return $this->lastName;
    }

    /**
     * {@inheritdoc}
     */
    public function position():? string
    {
        return $this->position;
    }

    /**
     * {@inheritdoc}
     */
    public function notes():? string
    {
        return $this->notes;
    }

    /**
     * {@inheritdoc}
     */
    public function project(): ProjectInterface
    {
        return $this->project;
    }

    /**
     * {@inheritdoc}
     */
    public function employee():? EmployeeInterface
    {
        return $this->employee;
    }

    /**
     * {@inheritdoc}
     */
    public function activated():? bool
    {
        return $this->activated();
    }
}
