<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\Command;

use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;

/**
 * Class SyncEmployeeCommandInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\Command
 */
interface SyncEmployeeCommandInterface
{
    /**
     * @return ProjectIdentifierInterface
     */
    public function projectId() : ProjectIdentifierInterface;
}
