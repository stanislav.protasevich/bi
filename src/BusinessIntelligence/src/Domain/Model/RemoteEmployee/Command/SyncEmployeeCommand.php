<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\Command;

use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;

/**
 * Class SyncEmployeeCommand
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\Command
 */
final class SyncEmployeeCommand implements SyncEmployeeCommandInterface
{
    /** @var string */
    private $projectId;

    /**
     * SyncEmployeeCommand constructor
     *
     * @param string $projectId
     */
    private function __construct($projectId)
    {
        $this->projectId = $projectId;
    }

    /**
     * @param $projectId
     *
     * @return SyncEmployeeCommand
     */
    public static function forProject($projectId)
    {
        return new self($projectId);
    }

    /**
     * {@inheritdoc}
     */
    public function projectId(): ProjectIdentifierInterface
    {
        return ProjectId::fromString($this->projectId);
    }
}
