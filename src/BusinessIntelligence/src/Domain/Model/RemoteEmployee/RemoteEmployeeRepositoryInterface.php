<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee;

/**
 * Interface RemoteEmployeeRepositoryInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee
 */
interface RemoteEmployeeRepositoryInterface
{
    /**
     * @param RemoteEmployeeIdentifierInterface $chargeTypeIdentifier
     *
     * @return null|RemoteEmployeeInterface
     */
    public function findById(RemoteEmployeeIdentifierInterface $chargeTypeIdentifier) :? RemoteEmployeeInterface;

    /**
     * @param array $conditions
     * @return mixed
     */
    public function findAll(array $conditions = []);

    /**
     * @param RemoteEmployeeInterface $chargeType
     */
    public function create(RemoteEmployeeInterface $chargeType);

    /**
     * @param RemoteEmployeeInterface $chargeType
     */
    public function update(RemoteEmployeeInterface $chargeType);

    /**
     * @param RemoteEmployeeInterface $chargeType
     */
    public function delete(RemoteEmployeeInterface $chargeType);

    /**
     * @return RemoteEmployeeIdentifierInterface
     */
    public function nextIdentifier() : RemoteEmployeeIdentifierInterface;
}
