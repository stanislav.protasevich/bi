<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee;

/**
 * Class RemoteEmployeeIdentifier
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee
 */
class RemoteEmployeeIdentifier implements RemoteEmployeeIdentifierInterface
{
    /**
     * @var string
     */
    private $id;

    private function __construct($id = null)
    {
        $this->id = $id;
    }

    public static function fromString($anId)
    {
        return new static($anId);
    }

    public function toString()
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->id;
    }
}
