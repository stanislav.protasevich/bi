<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\Handler;

use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\Command\SyncEmployeeCommandInterface;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployee;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployeeRepositoryInterface;
use Zend\Http\Client;
use Zend\Http\Headers;
use Zend\Http\Request;
use Zend\Stdlib\Parameters;

/**
 * Class SyncEmployeeCommandHandler
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\Handler
 */
final class SyncEmployeeCommandHandler
{
    /** @var Client */
    private $httpClient;

    /** @var ProjectRepositoryInterface */
    private $projectRepository;

    /** @var RemoteEmployeeRepositoryInterface */
    private $remoteEmployeeRepository;

    /**
     * SyncEmployeeCommandHandler constructor
     *
     * @param RemoteEmployeeRepositoryInterface $remoteEmployeeRepository
     * @param ProjectRepositoryInterface $projectRepository
     */
    public function __construct(ProjectRepositoryInterface $projectRepository, RemoteEmployeeRepositoryInterface $remoteEmployeeRepository)
    {
        $this->projectRepository = $projectRepository;
        $this->remoteEmployeeRepository = $remoteEmployeeRepository;

        $this->httpClient = new Client();
    }

    /**
     * @param SyncEmployeeCommandInterface $command
     */
    public function __invoke(SyncEmployeeCommandInterface $command)
    {
        $project = $this->projectRepository->findByIdOrFail($command->projectId());

        $remoteEmployees = $this->getRemoteEmployeesOrFail($project);

        foreach ($remoteEmployees['data'] as $rawEmployee) {
            if (empty($rawEmployee['first_name']) && empty($rawEmployee['last_name'])) {
                continue;
            }

            $employees = $this->remoteEmployeeRepository->findAll([
                'project' => $project,
                'remoteId' => $rawEmployee['id']
            ]);

            if (count($employees) > 0) {
                continue;
            }

            $remoteEmployee = RemoteEmployee::create(
                $this->remoteEmployeeRepository->nextIdentifier(),
                null,
                $rawEmployee['id'],
                $rawEmployee['first_name'],
                $rawEmployee['last_name'],
                null,
                null,
                $project,
                null,
                1
            );

            $this->remoteEmployeeRepository->create($remoteEmployee);
        }
    }

    /**
     * @param ProjectInterface $project
     *
     * @return mixed
     *
     * @throws \Exception
     */
    private function getRemoteEmployeesOrFail(ProjectInterface $project)
    {
        $params = [];
        $params['activated'] = 1;
        $params['size'] = 100;
        $params['token'] = $project->token();

        $request = new Request();
        $request->setUri($project->apiUrl() . 'bi/managers');
        $request->setHeaders((new Headers())->addHeaders(['X-AUTH-TOKEN' => $project->token()]));
        $request->setMethod(Request::METHOD_GET);
        $request->setQuery(new Parameters($params));

        $this->httpClient->setRequest($request);

        $response = $this->httpClient->send();


        if (!$response->isOk()) {
            throw new \Exception('No valid responce from ' . $project->apiUrl());
        }

        return json_decode($response->getBody(), true);
    }
}
