<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee;

/**
 * Interface RemoteEmployeeMapperInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee
 */
interface RemoteEmployeeMapperInterface
{
    /**
     * @param RemoteEmployeeIdentifierInterface $chargeTypeIdentifier
     *
     * @return null|RemoteEmployeeInterface
     */
    public function fetchById(RemoteEmployeeIdentifierInterface $chargeTypeIdentifier) :? RemoteEmployeeInterface;

    /**
     * @param array $conditions
     * @return mixed
     */
    public function fetchAll(array $conditions = []);

    /**
     * @param RemoteEmployeeInterface $chargeType
     */
    public function create(RemoteEmployeeInterface $chargeType);

    /**
     * @param RemoteEmployeeInterface $chargeType
     */
    public function update(RemoteEmployeeInterface $chargeType);

    /**
     * @param RemoteEmployeeInterface $chargeType
     */
    public function delete(RemoteEmployeeInterface $chargeType);
}
