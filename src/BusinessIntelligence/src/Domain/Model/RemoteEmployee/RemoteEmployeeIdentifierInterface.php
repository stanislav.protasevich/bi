<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee;

/**
 * Interface RemoteEmployeeIdentifierInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee
 */
interface RemoteEmployeeIdentifierInterface {}
