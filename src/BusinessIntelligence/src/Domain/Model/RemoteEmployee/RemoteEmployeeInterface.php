<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee;

use DateTime;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;

/**
 * Interface RemoteEmployeeInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee
 */
interface RemoteEmployeeInterface
{
    /**
     * @return RemoteEmployeeIdentifierInterface
     */
    public function id(): RemoteEmployeeIdentifierInterface;

    /**
     * @return DateTime
     */
    public function employedSince(): DateTime;

    /**
     * @return string
     */
    public function remoteId():? string;

    /**
     * @return null|string
     */
    public function firstName():? string;

    /**
     * @return null|string
     */
    public function lastName():? string;

    /**
     * @return null|string
     */
    public function position():? string;

    /**
     * @return null|string
     */
    public function notes():? string;

    /**
     * @return ProjectInterface
     */
    public function project(): ProjectInterface;

    /**
     * @return EmployeeInterface
     */
    public function employee():? EmployeeInterface;
}
