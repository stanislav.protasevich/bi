<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Head;

use Orangear\MembershipBundle\Domain\Model\Member\MemberInterface;

/**
 * Interface HeadInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\Head
 */
interface HeadInterface extends MemberInterface
{

}
