<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Head;

use Orangear\BusinessIntelligence\Domain\Model\IdentifierInterface;

/**
 * Interface HeadIdentifierInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\Head
 */
interface HeadIdentifierInterface extends IdentifierInterface {}
