<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Head;

use DateTime;
use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\Member\Member;
use Orangear\MembershipBundle\Domain\Model\Member\MemberInterface;

/**
 * Class Head
 * @package Orangear\BusinessIntelligence\Domain\Model\Head
 */
class Head extends Member implements HeadInterface
{
    /**
     * @inheritdoc
     */
    public function getRoles()
    {
        return ['ROLE_HEAD'];
    }

    /**
     * @inheritdoc
     */
    protected function __construct(IdentifierInterface $id = null, $email = null, $password = null, $salt = null, DateTime $createdAt = null, DateTime $updatedAt = null, DateTime $deletedAt = null)
    {
        parent::__construct($id, $email, $password, $salt, $createdAt, $updatedAt, $deletedAt);
    }

    /**
     * @inheritdoc
     */
    public static function fromArray(array $payload): MemberInterface
    {
        return new static(
            $payload['id'] ?? null,
            $payload['email'] ?? null,
            $payload['password'] ?? null,
            $payload['salt'] ?? null,
            $payload['created_at'] ?? null,
            $payload['updated_at'] ?? null,
            $payload['deleted_at'] ?? null
        );
    }
}
