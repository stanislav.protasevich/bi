<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Head;

use Orangear\MembershipBundle\Domain\Model\Member\MemberInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberRepositoryInterface;

/**
 * Interface HeadRepositoryInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\Head
 */
interface HeadRepositoryInterface extends MemberRepositoryInterface
{
    /**
     * @param array $conditions
     * @return MemberInterface
     */
    public function findOrFail(array $conditions = []) : MemberInterface;
}
