<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Head\Exception;

use Orangear\BusinessIntelligence\Domain\Model\IdentifierInterface;

/**
 * Class HeadNotFound
 * @package Orangear\BusinessIntelligence\Domain\Model\Head\Exception
 */
class HeadNotFound extends \InvalidArgumentException
{
    /**
     * @param IdentifierInterface $memberId
     *
     * @return HeadNotFound
     */
    public static function withId(IdentifierInterface $memberId)
    {
        return new self(sprintf('Head with id %s cannot be found.', (string) $memberId));
    }
}
