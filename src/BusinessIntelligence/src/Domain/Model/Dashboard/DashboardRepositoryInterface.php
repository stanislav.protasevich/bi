<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Dashboard;

use Orangear\BusinessIntelligence\Domain\Model\RepositoryInterface;

/**
 * Interface DashboardRepositoryInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Dashboard
 */
interface DashboardRepositoryInterface extends RepositoryInterface
{
    /**
     * @param DashboardInterface $dashboard
     */
    public function create(DashboardInterface $dashboard);

    /**
     * @return DashboardIdInterface
     */
    public function nextIdentifier() : DashboardIdInterface;
}
