<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Dashboard\Query;

use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\IdentifierInterface as ProjectIdentifierInterface;

/**
 * Interface GetDashboardQueryInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\Dashboard\Query
 */
interface GetDashboardQueryInterface
{
    /**
     * @return IdentifierInterface
     */
    public function memberId(): IdentifierInterface;

    /**
     * @return null|ProjectIdentifierInterface
     */
    public function projectId(): ?ProjectIdentifierInterface;
}
