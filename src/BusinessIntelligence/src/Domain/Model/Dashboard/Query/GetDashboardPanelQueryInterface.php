<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Dashboard\Query;

use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;

/**
 * Interface GetDashboardPanelQueryInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\Dashboard\Query
 */
interface GetDashboardPanelQueryInterface
{
    /**
     * @return IdentifierInterface
     */
    public function memberId(): IdentifierInterface;
}
