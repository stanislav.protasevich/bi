<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Dashboard\Query;

use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;
use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberId;
use Orangear\BusinessIntelligence\Domain\Model\IdentifierInterface as ProjectIdentifierInterface;

/**
 * Class GetDashboardQuery
 * @package Orangear\BusinessIntelligence\Domain\Model\Dashboard\Query
 */
final class GetDashboardQuery implements GetDashboardQueryInterface
{
    /** @var string */
    private $memberId;

    /** @var string */
    private $projectId;

    /**
     * GetDashboardQuery constructor.
     * @param string $memberId
     * @param null|string $projectId
     */
    private function __construct(string $memberId, ?string $projectId)
    {
        $this->memberId = $memberId;
        $this->projectId = $projectId;
    }

    /**
     * @param string $headId
     * @param null|string $projectId
     * @return GetDashboardQuery
     */
    public static function forHead(string $headId, ?string $projectId)
    {
        return new self($headId, $projectId);
    }

    /**
     * @inheritdoc
     */
    public function memberId(): IdentifierInterface
    {
        return MemberId::fromString($this->memberId);
    }

    /**
     * @return null|ProjectIdentifierInterface
     */
    public function projectId(): ?ProjectIdentifierInterface
    {
        if ($this->projectId !== null) {
            return ProjectId::fromString($this->projectId);
        }

        return null;
    }
}
