<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Dashboard\Query;

use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberId;

/**
 * Class GetDashboardPanelQuery
 * @package Orangear\BusinessIntelligence\Domain\Model\Dashboard\Query
 */
final class GetDashboardPanelQuery implements GetDashboardPanelQueryInterface
{
    /** @var string */
    private $memberId;

    /**
     * GetDashboardPanelQuery constructor.
     * @param string $memberId
     */
    private function __construct(string $memberId)
    {
        $this->memberId = $memberId;
    }

    /**
     * @param string $headId
     * @return GetDashboardPanelQuery
     */
    public static function forHead(string $headId)
    {
        return new self($headId);
    }

    /**
     * @inheritdoc
     */
    public function memberId(): IdentifierInterface
    {
        return MemberId::fromString($this->memberId);
    }
}
