<?php

declare(strict_types=1);

namespace Orangear\BusinessIntelligence\Domain\Model\Dashboard;

/**
 * Class DashboardMapperInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Dashboard
 */
interface DashboardMapperInterface
{
    /**
     * @param DashboardIdInterface $id
     *
     * @return null|DashboardInterface
     */
    public function fetchById(DashboardIdInterface $id): ?DashboardInterface;

    /**
     * @param DashboardInterface $dashboard
     */
    public function create(DashboardInterface $dashboard);

    /**
     * @param array $parameters
     * @return null|DashboardInterface
     */
    public function fetchOneBy(array $parameters): ?DashboardInterface;
}
