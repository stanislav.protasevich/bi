<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Dashboard;

use Ramsey\Uuid\Uuid;

/**
 * Class DashboardId
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Dashboard
 */
class DashboardId implements DashboardIdInterface
{
    /** @var string */
    private $dashboardId;

    /**
     * PlatformId constructor
     *
     * @param null $dashboardId
     */
    private function __construct($dashboardId = null)
    {
        $this->dashboardId = $dashboardId ?: Uuid::uuid4()->toString();
    }

    /**
     * {@inheritdoc}
     */
    public static function fromString($dashboardId = null) : self
    {
        return new static($dashboardId);
    }

    /**
     * {@inheritdoc}
     */
    public function toString() : string
    {
        return $this->dashboardId;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->dashboardId;
    }
}
