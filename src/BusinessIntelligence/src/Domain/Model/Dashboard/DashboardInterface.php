<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Dashboard;

use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use \DateTime;

/**
 * Interface DashboardInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Dashboard
 */
interface DashboardInterface
{
    /**
     * @return DashboardIdInterface
     */
    public function dashboardId() : DashboardIdInterface;

    /**
     * @return ProjectInterface
     */
    public function project() : ProjectInterface;

    /**
     * @return DateTime
     */
    public function synchronizedAt() : DateTime;

    /**
     * @return float
     */
    public function incomeToday() : float;

    /**
     * @return float
     */
    public function incomeYesterday() : float;

    /**
     * @return float
     */
    public function incomeLastSevenDays() : float;

    /**
     * @return float
     */
    public function incomeThisMonth() : float;

    /**
     * @return float
     */
    public function incomeLastMonth() : float;

    /**
     * @return float
     */
    public function incomeAllTime() : float;

    /**
     * @return float
     */
    public function incomeSuperlinkToday() : float;

    /**
     * @return float
     */
    public function incomeSuperlinkYesterday() : float;

    /**
     * @return float
     */
    public function incomeSuperlinkLastSevenDays() : float;

    /**
     * @return float
     */
    public function incomeSuperlinkThisMonth() : float;

    /**
     * @return float
     */
    public function incomeSuperlinkLastMonth() : float;

    /**
     * @return float
     */
    public function incomeSuperlinkAllTime() : float;

    /**
     * @return float
     */
    public function revenueSuperlinkToday() : float;

    /**
     * @return float
     */
    public function revenueSuperlinkYesterday() : float;

    /**
     * @return float
     */
    public function revenueSuperlinkLastSevenDays() : float;

    /**
     * @return float
     */
    public function revenueSuperlinkThisMonth() : float;

    /**
     * @return float
     */
    public function revenueSuperlinkLastMonth() : float;

    /**
     * @return float
     */
    public function revenueSuperlinkAllTime() : float;

    /**
     * @return float
     */
    public function payoutToday() : float;

    /**
     * @return float
     */
    public function payoutYesterday() : float;

    /**
     * @return float
     */
    public function payoutLastSevenDays() : float;

    /**
     * @return float
     */
    public function payoutThisMonth() : float;

    /**
     * @return float
     */
    public function payoutLastMonth() : float;

    /**
     * @return float
     */
    public function payoutAllTime() : float;

    /**
     * @return int
     */
    public function offersActive() : int;

    /**
     * @return int
     */
    public function offersPrivate() : int;

    /**
     * @return int
     */
    public function offersSuspended() : int;

    /**
     * @return int
     */
    public function offersIncent() : int;

    /**
     * @return int
     */
    public function offersNonIncent() : int;

    /**
     * @return int
     */
    public function offersTotal() : int;

    /**
     * @return int
     */
    public function offersPerDay() : int;
}
