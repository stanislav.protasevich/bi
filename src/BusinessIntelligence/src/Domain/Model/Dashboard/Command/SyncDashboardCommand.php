<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Dashboard\Command;

/**
 * Class SyncDashboardInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Dashboard\Command
 */
final class SyncDashboardCommand implements SyncDashboardCommandInterface
{
    /** @var string */
    private $token;

    /** @var string */
    private $period;

    /** @var string */
    private $timezone;

    /** @var float */
    private $incomeToday;

    /** @var float */
    private $incomeYesterday;

    /** @var float */
    private $incomeLast7Days;

    /** @var float */
    private $incomeThisMonth;

    /** @var float */
    private $incomeLastMonth;

    /** @var float */
    private $incomeAllTime;

    /** @var float */
    private $incomeSuperlinkToday;

    /** @var float */
    private $incomeSuperlinkYesterday;

    /** @var float */
    private $incomeSuperlinkLast7Days;

    /** @var float */
    private $incomeSuperlinkThisMonth;

    /** @var float */
    private $incomeSuperlinkLastMonth;

    /** @var float */
    private $incomeSuperlinkAllTime;

    /** @var float */
    private $revenueSuperlinkToday;

    /** @var float */
    private $revenueSuperlinkYesterday;

    /** @var float */
    private $revenueSuperlinkLast7Days;

    /** @var float */
    private $revenueSuperlinkThisMonth;

    /** @var float */
    private $revenueSuperlinkLastMonth;

    /** @var float */
    private $revenueSuperlinkAllTime;

    /** @var float */
    private $payoutToday;

    /** @var float */
    private $payoutYesterday;

    /** @var float */
    private $payoutLast7Days;

    /** @var float */
    private $payoutThisMonth;

    /** @var float */
    private $payoutLastMonth;

    /** @var float */
    private $payoutAllTime;

    /** @var int */
    private $offersActive;

    /** @var int */
    private $offersPrivate;

    /** @var int */
    private $offersSuspended;

    /** @var int */
    private $offersIncent;

    /** @var int */
    private $offersNonIncent;

    /** @var int */
    private $offersTotal;

    /** @var int */
    private $offersPerDay;

    /**
     * SyncDashboardCommand constructor
     *
     * @param string $token
     * @param string $period
     * @param string $timezone
     * @param float $incomeToday
     * @param float $incomeYesterday
     * @param float $incomeLast7Days
     * @param float $incomeThisMonth
     * @param float $incomeLastMonth
     * @param float $incomeAllTime
     * @param float $incomeSuperlinkToday
     * @param float $incomeSuperlinkYesterday
     * @param float $incomeSuperlinkLast7Days
     * @param float $incomeSuperlinkThisMonth
     * @param float $incomeSuperlinkLastMonth
     * @param float $incomeSuperlinkAllTime
     * @param float $revenueSuperlinkToday
     * @param float $revenueSuperlinkYesterday
     * @param float $revenueSuperlinkLast7Days
     * @param float $revenueSuperlinkThisMonth
     * @param float $revenueSuperlinkLastMonth
     * @param float $revenueSuperlinkAllTime
     * @param float $payoutToday
     * @param float $payoutYesterday
     * @param float $payoutLast7Days
     * @param float $payoutThisMonth
     * @param float $payoutLastMonth
     * @param float $payoutAllTime
     * @param int $offersActive
     * @param int $offersPrivate
     * @param int $offersSuspended
     * @param int $offersIncent
     * @param int $offersNonIncent
     * @param int $offersTotal
     * @param int $offersPerDay
     */
    private function __construct($token, $period, $timezone, $incomeToday, $incomeYesterday, $incomeLast7Days, $incomeThisMonth, $incomeLastMonth, $incomeAllTime, $incomeSuperlinkToday, $incomeSuperlinkYesterday, $incomeSuperlinkLast7Days, $incomeSuperlinkThisMonth, $incomeSuperlinkLastMonth, $incomeSuperlinkAllTime, $revenueSuperlinkToday, $revenueSuperlinkYesterday, $revenueSuperlinkLast7Days, $revenueSuperlinkThisMonth, $revenueSuperlinkLastMonth, $revenueSuperlinkAllTime, $payoutToday, $payoutYesterday, $payoutLast7Days, $payoutThisMonth, $payoutLastMonth, $payoutAllTime, $offersActive, $offersPrivate, $offersSuspended, $offersIncent, $offersNonIncent, $offersTotal, $offersPerDay)
    {
        $this->token = $token;
        $this->period = $period;
        $this->timezone = $timezone;
        $this->incomeToday = $incomeToday;
        $this->incomeYesterday = $incomeYesterday;
        $this->incomeLast7Days = $incomeLast7Days;
        $this->incomeThisMonth = $incomeThisMonth;
        $this->incomeLastMonth = $incomeLastMonth;
        $this->incomeAllTime = $incomeAllTime;
        $this->incomeSuperlinkToday = $incomeSuperlinkToday;
        $this->incomeSuperlinkYesterday = $incomeSuperlinkYesterday;
        $this->incomeSuperlinkLast7Days = $incomeSuperlinkLast7Days;
        $this->incomeSuperlinkThisMonth = $incomeSuperlinkThisMonth;
        $this->incomeSuperlinkLastMonth = $incomeSuperlinkLastMonth;
        $this->incomeSuperlinkAllTime = $incomeSuperlinkAllTime;
        $this->revenueSuperlinkToday = $revenueSuperlinkToday;
        $this->revenueSuperlinkYesterday = $revenueSuperlinkYesterday;
        $this->revenueSuperlinkLast7Days = $revenueSuperlinkLast7Days;
        $this->revenueSuperlinkThisMonth = $revenueSuperlinkThisMonth;
        $this->revenueSuperlinkLastMonth = $revenueSuperlinkLastMonth;
        $this->revenueSuperlinkAllTime = $revenueSuperlinkAllTime;
        $this->payoutToday = $payoutToday;
        $this->payoutYesterday = $payoutYesterday;
        $this->payoutLast7Days = $payoutLast7Days;
        $this->payoutThisMonth = $payoutThisMonth;
        $this->payoutLastMonth = $payoutLastMonth;
        $this->payoutAllTime = $payoutAllTime;
        $this->offersActive = $offersActive;
        $this->offersPrivate = $offersPrivate;
        $this->offersSuspended = $offersSuspended;
        $this->offersIncent = $offersIncent;
        $this->offersNonIncent = $offersNonIncent;
        $this->offersTotal = $offersTotal;
        $this->offersPerDay = $offersPerDay;
    }

    /**
     * @param $token
     * @param $period
     * @param $timezone
     * @param $incomeToday
     * @param $incomeYesterday
     * @param $incomeLast7Days
     * @param $incomeThisMonth
     * @param $incomeLastMonth
     * @param $incomeAllTime
     * @param $incomeSuperlinkToday
     * @param $incomeSuperlinkYesterday
     * @param $incomeSuperlinkLast7Days
     * @param $incomeSuperlinkThisMonth
     * @param $incomeSuperlinkLastMonth
     * @param $incomeSuperlinkAllTime
     * @param $revenueSuperlinkToday
     * @param $revenueSuperlinkYesterday
     * @param $revenueSuperlinkLast7Days
     * @param $revenueSuperlinkThisMonth
     * @param $revenueSuperlinkLastMonth
     * @param $revenueSuperlinkAllTime
     * @param $payoutToday
     * @param $payoutYesterday
     * @param $payoutLast7Days
     * @param $payoutThisMonth
     * @param $payoutLastMonth
     * @param $payoutAllTime
     * @param $offersActive
     * @param $offersPrivate
     * @param $offersSuspended
     * @param $offersIncent
     * @param $offersNonIncent
     * @param $offersTotal
     * @param $offersPerDay
     *
     * @return self
     */
    public static function forProject($token, $period, $timezone, $incomeToday, $incomeYesterday, $incomeLast7Days, $incomeThisMonth, $incomeLastMonth, $incomeAllTime, $incomeSuperlinkToday, $incomeSuperlinkYesterday, $incomeSuperlinkLast7Days, $incomeSuperlinkThisMonth, $incomeSuperlinkLastMonth, $incomeSuperlinkAllTime, $revenueSuperlinkToday, $revenueSuperlinkYesterday, $revenueSuperlinkLast7Days, $revenueSuperlinkThisMonth, $revenueSuperlinkLastMonth, $revenueSuperlinkAllTime, $payoutToday, $payoutYesterday, $payoutLast7Days, $payoutThisMonth, $payoutLastMonth, $payoutAllTime, $offersActive, $offersPrivate, $offersSuspended, $offersIncent, $offersNonIncent, $offersTotal, $offersPerDay)
    {
        return new self($token, $period, $timezone, $incomeToday, $incomeYesterday, $incomeLast7Days, $incomeThisMonth, $incomeLastMonth, $incomeAllTime, $incomeSuperlinkToday, $incomeSuperlinkYesterday, $incomeSuperlinkLast7Days, $incomeSuperlinkThisMonth, $incomeSuperlinkLastMonth, $incomeSuperlinkAllTime, $revenueSuperlinkToday, $revenueSuperlinkYesterday, $revenueSuperlinkLast7Days, $revenueSuperlinkThisMonth, $revenueSuperlinkLastMonth, $revenueSuperlinkAllTime, $payoutToday, $payoutYesterday, $payoutLast7Days, $payoutThisMonth, $payoutLastMonth, $payoutAllTime, $offersActive, $offersPrivate, $offersSuspended, $offersIncent, $offersNonIncent, $offersTotal, $offersPerDay);
    }

    /**
     * {@inheritdoc}
     */
    public function projectToken() : string
    {
        return (string) $this->token;
    }

    /**
     * {@inheritdoc}
     */
    public function syncTime() : \DateTime
    {
        return (new \DateTime($this->period))->setTimezone(new \DateTimeZone($this->timezone));
    }

    /**
     * {@inheritdoc}
     */
    public function incomeToday() : float
    {
        return (float) $this->incomeToday;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeYesterday() : float
    {
        return (float) $this->incomeYesterday;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeLast7Days() : float
    {
        return (float) $this->incomeLast7Days;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeThisMonth() : float
    {
        return (float) $this->incomeThisMonth;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeLastMonth() : float
    {
        return (float) $this->incomeLastMonth;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeAllTime() : float
    {
        return (float) $this->incomeAllTime;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeSuperlinkToday() : float
    {
        return (float) $this->incomeSuperlinkToday;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeSuperlinkYesterday() : float
    {
        return (float) $this->incomeSuperlinkYesterday;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeSuperlinkLast7Days() : float
    {
        return (float) $this->incomeSuperlinkLast7Days;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeSuperlinkThisMonth() : float
    {
        return (float) $this->incomeSuperlinkThisMonth;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeSuperlinkLastMonth() : float
    {
        return (float) $this->incomeSuperlinkLastMonth;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeSuperlinkAllTime() : float
    {
        return (float) $this->incomeSuperlinkAllTime;
    }

    /**
     * {@inheritdoc}
     */
    public function revenueSuperlinkToday() : float
    {
        return (float) $this->revenueSuperlinkToday;
    }

    /**
     * {@inheritdoc}
     */
    public function revenueSuperlinkYesterday() : float
    {
        return (float) $this->revenueSuperlinkYesterday;
    }

    /**
     * {@inheritdoc}
     */
    public function revenueSuperlinkLast7Days() : float
    {
        return (float) $this->revenueSuperlinkLast7Days;
    }

    /**
     * {@inheritdoc}
     */
    public function revenueSuperlinkThisMonth() : float
    {
        return (float) $this->revenueSuperlinkThisMonth;
    }

    /**
     * {@inheritdoc}
     */
    public function revenueSuperlinkLastMonth() : float
    {
        return (float) $this->revenueSuperlinkLastMonth;
    }

    /**
     * {@inheritdoc}
     */
    public function revenueSuperlinkAllTime() : float
    {
        return (float) $this->revenueSuperlinkAllTime;
    }

    /**
     * {@inheritdoc}
     */
    public function payoutToday() : float
    {
        return (float) $this->payoutToday;
    }

    /**
     * {@inheritdoc}
     */
    public function payoutYesterday() : float
    {
        return (float) $this->payoutYesterday;
    }

    /**
     * {@inheritdoc}
     */
    public function payoutLast7Days() : float
    {
        return (float) $this->payoutLast7Days;
    }

    /**
     * {@inheritdoc}
     */
    public function payoutThisMonth() : float
    {
        return (float) $this->payoutThisMonth;
    }

    /**
     * {@inheritdoc}
     */
    public function payoutLastMonth() : float
    {
        return (float) $this->payoutLastMonth;
    }

    /**
     * {@inheritdoc}
     */
    public function payoutAllTime() : float
    {
        return (float) $this->payoutAllTime;
    }

    /**
     * {@inheritdoc}
     */
    public function offersActive() : int
    {
        return (int) $this->offersActive;
    }

    /**
     * {@inheritdoc}
     */
    public function offersPrivate() : int
    {
        return (int) $this->offersPrivate;
    }

    /**
     * {@inheritdoc}
     */
    public function offersSuspended() : int
    {
        return (int) $this->offersSuspended;
    }

    /**
     * {@inheritdoc}
     */
    public function offersIncent() : int
    {
        return (int) $this->offersIncent;
    }

    /**
     * {@inheritdoc}
     */
    public function offersNonIncent() : int
    {
        return (int) $this->offersNonIncent;
    }

    /**
     * {@inheritdoc}
     */
    public function offersTotal() : int
    {
        return (int) $this->offersTotal;
    }

    /**
     * {@inheritdoc}
     */
    public function offersPerDay() : int
    {
        return (int) $this->offersPerDay;
    }
}
