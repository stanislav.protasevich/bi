<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Dashboard\Command;

use Orangear\BusinessIntelligence\Domain\Model\CommandInterface;

/**
 * Interface SyncDashboardCommandInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Dashboard\Command
 */
interface SyncDashboardCommandInterface extends CommandInterface
{
    /**
     * @return string
     */
    public function projectToken() : string;

    /**
     * @return \DateTime
     */
    public function syncTime() : \DateTime;

    /**
     * @return float
     */
    public function incomeToday() : float;

    /**
     * @return float
     */
    public function incomeYesterday() : float;

    /**
     * @return float
     */
    public function incomeLast7Days() : float;

    /**
     * @return float
     */
    public function incomeThisMonth() : float;

    /**
     * @return float
     */
    public function incomeLastMonth() : float;

    /**
     * @return float
     */
    public function incomeAllTime() : float;

    /**
     * @return float
     */
    public function incomeSuperlinkToday() : float;

    /**
     * @return float
     */
    public function incomeSuperlinkYesterday() : float;

    /**
     * @return float
     */
    public function incomeSuperlinkLast7Days() : float;

    /**
     * @return float
     */
    public function incomeSuperlinkThisMonth() : float;

    /**
     * @return float
     */
    public function incomeSuperlinkLastMonth() : float;

    /**
     * @return float
     */
    public function incomeSuperlinkAllTime() : float;

    /**
     * @return float
     */
    public function revenueSuperlinkToday() : float;

    /**
     * @return float
     */
    public function revenueSuperlinkYesterday() : float;

    /**
     * @return float
     */
    public function revenueSuperlinkLast7Days() : float;

    /**
     * @return float
     */
    public function revenueSuperlinkThisMonth() : float;

    /**
     * @return float
     */
    public function revenueSuperlinkLastMonth() : float;

    /**
     * @return float
     */
    public function revenueSuperlinkAllTime() : float;

    /**
     * @return float
     */
    public function payoutToday() : float;

    /**
     * @return float
     */
    public function payoutYesterday() : float;

    /**
     * @return float
     */
    public function payoutLast7Days() : float;

    /**
     * @return float
     */
    public function payoutThisMonth() : float;

    /**
     * @return float
     */
    public function payoutLastMonth() : float;

    /**
     * @return float
     */
    public function payoutAllTime() : float;

    /**
     * @return int
     */
    public function offersActive() : int;

    /**
     * @return int
     */
    public function offersPrivate() : int;

    /**
     * @return int
     */
    public function offersSuspended() : int;

    /**
     * @return int
     */
    public function offersIncent() : int;

    /**
     * @return int
     */
    public function offersNonIncent() : int;

    /**
     * @return int
     */
    public function offersTotal() : int;

    /**
     * @return int
     */
    public function offersPerDay() : int;
}
