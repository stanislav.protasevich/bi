<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Dashboard\Handler;

use Orangear\BusinessIntelligence\Domain\Model\Dashboard\Command\SyncDashboardCommandInterface;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\Dashboard;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\DashboardRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectRepositoryInterface;
use Prooph\ServiceBus\EventBus;

/**
 * Class SyncDashboardHandler
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Dashboard\Handler
 */
class SyncDashboardHandler
{
    /** @var ProjectRepositoryInterface */
    private $projectRepository;

    /** @var DashboardRepositoryInterface */
    private $dashboardRepository;

    /** @var EventBus */
    private $eventBus;

    /**
     * SyncDashboardHandler constructor
     *
     * @param ProjectRepositoryInterface $projectRepository
     * @param DashboardRepositoryInterface $dashboardRepository
     * @param EventBus $eventBus
     */
    public function __construct(ProjectRepositoryInterface $projectRepository, DashboardRepositoryInterface $dashboardRepository, EventBus $eventBus)
    {
        $this->projectRepository = $projectRepository;
        $this->dashboardRepository = $dashboardRepository;
        $this->eventBus = $eventBus;
    }

    /**
     * @param SyncDashboardCommandInterface $command
     */
    public function __invoke(SyncDashboardCommandInterface $command)
    {
        $project = $this->projectRepository->findByTokenOrFail($command->projectToken());

        $dashboard = $this->dashboardRepository->findOneBy([
            'project' => $project
        ]);


        if (!$dashboard) {
            $dashboard = new Dashboard(
                $this->dashboardRepository->nextIdentifier(),
                $project,
                (new \DateTime('now'))->setTimezone(new \DateTimeZone('EEST')),
                $command->incomeToday(),
                $command->incomeYesterday(),
                $command->incomeLast7Days(),
                $command->incomeThisMonth(),
                $command->incomeLastMonth(),
                $command->incomeAllTime(),
                $command->incomeSuperlinkToday(),
                $command->incomeSuperlinkYesterday(),
                $command->incomeSuperlinkLast7Days(),
                $command->incomeSuperlinkThisMonth(),
                $command->incomeSuperlinkLastMonth(),
                $command->incomeSuperlinkAllTime(),
                $command->revenueSuperlinkToday(),
                $command->revenueSuperlinkYesterday(),
                $command->revenueSuperlinkLast7Days(),
                $command->revenueSuperlinkThisMonth(),
                $command->revenueSuperlinkLastMonth(),
                $command->revenueSuperlinkAllTime(),
                $command->payoutToday(),
                $command->payoutYesterday(),
                $command->payoutLast7Days(),
                $command->payoutThisMonth(),
                $command->payoutLastMonth(),
                $command->payoutAllTime(),
                $command->offersActive(),
                $command->offersPrivate(),
                $command->offersSuspended(),
                $command->offersIncent(),
                $command->offersNonIncent(),
                $command->offersTotal(),
                $command->offersPerDay()
            );
        } else {
            $dashboard->update(
                $project,
                (new \DateTime('now'))->setTimezone(new \DateTimeZone('EEST')),
                $command->incomeToday(),
                $command->incomeYesterday(),
                $command->incomeLast7Days(),
                $command->incomeThisMonth(),
                $command->incomeLastMonth(),
                $command->incomeAllTime(),
                $command->incomeSuperlinkToday(),
                $command->incomeSuperlinkYesterday(),
                $command->incomeSuperlinkLast7Days(),
                $command->incomeSuperlinkThisMonth(),
                $command->incomeSuperlinkLastMonth(),
                $command->incomeSuperlinkAllTime(),
                $command->revenueSuperlinkToday(),
                $command->revenueSuperlinkYesterday(),
                $command->revenueSuperlinkLast7Days(),
                $command->revenueSuperlinkThisMonth(),
                $command->revenueSuperlinkLastMonth(),
                $command->revenueSuperlinkAllTime(),
                $command->payoutToday(),
                $command->payoutYesterday(),
                $command->payoutLast7Days(),
                $command->payoutThisMonth(),
                $command->payoutLastMonth(),
                $command->payoutAllTime(),
                $command->offersActive(),
                $command->offersPrivate(),
                $command->offersSuspended(),
                $command->offersIncent(),
                $command->offersNonIncent(),
                $command->offersTotal(),
                $command->offersPerDay()
            );
        }

        $this->dashboardRepository->create($dashboard);
    }
}
