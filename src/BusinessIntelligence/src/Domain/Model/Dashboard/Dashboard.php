<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Dashboard;

use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use \DateTime;

/**
 * Class Dashboard
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Dashboard
 */
class Dashboard implements DashboardInterface
{
    /** @var DashboardIdInterface */
    private $id;

    /** @var ProjectInterface */
    private $project;

    /** @var DateTime */
    private $synchronizedAt;

    /** @var float */
    private $incomeToday;

    /** @var float */
    private $incomeYesterday;

    /** @var float */
    private $incomeLastSevenDays;

    /** @var float */
    private $incomeThisMonth;

    /** @var float */
    private $incomeLastMonth;

    /** @var float */
    private $incomeAllTime;

    /** @var float */
    private $incomeSuperlinkToday;

    /** @var float */
    private $incomeSuperlinkYesterday;

    /** @var float */
    private $incomeSuperlinkLastSevenDays;

    /** @var float */
    private $incomeSuperlinkThisMonth;

    /** @var float */
    private $incomeSuperlinkLastMonth;

    /** @var float */
    private $incomeSuperlinkAllTime;

    /** @var float */
    private $revenueSuperlinkToday;

    /** @var float */
    private $revenueSuperlinkYesterday;

    /** @var float */
    private $revenueSuperlinkLastSevenDays;

    /** @var float */
    private $revenueSuperlinkThisMonth;

    /** @var float */
    private $revenueSuperlinkLastMonth;

    /** @var float */
    private $revenueSuperlinkAllTime;

    /** @var float */
    private $payoutToday;

    /** @var float */
    private $payoutYesterday;

    /** @var float */
    private $payoutLastSevenDays;

    /** @var float */
    private $payoutThisMonth;

    /** @var float */
    private $payoutLastMonth;

    /** @var float */
    private $payoutAllTime;

    /** @var integer */
    private $offersActive;

    /** @var integer */
    private $offersPrivate;

    /** @var integer */
    private $offersSuspended;

    /** @var integer */
    private $offersIncent;

    /** @var integer */
    private $offersNonIncent;

    /** @var integer */
    private $offersTotal;

    /** @var integer */
    private $offersPerDay;

    /**
     * Dashboard constructor
     *
     * @param DashboardIdInterface $id
     * @param ProjectInterface $project
     * @param DateTime $synchronizedAt
     * @param float $incomeToday
     * @param float $incomeYesterday
     * @param float $incomeLastSevenDays
     * @param float $incomeThisMonth
     * @param float $incomeLastMonth
     * @param float $incomeAllTime
     * @param float $incomeSuperlinkToday
     * @param float $incomeSuperlinkYesterday
     * @param float $incomeSuperlinkLastSevenDays
     * @param float $incomeSuperlinkThisMonth
     * @param float $incomeSuperlinkLastMonth
     * @param float $incomeSuperlinkAllTime
     * @param float $revenueSuperlinkToday
     * @param float $revenueSuperlinkYesterday
     * @param float $revenueSuperlinkLastSevenDays
     * @param float $revenueSuperlinkThisMonth
     * @param float $revenueSuperlinkLastMonth
     * @param float $revenueSuperlinkAllTime
     * @param float $payoutToday
     * @param float $payoutYesterday
     * @param float $payoutLastSevenDays
     * @param float $payoutThisMonth
     * @param float $payoutLastMonth
     * @param float $payoutAllTime
     * @param int $offersActive
     * @param int $offersPrivate
     * @param int $offersSuspended
     * @param int $offersIncent
     * @param int $offersNonIncent
     * @param int $offersTotal
     * @param int $offersPerDay
     */
    public function __construct(DashboardIdInterface $id, ProjectInterface $project, DateTime $synchronizedAt, $incomeToday, $incomeYesterday, $incomeLastSevenDays, $incomeThisMonth, $incomeLastMonth, $incomeAllTime, $incomeSuperlinkToday, $incomeSuperlinkYesterday, $incomeSuperlinkLastSevenDays, $incomeSuperlinkThisMonth, $incomeSuperlinkLastMonth, $incomeSuperlinkAllTime, $revenueSuperlinkToday, $revenueSuperlinkYesterday, $revenueSuperlinkLastSevenDays, $revenueSuperlinkThisMonth, $revenueSuperlinkLastMonth, $revenueSuperlinkAllTime, $payoutToday, $payoutYesterday, $payoutLastSevenDays, $payoutThisMonth, $payoutLastMonth, $payoutAllTime, $offersActive, $offersPrivate, $offersSuspended, $offersIncent, $offersNonIncent, $offersTotal, $offersPerDay)
    {
        $this->id = $id;
        $this->project = $project;
        $this->synchronizedAt = $synchronizedAt;
        $this->incomeToday = $incomeToday;
        $this->incomeYesterday = $incomeYesterday;
        $this->incomeLastSevenDays = $incomeLastSevenDays;
        $this->incomeThisMonth = $incomeThisMonth;
        $this->incomeLastMonth = $incomeLastMonth;
        $this->incomeAllTime = $incomeAllTime;
        $this->incomeSuperlinkToday = $incomeSuperlinkToday;
        $this->incomeSuperlinkYesterday = $incomeSuperlinkYesterday;
        $this->incomeSuperlinkLastSevenDays = $incomeSuperlinkLastSevenDays;
        $this->incomeSuperlinkThisMonth = $incomeSuperlinkThisMonth;
        $this->incomeSuperlinkLastMonth = $incomeSuperlinkLastMonth;
        $this->incomeSuperlinkAllTime = $incomeSuperlinkAllTime;
        $this->revenueSuperlinkToday = $revenueSuperlinkToday;
        $this->revenueSuperlinkYesterday = $revenueSuperlinkYesterday;
        $this->revenueSuperlinkLastSevenDays = $revenueSuperlinkLastSevenDays;
        $this->revenueSuperlinkThisMonth = $revenueSuperlinkThisMonth;
        $this->revenueSuperlinkLastMonth = $revenueSuperlinkLastMonth;
        $this->revenueSuperlinkAllTime = $revenueSuperlinkAllTime;
        $this->payoutToday = $payoutToday;
        $this->payoutYesterday = $payoutYesterday;
        $this->payoutLastSevenDays = $payoutLastSevenDays;
        $this->payoutThisMonth = $payoutThisMonth;
        $this->payoutLastMonth = $payoutLastMonth;
        $this->payoutAllTime = $payoutAllTime;
        $this->offersActive = $offersActive;
        $this->offersPrivate = $offersPrivate;
        $this->offersSuspended = $offersSuspended;
        $this->offersIncent = $offersIncent;
        $this->offersNonIncent = $offersNonIncent;
        $this->offersTotal = $offersTotal;
        $this->offersPerDay = $offersPerDay;
    }

    public function update(ProjectInterface $project, DateTime $synchronizedAt, $incomeToday, $incomeYesterday, $incomeLastSevenDays, $incomeThisMonth, $incomeLastMonth, $incomeAllTime, $incomeSuperlinkToday, $incomeSuperlinkYesterday, $incomeSuperlinkLastSevenDays, $incomeSuperlinkThisMonth, $incomeSuperlinkLastMonth, $incomeSuperlinkAllTime, $revenueSuperlinkToday, $revenueSuperlinkYesterday, $revenueSuperlinkLastSevenDays, $revenueSuperlinkThisMonth, $revenueSuperlinkLastMonth, $revenueSuperlinkAllTime, $payoutToday, $payoutYesterday, $payoutLastSevenDays, $payoutThisMonth, $payoutLastMonth, $payoutAllTime, $offersActive, $offersPrivate, $offersSuspended, $offersIncent, $offersNonIncent, $offersTotal, $offersPerDay)
    {
        $this->project = $project;
        $this->synchronizedAt = $synchronizedAt;
        $this->incomeToday = $incomeToday;
        $this->incomeYesterday = $incomeYesterday;
        $this->incomeLastSevenDays = $incomeLastSevenDays;
        $this->incomeThisMonth = $incomeThisMonth;
        $this->incomeLastMonth = $incomeLastMonth;
        $this->incomeAllTime = $incomeAllTime;
        $this->incomeSuperlinkToday = $incomeSuperlinkToday;
        $this->incomeSuperlinkYesterday = $incomeSuperlinkYesterday;
        $this->incomeSuperlinkLastSevenDays = $incomeSuperlinkLastSevenDays;
        $this->incomeSuperlinkThisMonth = $incomeSuperlinkThisMonth;
        $this->incomeSuperlinkLastMonth = $incomeSuperlinkLastMonth;
        $this->incomeSuperlinkAllTime = $incomeSuperlinkAllTime;
        $this->revenueSuperlinkToday = $revenueSuperlinkToday;
        $this->revenueSuperlinkYesterday = $revenueSuperlinkYesterday;
        $this->revenueSuperlinkLastSevenDays = $revenueSuperlinkLastSevenDays;
        $this->revenueSuperlinkThisMonth = $revenueSuperlinkThisMonth;
        $this->revenueSuperlinkLastMonth = $revenueSuperlinkLastMonth;
        $this->revenueSuperlinkAllTime = $revenueSuperlinkAllTime;
        $this->payoutToday = $payoutToday;
        $this->payoutYesterday = $payoutYesterday;
        $this->payoutLastSevenDays = $payoutLastSevenDays;
        $this->payoutThisMonth = $payoutThisMonth;
        $this->payoutLastMonth = $payoutLastMonth;
        $this->payoutAllTime = $payoutAllTime;
        $this->offersActive = $offersActive;
        $this->offersPrivate = $offersPrivate;
        $this->offersSuspended = $offersSuspended;
        $this->offersIncent = $offersIncent;
        $this->offersNonIncent = $offersNonIncent;
        $this->offersTotal = $offersTotal;
        $this->offersPerDay = $offersPerDay;
    }

    /**
     * {@inheritdoc}
     */
    public function dashboardId() : DashboardIdInterface
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function project() : ProjectInterface
    {
        return $this->project;
    }

    /**
     * {@inheritdoc}
     */
    public function synchronizedAt() : DateTime
    {
        return $this->synchronizedAt;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeToday() : float
    {
        return $this->incomeToday;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeYesterday() : float
    {
        return $this->incomeYesterday;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeLastSevenDays() : float
    {
        return $this->incomeLastSevenDays;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeThisMonth() : float
    {
        return $this->incomeThisMonth;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeLastMonth() : float
    {
        return $this->incomeLastMonth;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeAllTime() : float
    {
        return $this->incomeAllTime;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeSuperlinkToday() : float
    {
        return $this->incomeSuperlinkToday;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeSuperlinkYesterday() : float
    {
        return $this->incomeSuperlinkYesterday;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeSuperlinkLastSevenDays() : float
    {
        return $this->incomeSuperlinkLastSevenDays;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeSuperlinkThisMonth() : float
    {
        return $this->incomeSuperlinkThisMonth;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeSuperlinkLastMonth() : float
    {
        return $this->incomeSuperlinkLastMonth;
    }

    /**
     * {@inheritdoc}
     */
    public function incomeSuperlinkAllTime() : float
    {
        return $this->incomeSuperlinkAllTime;
    }

    /**
     * {@inheritdoc}
     */
    public function revenueSuperlinkToday() : float
    {
        return $this->revenueSuperlinkToday;
    }

    /**
     * {@inheritdoc}
     */
    public function revenueSuperlinkYesterday() : float
    {
        return $this->revenueSuperlinkYesterday;
    }

    /**
     * {@inheritdoc}
     */
    public function revenueSuperlinkLastSevenDays() : float
    {
        return $this->revenueSuperlinkLastSevenDays;
    }

    /**
     * {@inheritdoc}
     */
    public function revenueSuperlinkThisMonth() : float
    {
        return $this->revenueSuperlinkThisMonth;
    }

    /**
     * {@inheritdoc}
     */
    public function revenueSuperlinkLastMonth() : float
    {
        return $this->revenueSuperlinkLastMonth;
    }

    /**
     * {@inheritdoc}
     */
    public function revenueSuperlinkAllTime() : float
    {
        return $this->revenueSuperlinkAllTime;
    }

    /**
     * {@inheritdoc}
     */
    public function payoutToday() : float
    {
        return $this->payoutToday;
    }

    /**
     * {@inheritdoc}
     */
    public function payoutYesterday() : float
    {
        return $this->payoutYesterday;
    }

    /**
     * {@inheritdoc}
     */
    public function payoutLastSevenDays() : float
    {
        return $this->payoutLastSevenDays;
    }

    /**
     * {@inheritdoc}
     */
    public function payoutThisMonth() : float
    {
        return $this->payoutThisMonth;
    }

    /**
     * {@inheritdoc}
     */
    public function payoutLastMonth() : float
    {
        return $this->payoutLastMonth;
    }

    /**
     * {@inheritdoc}
     */
    public function payoutAllTime() : float
    {
        return $this->payoutAllTime;
    }

    /**
     * {@inheritdoc}
     */
    public function offersActive() : int
    {
        return $this->offersActive;
    }

    /**
     * {@inheritdoc}
     */
    public function offersPrivate() : int
    {
        return $this->offersPrivate;
    }

    /**
     * {@inheritdoc}
     */
    public function offersSuspended() : int
    {
        return $this->offersSuspended;
    }

    /**
     * {@inheritdoc}
     */
    public function offersIncent() : int
    {
        return $this->offersIncent;
    }

    /**
     * {@inheritdoc}
     */
    public function offersNonIncent() : int
    {
        return $this->offersNonIncent;
    }

    /**
     * {@inheritdoc}
     */
    public function offersTotal() : int
    {
        return $this->offersTotal;
    }

    /**
     * {@inheritdoc}
     */
    public function offersPerDay() : int
    {
        return $this->offersPerDay;
    }
}
