<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge;

/**
 * Class EmployeeChargeMapperInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge
 */
interface EmployeeChargeMapperInterface
{
    /**
     * @param EmployeeChargeIdentifierInterface $employeeChargeIdentifier
     *
     * @return null|EmployeeChargeInterface
     */
    public function fetchById(EmployeeChargeIdentifierInterface $employeeChargeIdentifier):? EmployeeChargeInterface;

    /**
     * @param array $conditions
     * @return mixed
     */
    public function fetchAll(array $conditions = []);

    /**
     * @param EmployeeChargeInterface $employeeCharge
     */
    public function create(EmployeeChargeInterface $employeeCharge);

    /**
     * @param EmployeeChargeInterface $employeeCharge
     */
    public function update(EmployeeChargeInterface $employeeCharge);

    /**
     * @param EmployeeChargeInterface $employeeCharge
     */
    public function delete(EmployeeChargeInterface $employeeCharge);
}
