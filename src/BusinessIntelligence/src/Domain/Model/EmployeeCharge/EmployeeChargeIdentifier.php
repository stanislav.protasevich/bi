<?php

namespace Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge;

/**
 * Class EmployeeChargeIdentifier
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge
 */
class EmployeeChargeIdentifier implements EmployeeChargeIdentifierInterface
{
    /**
     * @var string
     */
    private $id;

    private function __construct($id = null)
    {
        $this->id = $id;
    }

    public static function fromString($anId)
    {
        return new static($anId);
    }

    public function toString(): string
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->id;
    }
}
