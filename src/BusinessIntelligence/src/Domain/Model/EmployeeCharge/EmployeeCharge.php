<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge;

use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeInterface;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeInterface;

/**
 * Class EmployeeCharge
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge
 */
class EmployeeCharge implements EmployeeChargeInterface
{
    /** @var EmployeeChargeIdentifierInterface */
    private $id;

    /** @var ChargeInterface */
    private $charge;

    /** @var EmployeeInterface */
    private $employee;

    /**
     * EmployeeCharge constructor
     *
     * @param EmployeeChargeIdentifierInterface $id
     * @param ChargeInterface $charge
     * @param EmployeeInterface $employee
     */
    private function __construct(EmployeeChargeIdentifierInterface $id, ChargeInterface $charge, EmployeeInterface $employee)
    {
        $this->id = $id;
        $this->charge = $charge;
        $this->employee = $employee;
    }

    /**
     * @param EmployeeChargeIdentifierInterface $id
     * @param ChargeInterface $charge
     * @param EmployeeInterface $employee
     *
     * @return EmployeeCharge
     */
    public static function withData(EmployeeChargeIdentifierInterface $id, ChargeInterface $charge, EmployeeInterface $employee)
    {
        return new self($id, $charge, $employee);
    }

    /**
     * @return EmployeeChargeIdentifierInterface
     */
    public function id(): EmployeeChargeIdentifierInterface
    {
        return $this->id;
    }

    /**
     * @return ChargeInterface
     */
    public function charge(): ChargeInterface
    {
        return $this->charge;
    }

    /**
     * @return EmployeeInterface
     */
    public function employee(): EmployeeInterface
    {
        return $this->employee;
    }
}
