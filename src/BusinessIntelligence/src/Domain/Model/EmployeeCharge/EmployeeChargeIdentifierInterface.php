<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge;

use Orangear\BusinessIntelligence\Domain\Model\IdentifierInterface;

/**
 * Interface EmployeeChargeIdentifierInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge
 */
interface EmployeeChargeIdentifierInterface extends IdentifierInterface {}