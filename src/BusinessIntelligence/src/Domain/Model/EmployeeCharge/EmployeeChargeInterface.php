<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge;

/**
 * Interface EmployeeChargeInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge
 */
interface EmployeeChargeInterface {}
