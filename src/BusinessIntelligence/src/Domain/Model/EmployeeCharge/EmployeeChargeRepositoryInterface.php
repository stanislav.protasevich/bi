<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge;

/**
 * Interface EmployeeChargeRepositoryInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge
 */
interface EmployeeChargeRepositoryInterface
{
    /**
     * @param EmployeeChargeIdentifierInterface $chargeTypeIdentifier
     *
     * @return null|EmployeeChargeInterface
     */
    public function findById(EmployeeChargeIdentifierInterface $chargeTypeIdentifier):? EmployeeChargeInterface;

    /**
     * @param array $conditions
     *
     * @return mixed
     */
    public function findAll(array $conditions = []);

    /**
     * @param EmployeeChargeInterface $chargeType
     */
    public function create(EmployeeChargeInterface $chargeType);

    /**
     * @param EmployeeChargeInterface $chargeType
     */
    public function update(EmployeeChargeInterface $chargeType);

    /**
     * @param EmployeeChargeInterface $chargeType
     */
    public function delete(EmployeeChargeInterface $chargeType);

    /**
     * @return EmployeeChargeIdentifierInterface
     */
    public function nextIdentifier(): EmployeeChargeIdentifierInterface;
}