<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Owner;

use Orangear\BusinessIntelligence\Domain\Model\AggregateRootInterface;
use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;

/**
 * Interface OwnerIdentifierInterface
 *
 * @package Orangear\BusinessIntelligence\src\Domain\Model\Owner
 */
interface OwnerIdentifierInterface extends AggregateRootInterface, IdentifierInterface {}
