<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Owner;

/**
 * Class OwnerInterface
 *
 * @package Orangear\BusinessIntelligence\src\Domain\Model\Owner
 */
interface OwnerInterface {}