<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Owner\Exception;

use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;

/**
 * Class OwnerNotFound
 *
 * @package Orangear\BusinessIntelligence\src\Domain\Model\Owner\Exception
 */
final class OwnerNotFound
{
    /**
     * @param IdentifierInterface $memberId
     *
     * @return OwnerNotFound
     */
    public static function withMemberId(IdentifierInterface $memberId)
    {
        return new self(sprintf('Owner with id %s cannot be found.', $memberId->toString()));
    }
}
