<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Owner;

use DateTime;
use Orangear\BusinessIntelligence\Domain\Model\AggregateRootInterface;
use Orangear\BusinessIntelligence\Domain\Model\AggregateRootTrait;
use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\Member\Member;
use Orangear\MembershipBundle\Domain\Model\Member\MemberInterface;

/**
 * Class Owner
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Owner
 */
class Owner extends Member implements AggregateRootInterface, OwnerInterface
{
    use AggregateRootTrait;

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return ['ROLE_OWNER'];
    }

    /**
     * {@inheritdoc}
     */
    protected function __construct(IdentifierInterface $id = null, $email = null, $password = null, $salt = null, DateTime $createdAt = null, DateTime $updatedAt = null, DateTime $deletedAt = null)
    {
        parent::__construct($id, $email, $password, $salt, $createdAt, $updatedAt, $deletedAt);
    }

    /**
     * {@inheritdoc}
     */
    public static function fromArray(array $payload): MemberInterface
    {
        return new static(
            $payload['id'] ?? null,
            $payload['email'] ?? null,
            $payload['password'] ?? null,
            $payload['salt'] ?? null,
            $payload['created_at'] ?? null,
            $payload['updated_at'] ?? null,
            $payload['deleted_at'] ?? null
        );
    }
}