<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ManagerSale;

/**
 * Interface ManagerSaleStatisticsMapperInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ManagerSale
 */
interface ManagerSaleStatisticsMapperInterface
{
    /**
     * @param ManagerSaleInterface $managerSale
     *
     * @return mixed
     */
    public function create(ManagerSaleInterface $managerSale);

    /**
     * @param array $conditions
     * @return mixed
     */
    public function fetchOneBy(array $conditions = []);
}