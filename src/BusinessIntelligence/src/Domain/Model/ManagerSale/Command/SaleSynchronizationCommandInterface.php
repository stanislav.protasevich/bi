<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Command;

use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;
use DateTime;

/**
 * Class SaleSynchronizationCommandInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Command
 */
interface SaleSynchronizationCommandInterface
{
    /**
     * @return string
     */
    public function token() : string;

    /**
     * @return array
     */
    public function data() : array;
}
