<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Command;

use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;
use DateTime;

/**
 * Class SyncEmployeeCommand
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Command
 */
final class SyncManagerSaleCommand implements SyncManagerSaleCommandInterface
{
    /** @var string */
    private $projectId;

    /** @var string */
    private $startDate;

    /** @var string */
    private $endDate;

    /**
     * SyncEmployeeCommand constructor
     *
     * @param string $projectId
     * @param string $startDate
     * @param string $endDate
     */
    private function __construct($projectId, $startDate, $endDate)
    {
        $this->projectId = $projectId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @param $projectId
     * @param string $startDate
     * @param string $endDate
     *
     * @return SyncManagerSaleCommand
     */
    public static function forProject($projectId, $startDate, $endDate)
    {
        return new self($projectId, $startDate, $endDate);
    }

    /**
     * {@inheritdoc}
     */
    public function projectId(): ProjectIdentifierInterface
    {
        return ProjectId::fromString($this->projectId);
    }

    /**
     * {@inheritdoc}
     */
    public function startDate(): DateTime
    {
        return DateTime::createFromFormat('Y-m-d', $this->startDate);
    }

    /**
     * {@inheritdoc}
     */
    public function endDate(): DateTime
    {
        return DateTime::createFromFormat('Y-m-d', $this->endDate);
    }
}
