<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Command;

/**
 * Class SaleSynchronizationCommand
 * @package Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Command
 */
final class SaleSynchronizationCommand implements SaleSynchronizationCommandInterface
{
    /** @var string */
    private $token;

    /** @var array */
    private $data;

    /**
     * SaleSynchronizationCommand constructor.
     * @param string $token
     * @param array $data
     */
    private function __construct(string $token, array $data)
    {
        $this->token = $token;
        $this->data = $data;
    }

    public static function forToken(string $token, array $data)
    {
        return new self($token, $data);
    }

    /**
     * @return string
     */
    public function token(): string
    {
        return $this->token;
    }

    /**
     * @return array
     */
    public function data(): array
    {
        return $this->data;
    }
}
