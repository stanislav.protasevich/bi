<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Command;

use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;
use DateTime;

/**
 * Class SyncEmployeeCommandInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Command
 */
interface SyncManagerSaleCommandInterface
{
    /**
     * @return ProjectIdentifierInterface
     */
    public function projectId() : ProjectIdentifierInterface;

    /**
     * @return DateTime
     */
    public function startDate() : DateTime;

    /**
     * @return DateTime
     */
    public function endDate() : DateTime;
}
