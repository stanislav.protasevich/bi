<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query;

/**
 * Interface GetStaffDataByProjectQueryInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query
 */
interface GetStaffDataByProjectQueryInterface
{
    public function memberId();
    public function projectId();
    public function startDate();
    public function endDate();

    public static function forHead(string $headId, string $projectId, string $startDate, string $endDate);
}