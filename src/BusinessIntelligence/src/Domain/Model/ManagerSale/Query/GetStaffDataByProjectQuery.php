<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query;

use Orangear\Admin\Core\Domain\Model\Employee\EmployeeId;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;
use Orangear\MembershipBundle\Domain\Model\Member\MemberId;

/**
 * Class GetStaffDataByProjectQuery
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query
 */
class GetStaffDataByProjectQuery implements GetStaffDataByProjectQueryInterface
{
    private $memberId;

    private $projectId;

    private $startDate;

    private $endDate;

    /**
     * GetStaffDataByProjectQuery constructor
     *
     * @param $memberId
     * @param $projectId
     * @param $startDate
     * @param $endDate
     */
    private function __construct($memberId, $projectId, $startDate, $endDate)
    {
        $this->memberId = $memberId;
        $this->projectId = $projectId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @param $memberId
     * @param $projectId
     * @param $startDate
     * @param $endDate
     *
     * @return GetStaffDataByProjectQuery
     */
    public static function forMember($memberId, $projectId, $startDate, $endDate)
    {
        return new self($memberId, $projectId, $startDate, $endDate);
    }

    /**
     * @inheritdoc
     */
    public static function forHead(string $headId, string $projectId, string $startDate, string $endDate)
    {
        return new self($headId, $projectId, $startDate, $endDate);
    }

    /**
     * @return \Orangear\MembershipBundle\Domain\Model\IdentifierInterface
     */
    public function memberId()
    {
        return MemberId::fromString($this->memberId);
    }

    /**
     * @return ProjectId
     */
    public function projectId()
    {
        return ProjectId::fromString($this->projectId);
    }

    /**
     * @return \DateTime
     */
    public function startDate()
    {
        return new \DateTime($this->startDate);
    }

    /**
     * @return \DateTime
     */
    public function endDate()
    {
        return new \DateTime($this->endDate);
    }
}
