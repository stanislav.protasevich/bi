<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query;

/**
 * Interface GetStaffDataByManagerQueryInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query
 */
interface GetStaffDataByManagerQueryInterface
{
    public function memberId();
    public function employeeId();
    public function startDate();
    public function endDate();
}