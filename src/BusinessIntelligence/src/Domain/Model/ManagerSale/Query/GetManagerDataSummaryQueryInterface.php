<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query;

/**
 * Interface GetStaffDataByManagerQueryInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query
 */
interface GetManagerDataSummaryQueryInterface
{
    public function memberId();
    public function employeeId();

    /**
     * @param $headId
     * @param $employeeId
     * @return GetManagerDataSummaryQueryInterface
     */
    public static function forHead($headId, $employeeId): GetManagerDataSummaryQueryInterface;
}
