<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query;

use Orangear\Admin\Core\Domain\Model\Employee\EmployeeId;
use Orangear\MembershipBundle\Domain\Model\Member\MemberId;

/**
 * Class GetManagerDataSummaryQuery
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query
 */
class GetManagerDataSummaryQuery implements GetManagerDataSummaryQueryInterface
{
    private $memberId;

    private $employeeId;

    /**
     * GetManagerDataSummaryQuery constructor
     *
     * @param $memberId
     * @param $employeeId
     */
    private function __construct($memberId, $employeeId)
    {
        $this->memberId = $memberId;
        $this->employeeId = $employeeId;
    }

    /**
     * @param $memberId
     * @param $employeeId
     *
     * @return GetManagerDataSummaryQueryInterface
     */
    public static function forMember($memberId, $employeeId)
    {
        return new self($memberId, $employeeId);
    }

    /**
     * @param $headId
     * @param $employeeId
     *
     * @return GetManagerDataSummaryQueryInterface
     */
    public static function forHead($headId, $employeeId): GetManagerDataSummaryQueryInterface
    {
        return new self($headId, $employeeId);
    }

    /**
     * @return \Orangear\MembershipBundle\Domain\Model\IdentifierInterface
     */
    public function memberId()
    {
        return MemberId::fromString($this->memberId);
    }

    /**
     * @return EmployeeId
     */
    public function employeeId()
    {
        return EmployeeId::create($this->employeeId);
    }
}
