<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query;

use Orangear\Admin\Core\Domain\Model\Employee\EmployeeId;
use Orangear\MembershipBundle\Domain\Model\Member\MemberId;

/**
 * Class GetStaffDataByManagerQuery
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query
 */
class GetStaffDataByManagerQuery implements GetStaffDataByManagerQueryInterface
{
    private $memberId;

    private $employeeId;

    private $startDate;

    private $endDate;

    /**
     * GetStaffDataByManagerQuery constructor
     *
     * @param $memberId
     * @param $employeeId
     * @param $startDate
     * @param $endDate
     */
    private function __construct($memberId, $employeeId, $startDate, $endDate)
    {
        $this->memberId = $memberId;
        $this->employeeId = $employeeId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @param $memberId
     * @param $employeeId
     * @param $startDate
     * @param $endDate
     *
     * @return GetStaffDataByManagerQuery
     */
    public static function forMember($memberId, $employeeId, $startDate, $endDate)
    {
        return new self($memberId, $employeeId, $startDate, $endDate);
    }

    /**
     * @param $headId
     * @param $employeeId
     * @param $startDate
     * @param $endDate
     *
     * @return GetStaffDataByManagerQuery
     */
    public static function forHead($headId, $employeeId, $startDate, $endDate)
    {
        return new self($headId, $employeeId, $startDate, $endDate);
    }

    /**
     * @return \Orangear\MembershipBundle\Domain\Model\IdentifierInterface
     */
    public function memberId()
    {
        return MemberId::fromString($this->memberId);
    }

    /**
     * @return EmployeeId
     */
    public function employeeId()
    {
        return EmployeeId::create($this->employeeId);
    }

    /**
     * @return \DateTime
     */
    public function startDate()
    {
        return new \DateTime($this->startDate);
    }

    /**
     * @return \DateTime
     */
    public function endDate()
    {
        return new \DateTime($this->endDate);
    }
}
