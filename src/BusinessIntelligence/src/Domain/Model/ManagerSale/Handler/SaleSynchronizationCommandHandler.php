<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Handler;

use DateTime;
use Orangear\BusinessIntelligence\Bot\Bot;
use Orangear\BusinessIntelligence\Bot\Chat;
use Orangear\BusinessIntelligence\Bot\Message;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeInterface;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Command\SaleSynchronizationCommandInterface;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\ManagerSale;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\ManagerSaleStatisticsRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployeeRepositoryInterface;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\TransactionManager;

/**
 * Class SaleSynchronizationCommandHandler
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\Handler
 */
final class SaleSynchronizationCommandHandler
{
    /** @var ProjectRepositoryInterface */
    private $projectRepository;

    /** @var RemoteEmployeeRepositoryInterface */
    private $remoteEmployeeRepository;

    /** @var EmployeeRepositoryInterface */
    private $employeeRepository;

    /** @var ManagerSaleStatisticsRepositoryInterface */
    private $managerSaleStatisticsRepository;

    /** @var Bot */
    private $bot;

    /** @var Chat */
    private $chat;

    /** @var TransactionManager */
    private $transactionManager;

    /**
     * SyncManagerSaleCommandHandler constructor
     *
     * @param RemoteEmployeeRepositoryInterface $remoteEmployeeRepository
     * @param ProjectRepositoryInterface $projectRepository
     * @param EmployeeRepositoryInterface $employeeRepository
     * @param ManagerSaleStatisticsRepositoryInterface $managerSaleStatisticsRepository
     * @param Bot $bot
     * @param Chat $chat
     * @param TransactionManager $transactionManager
     */
    public function __construct(ProjectRepositoryInterface $projectRepository, RemoteEmployeeRepositoryInterface $remoteEmployeeRepository, EmployeeRepositoryInterface $employeeRepository, ManagerSaleStatisticsRepositoryInterface $managerSaleStatisticsRepository, Bot $bot, Chat $chat, TransactionManager $transactionManager)
    {
        $this->projectRepository = $projectRepository;
        $this->remoteEmployeeRepository = $remoteEmployeeRepository;
        $this->employeeRepository = $employeeRepository;
        $this->managerSaleStatisticsRepository = $managerSaleStatisticsRepository;

        $this->bot = $bot;
        $this->chat = $chat;

        $this->transactionManager = $transactionManager;
    }

    /**
     * @param SaleSynchronizationCommandInterface $command
     * @throws \Exception
     */
    public function __invoke(SaleSynchronizationCommandInterface $command)
    {
        $project = $this->projectRepository->findByTokenOrFail($command->token());

        $sales = $command->data();

        $this->transactionManager->beginTransaction();

        $missingIds = [];

        try {
            foreach ($sales as $sale) {
                $remoteEmployees = $this->remoteEmployeeRepository->findAll([
                    'project' => $project,
                    'remoteId' => $sale['aff_id']
                ]);

                if (empty($remoteEmployees)) {
                    $missingIds[] = $sale['aff_id'];

                    continue;
                }

                if (!isset($sale['superlink_revenue'])) {
                    $sale['superlink_revenue'] = 0;
                }

                if (!isset($sale['superlink_profit'])) {
                    $sale['superlink_profit'] = 0;
                }

                /** @var EmployeeInterface $remoteEmployee */
                $remoteEmployee = $remoteEmployees[0];

                if ($remoteEmployee->employee() === null) {
                    $missingIds[] = $sale['aff_id'];

                    continue;
                }

                $employee = $this->employeeRepository->findById($remoteEmployee->employee()->id());

                /** @var ManagerSale $managerSale */
                $managerSale = $this->managerSaleStatisticsRepository->findOneBy([
                    'project' => $project,
                    'employee' => $employee,
                    'period' => new \DateTime($sale['date'] . '-01')
                ]);

                if ($managerSale) {
                    $managerSale->grossSale((float) $sale['revenues']);
                    $managerSale->grossProfit((float) $sale['profit']);
                    $managerSale->superlinkGrossSale((float) $sale['superlink_revenue']);
                    $managerSale->superlinkGrossProfit((float) $sale['superlink_profit']);
                    $managerSale->scrub((float) $sale['scrubs']);
                    $managerSale->synchronizedAt(new DateTime());

                    $this->managerSaleStatisticsRepository->update($managerSale);
                } else {
                    $managerSale = ManagerSale::create(
                        \Ramsey\Uuid\Uuid::uuid4(),
                        $project,
                        new \DateTime($sale['date'] . '-01'),
                        $sale['revenues'],
                        $sale['profit'],
                        $sale['superlink_revenue'],
                        $sale['superlink_profit'],
                        $sale['scrubs'],
                        $employee,
                        new DateTime()
                    );

                    $this->managerSaleStatisticsRepository->create($managerSale);
                }
            }

            if (count($missingIds) > 0) {
                $this->sendBotMessage(
                    'Missing Manager',
                    $project->name() . ': ' . implode(',', array_unique($missingIds)) . '.'
                );
            }

            $this->transactionManager->commit();
        } catch (\Exception $e) {
            $this->transactionManager->rollback();

            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @param string $header
     * @param string $text
     */
    public function sendBotMessage(string $header, string $text)
    {
        $message = Message::withData($header, $text);

        $this->bot->sendMessage($this->chat, $message);
    }
}
