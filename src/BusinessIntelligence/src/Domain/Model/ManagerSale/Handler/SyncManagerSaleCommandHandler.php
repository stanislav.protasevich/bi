<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Handler;

use Orangear\BusinessIntelligence\Bot\Bot;
use Orangear\BusinessIntelligence\Bot\Chat;
use Orangear\BusinessIntelligence\Bot\Message;
use Orangear\BusinessIntelligence\Bot\Token;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeInterface;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Command\SyncManagerSaleCommandInterface;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\ManagerSale;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\ManagerSaleStatisticsRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployeeRepositoryInterface;
use Zend\Http\Client;
use Zend\Http\Headers;
use Zend\Http\Request;
use Zend\Stdlib\Parameters;

/**
 * Class SyncManagerSaleCommandHandler
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\Handler
 */
final class SyncManagerSaleCommandHandler
{
    /** @var Client */
    private $httpClient;

    /** @var ProjectRepositoryInterface */
    private $projectRepository;

    /** @var RemoteEmployeeRepositoryInterface */
    private $remoteEmployeeRepository;

    /** @var EmployeeRepositoryInterface */
    private $employeeRepository;

    /** @var ManagerSaleStatisticsRepositoryInterface */
    private $managerSaleStatisticsRepository;

    /** @var Bot */
    private $bot;

    /** @var Chat */
    private $chat;

    /**
     * SyncManagerSaleCommandHandler constructor
     *
     * @param RemoteEmployeeRepositoryInterface $remoteEmployeeRepository
     * @param ProjectRepositoryInterface $projectRepository
     * @param EmployeeRepositoryInterface $employeeRepository
     * @param ManagerSaleStatisticsRepositoryInterface $managerSaleStatisticsRepository
     * @param Bot $bot
     * @param Chat $chat
     */
    public function __construct(ProjectRepositoryInterface $projectRepository, RemoteEmployeeRepositoryInterface $remoteEmployeeRepository, EmployeeRepositoryInterface $employeeRepository, ManagerSaleStatisticsRepositoryInterface $managerSaleStatisticsRepository, Bot $bot, Chat $chat)
    {
        $this->projectRepository = $projectRepository;
        $this->remoteEmployeeRepository = $remoteEmployeeRepository;
        $this->employeeRepository = $employeeRepository;
        $this->managerSaleStatisticsRepository = $managerSaleStatisticsRepository;

        $this->httpClient = new Client();
        $this->httpClient->setOptions(['timeout' => 300]);

        $this->bot = $bot;
        $this->chat = $chat;
    }

    /**
     * @param SyncManagerSaleCommandInterface $command
     */
    public function __invoke(SyncManagerSaleCommandInterface $command)
    {
        // @todo persist in transaction

        $project = $this->projectRepository->findByIdOrFail($command->projectId());

        $sales = $this->getManagerSales($project, $command->startDate(), $command->endDate());

        foreach ($sales['data'] as $sale) {
            $remoteEmployees = $this->remoteEmployeeRepository->findAll([
                'project' => $project,
                'remoteId' => $sale['aff_id']
            ]);

            if (empty($remoteEmployees)) {
                $this->sendBotMessage(
                'Missing Manager',
                $project->name() . ': ' . '#' . $sale['aff_id'] . '.'
                );

                continue;
            }

            /** @var EmployeeInterface $remoteEmployee */
            $remoteEmployee = $remoteEmployees[0];

            $employee = $this->employeeRepository->findById($remoteEmployee->employee()->id());

            /** @var ManagerSale $managerSale */
            $managerSale = $this->managerSaleStatisticsRepository->findOneBy([
                'employee' => $employee,
                'period' => new \DateTime($sale['date'] . '-01')
            ]);

            if ($managerSale) {
                $managerSale->grossSale((float) $sale['revenues']);
                $managerSale->grossProfit((float) $sale['profit']);
                $managerSale->scrub((float) $sale['scrubs']);

                $this->managerSaleStatisticsRepository->update($managerSale);
            } else {
                $managerSale = ManagerSale::create(
                    \Ramsey\Uuid\Uuid::uuid4(),
                    new \DateTime($sale['date'] . '-01'),
                    $sale['revenues'],
                    $sale['profit'],
                    $sale['scrubs'],
                    $employee
                );

                $this->managerSaleStatisticsRepository->create($managerSale);
            }
        }
    }

    /**
     * @param ProjectInterface $project
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     *
     * @return mixed
     *
     * @throws \Exception
     */
    private function getManagerSales(ProjectInterface $project, \DateTime $startDate, \DateTime $endDate)
    {
        $params = [];
        $params['dateFrom'] = $startDate->format('Y-m-d');
        $params['dateTo'] = $endDate->format('Y-m-d');
        $params['size'] = 100;
        $params['token'] = $project->token();

        $request = new Request();
        $request->setUri($project->apiUrl() . 'bi/statistics/managers');
        $request->setHeaders((new Headers())->addHeaders(['X-AUTH-TOKEN' => $project->token()]));
        $request->setMethod(Request::METHOD_GET);
        $request->setQuery(new Parameters($params));

        $this->httpClient->setRequest($request);

        $response = $this->httpClient->send();


        if (!$response->isOk()) {
            throw new \Exception('No valid responce from ' . $project->apiUrl());
        }

        return json_decode($response->getBody(), true);
    }

    /**
     * @param string $header
     * @param string $text
     */
    public function sendBotMessage(string $header, string $text)
    {
        $message = Message::withData($header, $text);

        $this->bot->sendMessage($this->chat, $message);
    }
}
