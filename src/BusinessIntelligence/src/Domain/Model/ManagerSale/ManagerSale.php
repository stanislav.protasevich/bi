<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ManagerSale;

use DateTime;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;

/**
 * Class ManagerSale
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ManagerSale
 */
class ManagerSale implements ManagerSaleInterface
{
    /** @var string */
    private $id;

    /** @var ProjectInterface */
    private $project;

    /** @var \DateTime */
    private $period;

    /** @var float */
    private $grossSale;

    /** @var float */
    private $grossProfit;

    /** @var float */
    private $superlinkGrossSale;

    /** @var float */
    private $superlinkGrossProfit;

    /** @var float */
    private $scrub;

    /** @var EmployeeInterface */
    private $employee;

    /** @var bool */
    private $synchronizedAt;

    /**
     * ManagerSale constructor
     *
     * @param string $id
     * @param ProjectInterface $project
     * @param \DateTime $period
     * @param float $grossSale
     * @param float $grossProfit
     * @param float $superlinkGrossSale
     * @param float $superlinkGrossProfit
     * @param float $scrub
     * @param EmployeeInterface $employee
     * @param DateTime $synchronizedAt
     */
    private function __construct($id, $project, \DateTime $period, $grossSale, $grossProfit, $superlinkGrossSale, $superlinkGrossProfit, $scrub, $employee, $synchronizedAt)
    {
        $this->id = $id;
        $this->project = $project;
        $this->period = $period;
        $this->grossSale = $grossSale;
        $this->grossProfit = $grossProfit;
        $this->superlinkGrossSale = $superlinkGrossSale;
        $this->superlinkGrossProfit = $superlinkGrossProfit;
        $this->scrub = $scrub;
        $this->employee = $employee;
        $this->synchronizedAt = $synchronizedAt;
    }

    /**
     * @param $id
     * @param ProjectInterface $project
     * @param \DateTime $period
     * @param $grossSale
     * @param $grossProfit
     * @param float $superlinkGrossSale
     * @param float $superlinkGrossProfit
     * @param $scrub
     * @param EmployeeInterface $employee
     * @param DateTime $synchronizedAt
     *
     * @return ManagerSale
     */
    public static function create($id, $project, \DateTime $period, $grossSale, $grossProfit, $superlinkGrossSale, $superlinkGrossProfit, $scrub, $employee, $synchronizedAt)
    {
        return new self($id, $project, $period, $grossSale, $grossProfit, $superlinkGrossSale, $superlinkGrossProfit, $scrub, $employee, $synchronizedAt);
    }

    /**
     * @param $id
     * @param ProjectInterface $project
     * @param \DateTime $period
     * @param $grossSale
     * @param $grossProfit
     * @param float $superlinkGrossSale
     * @param float $superlinkGrossProfit
     * @param EmployeeInterface $employee
     * @param $scrub
     * @param DateTime $synchronizedAt
     *
     * @return ManagerSale
     */
    public function withData($id, $project, \DateTime $period, $grossSale, $grossProfit, $superlinkGrossSale, $superlinkGrossProfit, $scrub, $employee, $synchronizedAt)
    {
        return new self($id, $project, $period, $grossSale, $grossProfit, $superlinkGrossSale, $superlinkGrossProfit, $scrub, $employee, $synchronizedAt);
    }

    /**
     * @param float $grossSale
     */
    public function grossSale(float $grossSale)
    {
        $this->grossSale = $grossSale;
    }

    /**
     * @param float $grossProfit
     */
    public function grossProfit(float $grossProfit)
    {
        $this->grossProfit = $grossProfit;
    }

    /**
     * @param float $superlinkGrossSale
     */
    public function superlinkGrossSale(float $superlinkGrossSale)
    {
        $this->superlinkGrossSale = $superlinkGrossSale;
    }

    /**
     * @param float $superlinkGrossProfit
     */
    public function superlinkGrossProfit(float $superlinkGrossProfit)
    {
        $this->superlinkGrossProfit = $superlinkGrossProfit;
    }

    /**
     * @param float $scrub
     */
    public function scrub(float $scrub)
    {
        $this->scrub = $scrub;
    }

    /**
     * @param \DateTime $period
     */
    public function period(\DateTime $period)
    {
        $this->period = $period;
    }

    /**
     * @param \DateTime $synchronizedAt
     */
    public function synchronizedAt(\DateTime $synchronizedAt)
    {
        $this->synchronizedAt = $synchronizedAt;
    }
}
