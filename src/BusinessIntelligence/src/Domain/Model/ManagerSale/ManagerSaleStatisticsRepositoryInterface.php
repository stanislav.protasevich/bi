<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ManagerSale;

/**
 * Interface ManagerSaleStatisticsRepositoryInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ManagerSale
 */
interface ManagerSaleStatisticsRepositoryInterface
{
    /**
     * @param ManagerSaleInterface $managerSale
     *
     * @return mixed
     */
    public function create(ManagerSaleInterface $managerSale);

    public function update(ManagerSaleInterface $managerSale);

    public function findOneBy($criteria = []);
}