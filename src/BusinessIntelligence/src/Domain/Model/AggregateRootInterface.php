<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model;

/**
 * Interface AggregateRootInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model
 */
interface AggregateRootInterface
{
    /**
     * @param EventInterface $event
     */
    public function record(EventInterface $event);

    /**
     * @return array
     */
    public function getRecordedEvents() : array;
}
