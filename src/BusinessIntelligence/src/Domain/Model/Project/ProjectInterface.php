<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Project;

use Orangear\BusinessIntelligence\Domain\Model\AggregateRootInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformInterface;
use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;

/**
 * Interface ProjectInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Project
 */
interface ProjectInterface extends AggregateRootInterface
{
    /**
     * @return ProjectIdentifierInterface
     */
    public function projectId() : ProjectIdentifierInterface;

    /**
     * @return PlatformInterface
     */
    public function platform() : PlatformInterface;

    /**
     * @return string
     */
    public function name() : string;

    /**
     * @return string
     */
    public function token() : string;

    /**
     * @return string
     */
    public function logo() : string;

    /**
     * @return int
     */
    public function order() : int;

    /**
     * @return string
     */
    public function apiUrl() : string;

    /**
     * @param IdentifierInterface $memberId
     * @param ProjectIdentifierInterface $id
     * @param PlatformInterface $platform
     * @param $name
     * @param $token
     * @param $logo
     * @param $order
     * @param $apiUrl
     *
     * @return ProjectInterface
     */
    public static function create(IdentifierInterface $memberId, ProjectIdentifierInterface $id, PlatformInterface $platform, $name, $token, $logo, $order, $apiUrl) : ProjectInterface;
}
