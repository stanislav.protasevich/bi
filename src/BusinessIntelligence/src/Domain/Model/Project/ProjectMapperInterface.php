<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Project;

/**
 * Class PlatformMapperInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Project
 */
interface ProjectMapperInterface
{
    /**
     * @param ProjectIdentifierInterface $id
     *
     * @return mixed
     */
    public function fetchById(ProjectIdentifierInterface $id);

    /**
     * @param string $token
     *
     * @return mixed
     */
    public function fetchByToken(string $token);

    /**
     * @param array $conditions
     *
     * @return mixed
     */
    public function fetchAll(array $conditions = array());

    /**
     * @param ProjectInterface $project
     */
    public function create(ProjectInterface $project);
}
