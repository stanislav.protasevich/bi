<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Project;

use Orangear\BusinessIntelligence\Domain\Model\Collection;

/**
 * Class PlatformCollection
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Platform
 */
final class ProjectCollection extends Collection implements ProjectCollectionInterface {}
