<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Project;

/**
 * Class ProjectCollectionInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Project
 */
interface ProjectCollectionInterface
{
    /**
     * @param $project
     */
    public function add($project);
}
