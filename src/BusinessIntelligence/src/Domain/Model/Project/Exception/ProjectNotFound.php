<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Project\Exception;

use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;

/**
 * Class ProjectNotFound
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Project\Exception
 */
final class ProjectNotFound extends \InvalidArgumentException
{
    /**
     * @param ProjectIdentifierInterface $projectIdentifier
     *
     * @return self
     */
    public static function withProjectId(ProjectIdentifierInterface $projectIdentifier)
    {
        return new self(sprintf('Platform with id %s cannot be found.', $projectIdentifier->toString()));
    }

    /**
     * @param string $projectName
     *
     * @return self
     */
    public static function withProjectName(string $projectName)
    {
        return new self(sprintf('Platform with name "%s" cannot be found.', $projectName));
    }

    /**
     * @param string $projectToken
     *
     * @return self
     */
    public static function withProjectToken(string $projectToken)
    {
        return new self(sprintf('Platform with token "%s" cannot be found.', $projectToken));
    }
}
