<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Project\Exception;

use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;

/**
 * Class InvalidProject
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Project\Exception
 */
final class InvalidProject extends \InvalidArgumentException
{
    /**
     * @param ProjectIdentifierInterface $projectIdentifier
     *
     * @return self
     */
    public static function withProjectId(ProjectIdentifierInterface $projectIdentifier)
    {
        return new self(sprintf('Invalid project identifier "%s".', $projectIdentifier->toString()));
    }

    /**
     * @param PlatformIdentifierInterface $platformIdentifier
     *
     * @return self
     */
    public static function withPlatformId(PlatformIdentifierInterface $platformIdentifier)
    {
        return new self(sprintf('Invalid project platform identifier "%s".', $platformIdentifier->toString()));
    }

    /**
     * @param string $projectName
     *
     * @return self
     */
    public static function withProjectName(string $projectName)
    {
        return new self(sprintf('Invalid project name "%s".', $projectName));
    }

    /**
     * @param string $projectToken
     *
     * @return self
     */
    public static function withProjectToken(string $projectToken)
    {
        return new self(sprintf('Invalid project token "%s".', $projectToken));
    }

    /**
     * @param string $projectLogo
     *
     * @return self
     */
    public static function withProjectLogo(string $projectLogo)
    {
        return new self(sprintf('Invalid project logo "%s".', $projectLogo));
    }

    /**
     * @param int $projectOrder
     *
     * @return self
     */
    public static function withProjectOrder(int $projectOrder = null)
    {
        return new self(sprintf('Invalid project order "%s".', $projectOrder));
    }

    /**
     * @param string $projectApiUrl
     *
     * @return self
     */
    public static function withProjectApiUrl(string $projectApiUrl)
    {
        return new self(sprintf('Invalid project\'s API url "%s".', $projectApiUrl));
    }
}
