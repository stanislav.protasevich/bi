<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Project\Exception;

/**
 * Class ProjectAlreadyExists
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Project\Exception
 */
final class ProjectAlreadyExists extends \InvalidArgumentException
{
    /**
     * @param string $projectName
     *
     * @return self
     */
    public static function withProjectName(string $projectName)
    {
        return new self(sprintf('Project with name "%s" already exists.', $projectName));
    }
}
