<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Project\Handler;

use Orangear\BusinessIntelligence\Domain\Model\Admin\AdminCollectionInterface;
use Orangear\BusinessIntelligence\Domain\Model\Admin\AdminInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\Exception\PlatformNotFound;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformCollectionInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\Command\CreateProjectCommandInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectCollectionInterface;
use Orangear\BusinessIntelligence\src\Domain\Model\Admin\Exception\AdminNotFound;
use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberInterface;

/**
 * Class CreateProjectHandler
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Project\Handler
 */
final class CreateProjectHandler
{
    /** @var ProjectCollectionInterface */
    private $projectCollection;

    /** @var PlatformCollectionInterface */
    private $platformCollection;

    /** @var AdminCollectionInterface */
    private $adminCollection;

    /**
     * CreateProjectHandler constructor
     *
     * @param ProjectCollectionInterface $projectCollection
     * @param PlatformCollectionInterface $platformCollection
     * @param AdminCollectionInterface $adminCollection
     */
    public function __construct(ProjectCollectionInterface $projectCollection, PlatformCollectionInterface $platformCollection, AdminCollectionInterface $adminCollection)
    {
        $this->projectCollection = $projectCollection;
        $this->platformCollection = $platformCollection;
        $this->adminCollection = $adminCollection;
    }

    /**
     * @param CreateProjectCommandInterface $command
     */
    public function __invoke(CreateProjectCommandInterface $command)
    {
        /** @var AdminInterface $member */
        $member = $this->findAdminOrFail($command->memberId());

        $platform = $this->findPlatformOrFail($command->platformId());

        $project = $member->createProject(
            $command->projectId(),
            $platform,
            $command->projectName(),
            $command->projectToken(),
            $command->projectLogo(),
            null,
            $command->projectApiUrl()
        );

        $this->projectCollection->add($project);
    }

    /**
     * @param IdentifierInterface $identifier
     *
     * @return MemberInterface
     */
    private function findAdminOrFail(IdentifierInterface $identifier) : MemberInterface
    {
        $member = $this->adminCollection->get($identifier);

        if ($member === null || !$member instanceof AdminInterface) {
            throw AdminNotFound::withMemberId($identifier);
        }

        return $member;
    }

    /**
     * @param PlatformIdentifierInterface $platformId
     *
     * @return PlatformInterface
     */
    private function findPlatformOrFail(PlatformIdentifierInterface $platformId) : PlatformInterface
    {
        $platform = $this->platformCollection->get($platformId);

        if ($platform === null) {
            throw PlatformNotFound::withPlatformId($platformId);
        }

        return $platform;
    }
}
