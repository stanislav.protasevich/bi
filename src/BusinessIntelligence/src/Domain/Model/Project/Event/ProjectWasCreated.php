<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Project\Event;

use Orangear\BusinessIntelligence\Domain\Model\EventInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;

/**
 * Class ProjectWasCreated
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Project\Event
 */
final class ProjectWasCreated implements EventInterface
{
    /** @var IdentifierInterface */
    private $memberId;

    /** @var ProjectIdentifierInterface */
    private $projectId;

    /** @var PlatformInterface */
    private $platform;

    /** @var string */
    private $platformName;

    /** @var string */
    private $platformToken;

    /** @var string */
    private $platformLogo;

    /** @var int */
    private $platformOrder;

    /** @var string */
    private $platformApiUrl;

    /**
     * ProjectWasCreated constructor
     *
     * @param IdentifierInterface $memberId
     * @param ProjectIdentifierInterface $projectId
     * @param PlatformInterface $platform
     * @param string $platformName
     * @param string $platformToken
     * @param string $platformLogo
     * @param int $platformOrder
     * @param string $platformApiUrl
     */
    private function __construct(IdentifierInterface $memberId, ProjectIdentifierInterface $projectId, PlatformInterface $platform, $platformName, $platformToken, $platformLogo, $platformOrder = null, $platformApiUrl)
    {
        $this->memberId = $memberId;
        $this->projectId = $projectId;
        $this->platform = $platform;
        $this->platformName = $platformName;
        $this->platformToken = $platformToken;
        $this->platformLogo = $platformLogo;
        $this->platformOrder = $platformOrder;
        $this->platformApiUrl = $platformApiUrl;
    }

    /**
     * @param IdentifierInterface $memberId
     * @param ProjectIdentifierInterface $projectId
     * @param PlatformInterface $platform
     * @param $platformName
     * @param $platformToken
     * @param $platformLogo
     * @param $platformOrder
     * @param $platformApiUrl
     *
     * @return ProjectWasCreated
     */
    public static function byAdmin(IdentifierInterface $memberId, ProjectIdentifierInterface $projectId, PlatformInterface $platform, $platformName, $platformToken, $platformLogo, $platformOrder = null, $platformApiUrl)
    {
        return new self($memberId, $projectId, $platform, $platformName, $platformToken, $platformLogo, $platformOrder, $platformApiUrl);
    }

    /**
     * @return IdentifierInterface
     */
    public function memberId(): IdentifierInterface
    {
        return $this->memberId;
    }

    /**
     * @return ProjectIdentifierInterface
     */
    public function projectId(): ProjectIdentifierInterface
    {
        return $this->projectId;
    }

    /**
     * @return PlatformInterface
     */
    public function platform(): PlatformInterface
    {
        return $this->platform;
    }

    /**
     * @return string
     */
    public function projectName(): string
    {
        return $this->platformName;
    }

    /**
     * @return string
     */
    public function projectToken(): string
    {
        return $this->platformToken;
    }

    /**
     * @return string
     */
    public function projectLogo(): string
    {
        return $this->platformLogo;
    }

    /**
     * @return int
     */
    public function projectOrder() :? int
    {
        return $this->platformOrder;
    }

    /**
     * @return string
     */
    public function projectApiUrl(): string
    {
        return $this->platformApiUrl;
    }
}
