<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Project;

use Assert\Assertion;
use Exception;
use Orangear\BusinessIntelligence\Domain\Model\AggregateRootTrait;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\Event\ProjectWasCreated;
use Orangear\BusinessIntelligence\Domain\Model\Project\Exception\InvalidProject;
use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;

/**
 * Class Project
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Project
 */
class Project implements ProjectInterface
{
    use AggregateRootTrait;

    /** @var ProjectIdentifierInterface */
    private $id;

    /** @var PlatformInterface */
    private $platform;

    /** @var string */
    private $name;

    /** @var string */
    private $token;

    /** @var string */
    private $logo;

    /** @var int */
    private $order;

    /** @var string */
    private $apiUrl;

    private $project;

    /**
     * Platform constructor
     *
     * @param ProjectIdentifierInterface $id
     * @param PlatformInterface $platform
     * @param string $name
     * @param string $token
     * @param string $logo
     * @param int $order
     * @param string $apiUrl
     */
    public function __construct(ProjectIdentifierInterface $id, PlatformInterface $platform, $name, $token, $logo, $order = null, $apiUrl)
    {
        $this->setId($id);
        $this->setPlatform($platform);
        $this->setName($name);
        $this->setToken($token);
        $this->setLogo($logo);
        $this->setOrder($order);
        $this->setApiUrl($apiUrl);
    }

    /**
     * {@inheritdoc}
     */
    public static function withData(ProjectIdentifierInterface $id, PlatformInterface $platform, $name, $token, $logo, $order = null, $apiUrl) : ProjectInterface
    {
        return new self($id, $platform, $name, $token, $logo, $order, $apiUrl);
    }

    /**
     * {@inheritdoc}
     */
    public static function create(IdentifierInterface $memberId, ProjectIdentifierInterface $id, PlatformInterface $platform, $name, $token, $logo, $order = null, $apiUrl) : ProjectInterface
    {
        $project = self::withData($id, $platform, $name, $token, $logo, $order, $apiUrl);

        $project->record(ProjectWasCreated::byAdmin($memberId, $id, $platform, $name, $token, $logo, $order, $apiUrl));

        return $project;
    }

    /**
     * {@inheritdoc}
     */
    public function projectId() : ProjectIdentifierInterface
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function platform() : PlatformInterface
    {
        return $this->platform;
    }

    /**
     * {@inheritdoc}
     */
    public function name() : string
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function token() : string
    {
        return $this->token;
    }

    /**
     * {@inheritdoc}
     */
    public function logo() : string
    {
        return $this->logo;
    }

    /**
     * {@inheritdoc}
     */
    public function order() : int
    {
        return $this->order;
    }

    /**
     * {@inheritdoc}
     */
    public function apiUrl() : string
    {
        return $this->apiUrl;
    }

    /**
     * @param ProjectIdentifierInterface$id
     */
    private function setId(ProjectIdentifierInterface $id)
    {
        try {
            Assertion::notNull($id);
            Assertion::implementsInterface($id, ProjectIdentifierInterface::class);
        } catch (Exception $e) {
            throw InvalidProject::withProjectId($id);
        }

        $this->id = $id;
    }

    /**
     * @param PlatformInterface $platform
     */
    private function setPlatform(PlatformInterface $platform = null)
    {
        try {
            Assertion::notNull($platform);
            Assertion::implementsInterface($platform, PlatformInterface::class);
        } catch (Exception $e) {
            throw InvalidProject::withPlatformId($platform->platformId());
        }

        $this->platform = $platform;
    }

    /**
     * @param string $name
     */
    private function setName(string $name = null)
    {
        try {
            Assertion::notNull($name);
            Assertion::string($name);
        } catch (Exception $e) {
            throw InvalidProject::withProjectName($name);
        }

        $this->name = $name;
    }

    /**
     * @param string $token
     */
    private function setToken(string $token = null)
    {
        try {
            Assertion::notNull($token);
            Assertion::string($token);
        } catch (Exception $e) {
            throw InvalidProject::withProjectToken($token);
        }

        $this->token = $token;
    }

    /**
     * @param string $logo
     */
    private function setLogo(string $logo = null)
    {
        try {
            Assertion::notNull($logo);
            Assertion::string($logo);
        } catch (Exception $e) {
            throw InvalidProject::withProjectLogo($logo);
        }

        $this->logo = $logo;
    }

    /**
     * @param int $order
     */
    private function setOrder(int $order = null)
    {
        if ($order !== null) {
            try {
                Assertion::notNull($order);
                Assertion::integer($order);
            } catch (Exception $e) {
                throw InvalidProject::withProjectOrder($order);
            }
        }

        $this->order = $order;
    }

    /**
     * @param string $apiUrl
     */
    private function setApiUrl(string $apiUrl = null)
    {
        try {
            Assertion::notNull($apiUrl);
            Assertion::string($apiUrl);
            Assertion::url($apiUrl);
        } catch (Exception $e) {
            throw InvalidProject::withProjectApiUrl($apiUrl);
        }

        $this->apiUrl = $apiUrl;
    }
}
