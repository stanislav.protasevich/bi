<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Project;

use Ramsey\Uuid\Uuid;

/**
 * Class ProjectId
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Project
 */
final class ProjectId implements ProjectIdentifierInterface
{
    /** @var string */
    private $projectId;

    /**
     * PlatformId constructor
     *
     * @param null $projectId
     */
    private function __construct($projectId = null)
    {
        $this->projectId = $projectId;
    }

    /**
     * {@inheritdoc}
     */
    public static function fromString($projectId = null) : self
    {
        return new static($projectId);
    }

    /**
     * {@inheritdoc}
     */
    public function toString() : string
    {
        return $this->projectId;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->projectId;
    }

    /**
     * @return bool
     */
    public function isNull() : bool
    {
        return $this->projectId === null ? true : false;
    }
}