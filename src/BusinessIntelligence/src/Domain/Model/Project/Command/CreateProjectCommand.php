<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Project\Command;

use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformId;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberId;
use Orangear\Common\Identifier\IdentifierInterface as CommonIdentifierInterface;

/**
 * Class CreateProjectCommand
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Project\Command
 */
final class CreateProjectCommand implements CreateProjectCommandInterface
{
    /** @var IdentifierInterface */
    private $memberId;

    /** @var ProjectIdentifierInterface */
    private $projectId;

    /** @var PlatformIdentifierInterface */
    private $platformId;

    /** @var string */
    private $projectName;

    /** @var string */
    private $projectLogo;

    /** @var string */
    private $projectApiUrl;

    /**
     * CreateProjectCommand constructor
     *
     * @param string $memberId
     * @param string $platformId
     * @param string $projectName
     * @param string $projectLogo
     * @param string $projectApiUrl
     */
    public function __construct($memberId, $platformId, $projectName, $projectLogo, $projectApiUrl)
    {
        $this->memberId = $memberId;
        $this->platformId = $platformId;
        $this->projectName = $projectName;
        $this->projectLogo = $projectLogo;
        $this->projectApiUrl = $projectApiUrl;
    }

    /**
     * @param string $memberId
     * @param string $platformId
     * @param string $projectName
     * @param string $projectLogo
     * @param string $projectApiUrl
     *
     * @return CreateProjectCommand
     */
    public static function forAdmin($memberId, $platformId, $projectName, $projectLogo, $projectApiUrl)
    {
        return new self($memberId, $platformId, $projectName, $projectLogo, $projectApiUrl);
    }

    /**
     * {@inheritdoc}
     */
    public function memberId() : IdentifierInterface
    {
        return MemberId::fromString($this->memberId);
    }

    /**
     * {@inheritdoc}
     */
    public function projectId() : ProjectIdentifierInterface
    {
        return ProjectId::fromString($this->projectId);
    }

    /**
     * {@inheritdoc}
     */
    public function platformId(): CommonIdentifierInterface
    {
        return PlatformId::fromString($this->platformId);
    }

    /**
     * {@inheritdoc}
     */
    public function projectName(): string
    {
        return (string) $this->projectName;
    }

    /**
     * {@inheritdoc}
     */
    public function projectToken(): string
    {
        return (string) md5((string) time());
    }

    /**
     * {@inheritdoc}
     */
    public function projectLogo(): string
    {
        return (string) $this->projectLogo;
    }

    /**
     * {@inheritdoc}
     */
    public function projectApiUrl(): string
    {
        return (string) $this->projectApiUrl;
    }
}
