<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Project\Command;

use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;
use Orangear\Common\Identifier\IdentifierInterface as CommonIdentifierInterface;

/**
 * Interface CreateProjectCommandInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Project\Command
 */
interface CreateProjectCommandInterface
{
    /**
     * @return IdentifierInterface
     */
    public function memberId() : IdentifierInterface;

    /**
     * @return ProjectIdentifierInterface
     */
    public function projectId() : ProjectIdentifierInterface;

    /**
     * @return CommonIdentifierInterface
     */
    public function platformId() : CommonIdentifierInterface;

    /**
     * @return string
     */
    public function projectName() : string;

    /**
     * @return string
     */
    public function projectToken() : string;

    /**
     * @return string
     */
    public function projectLogo() : string;

    /**
     * @return string
     */
    public function projectApiUrl() : string;
}
