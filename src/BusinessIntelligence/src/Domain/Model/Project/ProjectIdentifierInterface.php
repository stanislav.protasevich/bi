<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Project;

use Orangear\BusinessIntelligence\Domain\Model\IdentifierInterface;

/**
 * Interface ProjectIdentifierInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Project
 */
interface ProjectIdentifierInterface extends IdentifierInterface
{
    public function isNull() : bool;
}
