<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Project;

use Prooph\EventStore\Exception\ProjectionNotFound;

/**
 * Interface PlatformRepositoryInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Project
 */
interface ProjectRepositoryInterface
{
    /**
     * @param ProjectIdentifierInterface $identifier
     *
     * @return ProjectInterface
     */
    public function findById(ProjectIdentifierInterface $identifier) :? ProjectInterface;

    /**
     * @param string $token
     *
     * @return ProjectInterface
     */
    public function findByToken(string $token) :? ProjectInterface;

    /**
     * @param string $name
     *
     * @return array
     */
    public function findByName(string $name) : array;

    /**
     * @param ProjectInterface $project
     */
    public function create(ProjectInterface $project);

    /**
     * @param string $token
     *
     * @throws ProjectionNotFound
     *
     * @return ProjectInterface
     */
    public function findByTokenOrFail(string $token) : ProjectInterface;


    /**
     * @param ProjectIdentifierInterface $identifier
     *
     * @throws ProjectionNotFound
     *
     * @return ProjectInterface
     */
    public function findByIdOrFail(ProjectIdentifierInterface $identifier) :? ProjectInterface;
}
