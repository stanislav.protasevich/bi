<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Booker;

use Orangear\MembershipBundle\Domain\Model\Member\MemberInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberRepositoryInterface;

/**
 * Interface BookerRepositoryInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\Booker
 */
interface BookerRepositoryInterface extends MemberRepositoryInterface
{
    /**
     * @param array $conditions
     * @return MemberInterface
     */
    public function findOrFail(array $conditions = []): MemberInterface;
}
