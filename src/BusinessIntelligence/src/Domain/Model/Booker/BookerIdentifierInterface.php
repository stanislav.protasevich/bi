<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Booker;

use Orangear\BusinessIntelligence\Domain\Model\IdentifierInterface;

/**
 * Interface BookerIdentifierInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\Booker
 */
interface BookerIdentifierInterface extends IdentifierInterface {}
