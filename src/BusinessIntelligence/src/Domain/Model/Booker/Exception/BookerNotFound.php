<?php

declare(strict_types=1);

namespace Orangear\BusinessIntelligence\Domain\Model\Booker\Exception;

use Orangear\BusinessIntelligence\Domain\Model\IdentifierInterface;

/**
 * Class BookerNotFound
 * @package Orangear\BusinessIntelligence\Domain\Model\Booker\Exception
 */
class BookerNotFound extends \InvalidArgumentException
{
    /**
     * @param IdentifierInterface $bookerId
     *
     * @return BookerNotFound
     */
    public static function withId(IdentifierInterface $bookerId)
    {
        return new self(sprintf('Booker with id %s cannot be found.', (string) $bookerId));
    }
}
