<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Booker;

use Orangear\MembershipBundle\Domain\Model\Member\MemberInterface;

/**
 * Interface BookerInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\Booker
 */
interface BookerInterface extends MemberInterface {}
