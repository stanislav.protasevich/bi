<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\FinancialData\Query;

use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\IdentifierInterface as ProjectIdentifierInterface;
use DateTime;

/**
 * Interface GetFinancialDataQueryInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\FinancialData\Query
 */
interface GetFinancialDataQueryInterface
{
    /**
     * @return IdentifierInterface
     */
    public function memberId(): IdentifierInterface;

    /**
     * @return null|ProjectIdentifierInterface
     */
    public function projectId(): ?ProjectIdentifierInterface;

    /**
     * @return DateTime
     */
    public function startDate(): DateTime;

    /**
     * @return DateTime
     */
    public function endDate(): DateTime;
}
