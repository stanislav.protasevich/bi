<?php

declare(strict_types=1);

namespace Orangear\BusinessIntelligence\Domain\Model;

use Orangear\BusinessIntelligence\Domain\Model\Dashboard\DashboardInterface;

/**
 * Class RepositoryInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model
 */
interface RepositoryInterface
{
    /**
     * @param IdentifierInterface $identifier
     */
    public function findById(IdentifierInterface $identifier);

    /**
     * @param array $parameters
     * @return null|DashboardInterface
     */
    public function findOneBy(array $parameters): ?DashboardInterface;
}
