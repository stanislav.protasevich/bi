<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model;

/**
 * Interface CommandInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model
 */
interface CommandInterface {}
