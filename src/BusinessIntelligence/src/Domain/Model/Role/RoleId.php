<?php
declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Role;

use Ramsey\Uuid\Uuid;

/**
 * Class RoleId
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Role
 */
final class RoleId
{
    /** @var string */
    private $roleId;

    /**
     * PlatformId constructor
     *
     * @param null $roleId
     */
    private function __construct($roleId = null)
    {
        $this->roleId = $roleId ?: Uuid::uuid4()->toString();
    }

    /**
     * {@inheritdoc}
     */
    public static function fromString($userId = null) : self
    {
        return new static($userId);
    }

    /**
     * {@inheritdoc}
     */
    public function toString() : string
    {
        return $this->roleId;
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->roleId;
    }
}
