<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\BillingReport\Query;

use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;
use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberId;
use Orangear\BusinessIntelligence\Domain\Model\IdentifierInterface as ProjectIdentifierInterface;
use DateTime;

/**
 * Class GetBillingReportQuery
 * @package Orangear\BusinessIntelligence\Domain\Model\BillingReport\Query
 */
final class GetBillingReportQuery implements GetBillingReportQueryInterface
{
    /** @var string */
    private $memberId;

    /** @var string */
    private $projectId;

    /** @var string */
    private $startDate;

    /** @var string */
    private $endDate;

    /**
     * GetDashboardQuery constructor.
     * @param string $memberId
     * @param null|string $projectId
     * @param null|string $startDate
     * @param null|string $endDate
     */
    private function __construct(string $memberId, ?string $projectId, ?string $startDate, ?string $endDate)
    {
        $this->memberId = $memberId;
        $this->projectId = $projectId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @param string $headId
     * @param null|string $projectId
     * @param null|string $startDate
     * @param null|string $endDate
     * @return GetBillingReportQuery
     */
    public static function forHead(string $headId, ?string $projectId, ?string $startDate, ?string $endDate)
    {
        return new self($headId, $projectId, $startDate, $endDate);
    }

    /**
     * @inheritdoc
     */
    public function memberId(): IdentifierInterface
    {
        return MemberId::fromString($this->memberId);
    }

    /**
     * @inheritdoc
     */
    public function projectId(): ?ProjectIdentifierInterface
    {
        if ($this->projectId !== null) {
            return ProjectId::fromString($this->projectId);
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function startDate(): ?DateTime
    {
        if ($this->startDate !== null) {
            return new DateTime($this->startDate);
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function endDate(): ?DateTime
    {
        if ($this->endDate !== null) {
            return new DateTime($this->endDate);
        }

        return null;
    }
}
