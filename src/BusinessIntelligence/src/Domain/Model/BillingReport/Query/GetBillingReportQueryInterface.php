<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\BillingReport\Query;

use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\IdentifierInterface as ProjectIdentifierInterface;
use DateTime;

/**
 * Interface GetBillingReportQueryInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\Dashboard\Query
 */
interface GetBillingReportQueryInterface
{
    /**
     * @return IdentifierInterface
     */
    public function memberId(): IdentifierInterface;

    /**
     * @return null|ProjectIdentifierInterface
     */
    public function projectId(): ?ProjectIdentifierInterface;

    /**
     * @return null|DateTime
     */
    public function startDate(): ?DateTime;

    /**
     * @return null|DateTime
     */
    public function endDate(): ?DateTime;
}
