<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ChargeType\Command;

use Orangear\MembershipBundle\Domain\Model\IdentifierInterface as MemberIdentifierInterface;

interface CreateChargeTypeCommandInterface
{
    /**
     * @return MemberIdentifierInterface
     */
    public function memberId(): MemberIdentifierInterface;

    /**
     * @return string
     */
    public function name(): string;

    /**
     * @return string
     */
    public function key(): string;

    /**
     * @return string
     */
    public function description(): string;
}
