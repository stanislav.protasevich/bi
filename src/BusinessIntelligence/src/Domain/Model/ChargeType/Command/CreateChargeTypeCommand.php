<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ChargeType\Command;

use Orangear\MembershipBundle\Domain\Model\Member\MemberId;
use Orangear\MembershipBundle\Domain\Model\IdentifierInterface as MemberIdentifierInterface;

/**
 * Class CreateChargeTypeCommand
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ChargeType\Command
 */
final class CreateChargeTypeCommand implements CreateChargeTypeCommandInterface
{
    /** @var string */
    private $memberId;

    /** @var string */
    private $name;

    /** @var string */
    private $description;

    /**
     * CreateChargeTypeCommand constructor
     *
     * @param string $memberId
     * @param string $name
     * @param string $description
     */
    private function __construct(string $memberId, string $name, string $description = null)
    {
        $this->memberId = $memberId;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @param string $memberId
     * @param string $name
     * @param string $description
     *
     * @return CreateChargeTypeCommand
     */
    public static function forMember(string $memberId, string $name, string $description = null)
    {
        return new self($memberId, $name, $description);
    }

    /**
     * {@inheritdoc}
     */
    public function memberId(): MemberIdentifierInterface
    {
        return MemberId::fromString($this->memberId);
    }

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return (string) $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function key(): string
    {
        return (string) str_replace(' ', '_', strtolower($this->name));
    }

    /**
     * {@inheritdoc}
     */
    public function description(): string
    {
        return (string) $this->description;
    }
}
