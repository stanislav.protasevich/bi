<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ChargeType;

/**
 * Interface ChargeTypeMapperInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ChargeType
 */
interface ChargeTypeRepositoryInterface
{
    /**
     * @param ChargeTypeIdentifierInterface $chargeTypeIdentifier
     *
     * @return null|ChargeTypeInterface
     */
    public function findById(ChargeTypeIdentifierInterface $chargeTypeIdentifier) :? ChargeTypeInterface;

    /**
     * @param string $name
     * @return null|ChargeTypeInterface
     */
    public function findByName(string $name) :? ChargeTypeInterface;

    /**
     * @param array $conditions
     * @return mixed
     */
    public function findOneBy(array $conditions = []);

    /**
     * @param array $conditions
     * @return mixed
     */
    public function findAll(array $conditions = []);

    /**
     * @param ChargeTypeInterface $chargeType
     */
    public function create(ChargeTypeInterface $chargeType);

    /**
     * @param ChargeTypeInterface $chargeType
     */
    public function update(ChargeTypeInterface $chargeType);

    /**
     * @param ChargeTypeInterface $chargeType
     */
    public function delete(ChargeTypeInterface $chargeType);

    /**
     * @return ChargeTypeIdentifierInterface
     */
    public function nextIdentifier() : ChargeTypeIdentifierInterface;
}
