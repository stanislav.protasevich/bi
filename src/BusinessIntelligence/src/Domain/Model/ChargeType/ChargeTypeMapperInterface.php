<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ChargeType;

/**
 * Interface ChargeTypeMapperInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\ChargeType
 */
interface ChargeTypeMapperInterface
{
    /**
     * @param ChargeTypeIdentifierInterface $chargeTypeIdentifier
     *
     * @return null|ChargeTypeInterface
     */
    public function fetchById(ChargeTypeIdentifierInterface $chargeTypeIdentifier) :? ChargeTypeInterface;

    /**
     * @param string $name
     * @return null|ChargeTypeInterface
     */
    public function fetchByName(string $name) :? ChargeTypeInterface;

    /**
     * @param array $conditions
     * @return array|ChargeTypeInterface
     */
    public function fetchOneBy($conditions = []):? ChargeTypeInterface;

    /**
     * @param array $conditions
     * @return mixed
     */
    public function fetchAll(array $conditions = []);

    /**
     * @param ChargeTypeInterface $chargeType
     */
    public function create(ChargeTypeInterface $chargeType);

    /**
     * @param ChargeTypeInterface $chargeType
     */
    public function update(ChargeTypeInterface $chargeType);

    /**
     * @param ChargeTypeInterface $chargeType
     */
    public function delete(ChargeTypeInterface $chargeType);
}
