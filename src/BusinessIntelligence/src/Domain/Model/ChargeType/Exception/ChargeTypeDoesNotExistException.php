<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ChargeType\Exception;

/**
 * Class ChargeTypeDoesNotExistException
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Employee\Service\Exception
 */
final class ChargeTypeDoesNotExistException extends \InvalidArgumentException {}
