<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ChargeType\Exception;

/**
 * Class ChargeTypeAlreadyExistsException
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\Service\Exception
 */
final class ChargeTypeAlreadyExistsException extends \InvalidArgumentException {}
