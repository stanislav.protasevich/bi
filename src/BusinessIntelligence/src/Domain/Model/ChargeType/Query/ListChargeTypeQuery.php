<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ChargeType\Query;

use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberId;

/**
 * Class ListChargeTypeQuery
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ChargeType\Query
 */
final class ListChargeTypeQuery implements ListChargeTypeQueryInterface
{
    /** @var string */
    private $memberId;

    /**
     * ListChargeTypeQuery constructor
     *
     * @param string $memberId
     */
    private function __construct(string $memberId)
    {
        $this->memberId = $memberId;
    }

    /**
     * {@inheritdoc}
     */
    public function memberId(): IdentifierInterface
    {
        return MemberId::fromString($this->memberId);
    }

    /**
     * {@inheritdoc}
     */
    public static function forMember(string $memberId)
    {
        return new self($memberId);
    }
}
