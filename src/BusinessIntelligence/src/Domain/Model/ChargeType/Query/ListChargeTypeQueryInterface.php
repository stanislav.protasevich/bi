<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ChargeType\Query;

use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;

/**
 * Interface ListChargeTypeQuery
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ChargeType\Query
 */
interface ListChargeTypeQueryInterface
{
    /**
     * @return IdentifierInterface
     */
    public function memberId(): IdentifierInterface;

    /**
     * @param string $memberId
     */
    public static function forMember(string $memberId);
}
