<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ChargeType\Handler;

use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeType;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeInterface;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\Command\CreateChargeTypeCommandInterface;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\Exception\ChargeTypeAlreadyExistsException;

/**
 * Class CreateChargeTypeCommandHandler
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ChargeType\Handler
 */
final class CreateChargeTypeCommandHandler
{
    /** @var ChargeTypeInterface */
    private $chargeTypeRepository;

    /**
     * CreateChargeTypeCommandHandler constructor
     *
     * @param ChargeTypeRepositoryInterface $chargeTypeRepository
     */
    public function __construct(ChargeTypeRepositoryInterface $chargeTypeRepository)
    {
        $this->chargeTypeRepository = $chargeTypeRepository;
    }

    /**
     * @param CreateChargeTypeCommandInterface $command
     */
    public function __invoke(CreateChargeTypeCommandInterface $command)
    {
        //@todo make by owner group

        if ($this->chargeTypeRepository->findByName($command->name())) {
            throw new ChargeTypeAlreadyExistsException('Charge type already exists');
        }

        $chargeType = ChargeType::create(
            $this->chargeTypeRepository->nextIdentifier(),
            $command->name(),
            $command->key(),
            $command->description()
        );

        $this->chargeTypeRepository->create($chargeType);
    }
}
