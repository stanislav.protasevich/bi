<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ChargeType;

/**
 * Interface ChargeTypeIdentifierInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ChargeType
 */
interface ChargeTypeIdentifierInterface {}
