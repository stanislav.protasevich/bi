<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ChargeType;

/**
 * Interface ChargeTypeInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ChargeType
 */
interface ChargeTypeInterface
{
    /**
     * @return ChargeTypeIdentifierInterface
     */
    public function id(): ChargeTypeIdentifierInterface;

    /**
     * @return string
     */
    public function key(): string;

    /**
     * @return string
     */
    public function name(): string;

    /**
     * @return null|string
     */
    public function description():? string;
}