<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\ChargeType;

/**
 * Class ChargeType
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ChargeType
 */
class ChargeType implements ChargeTypeInterface
{
    /** @var ChargeTypeIdentifierInterface */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $key;

    /** @var string */
    private $description;

    /**
     * ChargeType constructor
     *
     * @param ChargeTypeIdentifierInterface $id
     * @param string $name
     * @param string $key
     * @param string $description
     */
    private function __construct(ChargeTypeIdentifierInterface $id, string $name, string $key, string $description = null)
    {
        $this->id = $id;
        $this->key = $key;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @param ChargeTypeIdentifierInterface $id
     * @param string $name
     * @param string $key
     * @param string|null $description
     *
     * @return ChargeType
     */
    public static function create(ChargeTypeIdentifierInterface $id, string $name, string $key, string $description = null)
    {
        return new self($id, $name, $key, $description);
    }

    /**
     * {@inheritdoc}
     */
    public function id(): ChargeTypeIdentifierInterface
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function key(): string
    {
        return $this->key;
    }

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function description():? string
    {
        return $this->description;
    }
}
