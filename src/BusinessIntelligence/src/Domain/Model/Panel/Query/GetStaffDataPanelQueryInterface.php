<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Panel\Query;

use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;

/**
 * Interface GetStaffDataPanelQueryInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Panel\Query
 */
interface GetStaffDataPanelQueryInterface
{
    public function memberId(): IdentifierInterface;
}
