<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Panel\Query;

use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberId;

/**
 * Class GetStaffDataPanelQuery
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Panel\Query
 */
class GetStaffDataPanelQuery implements GetStaffDataPanelQueryInterface
{
    /** @var string */
    private $memberId;

    /**
     * GetDashboardPanelQuery constructor.
     * @param string $memberId
     */
    private function __construct(string $memberId)
    {
        $this->memberId = $memberId;
    }

    /**
     * @param string $headId
     * @return GetStaffDataPanelQuery
     */
    public static function forMember(string $headId)
    {
        return new self($headId);
    }

    /**
     * @param string $headId
     * @return GetStaffDataPanelQuery
     */
    public static function forHead(string $headId)
    {
        return new self($headId);
    }

    /**
     * @inheritdoc
     */
    public function memberId(): IdentifierInterface
    {
        return MemberId::fromString($this->memberId);
    }
}
