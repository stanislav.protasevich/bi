<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Application\Query;

/**
 * Interface FindCompaniesQueryInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\Application\Query
 */
interface FindCompaniesQueryInterface
{
    public function __invoke($token);
}