<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Application\Query;

/**
 * Interface FindTokenQueryInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\Application\Query
 */
interface FindTokenQueryInterface
{
    public function __invoke($token);
}