<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Application;

/**
 * Class Application
 * @package Orangear\BusinessIntelligence\Domain\Model\Application
 */
final class Application implements ApplicationInterface
{
    private $id;
    private $token;
    private $access = [];
    private $notes = '';

    /**
     * Application constructor.
     * @param int $id
     * @param string $token
     * @param string $access
     * @param string $notes
     */
    private function __construct(int $id, string $token, string $access, string $notes)
    {
        $this->id = $id;

        $this->token = $token;

        $this->access = json_decode( $access, true );

        $this->notes = $notes;
    }

    /**
     * @param int $id
     * @param string $token
     * @param string $access
     * @param string $notes
     * @return Application
     */
    public static function fromArray(int $id, string $token, string $access, string $notes)
    {
        return new self( $id, $token, $access, $notes );
    }

    /**
     * @return mixed
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function token(): string
    {
        return $this->token;
    }

    /**
     * @return array
     */
    public function access(): array
    {
        return $this->access;
    }

    /**
     * @return string
     */
    public function notes(): string
    {
        return $this->notes;
    }
}