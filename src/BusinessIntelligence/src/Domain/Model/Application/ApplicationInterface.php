<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Application;

/**
 * Interface ApplicationInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\Application
 */
interface ApplicationInterface
{
    /**
     * @return int
     */
    public function id(): int;

    /**
     * @return string
     */
    public function token(): string;

    /**
     * @return array
     */
    public function access(): array;

    /**
     * @return string
     */
    public function notes(): string;
}