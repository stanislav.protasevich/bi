<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Admin;

use Orangear\BusinessIntelligence\Domain\Model\Collection;

/**
 * Class AdminCollection
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Admin
 */
final class AdminCollection extends Collection implements AdminCollectionInterface
{
    /**
     * @param AdminIdentifierInterface $identifier
     *
     * @return null|AdminInterface
     */
    public function get($identifier) :? AdminInterface
    {
        return $this->repository->findById($identifier);
    }
}
