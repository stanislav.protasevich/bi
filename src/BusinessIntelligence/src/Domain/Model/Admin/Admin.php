<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Admin;

use DateTime;
use Orangear\BusinessIntelligence\Domain\Model\Platform\Platform;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\Member\Member;
use Orangear\MembershipBundle\Domain\Model\Member\MemberInterface;

/**
 * Class Admin
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Admin
 */
class Admin extends Member implements AdminInterface
{
    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return ['ROLE_ADMIN'];
    }

    /**
     * {@inheritdoc}
     */
    protected function __construct(IdentifierInterface $id = null, $email = null, $password = null, $salt = null, DateTime $createdAt = null, DateTime $updatedAt = null, DateTime $deletedAt = null)
    {
        parent::__construct($id, $email, $password, $salt, $createdAt, $updatedAt, $deletedAt);
    }

    /**
     * {@inheritdoc}
     */
    public static function fromArray(array $payload): MemberInterface
    {
        return new static(
            $payload['id'] ?? null,
            $payload['email'] ?? null,
            $payload['password'] ?? null,
            $payload['salt'] ?? null,
            $payload['created_at'] ?? null,
            $payload['updated_at'] ?? null,
            $payload['deleted_at'] ?? null
        );
    }

    /**
     * {@inheritdoc}
     */
    public function createPlatform(PlatformIdentifierInterface $platformIdentifier, string $name, string $logo): PlatformInterface
    {
        return Platform::create($this->id, $platformIdentifier, $name, $logo);
    }

    /**
     * {@inheritdoc}
     */
    public function createProject(ProjectIdentifierInterface $projectId, PlatformInterface $platform, string $platformName, string $platformToken, string $platformLogo, int $platformOrder = null, string $platformApiUrl)
    {
        return Project::create($this->id, $projectId, $platform,  $platformName, $platformToken, $platformLogo, $platformOrder, $platformApiUrl);
    }
}
