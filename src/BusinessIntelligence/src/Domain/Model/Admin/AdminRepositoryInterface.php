<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Admin;

use Orangear\BusinessIntelligence\src\Domain\Model\Admin\Exception\AdminNotFound;
use Orangear\MembershipBundle\Domain\Model\Member\MemberIdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberRepositoryInterface;

/**
 * Interface AdminRepositoryInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Admin
 */
interface AdminRepositoryInterface extends MemberRepositoryInterface
{
    /**
     * @param MemberIdentifierInterface $identifier
     *
     * @throws AdminNotFound
     *
     * @return AdminInterface
     */
    public function findAdminByIdOrFail(MemberIdentifierInterface $identifier) : AdminInterface;
}