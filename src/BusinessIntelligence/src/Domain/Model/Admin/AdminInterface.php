<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Admin;

use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberInterface;

/**
 * Interface AdminInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Admin
 */
interface AdminInterface extends MemberInterface
{
    /**
     * Create platform
     *
     * @param PlatformIdentifierInterface $platformIdentifier
     * @param string $name
     * @param string $logo
     *
     * @return PlatformInterface
     */
    public function createPlatform(PlatformIdentifierInterface $platformIdentifier, string $name, string $logo) : PlatformInterface;

    /**
     * Create project
     *
     * @param ProjectIdentifierInterface $projectId
     * @param PlatformInterface $platform
     * @param string $platformName
     * @param string $platformToken
     * @param string $platformLogo
     * @param int $platformOrder
     * @param string $platformApiUrl
     *
     * @return ProjectInterface
     */
    public function createProject(ProjectIdentifierInterface $projectId, PlatformInterface $platform, string $platformName, string $platformToken, string $platformLogo, int $platformOrder = null, string $platformApiUrl);
}
