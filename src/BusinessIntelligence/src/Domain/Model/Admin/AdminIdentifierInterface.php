<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Admin;

use Orangear\BusinessIntelligence\Domain\Model\IdentifierInterface;

/**
 * Interface AdminIdentifierInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Admin
 */
interface AdminIdentifierInterface extends IdentifierInterface {}
