<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\src\Domain\Model\Admin\Exception;

use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;

/**
 * Class AdminNotFound
 *
 * @package Orangear\BusinessIntelligence\src\Domain\Model\Admin\Exception
 */
class AdminNotFound extends \InvalidArgumentException
{
    /**
     * @param IdentifierInterface $memberId
     *
     * @return AdminNotFound
     */
    public static function withMemberId(IdentifierInterface $memberId)
    {
        return new self(sprintf('Admin with id %s cannot be found.', $memberId->toString()));
    }
}
