<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Admin;

/**
 * Interface AdminCollectionInterface
 */
interface AdminCollectionInterface
{
    /**
     * @param Admin $admin
     */
    public function add($admin);

    /**
     * @param $adminIdentifier
     *
     * @return null|AdminInterface
     */
    public function get($adminIdentifier) :? AdminInterface;
}
