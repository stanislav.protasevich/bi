<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\StaffData\Query;

use DateTime;

/**
 * Class ManagerKpiChartQuery
 * @package Orangear\BusinessIntelligence\Domain\Model\StaffData\Query
 */
final class ManagerKpiChartQuery implements ManagerKpiChartQueryInterface
{
    private $startDate;

    private $endDate;

    private $direction;

    const DIRECTION_BEST  = 'best';
    const DIRECTION_WORST = 'worst';

    /**
     * ManagerKpiChartQuery constructor.
     * @param $startDate
     * @param $endDate
     * @param $direction
     */
    private function __construct($startDate, $endDate, $direction)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->setDirection($direction);
    }

    /**
     * @param null $adminIdentifier
     * @param $startDate
     * @param $endDate
     * @param $direction
     * @return self
     */
    public static function forAdmin($adminIdentifier = null, $startDate, $endDate, $direction) : self
    {
        return new self($startDate, $endDate, $direction);
    }

    /**
     * {@inheritdoc}
     */
    public function startDate() : DateTime
    {
        return new DateTime($this->startDate);
    }

    /**
     * {@inheritdoc}
     */
    public function endDate() : DateTime
    {
        return new DateTime($this->endDate);
    }

    /**
     * {@inheritdoc}
     */
    public function direction() : string
    {
        return $this->direction;
    }

    /**
     * @param $direction
     */
    private function setDirection($direction)
    {
        if (!in_array($direction, [self::DIRECTION_BEST, self::DIRECTION_WORST])) {
            throw new \InvalidArgumentException('Invalid direction');
        }

        $this->direction = $direction;
    }
}
