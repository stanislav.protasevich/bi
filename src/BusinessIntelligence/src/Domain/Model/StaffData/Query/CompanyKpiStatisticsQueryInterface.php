<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\StaffData\Query;

use DateTime;

/**
 * Interface CompanyKpiStatisticsQueryInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\StaffData\Query
 */
interface CompanyKpiStatisticsQueryInterface
{
    /**
     * @return DateTime
     */
    public function startDate(): DateTime;

    /**
     * @return DateTime
     */
    public function endDate(): DateTime;
}
