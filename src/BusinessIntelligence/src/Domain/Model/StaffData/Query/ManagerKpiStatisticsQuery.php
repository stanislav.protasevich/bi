<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\StaffData\Query;

use DateTime;

/**
 * Class ManagerKpiStatisticsQuery
 * @package Orangear\BusinessIntelligence\Domain\Model\StaffData\Query
 */
final class ManagerKpiStatisticsQuery implements ManagerKpiStatisticsQueryInterface
{
    private $startDate;

    private $endDate;

    /**
     * ManagerKpiStatisticsQuery constructor.
     * @param $startDate
     * @param $endDate
     */
    private function __construct($startDate, $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @param null $adminId
     * @param $startDate
     * @param $endDate
     * @return self
     */
    public static function forAdmin($adminId = null, $startDate, $endDate) : self
    {
        return new self($startDate, $endDate);
    }

    /**
     * {@inheritdoc}
     */
    public function startDate() : DateTime
    {
        return new DateTime($this->startDate);
    }

    /**
     * {@inheritdoc}
     */
    public function endDate() : DateTime
    {
        return new DateTime($this->endDate);
    }
}
