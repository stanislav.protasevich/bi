<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\StaffData\Query;

use DateTime;

/**
 * Interface  ManagerKpiChartQueryInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\StaffData\Query
 */
interface ManagerKpiChartQueryInterface
{
    /**
     * @return DateTime
     */
    public function startDate() : DateTime;

    /**
     * @return DateTime
     */
    public function endDate() : DateTime;

    /**
     * @return string
     */
    public function direction() : string;
}
