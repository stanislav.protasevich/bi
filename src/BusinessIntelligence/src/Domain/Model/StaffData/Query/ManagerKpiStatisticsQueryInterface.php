<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\StaffData\Query;

use DateTime;

/**
 * Interface ManagerKpiStatisticsQueryInterface
 * @package Orangear\BusinessIntelligence\Domain\Model\StaffData\Query
 */
interface ManagerKpiStatisticsQueryInterface
{
    /**
     * @return DateTime
     */
    public function startDate(): DateTime;

    /**
     * @return DateTime
     */
    public function endDate(): DateTime;
}
