<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Charge;

/**
 * Class ChargeFrequency
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Charge
 */
class ChargeFrequency
{
    /**
     * @var int
     */
    private $frequency;

    const ONCE = 1;
    const DAILY = 2;
    const WEEKLY = 3;
    const MONTHLY = 4;

    public static $frequencies = [
        self::ONCE    => 'Once',
        self::DAILY   => 'Daily',
        self::WEEKLY  => 'Weekly',
        self::MONTHLY => 'Monthly',
    ];

    /**
     * ChargeFrequency constructor
     *
     * @param null $frequency
     *
     * @throws \Exception
     */
    private function __construct($frequency = null)
    {
        if (!in_array($frequency, [self::ONCE, self::DAILY, self::WEEKLY, self::MONTHLY])) {
            throw new \Exception('Invalid charge frequency');
        }

        $this->frequency = $frequency;
    }

    /**
     * @param null $frequency
     *
     * @return static
     */
    public static function create($frequency = null)
    {
        return new static($frequency);
    }

    /**
     * @return string
     */
    public function frequency()
    {
        return $this->frequency;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->frequency;
    }
}