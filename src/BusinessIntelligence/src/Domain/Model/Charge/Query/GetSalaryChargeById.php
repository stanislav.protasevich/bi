<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Charge\Query;

use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeIdentifier;

/**
 * Class GetSalaryChargeById
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Query\Charge
 */
final class GetSalaryChargeById
{
    /** @var string */
    protected $chargeId;

    /**
     * GetExpenseChargeById constructor
     *
     * @param string $projectId
     */
    private function __construct($projectId)
    {
        $this->chargeId = $projectId;
    }

    /**
     * @param $chargeId
     *
     * @return GetSalaryChargeById
     */
    public static function withData($chargeId)
    {
        return new self(
            $chargeId
        );
    }

    /**
     * @return string
     */
    public function chargeId(): string
    {
        return ChargeIdentifier::fromString($this->chargeId);
    }
}