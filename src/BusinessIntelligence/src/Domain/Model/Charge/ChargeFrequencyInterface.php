<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Charge;

/**
 * Interface ChargeFrequencyInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Charge
 */
interface ChargeFrequencyInterface
{
    const ONCE    = 1;
    const DAILY   = 2;
    const WEEKLY  = 3;
    const MONTHLY = 4;
}
