<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Charge;

use Orangear\BusinessIntelligence\Domain\Model\IdentifierInterface;

/**
 * Interface ChargeIdentifierInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Charge
 */
interface ChargeIdentifierInterface extends IdentifierInterface {}
