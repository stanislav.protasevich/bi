<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Charge;

/**
 * Interface ChargeRepositoryInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Charge
 */
interface ChargeRepositoryInterface
{
    /**
     * @param ChargeIdentifierInterface $identifier
     *
     * @return null|ChargeInterface
     */
    public function findById(ChargeIdentifierInterface $identifier):? ChargeInterface;

    /**
     * @param array $conditions
     * @return mixed
     */
    public function findOneBy(array $conditions = []);

    /**
     * @param array $conditions
     * @return mixed
     */
    public function findAll(array $conditions = []);

    /**
     * @param ChargeInterface $chargeType
     */
    public function create(ChargeInterface $chargeType);

    /**
     * @param ChargeInterface $chargeType
     */
    public function update(ChargeInterface $chargeType);

    /**
     * @param ChargeInterface $chargeType
     */
    public function delete(ChargeInterface $chargeType);

    /**
     * @return ChargeIdentifierInterface
     */
    public function nextIdentifier(): ChargeIdentifierInterface;
}
