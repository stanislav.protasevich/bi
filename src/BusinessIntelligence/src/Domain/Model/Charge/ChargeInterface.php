<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Charge;

use \DateTime;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;

/**
 * Interface ChargeInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Charge
 */
interface ChargeInterface
{
    /**
     * @return ChargeIdentifierInterface
     */
    public function id(): ChargeIdentifierInterface;

    /**
     * @return ProjectInterface
     */
    public function project(): ProjectInterface;

    /**
     * @return ChargeTypeInterface
     */
    public function chargeType(): ChargeTypeInterface;

    /**
     * @return DateTime
     */
    public function startDate(): DateTime;

    /**
     * @return DateTime
     */
    public function endDate(): DateTime;

    /**
     * @return int
     */
    public function frequency(): int;

    /**
     * @return float
     */
    public function amount() : float;
}
