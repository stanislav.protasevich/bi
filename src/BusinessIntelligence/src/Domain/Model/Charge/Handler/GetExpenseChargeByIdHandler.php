<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Charge\Handler;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeFrequency;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Exception\ChargeNotFound;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Query\GetExpenseChargeById;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\EmployeeGroup;
use Orangear\Admin\Core\InfrastructureBundle\Repository\ChargeRepository;
use Orangear\Admin\Core\InfrastructureBundle\Repository\ChargeTypeRepository;
use React\Promise\Deferred;

/**
 * Class GetExpenseChargeByIdHandler
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Query\Charge
 */
final class GetExpenseChargeByIdHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * GetExpenseChargeByIdHandler constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param GetExpenseChargeById $query
     * @param Deferred|null $deferred
     *
     * @return array
     */
    public function __invoke(GetExpenseChargeById $query, Deferred $deferred = null)
    {
        $data = null;

        $charge = $this->findExpenseOrFail($query->chargeId());

        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select([
            ChargeRepository::ALIAS . '.id AS charge_id',
            ChargeRepository::ALIAS . '.amount AS amount',
            ChargeRepository::ALIAS . '.frequency AS frequency',
            ChargeRepository::ALIAS . '.startDate AS start_date',
            ChargeRepository::ALIAS . '.endDate AS end_date',

            ChargeTypeRepository::ALIAS . '.id AS charge_type_id',
            ChargeTypeRepository::ALIAS . '.name AS charge_name',
            ChargeTypeRepository::ALIAS . '.description AS charge_description',
        ])
            ->from(Charge::class, ChargeRepository::ALIAS)
            ->join(
                EmployeeGroup::class,
                ChargeTypeRepository::ALIAS,
                Join::WITH,
                ChargeTypeRepository::ALIAS . '.id = ' . ChargeRepository::ALIAS . '.chargeType')
            ->where(ChargeRepository::ALIAS . '.id = :charge_id')
            ->setParameters([
                'charge_id' => $charge,
            ]);

        /** @var Charge $charge */
        foreach ($queryBuilder->getQuery()->getResult() as $charge) {
            $row['id']         = $charge['charge_id'];
            $row['amount']     = $charge['amount'];
            $row['start_date'] = $charge['start_date']->format('Y-m-d');
            $row['end_date']   = $charge['end_date']->format('Y-m-d');

            $row['frequency']['id']   = $charge['frequency'];
            $row['frequency']['name'] = ChargeFrequency::$frequencies[$charge['frequency']];

            $row['charge_type']['id']          = $charge['charge_type_id'];
            $row['charge_type']['name']        = $charge['charge_name'];
            $row['charge_type']['description'] = $charge['charge_description'];

            $data = $row;
        }

        if (null === $deferred) {
            return $data;
        }

        $deferred->resolve($data);
    }

    /**
     * @param $charge
     *
     * @return null|object
     *
     * @throws ChargeNotFound
     */
    protected function findExpenseOrFail($charge)
    {
        $chargeRepository = $this->entityManager->getRepository(Charge::class);

        $charge = $chargeRepository->findOneBy(['id' => $charge]);

        if (!$charge) {
            throw new ChargeNotFound('Charge does not exist');
        }

        return $charge;
    }
}