<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Charge\Handler;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Command\DeleteChargeCommand;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Command\UpdateChargeCommand;
use Prooph\ServiceBus\EventBus;

/**
 * Class UpdateChargeHandler
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Charge\Handler
 */
final class UpdateChargeHandler
{
    /** @var EventBus */
    private $eventBus;

    /**
     * DeleteChargeHandler constructor
     *
     * @param EventBus $eventBus
     */
    public function __construct(EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    /**
     * @param UpdateChargeCommand $command
     * 
     * @return \Orangear\BusinessIntelligence\Domain\Model\Charge\Event\ChargeWasDeleted
     */
    public function __invoke(UpdateChargeCommand $command)
    {
        $event = Charge::updateChargeById(
            $command->chargeId(),
            $command->chargeStartDate(),
            $command->chargeEndDate(),
            $command->chargeAmount()
        );

        $this->eventBus->dispatch($event);
    }
}