<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Charge\Handler;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeFrequency;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Exception\ChargeNotFound;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Query\GetExpenseChargeById;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Query\GetSalaryChargeById;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\EmployeeGroup;
use Orangear\BusinessIntelligence\Domain\Model\Employee\Employee;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeCharge;
use Orangear\Admin\Core\InfrastructureBundle\Repository\ChargeRepository;
use Orangear\Admin\Core\InfrastructureBundle\Repository\ChargeTypeRepository;
use Orangear\Admin\Core\InfrastructureBundle\Repository\EmployeeChargeRepository;
use Orangear\Admin\Core\InfrastructureBundle\Repository\EmployeeRepository;
use React\Promise\Deferred;

/**
 * Class GetSalaryChargeByIdHandler
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Query\Charge
 */
final class GetSalaryChargeByIdHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * GetSalaryChargeByIdHandler constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param GetSalaryChargeById $query
     * @param Deferred|null $deferred
     *
     * @return array
     */
    public function __invoke(GetSalaryChargeById $query, Deferred $deferred = null)
    {
        $data = null;

        $charge = $this->findExpenseOrFail($query->chargeId());

        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select([
            ChargeRepository::ALIAS . '.id AS charge_id',
            ChargeRepository::ALIAS . '.amount AS amount',
            ChargeRepository::ALIAS . '.frequency AS frequency',
            ChargeRepository::ALIAS . '.startDate AS start_date',
            ChargeRepository::ALIAS . '.endDate AS end_date',

            ChargeTypeRepository::ALIAS . '.id AS charge_type_id',
            ChargeTypeRepository::ALIAS . '.name AS charge_name',
            ChargeTypeRepository::ALIAS . '.description AS charge_description',

            EmployeeRepository::ALIAS . '.id AS employee_id',
            EmployeeRepository::ALIAS . '.firstName AS employee_first_name',
            EmployeeRepository::ALIAS . '.lastName AS employee_last_name',
            EmployeeRepository::ALIAS . '.notes AS employee_notes',
        ])
            ->from(Charge::class, ChargeRepository::ALIAS)
            ->join(
                EmployeeGroup::class,
                ChargeTypeRepository::ALIAS,
                Join::WITH,
                ChargeTypeRepository::ALIAS . '.id = ' . ChargeRepository::ALIAS . '.chargeType')
            ->join(
                EmployeeCharge::class,
                EmployeeChargeRepository::ALIAS,
                Join::WITH,
                EmployeeChargeRepository::ALIAS . '.charge = ' . ChargeRepository::ALIAS . '.id')
            ->join(
                Employee::class,
                EmployeeRepository::ALIAS,
                Join::WITH,
                EmployeeChargeRepository::ALIAS . '.employee = ' . EmployeeRepository::ALIAS . '.id')
            ->where(ChargeRepository::ALIAS . '.id = :charge_id')
            ->setParameters([
                'charge_id' => $charge,
            ]);

        /** @var Charge $charge */
        foreach ($queryBuilder->getQuery()->getResult() as $charge) {
            $row['id']         = $charge['charge_id'];
            $row['amount']     = $charge['amount'];
            $row['start_date'] = $charge['start_date']->format('Y-m-d');
            $row['end_date']   = $charge['end_date']->format('Y-m-d');

            $row['frequency']['id']   = $charge['frequency'];
            $row['frequency']['name'] = ChargeFrequency::$frequencies[$charge['frequency']];

            $row['charge_type']['id']          = $charge['charge_type_id'];
            $row['charge_type']['name']        = $charge['charge_name'];
            $row['charge_type']['description'] = $charge['charge_description'];

            $row['employee']['id']          = $charge['employee_id'];
            $row['employee']['first_name']  = $charge['employee_first_name'];
            $row['employee']['last_name']   = $charge['employee_last_name'];
            $row['employee']['notes']       = $charge['employee_notes'];

            $data = $row;
        }

        if (null === $deferred) {
            return $data;
        }

        $deferred->resolve($data);
    }

    /**
     * @param $charge
     *
     * @return null|object
     *
     * @throws ChargeNotFound
     */
    protected function findExpenseOrFail($charge)
    {
        $chargeRepository = $this->entityManager->getRepository(Charge::class);

        $charge = $chargeRepository->findOneBy(['id' => $charge]);

        if (!$charge) {
            throw new ChargeNotFound('Charge does not exist');
        }

        return $charge;
    }
}