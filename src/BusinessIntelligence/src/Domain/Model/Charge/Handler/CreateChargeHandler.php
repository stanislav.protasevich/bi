<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Charge\Handler;

use Orangear\Admin\Core\InfrastructureBundle\Repository\EmployeeChargeRepository;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Command\CreateChargeCommandInterface;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeInterface;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeInterface;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeCharge;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectRepositoryInterface;

/**
 * Class CreateChargeHandler
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Charge\Handler
 */
class CreateChargeHandler
{
    /** @var ChargeRepositoryInterface */
    private $chargeRepository;

    /** @var ProjectRepositoryInterface */
    private $projectRepository;

    /** @var ChargeTypeRepositoryInterface */
    private $chargeTypeRepository;

    /** @var EmployeeRepositoryInterface */
    private $employeeRepository;

    /** @var EmployeeChargeRepository */
    private $employeeChargeRepository;

    /**
     * CreateChargeHandler constructor
     *
     * @param $chargeRepository
     * @param ProjectRepositoryInterface $projectRepository
     * @param ChargeTypeRepositoryInterface $chargeTypeRepository
     * @param EmployeeRepositoryInterface $employeeRepository
     * @param EmployeeChargeRepository $employeeChargeRepository
     */
    public function __construct($chargeRepository, ProjectRepositoryInterface $projectRepository, ChargeTypeRepositoryInterface $chargeTypeRepository, EmployeeRepositoryInterface $employeeRepository, EmployeeChargeRepository $employeeChargeRepository)
    {
        $this->chargeRepository = $chargeRepository;
        $this->projectRepository = $projectRepository;
        $this->chargeTypeRepository = $chargeTypeRepository;
        $this->employeeRepository = $employeeRepository;
        $this->employeeChargeRepository = $employeeChargeRepository;
    }

    /**
     * @param CreateChargeCommandInterface $command
     */
    public function __invoke(CreateChargeCommandInterface $command)
    {
        //@todo make transaction

        $project = $this->findProjectOrFail($command->projectId());
        $chargeType = $this->findChargeTypeOrFail($command->chargeTypeId());
        $employee = $this->findEmployeeOrFail($command->employeeId());

        $charge = Charge::withData(
            $this->chargeRepository->nextIdentifier(),
            $project,
            $chargeType,
            $command->startDate(),
            $command->endDate(),
            $command->frequency(),
            $command->amount()
        );

        $employeeCharge = EmployeeCharge::withData(
            'sasa',
            $charge,
            $employee
        );

        $this->chargeRepository->create($charge);
        $this->employeeChargeRepository->create($employeeCharge);
    }

    /**
     * @param $id
     *
     * @return null|ProjectInterface
     *
     * @throws \Exception
     */
    private function findProjectOrFail($id)
    {
        $project = $this->projectRepository->findById($id);

        if ($project === null) {
            throw new \Exception('Project does not exists');
        }

        return $project;
    }

    /**
     * @param $chargeTypeId
     *
     * @return null|ChargeTypeInterface
     *
     * @throws \Exception
     */
    private function findChargeTypeOrFail($chargeTypeId)
    {
        $chargeType = $this->chargeTypeRepository->findById($chargeTypeId);

        if ($chargeTypeId === null) {
            throw new \Exception('Charge type does not exists');
        }

        return $chargeType;
    }

    /**
     * @param $employeeId
     *
     * @return null|EmployeeInterface
     *
     * @throws \Exception
     */
    private function findEmployeeOrFail($employeeId)
    {
        $employee = $this->employeeRepository->findById($employeeId);

        if ($employee === null) {
            throw new \Exception('Employee does not exists');
        }

        return $employee;
    }
}
