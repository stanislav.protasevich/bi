<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Charge\Handler;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Command\DeleteChargeCommand;
use Prooph\ServiceBus\EventBus;

/**
 * Class DeleteChargeHandler
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Charge\Handler
 */
final class DeleteChargeHandler
{
    /** @var EventBus */
    private $eventBus;

    /**
     * DeleteChargeHandler constructor
     *
     * @param EventBus $eventBus
     */
    public function __construct(EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    /**
     * @param DeleteChargeCommand $command
     * 
     * @return \Orangear\BusinessIntelligence\Domain\Model\Charge\Event\ChargeWasDeleted
     */
    public function __invoke(DeleteChargeCommand $command)
    {
        $event = Charge::deleteByChargeId($command->chargeId());

        $this->eventBus->dispatch($event);
    }
}