<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Charge;

/**
 * Class ChargeId
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Charge
 */
final class ChargeIdentifier implements ChargeIdentifierInterface
{
    /**
     * @var string
     */
    private $id;

    /**
     * ChargeIdentifier constructor
     *
     * @param null $id
     */
    private function __construct($id = null)
    {
        $this->id = $id;
    }

    /**
     * @param $anId
     *
     * @return static
     */
    public static function fromString($anId)
    {
        return new static($anId);
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function __toString()
    {
        return $this->id;
    }
}