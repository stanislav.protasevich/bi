<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Charge\Service;

use Orangear\Admin\Core\ApplicationBundle\Command\Charge\CreateChargeCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeFrequency;
use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\EmployeeGroup;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\Service\Exception\ChargeTypeDoesNotExistException;
use Orangear\BusinessIntelligence\Domain\Model\Employee\Service\Exception\EmployeeDoesNotExistException;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeCharge;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeChargeId;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;
use Orangear\BusinessIntelligence\Domain\Model\Project\Service\Exception\ProjectDoesNotExistException;
use Orangear\BusinessIntelligence\Domain\ServiceInterface;
use Orangear\Admin\Core\InfrastructureBundle\Repository\ChargeRepository;
use Orangear\Admin\Core\InfrastructureBundle\Repository\ChargeTypeRepository;
use \DateTime;
use Orangear\Admin\Core\InfrastructureBundle\Repository\EmployeeChargeRepository;
use Orangear\Admin\Core\InfrastructureBundle\Repository\EmployeeRepository;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\ProjectRepository;

/**
 * Class CreateChargeService
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ChargeType\Service
 */
class CreateChargeService implements ServiceInterface
{
    /** @var ChargeRepository */
    private $chargeRepository;

    /** @var ProjectRepository */
    private $projectRepository;

    /** @var ChargeTypeRepository */
    private $chargeTypeRepository;

    /** @var EmployeeRepository */
    private $employeeRepository;

    /** @var EmployeeChargeRepository */
    private $employeeChargeRepository;

    /**
     * CreateChargeService constructor
     *
     * @param ChargeRepository $chargeRepository
     * @param ProjectRepository $projectRepository
     * @param ChargeTypeRepository $chargeTypeRepository
     * @param EmployeeRepository $employeeRepository
     * @param EmployeeChargeRepository $employeeChargeRepository
     */
    public function __construct(
        ChargeRepository $chargeRepository,
        ProjectRepository $projectRepository,
        ChargeTypeRepository $chargeTypeRepository,
        EmployeeRepository $employeeRepository,
        EmployeeChargeRepository $employeeChargeRepository
    ) {
        $this->chargeRepository = $chargeRepository;
        $this->projectRepository = $projectRepository;
        $this->chargeTypeRepository = $chargeTypeRepository;
        $this->employeeRepository = $employeeRepository;
        $this->employeeChargeRepository = $employeeChargeRepository;
    }


    /**
     * @param CommandInterface $createChargeCommand
     */
    public function execute(CommandInterface $createChargeCommand)
    {
        /** @var CreateChargeCommand $createChargeCommand */

        $id = ChargeIdentifier::create();
        $projectId = \Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId::fromString($createChargeCommand->projectId);
        $chargeTypeId = $createChargeCommand->chargeTypeId;
        $employeeId = $createChargeCommand->employeeId;
        $startDate = new DateTime($createChargeCommand->startDate);
        $endDate = new DateTime($createChargeCommand->endDate);
        $frequency = ChargeFrequency::create($createChargeCommand->type);
        $amount = $createChargeCommand->amount;

        $project = $this->findProjectOrFail($projectId);
        $chargeType = $this->findChargeTypeOrFail($chargeTypeId);

        $employee = null;
        if ($employeeId !== null) {
            $employee = $this->findEmployeeOrFail($employeeId);
        }

        $charge = new Charge(
            $id,
            $project,
            $chargeType,
            $startDate,
            $endDate,
            $frequency,
            $amount
        );

        $this->chargeRepository->create($charge);

        if ($employee !== null) {
            $employeeCharge = new EmployeeCharge(
                EmployeeChargeId::create(),
                $charge,
                $employee
            );

            $this->employeeChargeRepository->create($employeeCharge);
        }
    }

    /**
     * @param $id
     *
     * @return null|ProjectInterface
     *
     * @throws ProjectDoesNotExistException
     */
    protected function findProjectOrFail($id)
    {
        $project = $this->projectRepository->findById($id);

        if ($project === null) {
            throw new ProjectDoesNotExistException('Project does not exists');
        }

        return $project;
    }

    /**
     * @param $chargeTypeId
     *
     * @return null|EmployeeGroup
     *
     * @throws ChargeTypeDoesNotExistException
     */
    protected function findChargeTypeOrFail($chargeTypeId)
    {
        $chargeType = $this->chargeTypeRepository->byId($chargeTypeId);

        if ($chargeTypeId === null) {
            throw new ChargeTypeDoesNotExistException('Charge type does not exists');
        }

        return $chargeType;
    }

    /**
     * @param $employeeId
     *
     * @return null|\Orangear\BusinessIntelligence\Domain\Model\Employee\Employee
     *
     * @throws EmployeeDoesNotExistException
     */
    protected function findEmployeeOrFail($employeeId)
    {
        $employee = $this->employeeRepository->byId($employeeId);

        if ($employee === null) {
            throw new EmployeeDoesNotExistException('Employee does not exists');
        }

        return $employee;
    }
}