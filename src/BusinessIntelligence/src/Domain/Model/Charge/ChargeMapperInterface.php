<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Charge;

/**
 * Interface ChargeMapperInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Charge
 */
interface ChargeMapperInterface
{
    /**
     * @param ChargeIdentifierInterface $chargeTypeIdentifier
     *
     * @return null|ChargeInterface
     */
    public function fetchById(ChargeIdentifierInterface $chargeTypeIdentifier):? ChargeInterface;

    /**
     * @param array $conditions
     * @return array|ChargeInterface
     */
    public function fetchOneBy($conditions = []):? ChargeInterface;

    /**
     * @param array $conditions
     * @return mixed
     */
    public function fetchAll(array $conditions = []);

    /**
     * @param ChargeInterface $chargeType
     */
    public function create(ChargeInterface $chargeType);

    /**
     * @param ChargeInterface $chargeType
     */
    public function update(ChargeInterface $chargeType);

    /**
     * @param ChargeInterface $chargeType
     */
    public function delete(ChargeInterface $chargeType);
}