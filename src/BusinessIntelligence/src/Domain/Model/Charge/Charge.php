<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Charge;

use Orangear\Admin\Core\Domain\Model\Charge\Event\ChargeWasDeleted;
use Orangear\Admin\Core\Domain\Model\Charge\Event\ChargeWasUpdated;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeType;
use DateTime;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;

/**
 * Class Charge
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Charge
 */
class Charge implements ChargeInterface
{
    /** @var ChargeIdentifierInterface */
    private $id;

    /** @var ProjectInterface */
    private $project;

    /** @var ChargeTypeInterface */
    private $chargeType;

    /** @var DateTime */
    private $startDate;

    /** @var DateTime */
    private $endDate;

    /** @var int */
    private $frequency;

    /** @var float */
    private $amount;

    /**
     * Charge constructor
     *
     * @param ChargeIdentifierInterface $id
     * @param ProjectInterface $project
     * @param ChargeTypeInterface $chargeType
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param int $frequency
     * @param float $amount
     */
    private function __construct(ChargeIdentifierInterface $id, ProjectInterface $project, ChargeTypeInterface $chargeType, DateTime $startDate, DateTime $endDate, $frequency, $amount)
    {
        $this->id = $id;
        $this->project = $project;
        $this->chargeType = $chargeType;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->frequency = $frequency;
        $this->amount = $amount;
    }

    /**
     * @param ChargeIdentifierInterface $id
     * @param ProjectInterface $project
     * @param ChargeTypeInterface $chargeType
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param $frequency
     * @param $amount
     *
     * @return ChargeInterface
     */
    public static function withData(ChargeIdentifierInterface $id, ProjectInterface $project, ChargeTypeInterface $chargeType, DateTime $startDate, DateTime $endDate, $frequency, $amount)
    {
        return new self($id, $project, $chargeType, $startDate, $endDate, $frequency, $amount);
    }

    /**
     * {@inheritdoc}
     */
    public function id(): ChargeIdentifierInterface
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function project(): ProjectInterface
    {
        return $this->project;
    }

    /**
     * {@inheritdoc}
     */
    public function chargeType(): ChargeTypeInterface
    {
        return $this->chargeType;
    }

    /**
     * {@inheritdoc}
     */
    public function startDate(): DateTime
    {
        return $this->startDate;
    }

    /**
     * {@inheritdoc}
     */
    public function endDate(): DateTime
    {
        return $this->endDate;
    }

    /**
     * {@inheritdoc}
     */
    public function frequency(): int
    {
        return $this->frequency;
    }

    /**
     * {@inheritdoc}
     */
    public function amount(): float
    {
        return $this->amount;
    }

    /**
     * @param ChargeIdentifier $chargeId
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param float $amount
     *
     * @return ChargeWasUpdated
     */
    public static function updateChargeById(ChargeIdentifier $chargeId, \DateTime $startDate, \DateTime $endDate, float $amount)
    {
        return ChargeWasUpdated::withData($chargeId, $startDate, $endDate, $amount);
    }

    /**
     * @param ChargeIdentifier $chargeId
     *
     * @return ChargeWasDeleted
     */
    public static function deleteByChargeId(ChargeIdentifier $chargeId)
    {
        return ChargeWasDeleted::withData($chargeId);
    }

    /**
     * @param DateTime $startDate
     *
     * @return $this
     */
    public function setStartDate(DateTime $startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @param DateTime $endDate
     *
     * @return $this
     */
    public function setEndDate(DateTime $endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @param float $amount
     *
     * @return $this
     */
    public function setAmount(float $amount)
    {
        $this->amount = $amount;

        return $this;
    }
}