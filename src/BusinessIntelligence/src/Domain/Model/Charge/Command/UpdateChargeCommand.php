<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Charge\Command;

use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeIdentifier;

/**
 * Class UpdateChargeCommand
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Charge\Command
 */
final class UpdateChargeCommand
{
    /** @var string */
    private $chargeId;

    /** @var string */
    private $startDate;

    /** @var string */
    private $endDate;

    /** @var float */
    private $amount;

    /**
     * EditChargeCommand constructor
     *
     * @param string $chargeId
     * @param string $startDate
     * @param string $endDate
     * @param string $amount
     */
    public function __construct($chargeId, $startDate, $endDate, $amount)
    {
        $this->chargeId = $chargeId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->amount = $amount;
    }

    /**
     * @param string $chargeId
     * @param string $startDate
     * @param string $endDate
     * @param string $amount
     *
     * @return UpdateChargeCommand
     */
    public static function withData($chargeId, $startDate, $endDate, $amount)
    {
        return new self(
            $chargeId,
            $startDate,
            $endDate,
            $amount
        );
    }

    /**
     * @return ChargeIdentifier
     */
    public function chargeId()
    {
        return ChargeIdentifier::fromString($this->chargeId);
    }

    /**
     * @return \DateTime
     */
    public function chargeStartDate()
    {
        return new \DateTime($this->startDate);
    }

    /**
     * @return \DateTime
     */
    public function chargeEndDate()
    {
        return new \DateTime($this->endDate);
    }

    /**
     * @return float
     */
    public function chargeAmount()
    {
        return floatval($this->amount);
    }
}