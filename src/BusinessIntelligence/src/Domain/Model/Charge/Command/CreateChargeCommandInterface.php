<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Charge\Command;

use DateTime;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;

/**
 * Interface CreateChargeCommandInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Charge\Command
 */
interface CreateChargeCommandInterface
{
    /**
     * @return IdentifierInterface
     */
    public function memberId(): IdentifierInterface;

    /**
     * @return ProjectIdentifierInterface
     */
    public function projectId(): ProjectIdentifierInterface;

    /**
     * @return ChargeTypeIdentifierInterface
     */
    public function chargeTypeId(): ChargeTypeIdentifierInterface;

    /**
     * @return EmployeeIdentifierInterface
     */
    public function employeeId(): EmployeeIdentifierInterface;

    /**
     * @return DateTime
     */
    public function startDate(): DateTime;

    /**
     * @return DateTime
     */
    public function endDate(): DateTime;

    /**
     * @return int
     */
    public function frequency(): int;

    /**
     * @return float
     */
    public function amount(): float;
}
