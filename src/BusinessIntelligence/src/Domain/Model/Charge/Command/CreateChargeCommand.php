<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Charge\Command;

use DateTime;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\IdentifierInterface;
use Orangear\MembershipBundle\Domain\Model\Member\MemberId;

/**
 * Class CreateChargeCommandInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Charge\Command
 */
final class CreateChargeCommand implements CreateChargeCommandInterface
{
    /** @var string */
    private $memberId;

    /** @var string */
    private $projectId;

    /** @var string */
    private $chargeTypeId;

    /** @var string */
    private $employeeId;

    /** @var string */
    private $startDate;

    /** @var string */
    private $endDate;

    /** @var int */
    private $frequency;

    /** @var float */
    private $amount;

    /**
     * CreateChargeCommandInterface constructor
     *
     * @param string $memberId
     * @param string $projectId
     * @param string $chargeTypeId
     * @param string $employeeId
     * @param string $startDate
     * @param string $endDate
     * @param int $frequency
     * @param float $amount
     */
    private function __construct($memberId, $projectId, $chargeTypeId, $employeeId, $startDate, $endDate, $frequency, $amount)
    {
        $this->memberId = $memberId;
        $this->projectId = $projectId;
        $this->chargeTypeId = $chargeTypeId;
        $this->employeeId = $employeeId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->frequency = $frequency;
        $this->amount = $amount;
    }

    /**
     * @param $memberId
     * @param $projectId
     * @param $chargeTypeId
     * @param $employeeId
     * @param $startDate
     * @param $endDate
     * @param $frequency
     * @param $amount
     *
     * @return self
     */
    public static function forMember($memberId, $projectId, $chargeTypeId, $employeeId, $startDate, $endDate, $frequency, $amount)
    {
        return new self($memberId, $projectId, $chargeTypeId, $employeeId, $startDate, $endDate, $frequency, $amount);
    }

    /**
     * {@inheritdoc}
     */
    public function memberId(): IdentifierInterface
    {
        return MemberId::fromString($this->memberId);
    }

    /**
     * {@inheritdoc}
     */
    public function projectId(): ProjectIdentifierInterface
    {
        return ProjectId::fromString($this->projectId);
    }

    /**
     * {@inheritdoc}
     */
    public function chargeTypeId(): ChargeTypeIdentifierInterface
    {
        return ChargeTypeIdentifier::fromString($this->chargeTypeId);
    }

    /**
     * {@inheritdoc}
     */
    public function employeeId(): EmployeeIdentifierInterface
    {
        return EmployeeIdentifier::fromString($this->employeeId);
    }

    /**
     * {@inheritdoc}
     */
    public function startDate(): DateTime
    {
        return new DateTime($this->startDate);
    }

    /**
     * {@inheritdoc}
     */
    public function endDate(): DateTime
    {
        return new DateTime($this->endDate);
    }

    /**
     * {@inheritdoc}
     */
    public function frequency(): int
    {
        return (int) $this->frequency;
    }

    /**
     * {@inheritdoc}
     */
    public function amount(): float
    {
        return (float) $this->amount;
    }
}