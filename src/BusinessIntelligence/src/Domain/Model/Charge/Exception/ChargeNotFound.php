<?php

namespace Orangear\BusinessIntelligence\Domain\Model\Charge\Exception;

use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeIdentifier;

/**
 * Class ChargeNotFound
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Charge
 */
final class ChargeNotFound extends \InvalidArgumentException
{
    /**
     * @param ChargeIdentifier $chargeId
     *
     * @return ChargeNotFound
     */
    public static function withChargeId(ChargeIdentifier $chargeId)
    {
        return new self(sprintf('Charge with id %s cannot be found.', $chargeId->id()));
    }
}