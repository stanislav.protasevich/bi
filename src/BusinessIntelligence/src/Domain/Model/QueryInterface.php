<?php

namespace Orangear\BusinessIntelligence\Domain\Model;

/**
 * Interface QueryInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model
 */
interface QueryInterface {}
