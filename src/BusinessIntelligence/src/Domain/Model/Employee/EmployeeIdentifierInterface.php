<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Employee;

/**
 * Interface RemoteEmployeeIdentifierInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Employee
 */
interface EmployeeIdentifierInterface {}
