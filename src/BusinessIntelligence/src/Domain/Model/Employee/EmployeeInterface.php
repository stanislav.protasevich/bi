<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Employee;

/**
 * Interface RemoteEmployeeInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Employee
 */
interface EmployeeInterface
{
    /**
     * @return EmployeeIdentifierInterface
     */
    public function id(): EmployeeIdentifierInterface;
    public function firstName():? string;
    public function lastName():? string;
    public function position():? string;
}
