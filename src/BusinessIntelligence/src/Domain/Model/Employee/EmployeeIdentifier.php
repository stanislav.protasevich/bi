<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Employee;

/**
 * Class EmployeeIdentifier
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee
 */
class EmployeeIdentifier implements EmployeeIdentifierInterface
{
    /**
     * @var string
     */
    private $id;

    private function __construct($id = null)
    {
        $this->id = $id;
    }

    public static function fromString($anId)
    {
        return new static($anId);
    }

    public function toString()
    {
        return $this->id;
    }

    public function __toString()
    {
        if ($this->id !== null)
            return $this->id;

        return "";
    }
}
