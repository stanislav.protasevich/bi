<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Employee;

/**
 * Interface RemoteEmployeeMapperInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Employee
 */
interface EmployeeMapperInterface
{
    /**
     * @param EmployeeIdentifierInterface $chargeTypeIdentifier
     *
     * @return null|EmployeeInterface
     */
    public function fetchById(EmployeeIdentifierInterface $chargeTypeIdentifier) :? EmployeeInterface;

    /**
     * @param array $conditions
     * @return mixed
     */
    public function fetchAll(array $conditions = []);

    /**
     * @param EmployeeInterface $chargeType
     */
    public function create(EmployeeInterface $chargeType);

    /**
     * @param EmployeeInterface $chargeType
     */
    public function update(EmployeeInterface $chargeType);

    /**
     * @param EmployeeInterface $chargeType
     */
    public function delete(EmployeeInterface $chargeType);
}
