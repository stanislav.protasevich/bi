<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Employee;

/**
 * Class RemoteEmployee
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Employee
 */
class Employee implements EmployeeInterface
{
    /** @var EmployeeIdentifierInterface */
    private $id;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var string */
    private $position;

    /**
     * Employee constructor
     *
     * @param EmployeeIdentifierInterface $id
     * @param string $firstName
     * @param string $lastName
     * @param string $position
     */
    private function __construct(EmployeeIdentifierInterface $id, $firstName, $lastName, $position)
    {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->position = $position;
    }

    /**
     * @param EmployeeIdentifierInterface $id
     *
     * @return Employee
     * @param string $firstName
     * @param string $lastName
     * @param string $position
     */
    public static function create(EmployeeIdentifierInterface $id, $firstName, $lastName, $position)
    {
        return new self($id, $firstName, $lastName, $position);
    }

    /**
     * @return EmployeeIdentifierInterface
     */
    public function id(): EmployeeIdentifierInterface
    {
        return $this->id;
    }


    /**
     * {@inheritdoc}
     */
    public function firstName():? string
    {
        return $this->firstName;
    }

    /**
     * {@inheritdoc}
     */
    public function lastName():? string
    {
        return $this->lastName;
    }

    /**
     * {@inheritdoc}
     */
    public function position():? string
    {
        return $this->position;
    }
}
