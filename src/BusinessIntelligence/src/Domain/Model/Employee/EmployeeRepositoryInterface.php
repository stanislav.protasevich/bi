<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\Employee;

/**
 * Interface RemoteEmployeeRepositoryInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\Employee
 */
interface EmployeeRepositoryInterface
{
    /**
     * @param EmployeeIdentifierInterface $chargeTypeIdentifier
     *
     * @return null|EmployeeInterface
     */
    public function findById(EmployeeIdentifierInterface $chargeTypeIdentifier) :? EmployeeInterface;

    /**
     * @param array $conditions
     * @return mixed
     */
    public function findAll(array $conditions = []);

    /**
     * @param EmployeeInterface $chargeType
     */
    public function create(EmployeeInterface $chargeType);

    /**
     * @param EmployeeInterface $chargeType
     */
    public function update(EmployeeInterface $chargeType);

    /**
     * @param EmployeeInterface $chargeType
     */
    public function delete(EmployeeInterface $chargeType);

    /**
     * @return EmployeeIdentifierInterface
     */
    public function nextIdentifier() : EmployeeIdentifierInterface;
}
