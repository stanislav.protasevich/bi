<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model;

use Prooph\ServiceBus\EventBus;

/**
 * Class Collection
 *
 * @package Orangear\BusinessIntelligence\Domain\Model
 */
abstract class Collection implements CollectionInterface
{
    /** @var RepositoryInterface */
    protected $repository;

    /** @var EventBus */
    protected $eventBus;

    /**
     * Collection constructor
     *
     * @param $repository
     * @param EventBus $eventBus
     */
    public function __construct($repository, EventBus $eventBus)
    {
        $this->repository = $repository;
        $this->eventBus = $eventBus;
    }

    /**
     * @param AggregateRootInterface $aggregateRoot
     */
    public function add($aggregateRoot)
    {
        foreach ($aggregateRoot->getRecordedEvents() as $event) {
            $this->eventBus->dispatch($event);
        }
    }
}
