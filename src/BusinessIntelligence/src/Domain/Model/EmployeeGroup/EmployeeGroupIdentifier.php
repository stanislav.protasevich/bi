<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup;

/**
 * Class ChargeTypeIdentifier
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup
 */
class EmployeeGroupIdentifier implements EmployeeGroupIdentifierInterface
{
    /**
     * @var string
     */
    private $id;

    private function __construct($id = null)
    {
        $this->id = $id;
    }

    public static function fromString($anId)
    {
        return new static($anId);
    }

    public function toString()
    {
        return $this->id;
    }

    public function __toString()
    {
        return $this->id;
    }
}
