<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup;

/**
 * Interface ChargeTypeIdentifierInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup
 */
interface EmployeeGroupIdentifierInterface {}
