<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\Handler;

use Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\EmployeeGroup;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\EmployeeGroupRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\Command\CreateEmployeeGroupCommandInterface;

/**
 * Class CreateEmployeeGroupCommandHandler
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\ChargeType\Handler
 */
final class CreateEmployeeGroupCommandHandler
{
    /** @var EmployeeGroupRepositoryInterface */
    private $chargeTypeRepository;

    /**
     * CreateEmployeeGroupCommandHandler constructor
     *
     * @param EmployeeGroupRepositoryInterface $chargeTypeRepository
     */
    public function __construct(EmployeeGroupRepositoryInterface $chargeTypeRepository)
    {
        $this->chargeTypeRepository = $chargeTypeRepository;
    }

    /**
     * @param CreateEmployeeGroupCommandInterface $command
     */
    public function __invoke(CreateEmployeeGroupCommandInterface $command)
    {
        //@todo make by owner group

        if ($this->chargeTypeRepository->findByName($command->name())) {
        }

        $chargeType = EmployeeGroup::create(
            $this->chargeTypeRepository->nextIdentifier(),
            $command->name(),
            $command->key(),
            $command->description()
        );

        $this->chargeTypeRepository->create($chargeType);
    }
}
