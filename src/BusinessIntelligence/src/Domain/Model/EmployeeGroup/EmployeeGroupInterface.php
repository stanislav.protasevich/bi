<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup;

/**
 * Interface ChargeTypeInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup
 */
interface EmployeeGroupInterface
{
    /**
     * @return EmployeeGroupIdentifierInterface
     */
    public function id(): EmployeeGroupIdentifierInterface;

    /**
     * @return string
     */
    public function key(): string;

    /**
     * @return string
     */
    public function name(): string;

    /**
     * @return null|string
     */
    public function description():? string;
}