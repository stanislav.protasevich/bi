<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup;

/**
 * Interface ChargeTypeMapperInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup
 */
interface EmployeeGroupMapperInterface
{
    /**
     * @param EmployeeGroupIdentifierInterface $chargeTypeIdentifier
     *
     * @return null|EmployeeGroupInterface
     */
    public function fetchById(EmployeeGroupIdentifierInterface $chargeTypeIdentifier) :? EmployeeGroupInterface;

    /**
     * @param string $name
     * @return null|EmployeeGroupInterface
     */
    public function fetchByName(string $name) :? EmployeeGroupInterface;

    /**
     * @param array $conditions
     * @return mixed
     */
    public function fetchAll(array $conditions = []);

    /**
     * @param EmployeeGroupInterface $chargeType
     */
    public function create(EmployeeGroupInterface $chargeType);

    /**
     * @param EmployeeGroupInterface $chargeType
     */
    public function update(EmployeeGroupInterface $chargeType);

    /**
     * @param EmployeeGroupInterface $chargeType
     */
    public function delete(EmployeeGroupInterface $chargeType);
}
