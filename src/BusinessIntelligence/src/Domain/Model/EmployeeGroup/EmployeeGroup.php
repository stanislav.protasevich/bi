<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup;

/**
 * Class ChargeType
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup
 */
class EmployeeGroup implements EmployeeGroupInterface
{
    /** @var EmployeeGroupIdentifierInterface */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $key;

    /** @var string */
    private $description;

    /**
     * ChargeType constructor
     *
     * @param EmployeeGroupIdentifierInterface $id
     * @param string $name
     * @param string $key
     * @param string $description
     */
    private function __construct(EmployeeGroupIdentifierInterface $id, string $name, string $key, string $description = null)
    {
        $this->id = $id;
        $this->key = $key;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @param EmployeeGroupIdentifierInterface $id
     * @param string $name
     * @param string $key
     * @param string|null $description
     *
     * @return EmployeeGroup
     */
    public static function create(EmployeeGroupIdentifierInterface $id, string $name, string $key, string $description = null)
    {
        return new self($id, $name, $key, $description);
    }

    /**
     * {@inheritdoc}
     */
    public function id(): EmployeeGroupIdentifierInterface
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function key(): string
    {
        return $this->key;
    }

    /**
     * {@inheritdoc}
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function description():? string
    {
        return $this->description;
    }
}
