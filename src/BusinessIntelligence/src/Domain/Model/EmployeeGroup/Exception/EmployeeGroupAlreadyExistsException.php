<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\Exception;

/**
 * Class ChargeTypeAlreadyExistsException
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\Exception
 */
final class EmployeeGroupAlreadyExistsException extends \InvalidArgumentException {}
