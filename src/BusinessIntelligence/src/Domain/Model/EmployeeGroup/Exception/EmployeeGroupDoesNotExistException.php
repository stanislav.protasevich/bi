<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\Exception;

/**
 * Class ChargeTypeDoesNotExistException
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\Exception
 */
final class EmployeeGroupDoesNotExistException extends \InvalidArgumentException {}
