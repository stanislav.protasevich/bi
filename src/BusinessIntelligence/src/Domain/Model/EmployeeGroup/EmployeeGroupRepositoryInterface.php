<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup;

/**
 * Interface ChargeTypeMapperInterface
 *
 * @package Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup
 */
interface EmployeeGroupRepositoryInterface
{
    /**
     * @param EmployeeGroupIdentifierInterface $chargeTypeIdentifier
     *
     * @return null|EmployeeGroupInterface
     */
    public function findById(EmployeeGroupIdentifierInterface $chargeTypeIdentifier) :? EmployeeGroupInterface;

    /**
     * @param string $name
     * @return null|EmployeeGroupInterface
     */
    public function findByName(string $name) :? EmployeeGroupInterface;

    /**
     * @param array $conditions
     * @return mixed
     */
    public function findAll(array $conditions = []);

    /**
     * @param EmployeeGroupInterface $chargeType
     */
    public function create(EmployeeGroupInterface $chargeType);

    /**
     * @param EmployeeGroupInterface $chargeType
     */
    public function update(EmployeeGroupInterface $chargeType);

    /**
     * @param EmployeeGroupInterface $chargeType
     */
    public function delete(EmployeeGroupInterface $chargeType);

    /**
     * @return EmployeeGroupIdentifierInterface
     */
    public function nextIdentifier() : EmployeeGroupIdentifierInterface;
}
