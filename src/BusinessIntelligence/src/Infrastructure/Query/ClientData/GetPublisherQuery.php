<?php

namespace Orangear\BusinessIntelligence\Infrastructure\Query\ClientData;

use DateTime;
use DatePeriod;
use DateInterval;
use Doctrine\ORM\EntityManagerInterface;
use ClickHouse\Client as ClickHouseClient;
use PDO;

class GetPublisherQuery
{
    /** @var PDOConnection */
    private $dbh;

    /** @var ClickHouseClient */
    private $clickHouse;

    private $from;
    private $to;
    private $companyId;

    private $amountFrom;
    private $amountTo;

    private $country;

    private $advertiserId;

    private $token;

    private $projects = [];

    private $database = 'default';

    /**
     * BestPublisherChartQuery constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ClickHouseClient $clickHouse
     * @param string $database
     */
    public function __construct(EntityManagerInterface $entityManager, ClickHouseClient $clickHouse, $database)
    {
        $this->dbh = $entityManager->getConnection()->getWrappedConnection();
        $this->clickHouse = $clickHouse;
        $this->database = $database;
    }

    public function __invoke($companyId, $advertiserId, DateTime $from, DateTime $to, $country = null, $amountFrom = null, $amountTo = null, $token = null)
    {
        $this->advertiserId = $advertiserId;
        $this->from = $from;
        $this->to = $to;
        $this->companyId = $companyId;
        $this->amountFrom = $amountFrom;
        $this->amountTo = $amountTo;
        $this->country = $country;
        $this->token = $token;

        if ($this->token !== null) {
            $this->getProjects($this->token);
        }

        $return = [];

        $skeleton = $this->getDatesSkeleton();
        $statistics = $this->getAdvertiserStatistics();
        $companies = $this->getCompanies();
        $advertisers = $this->getAdvertisersInfo();
        $advertiser = $this->getAdvertiserInfo();

        foreach ($skeleton as $date => $data) {
            $revenue = 0;
            $income = 0;
            $cost = 0;

            $data = [];

            if (isset($statistics[$date])) {
                $data['date'] = $date;

                foreach ($statistics[$date] as $stata) {
                    $data['items'][] = [
                        'revenue' => $stata['revenue'],
                        'income' => $stata['profit'],
                        'cost' => $stata['cost'],

                        'advertiser' => [
                            'external_id' => $advertisers[$stata['company_id'] . $stata['id']]['external_id'],
                            'name' => $advertisers[$stata['company_id'] . $stata['id']]['company']
                        ],

                        'company' => [
                            'id' => $stata['company_id'],
                            'name' => $companies[$stata['company_id']]['name']
                        ]
                    ];

                    $revenue += $stata['revenue'];
                    $income += $stata['profit'];
                    $cost += $stata['cost'];
                }

                $data['revenue'] = $revenue;
                $data['income'] = $income;
                $data['cost'] = $cost;
            } else {
                $data['date'] = $date;
                $data['revenue'] = 0;
                $data['income'] = 0;
                $data['cost'] = 0;
                $data['items'] = [];
                $data['advertiser'] = null;
                $data['company'] = null;
            }

            $return[] = $data;
        }

//        var_dump($advertisers); exit;

        $response = [
            'id' => $advertiserId,
            'title' => $advertiser['name'] ?? null,
            'start_date' => $this->from->format('Y-m-d'),
            'end_date' => $this->to->modify('- 1 day')->format('Y-m-d'),
        ];

        $response['data'] = $return;

        return $response;
    }

    public function getAdvertisersInfo()
    {
        $sql = <<<SQL
SELECT * FROM remote_publishers JOIN publishers ON remote_publishers.internal_id = publishers.id
:WHERE: 
SQL;
        $wheres = [];

        if ($this->companyId !== null) {
            $wheres[] = 'company_id = :companyId';
        }


        $wheres[] = 'internal_id = :remoteId';

        if ($this->token !== null) {
            $where[] = "company_id IN (" . implode(',', $this->projects) . ")";
        }

        $sql = str_replace( ':WHERE:', 'WHERE ' . implode( ' AND ', $wheres ), $sql );

        $stmt = $this->dbh->prepare( $sql );

        if ($this->companyId !== null) {
            $stmt->bindParam( ':companyId', $this->companyId, PDO::PARAM_STR );
        }


        $stmt->bindParam( ':remoteId', $this->advertiserId, PDO::PARAM_STR );
        $stmt->execute();


        $advertisers = [];
        foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $advertiser) {
            $advertisers[$advertiser['company_id'] . $advertiser['internal_id']] = $advertiser;
        }
        return $advertisers;
    }

    public function getAdvertiserInfo()
    {
        $sql = <<<SQL
SELECT * FROM publishers JOIN remote_publishers ON publishers.id = remote_publishers.internal_id
WHERE publishers.id = :id
SQL;

        if ($this->token !== null) {
            $sql .= " AND company_id IN (" . implode(',', $this->projects) . ")";
        }

        $stmt = $this->dbh->prepare( $sql );

        $stmt->bindParam( ':id', $this->advertiserId, PDO::PARAM_STR );
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    private function getAdvertiserStatistics()
    {
        $query = <<<CLICKHOUSE
SELECT toDate(conversion_datetime) as date, project_id, internal_id, SUM(paid) AS revenue, SUM(paid) - SUM(earn) AS profit, SUM(earn) AS cost
FROM (
  SELECT *, internal_id FROM (
    SELECT * FROM {$this->database}.conversions WHERE (toDate(conversion_datetime) BETWEEN toDate('{$this->from->format( 'Y-m-d' )}') AND toDate('{$this->to->modify('- 1 day')->format( 'Y-m-d' )}'))
  ) AS c
  ANY INNER JOIN (
    SELECT *, company_id AS project_id, external_id AS internal_publisher_id, external_id AS publisher_id FROM {$this->database}.publishers
      ) AS a
  USING project_id, internal_publisher_id
)
:WHERE: 
GROUP BY (toDate(conversion_datetime), project_id, internal_id)
:HAVING:
ORDER BY toDate(conversion_datetime) ASC
CLICKHOUSE;

        $where = [];
        $where[] = "internal_id = '{$this->advertiserId}'";

        if (!is_null( $this->companyId )) {
            $where[] = "project_id = '{$this->companyId}'";
        }

        if ($this->country !== null) {
            $where[] = "country = '{$this->country}'";
        }

        if ($this->token !== null) {
            $where[] = "project_id IN (" . implode(',', $this->projects) . ")";
        }

        $query = str_replace( ':WHERE:', 'WHERE ' . implode( ' AND ', $where ), $query );

        $having = [];

        if (count( $having ) > 0) {
            $query = str_replace( ':HAVING:', 'HAVING ' . implode( ' AND ', $having ), $query );
        } else {
            $query = str_replace( ':HAVING:', '', $query );
        }

//        var_dump($query); exit;

        $result = $this->clickHouse->select( $query );

        $data = [];
        foreach ($result->fetchAll() as $result) {
            $data[$result->date][$result->project_id] = [
                'date' => $result->date,
                'id' => $result->internal_id,
                'company_id' => $result->project_id,
                'revenue' => $result->revenue,
                'profit' => $result->profit,
                'cost' => $result->cost,
            ];
        }

        return $data;
    }

    private function getCompanies()
    {
        $sql = <<<SQL
SELECT id, name FROM project
SQL;

        $stmt = $this->dbh->prepare( $sql );
        $stmt->execute();

        $companies = [];
        foreach ($stmt->fetchAll() as $company) {
            $companies[$company['id']] = [
                'id' => $company['id'],
                'name' => $company['name'],
            ];
        }

        return $companies;
    }

    private function getDatesSkeleton()
    {
        $period = new DatePeriod( $this->from, DateInterval::createFromDateString( '1 day' ), $this->to->modify( '+ 1 day' ) );

        $template = [
            "date" => null,
            "revenue" => 0,
            "income" => 0,
            "cost" => 0,
            "items" => []
        ];

        $dates = [];
        foreach ($period as $dateTime) {
            $temp = $template;

            $date = $dateTime->format( 'Y-m-d' );

            $dates[$date] = $temp;
            $dates[$date]['date'] = $date;
        }

        return $dates;
    }

    public function getProjects($token)
    {
        $sql = <<<SQL
SELECT * FROM member_project
WHERE member_id = '{$token}'
SQL;

        $stmt = $this->dbh->prepare( $sql );
        $stmt->execute();

        foreach ($stmt->fetchAll() as $project) {
            $this->projects[] = "'" . $project['project_id'] . "'";
        }
    }
}