<?php

namespace Orangear\BusinessIntelligence\Infrastructure\Query\ClientData;

use Doctrine\DBAL\Driver\PDOConnection;
use ClickHouse\Client as ClickHouseClient;
use Doctrine\ORM\EntityManagerInterface;
use DateTime;
use PDO;

class FindAdvertiserQuery
{
    /** @var PDOConnection */
    private $dbh;

    /** @var ClickHouseClient */
    private $clickHouse;

    private $from;
    private $to;
    private $companyId;

    private $amountFrom;
    private $amountTo;

    private $type;

    private $country;

    private $name;
    private $ids;

    private $token;
    private $projects;

    private $database = 'default';

    /**
     * BestPublisherChartQuery constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ClickHouseClient $clickHouse
     * @param string $database
     */
    public function __construct(EntityManagerInterface $entityManager, ClickHouseClient $clickHouse, $database)
    {
        $this->dbh = $entityManager->getConnection()->getWrappedConnection();
        $this->clickHouse = $clickHouse;
        $this->database = $database;
    }

    public function __invoke($companyId = null, $type = null, string $name, DateTime $from, DateTime $to, $amountFrom = null, $amountTo = null, $country = null, $ids = [], $token = null)
    {
        $this->from = $from;
        $this->to = $to;
        $this->companyId = $companyId;
        $this->amountFrom = $amountFrom;
        $this->amountTo = $amountTo;
        $this->country = $country;
        $this->name = $name;
        $this->type = $type;
        $this->ids = $ids;
        $this->token = $token;

        if (!in_array($this->type, ['profit', 'revenue', 'cost'])) {
            $this->type = 'profit';
        }

        if ($this->token !== null) {
            $this->getProjects($this->token);
        }

        $publishers = $this->findPublishers();
        $companies = $this->getCompanies();

        if (count($publishers) <= 0)
            return [];

        $data = $this->getPublisherData($publishers);

//        var_dump($publishers);
//        var_dump($data);
//        exit;

//        $companies = $this->getCompanies($publishers);

        $response = [];
        foreach ($publishers as $id => $publisher) {
            $answer = [];
            $answer['id'] = $id;
            $answer['title'] = $publisher[0]['company'] ?? null;
            $answer['amount'] = 0;

            if (isset($data[$id])) {
                foreach ($data[$id] as $money) {
                    $answer['amount'] += $money['amount'];
                    $answer['companies'][] = $companies[$money['project_id']];
                }
                $response[] = $answer;
            }
        }

        return $response;
    }

    public function findPublishers()
    {
        $publishers = [];

        $sql = <<<SQL
SELECT * FROM remote_advertisers
:WHERE: 
SQL;
        $wheres = [];

        $wheres[] = "company LIKE :name";

        if (!is_null( $this->companyId ) && !empty($this->companyId)) {
            $wheres[] = 'remote_advertisers.company_id = :companyId';
        } else if ($this->token !== null) {
            $wheres[] = 'remote_advertisers.company_id IN (' . implode( ',', $this->projects ) . ')';
        }

        if (count($this->ids) > 0) {
            $_ids = [];

            foreach ($this->ids as $exclude) {
                $userId = $exclude[0];

                $_ids[] = "'{$userId}'";
            }

            $wheres[] = "internal_id NOT IN (" . implode(',', $_ids) . ')';
        }

        $sql = str_replace( ':WHERE:', 'WHERE ' . implode( ' AND ', $wheres ), $sql );

        $stmt = $this->dbh->prepare( $sql );
        $this->name = '%' . $this->name . '%';
        $stmt->bindParam( ':name', $this->name, PDO::PARAM_STR );

        if (!is_null( $this->companyId ) && !empty($this->companyId)) {
            $stmt->bindParam( ':companyId', $this->companyId, PDO::PARAM_STR );
        }

        $stmt->execute();

        $ids = [];
        foreach ($stmt->fetchAll() as $pub) {
            $ids[] = $pub['internal_id'];
        }

        $sql = <<<SQL
SELECT * FROM remote_advertisers
:WHERE: 
SQL;
        $wheres = [];
        $wheres[] = "internal_id IN ('" . implode("','", $ids) . "')";

        if (!is_null( $this->companyId ) && !empty($this->companyId)) {
            $wheres[] = "company_id = '" . $this->companyId ."'";
        }

        $sql = str_replace( ':WHERE:', 'WHERE ' . implode( ' AND ', $wheres ), $sql );

        $stmt = $this->dbh->prepare( $sql );
        $stmt->execute();

        foreach ($stmt->fetchAll() as $pub) {
            $publishers[$pub['internal_id']][] = [
                'remote_id' => $pub['internal_id'],
                'email' => $pub['email'],
                'first_name' => $pub['first_name'],
                'last_name' => $pub['last_name'],
                'company' => $pub['company'],
                'company_id' => $pub['company_id'],
            ];
        }

        return $publishers;
    }

    private function getPublisherData(array $publishers)
    {
        $query = <<<CLICKHOUSE
SELECT project_id, internal_id, SUM(paid) AS revenue, SUM(paid) - SUM(earn) AS profit, SUM(earn) AS cost
FROM (
  SELECT *, internal_id FROM (
    SELECT * FROM {$this->database}.conversions WHERE (toDate(conversion_datetime) BETWEEN toDate('{$this->from->format( 'Y-m-d' )}') AND toDate('{$this->to->format( 'Y-m-d' )}'))
  ) AS c
  ANY INNER JOIN (
    SELECT *, company_id AS project_id, external_id AS advertiser_id FROM {$this->database}.advertisers
      ) AS a
  USING project_id, advertiser_id
)
:WHERE: 
GROUP BY (project_id, internal_id)
:HAVING:
CLICKHOUSE;


        $ids = [];
        foreach ($publishers as $group) {

            foreach ($group as $publisher) {
                $ids[] = "('{$publisher['remote_id']}', '{$publisher['company_id']}')";
            }


        }

        $where = [];
        $where[] = "(internal_id, project_id) IN (" . implode(',', $ids) . ")";

        if ($this->country !== null) {
            $where[] = "country = '{$this->country}'";
        }

        if ($this->token !== null) {
            $where[] = "project_id IN (" . implode(',', $this->projects) . ")";
        }

        $query = str_replace( ':WHERE:', 'WHERE ' . implode( ' AND ', $where ), $query );

        $having = [];
        if ($this->amountFrom !== null && !empty($this->amountFrom)) {
            $having[] = "{$this->type} >= {$this->amountFrom}";
        }

        if ($this->amountTo !== null && !empty($this->amountTo)) {
            $having[] = "{$this->type} <= {$this->amountTo}";
        }

        if (count( $having ) > 0) {
            $query = str_replace( ':HAVING:', 'HAVING ' . implode( ' AND ', $having ), $query );
        } else {
            $query = str_replace( ':HAVING:', '', $query );
        }

        $result = $this->clickHouse->select( $query );

        $data = [];
        foreach ($result->fetchAll() as $result) {
            $data[$result->internal_id][$result->project_id] = [
                'internal_id' => $result->internal_id,
                'project_id' => $result->project_id,
                'company_id' => $result->project_id,
                'amount' => $result->{$this->type},
            ];
        }

        return $data;
    }

    private function getCompanies()
    {
        $sql = <<<SQL
SELECT id, name FROM project
SQL;

        $stmt = $this->dbh->prepare( $sql );
        $stmt->execute();

        $companies = [];
        foreach ($stmt->fetchAll() as $company) {
            $companies[$company['id']] = [
                'id' => $company['id'],
                'name' => $company['name'],
            ];
        }

        return $companies;
    }

    public function getProjects($token)
    {
        $sql = <<<SQL
SELECT * FROM member_project
WHERE member_id = '{$token}'
SQL;

        $stmt = $this->dbh->prepare( $sql );
        $stmt->execute();

        foreach ($stmt->fetchAll() as $project) {
            $this->projects[] = "'" . $project['project_id'] . "'";
        }
    }
}