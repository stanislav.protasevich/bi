<?php

namespace Orangear\BusinessIntelligence\Infrastructure\Query\ClientData;

use Doctrine\DBAL\Driver\PDOConnection;
use ClickHouse\Client as ClickHouseClient;
use Doctrine\ORM\EntityManagerInterface;
use DateTime;
use PDO;

class WorstPublisherChartQuery
{

    /** @var PDOConnection */
    private $dbh;

    /** @var ClickHouseClient */
    private $clickHouse;

    private $from;
    private $to;
    private $companyId;

    private $type;
    private $amountFrom;
    private $amountTo;

    private $country;

    private $token;

    private $database = 'default';

    /**
     * BestPublisherChartQuery constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ClickHouseClient $clickHouse
     * @param string $database
     */
    public function __construct(EntityManagerInterface $entityManager, ClickHouseClient $clickHouse, $database)
    {
        $this->dbh = $entityManager->getConnection()->getWrappedConnection();
        $this->clickHouse = $clickHouse;
        $this->database = $database;
    }

    public function __invoke(DateTime $from, DateTime $to, $type = null, $companyId = null, $amountFrom = null, $amountTo = null, $country = null, $token = null)
    {
        $this->from = $from;
        $this->to = $to;
        $this->type = $type;
        $this->companyId = $companyId;
        $this->amountFrom = $amountFrom;
        $this->amountTo = $amountTo;
        $this->country = $country;
        $this->token = $token;

        if ($this->token !== null) {
            $this->getProjects($this->token);
        }

        if (!in_array($this->type, ['profit', 'revenue', 'cost'])) {
            $this->type = 'profit';
        }

        $companies = $this->getCompanies();

        if ($this->companyId === null) {
            $data = $this->getGlobalBestAdvertiserIdentifiers();
        } else {
            $data = $this->getBestAdvertiserIdentifiers();
        }

//        var_dump($data); exit;

        $ids = [];
        foreach ($data as $_data) {
            $ids[] = [
                'id' => $_data['advertiser_id'],
//                'company_id' => $_data['company_id'],
            ];
        }

        if ($this->companyId === null) {
            $_advertisers = $this->getCommonAdvertiserInfo( $ids );
        } else {
            $_advertisers = $this->getCommonAdvertiserInfoByCompany( $data );
        }

//        var_dump($_advertisers); exit;

        if ($this->companyId === null) {
            $advertisers = $this->getGlobalAdvertiserInfo( $ids );
        } else {
            $advertisers = $this->getAdvertiserInfo( $ids );
        }

//        var_dump($companies);
//        var_dump($data);
//        var_dump($advertisers);
//        exit;

        foreach ($advertisers as $key => $advertiser) {
            $data[$key]['advertiser_id'] = $advertiser[0]['internal_id'];

            foreach ($advertiser as $_advertiser) {
                $data[$key]['companies'][] = $companies[$_advertiser['company_id']];
            }
        }

//        var_dump($data);
//        exit;

        $exit = [];
        foreach ($data as $result) {
            $exit[] = [
                'id' => $result['advertiser_id'],
                'title' => $_advertisers[$result['advertiser_id']]['name'], //trim( $result['company'] ),
                'amount' => $result['amount'],
                'companies' => $result['companies'] ?? []
            ];
        }

        return $exit;
    }

    public function getAdvertiserInfo(array $ids)
    {
        if (empty($ids)) { return []; }

        $advertisers = [];

        $qwe = [];
        foreach ($ids as $_ids) {
            $qwe[] = "('" . $_ids['id'] . "', '" . $this->companyId . "')";
        }

        $sql = <<<SQL
SELECT * FROM remote_publishers
:WHERE: 
SQL;
        $wheres = [];

        if (count( $qwe ) > 0) {
            $wheres[] = "(external_id, company_id) IN (" . implode( $qwe, ',' ) . ")";
        }

        $sql = str_replace( ':WHERE:', 'WHERE ' . implode( ' AND ', $wheres ), $sql );

        $stmt = $this->dbh->prepare( $sql );

        $stmt->execute();

        foreach ($stmt->fetchAll() as $pub) {
            $advertisers[$pub['external_id']][] = [
                'company_id' => $pub['company_id'],
                'internal_id' => $pub['internal_id'],
                'company' => $pub['company'],
            ];
        }

        return $advertisers;
    }

    private function getBestAdvertiserIdentifiers()
    {
        $query = <<<CLICKHOUSE
SELECT internal_publisher_id AS advertiser_id, project_id, SUM(conversions.paid) AS revenue, SUM(conversions.paid) - SUM(conversions.earn) AS profit, SUM(conversions.earn) AS cost
FROM {$this->database}.conversions
:WHERE: 
GROUP BY internal_publisher_id, project_id
:HAVING:
ORDER BY {$this->type} ASC
LIMIT 10
CLICKHOUSE;

        $where = [];
        $where[] = "(toDate(conversion_datetime) BETWEEN toDate('{$this->from->format( 'Y-m-d' )}') AND toDate('{$this->to->format( 'Y-m-d' )}'))";

        if (!is_null( $this->companyId )) {
            $where[] = "project_id = '{$this->companyId}'";
        }

        if ($this->country !== null) {
            $where[] = "country = '{$this->country}'";
        }

        $query = str_replace( ':WHERE:', 'WHERE ' . implode( ' AND ', $where ), $query );

        $having = [];
        if ($this->amountFrom !== null) {
            $having[] = "{$this->type} >= {$this->amountFrom}";
        }

        if ($this->amountTo !== null) {
            $having[] = "{$this->type} <= {$this->amountTo}";
        }

        if (count( $having ) > 0) {
            $query = str_replace( ':HAVING:', 'HAVING ' . implode( ' AND ', $having ), $query );
        } else {
            $query = str_replace( ':HAVING:', '', $query );
        }

        $result = $this->clickHouse->select( $query );

        $data = [];
        foreach ($result->fetchAll() as $result) {
            $data[$result->advertiser_id] = [
                'advertiser_id' => $result->advertiser_id,
                'company_id' => $result->project_id,
                'amount' => $result->{$this->type},
            ];
        }

        return $data;
    }

    private function getGlobalBestAdvertiserIdentifiers()
    {
        $query = <<<CLICKHOUSE
SELECT internal_id, SUM(paid) AS revenue, SUM(paid) - SUM(earn) AS profit, SUM(earn) AS cost
FROM (
  SELECT *, internal_id FROM (
    SELECT * FROM {$this->database}.conversions WHERE (toDate(conversion_datetime) BETWEEN toDate('{$this->from->format( 'Y-m-d' )}') AND toDate('{$this->to->format( 'Y-m-d' )}'))
  ) AS c
  ANY INNER JOIN (
    SELECT *, company_id AS project_id, external_id AS internal_publisher_id, external_id AS publisher_id FROM {$this->database}.publishers
      ) AS a
  USING project_id, internal_publisher_id
)
:WHERE: 
GROUP BY (internal_id)
:HAVING:
ORDER BY {$this->type} ASC
LIMIT 10
CLICKHOUSE;

        $where = [];

        if ($this->country !== null) {
            $where[] = "country = '{$this->country}'";
        }

        if ($this->token !== null) {
            $where[] = "project_id IN (" . implode(',', $this->projects) . ")";
        }

        if (count($where) > 0) {
            $query = str_replace( ':WHERE:', 'WHERE ' . implode( ' AND ', $where ), $query );
        } else {
            $query = str_replace( ':WHERE:', '', $query );
        }

        $having = [];
        if ($this->amountFrom !== null) {
            $having[] = "{$this->type} >= {$this->amountFrom}";
        }

        if ($this->amountTo !== null) {
            $having[] = "{$this->type} <= {$this->amountTo}";
        }

        if (count( $having ) > 0) {
            $query = str_replace( ':HAVING:', 'HAVING ' . implode( ' AND ', $having ), $query );
        } else {
            $query = str_replace( ':HAVING:', '', $query );
        }

        $result = $this->clickHouse->select( $query );

        $data = [];
        foreach ($result->fetchAll() as $result) {
            $data[$result->internal_id] = [
                'advertiser_id' => $result->internal_id,
                'amount' => $result->{$this->type},
            ];
        }

        return $data;
    }

    private function getCompanies()
    {
        $sql = <<<SQL
SELECT id, name FROM project
SQL;

        $stmt = $this->dbh->prepare( $sql );
        $stmt->execute();

        $companies = [];
        foreach ($stmt->fetchAll() as $company) {
            $companies[$company['id']] = [
                'id' => $company['id'],
                'name' => $company['name'],
            ];
        }

        return $companies;
    }

    public function getGlobalAdvertiserInfo( $ids )
    {
        $query = <<<CLICKHOUSE
SELECT project_id, internal_id, SUM(paid) AS revenue, SUM(paid) - SUM(earn) AS profit, SUM(earn) AS cost
FROM (
  SELECT *, internal_id FROM (
    SELECT * FROM {$this->database}.conversions WHERE (toDate(conversion_datetime) BETWEEN toDate('{$this->from->format( 'Y-m-d' )}') AND toDate('{$this->to->format( 'Y-m-d' )}'))
  ) AS c
  ANY INNER JOIN (
    SELECT *, company_id AS project_id, external_id AS internal_publisher_id, external_id AS publisher_id FROM {$this->database}.publishers
      ) AS a
  USING project_id, internal_publisher_id
)
:WHERE: 
GROUP BY (project_id, internal_id)
:HAVING:
ORDER BY {$this->type} ASC
CLICKHOUSE;

        $where = [];

        if ($this->country !== null) {
            $where[] = "country = '{$this->country}'";
        }

        $qwe = array_map( function ($arr) {
            return "('" . implode( "', '", $arr ) . "')";
        }, $ids );

        $where[] = "internal_id IN (" . implode( $qwe, ',' ) . ")";

        if ($this->token !== null) {
            $where[] = "project_id IN (" . implode(',', $this->projects) . ")";
        }

        if (count($where) > 0) {
            $query = str_replace( ':WHERE:', 'WHERE ' . implode( ' AND ', $where ), $query );
        } else {
            $query = str_replace( ':WHERE:', '', $query );
        }

        $having = [];
        if ($this->amountFrom !== null) {
            $having[] = "{$this->type} >= {$this->amountFrom}";
        }

        if ($this->amountTo !== null) {
            $having[] = "{$this->type} <= {$this->amountTo}";
        }


        if (count( $having ) > 0) {
            $query = str_replace( ':HAVING:', 'HAVING ' . implode( ' AND ', $having ), $query );
        } else {
            $query = str_replace( ':HAVING:', '', $query );
        }

        $result = $this->clickHouse->select( $query );

//        var_dump($query); exit;

        $data = [];
        foreach ($result->fetchAll() as $result) {
            $data[] = [
                'project_id' => $result->project_id,
                'advertiser_id' => $result->internal_id,
            ];
        }

        $qwe = array_map( function ($arr) {
            return "('" . implode( "', '", $arr ) . "')";
        }, $data );

        $qwe = implode( $qwe, ',' );

        $sql = <<<SQL
SELECT company_id, internal_id, company FROM remote_publishers
WHERE (company_id, internal_id) IN ($qwe)
SQL;
//e9f2a59a-024c-4950-bed5-e6cde675080a
//        var_dump($qwe); exit;

        if ($this->token !== null) {
            $sql .= " AND company_id IN (" . implode(',', $this->projects) . ")";
        }

        $stmt = $this->dbh->prepare( $sql );
        $stmt->execute();

        $companies = [];
        foreach ($stmt->fetchAll() as $company) {
            $companies[$company['internal_id']][] = [
                'company_id' => $company['company_id'],
                'internal_id' => $company['internal_id'],
                'name' => $company['company'],
            ];
        }

        return $companies;

    }

    public function getCommonAdvertiserInfo( $ids )
    {
        $qwe = array_map( function ($arr) {
            return implode( "', '", $arr );
        }, $ids );

        $qwe = "'" . implode( $qwe, "','" ) . "'";

        $sql = <<<SQL
SELECT * FROM publishers JOIN remote_publishers ON publishers.id = remote_publishers.internal_id
WHERE publishers.id IN ($qwe)
SQL;

        if ($this->token !== null) {
            $where[] = "company_id IN (" . implode(',', $this->projects) . ")";
        }

        $stmt = $this->dbh->prepare( $sql );
        $stmt->execute();

        $advertisers = [];
        foreach ($stmt->fetchAll() as $advertiser) {
            $advertisers[$advertiser['internal_id']] = [
                'name' => $advertiser['name']
            ];
        }

        return $advertisers;
    }

    public function getCommonAdvertiserInfoByCompany( $data )
    {
        $qwe = [];
        foreach ($data as $_data) {
            $qwe[]= "'" . $_data['advertiser_id'] . "'";
        }



        $qwe = implode( $qwe, "," );

        $sql = <<<SQL
SELECT * FROM publishers
WHERE id IN (
  SELECT internal_id FROM remote_publishers WHERE company_id = '{$this->companyId}' AND external_id IN ($qwe)
)
SQL;

        $stmt = $this->dbh->prepare( $sql );
        $stmt->execute();

        $advertisers = [];
        foreach ($stmt->fetchAll() as $advertiser) {
            $advertisers[$advertiser['id']] = [
                'name' => $advertiser['name']
            ];
        }

        return $advertisers;
    }

    public function getProjects($token)
    {
        $sql = <<<SQL
SELECT * FROM member_project
WHERE member_id = '{$token}'
SQL;

        $stmt = $this->dbh->prepare( $sql );
        $stmt->execute();

        foreach ($stmt->fetchAll() as $project) {
            $this->projects[] = "'" . $project['project_id'] . "'";
        }
    }
}