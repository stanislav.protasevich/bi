<?php

namespace Orangear\BusinessIntelligence\Infrastructure\Query\BillingReport;

use Doctrine\DBAL\Driver\PDOConnection;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;

final class GetBillingReportQuery
{
    /** @var PDOConnection */
    private $dbh;

    /**
     * GetBillingReportQuery constructor.
     * @param PDOConnection $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->dbh = $entityManager->getConnection()->getWrappedConnection();
    }

    public function __invoke($ids = [], $startDate = null, $endDate = null)
    {
        if (count( $ids ) <= 0) {
            throw new \Exception( 'Contact Developer' );
        }

        if (!is_null( $startDate )) {
            $startDate = new \DateTime( $startDate );
        }

        if (!is_null( $endDate )) {
            $endDate = new \DateTime( $endDate );
        }

        $companies = $this->getCompanies( $ids );
        $salaries = $this->getSalaries( $ids, $startDate, $endDate );
        $expenses = $this->getExpenses( $ids, $startDate, $endDate );
        $reports = $this->getReports( $ids, $startDate, $endDate );

        $totals = [];
        $totals['revenue'] = 0;
        $totals['revenue_received'] = 0;
        $totals['cross_payment'] = 0;
        $totals['deduction'] = 0;
        $totals['revenue_unpaid'] = 0;
        $totals['publisher_amount'] = 0;
        $totals['publisher_paid'] = 0;
        $totals['publisher_unpaid'] = 0;
        $totals['gross_income'] = 0;

        $totals['net_revenue'] = 0;
        $totals['net_income'] = 0;

        $totals['salaries'] = 0;
        $totals['other_expenses'] = 0;
        $totals['total_expenses'] = 0;

        $totals['gross_profit'] = 0;
        $totals['net_profit'] = 0;
        $totals['discrepancy'] = 0;
        $totals['net_margin'] = 0;
        $totals['our_amount'] = 0;

        $data = [];
        foreach ($reports as $report) {
            $totals['revenue'] += $temp['revenue'] = (float)$report['revenue'];
            $totals['revenue_received'] += $temp['revenue_received'] = (float)$report['revenue_received'];
            $totals['cross_payment'] += $temp['cross_payment'] = (float)$report['cross_payment'];
            $totals['deduction'] += $temp['deduction'] = (float)$report['deduction'];
            $totals['revenue_unpaid'] += $temp['revenue_unpaid'] = (float)$report['revenue_unpaid'];
            $totals['publisher_amount'] += $temp['publisher_amount'] = (float)$report['publisher_amount'];
            $totals['publisher_paid'] += $temp['publisher_paid'] = (float)$report['publisher_paid'];
            $totals['publisher_unpaid'] += $temp['publisher_unpaid'] = (float)$report['publisher_unpaid'];
            $totals['gross_income'] += $temp['gross_income'] = (float)$report['gross_income'];

            $totals['net_revenue'] += $temp['net_revenue'] = $temp['revenue'] - $temp['cross_payment'] - $temp['deduction'];
            $totals['net_income'] += $temp['net_income'] = $temp['revenue_received'] - $temp['publisher_paid'];

            $temp['salaries'] = 0;

            $_dateYear = date( 'Y', strtotime( $report['date'] ) );
            $_dateMonth = date( 'm', strtotime( $report['date'] ) );

            if (isset( $salaries[$report['project_id']][$_dateYear][$_dateMonth] )) {
                $totals['salaries'] += $temp['salaries'] = (float)$salaries[$report['project_id']][$_dateYear][$_dateMonth];
            }

            $temp['other_expenses'] = 0;
            if (isset( $expenses[$report['project_id']][$_dateYear][$_dateMonth] )) {
                $totals['other_expenses'] += $temp['other_expenses'] = (float)$expenses[$report['project_id']][$_dateYear][$_dateMonth];
            }

            $totals['total_expenses'] += $temp['total_expenses'] = $temp['salaries'] + $temp['other_expenses'];

            $totals['gross_profit'] += $temp['gross_profit'] = 0;
            $totals['net_profit'] += $temp['net_profit'] = $temp['net_income'] - $temp['total_expenses'];
            $totals['discrepancy'] += $temp['discrepancy'] = 0;

            if ($temp['net_revenue'] != 0) {
                $totals['net_margin'] += $temp['net_margin'] = $temp['net_income'] / $temp['net_revenue'] * 100;
            } else {
                $totals['net_margin'] += $temp['net_margin'] = 0;
            }

            /** @todo Our amount */
            $totals['our_amount'] += $temp['our_amount'] = 0;
            $totals['discrepancy'] += $temp['discrepancy'] = $temp['revenue'] - $temp['our_amount'];

            $temp['company'] = $companies[$report['project_id']];

            $temp['date_period'] = [];
            $temp['date_period']['start_date'] = date('Y-m-01', strtotime($report['date']));
            $temp['date_period']['end_date'] = date('Y-m-t', strtotime($report['date']));

            $data['reports'][] = $temp;

            $data['total'] = $totals;

        }

        return $data;
    }

    private function getCompanies($ids = [])
    {
        $sql = <<< SQL
SELECT *
FROM project
:WHERE:
SQL;

        $wheres = [];
        $wheres[] = 'id IN (' . "'" . implode( "','", $ids ) . "'" . ")";
        $sql = str_replace( ':WHERE:', 'WHERE ' . implode( ' AND ', $wheres ), $sql );

        $stmt = $this->dbh->prepare( $sql );
        $stmt->execute();

        $companies = [];

        foreach ($stmt->fetchAll(\PDO::FETCH_ASSOC) as $company) {
            $companies[$company['id']] = [
                'id' => $company['id'],
                'name' => $company['name']
            ];
        }

        return $companies;
    }

    private function getSalaries($ids = [], \DateTime $startDate = null, \DateTime $endDate = null)
    {
        $sql = <<< SQL
SELECT project_id, SUM(amount) AS salaries, DATE_FORMAT(start_date, '%Y') AS `year`, DATE_FORMAT(start_date, '%m') AS `month`
FROM charge
:WHERE:
GROUP BY project_id, start_date DESC
SQL;

        $wheres = [];

        $wheres[] = 'project_id IN (' . "'" . implode( "','", $ids ) . "'" . ")";
        $wheres[] = 'charge_type_id IN (SELECT charge_type.id FROM charge_type WHERE `key` = \'salary\')';

        if ($startDate !== null) {
            $wheres[] = 'start_date >= \'' . $startDate->format( 'Y-m-d 00:00:00' ) . '\'';

        }

        if ($endDate !== null) {
            $wheres[] = 'end_date <= \'' . $endDate->format( 'Y-m-t 23:59:59' ) . '\'';
        }

        $sql = str_replace( ':WHERE:', 'WHERE ' . implode( ' AND ', $wheres ), $sql );

        $stmt = $this->dbh->prepare( $sql );
        $stmt->execute();

        $data = [];
        foreach ($stmt->fetchAll() as $charge) {
            $data[$charge['project_id']][$charge['year']][$charge['month']] = $charge['salaries'];
        }

        return $data;
    }

    private function getExpenses($ids = [], \DateTime $startDate = null, \DateTime $endDate = null)
    {
        $sql = <<< SQL
SELECT project_id, SUM(amount) AS salaries, DATE_FORMAT(start_date, '%Y') AS `year`, DATE_FORMAT(start_date, '%m') AS `month`
FROM charge
:WHERE:
GROUP BY project_id, start_date DESC
SQL;

        $wheres = [];

        $wheres[] = 'project_id IN (' . "'" . implode( "','", $ids ) . "'" . ")";
        $wheres[] = 'charge_type_id IN (SELECT charge_type.id FROM charge_type WHERE `key` <> \'salary\')';

        if ($startDate !== null) {
            $wheres[] = 'start_date >= \'' . $startDate->format( 'Y-m-d 00:00:00' ) . '\'';

        }

        if ($endDate !== null) {
            $wheres[] = 'end_date <= \'' . $endDate->format( 'Y-m-t 23:59:59' ) . '\'';
        }

        $sql = str_replace( ':WHERE:', 'WHERE ' . implode( ' AND ', $wheres ), $sql );

        $stmt = $this->dbh->prepare( $sql );
        $stmt->execute();

        $data = [];
        foreach ($stmt->fetchAll() as $charge) {
            $data[$charge['project_id']][$charge['year']][$charge['month']] = $charge['salaries'];
        }

        return $data;
    }

    private function getReports($ids = [], \DateTime $startDate = null, \DateTime $endDate = null)
    {
        $sql = <<< SQL
SELECT *
FROM billing_report
:WHERE:
ORDER BY `date` DESC
SQL;

        $wheres = [];

        $wheres[] = 'project_id IN (' . "'" . implode( "','", $ids ) . "'" . ")";

        if ($startDate !== null) {
            $wheres[] = 'date >= \'' . $startDate->format( 'Y-m-d 00:00:00' ) . '\'';
        }

        if ($endDate !== null) {
            $wheres[] = 'date <= \'' . $endDate->format( 'Y-m-t 23:59:59' ) . '\'';
        }

        $sql = str_replace( ':WHERE:', 'WHERE ' . implode( ' AND ', $wheres ), $sql );

        $stmt = $this->dbh->prepare( $sql );
        $stmt->execute();

        return $stmt->fetchAll( \PDO::FETCH_ASSOC );
    }
}