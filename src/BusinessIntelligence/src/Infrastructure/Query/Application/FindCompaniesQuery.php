<?php

namespace Orangear\BusinessIntelligence\Infrastructure\Query\Application;

use Doctrine\DBAL\Driver\PDOConnection;
use Doctrine\ORM\EntityManagerInterface;
use Orangear\BusinessIntelligence\Domain\Model\Application\Query\FindCompaniesQueryInterface;

/**
 * Class FindCompaniesQuery
 * @package Orangear\BusinessIntelligence\Infrastructure\Query\Application
 */
final class FindCompaniesQuery implements FindCompaniesQueryInterface
{
    /** @var PDOConnection */
    private $dbh;

    /**
     * FindTokenQuery constructor.
     * @param PDOConnection $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->dbh = $entityManager->getConnection()->getWrappedConnection();
    }

    public function __invoke($ids)
    {
        if (count($ids) <= 0) {
            throw new \Exception('Contact developer');
        }

        $sql = <<< SQL
SELECT id, name FROM project WHERE id IN (:identifiers) ORDER BY name ASC
SQL;
        $sql = str_replace(':identifiers', "'" . implode("','", $ids) . "'", $sql);

        $stmt = $this->dbh->prepare($sql);
        $stmt->execute();

        $companies = $stmt->fetchAll( \PDO::FETCH_ASSOC );

        if (!$companies) {
            throw new \Exception('Invalid companies');
        }

        $data = [];
        foreach ($companies as $company) {
            $data[] = [
                'id' => $company['id'],
                'name' => $company['name'],
            ];
        }

        return ['companies' =>$data];
    }
}
