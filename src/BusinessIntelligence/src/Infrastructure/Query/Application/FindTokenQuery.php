<?php

namespace Orangear\BusinessIntelligence\Infrastructure\Query\Application;

use Doctrine\DBAL\Driver\PDOConnection;
use Doctrine\ORM\EntityManagerInterface;
use Orangear\BusinessIntelligence\Domain\Model\Application\Application;
use Orangear\BusinessIntelligence\Domain\Model\Application\Query\FindTokenQueryInterface;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;

/**
 * Class FindTokenQuery
 * @package Orangear\BusinessIntelligence\Infrastructure\Query\Application
 */
final class FindTokenQuery implements FindTokenQueryInterface
{
    /** @var PDOConnection */
    private $dbh;

    /**
     * FindTokenQuery constructor.
     * @param PDOConnection $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->dbh = $entityManager->getConnection()->getWrappedConnection();
    }

    public function __invoke($token)
    {
        $sql = <<< SQL
SELECT * FROM application WHERE token = :token LIMIT 1
SQL;

        $stmt = $this->dbh->prepare( $sql );
        $stmt->bindParam( ':token', $token, \PDO::PARAM_STR );
        $stmt->execute();

        $application = $stmt->fetch( \PDO::FETCH_ASSOC );

        if (!$application) {
            throw new \Exception('Invalid Token');
        }

        return Application::fromArray(
            (int) $application['id'],
            $application['token'],
            $application['access'],
            $application['notes']
        );
    }
}
