<?php

namespace Orangear\BusinessIntelligence\Infrastructure\Query\Dashboard;

use Doctrine\DBAL\Driver\PDOConnection;
use Doctrine\ORM\EntityManagerInterface;

final class GetDashboardQuery
{
    /** @var PDOConnection */
    private $dbh;

    /**
     * GetDashboardQuery constructor.
     * @param PDOConnection $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->dbh = $entityManager->getConnection()->getWrappedConnection();
    }

    public function __invoke($ids = [])
    {
        $sql = "
            SELECT
              SUM(dashboard.income_today) AS incomeToday,
              SUM(dashboard.income_yesterday) AS incomeYesterday,
              SUM(dashboard.income_last_seven_days) AS incomeLastSevenDays,
              SUM(dashboard.income_this_month) AS incomeThisMonth,
              SUM(dashboard.income_last_month) AS incomeLastMonth,
              SUM(dashboard.income_all_time) AS incomeAllTime,
              
              SUM(dashboard.income_superlink_today) AS incomeSuperlinkToday,
              SUM(dashboard.income_superlink_yesterday) AS incomeSuperlinkYesterday,
              SUM(dashboard.income_superlink_last_seven_days) AS incomeSuperlinkLastSevenDays,
              SUM(dashboard.income_superlink_this_month) AS incomeSuperlinkThisMonth,
              SUM(dashboard.income_superlink_last_month) AS incomeSuperlinkLastMonth,
              SUM(dashboard.income_superlink_all_time) AS incomeSuperlinkAllTime,
              
              SUM(dashboard.payout_today) AS payoutToday,
              SUM(dashboard.payout_yesterday) AS payoutYesterday,
              SUM(dashboard.payout_last_seven_days) AS payoutLastSevenDays,
              SUM(dashboard.payout_this_month) AS payoutThisMonth,
              SUM(dashboard.payout_last_month) AS payoutLastMonth,
              SUM(dashboard.payout_all_time) AS payoutAllTime,
              
              SUM(dashboard.offers_active) AS offersActive,
              SUM(dashboard.offers_private) AS offersPrivate,
              SUM(dashboard.offers_suspended) AS offersSuspended,
              SUM(dashboard.offers_incent) AS offersIncent,
              SUM(dashboard.offers_non_incent) AS offersNonIncent,
              SUM(dashboard.offers_total) AS offersTotal,
              SUM(dashboard.offers_per_day) AS offersPerDay,
              
              SUM(dashboard.revenue_superlink_today) AS revenueSuperlinkToday,
              SUM(dashboard.revenue_superlink_yesterday) AS revenueSuperlinkYesterday,
              SUM(dashboard.revenue_superlink_last_seven_days) AS revenueSuperlinkLastSevenDays,
              SUM(dashboard.revenue_superlink_this_month) AS revenueSuperlinkThisMonth,
              SUM(dashboard.revenue_superlink_last_month) AS revenueSuperlinkLastMonth,
              SUM(dashboard.revenue_superlink_all_time) AS revenueSuperlinkAllTime
            FROM project
              JOIN dashboard ON dashboard.project_id = project.id            
        ";

        $where = [];
        $where[] = "(dashboard.project_id, UNIX_TIMESTAMP(dashboard.synchronized_at)) IN (SELECT project_id, MAX(UNIX_TIMESTAMP(dashboard.synchronized_at)) AS synchronized_at FROM dashboard GROUP BY dashboard.project_id)";

        if (!empty($ids)) {
            $where[] = "project.id IN ('" . implode('\',\'', $ids) . "')";
        }

        $sql .= ' WHERE ' . implode(' AND ', $where);

        $sql .= ' ORDER BY project.name ASC';

        $stmt = $this->dbh->prepare($sql);
        $stmt->execute();

        $dashboard = $stmt->fetch(\PDO::FETCH_ASSOC);

        if (!$dashboard) {
            throw new \Exception('No dashboard');
        }

        $data = [];

        $data['dashboard']['income_today'] = (float) $dashboard['incomeToday'] ?? 0;
        $data['dashboard']['income_yesterday'] = (float) $dashboard['incomeYesterday'] ?? 0;
        $data['dashboard']['income_last_week'] = (float) $dashboard['incomeLastSevenDays'] ?? 0;
        $data['dashboard']['income_this_month'] = (float) $dashboard['incomeThisMonth'] ?? 0;
        $data['dashboard']['income_last_month'] = (float) $dashboard['incomeLastMonth'] ?? 0;
        $data['dashboard']['income_all_time'] = (float) $dashboard['incomeAllTime'] ?? 0;

        $data['dashboard']['income_superlink_today'] = (float) $dashboard['incomeSuperlinkToday'] ?? 0;
        $data['dashboard']['income_superlink_yesterday'] = (float) $dashboard['incomeSuperlinkYesterday'] ?? 0;
        $data['dashboard']['income_superlink_last_week'] = (float) $dashboard['incomeSuperlinkLastSevenDays'] ?? 0;
        $data['dashboard']['income_superlink_this_month'] = (float) $dashboard['incomeSuperlinkThisMonth'] ?? 0;
        $data['dashboard']['income_superlink_last_month'] = (float) $dashboard['incomeSuperlinkLastMonth'] ?? 0;
        $data['dashboard']['income_superlink_all_time'] = (float) $dashboard['incomeSuperlinkAllTime'] ?? 0;

        $data['dashboard']['payout_today'] = (float) $dashboard['payoutToday'] ?? 0;
        $data['dashboard']['payout_yesterday'] = (float) $dashboard['payoutYesterday'] ?? 0;
        $data['dashboard']['payout_last_week'] = (float) $dashboard['payoutLastSevenDays'] ?? 0;
        $data['dashboard']['payout_this_month'] = (float) $dashboard['payoutThisMonth'] ?? 0;
        $data['dashboard']['payout_last_month'] = (float) $dashboard['payoutLastMonth'] ?? 0;
        $data['dashboard']['payout_all_time'] = (float) $dashboard['payoutAllTime'] ?? 0;

        $data['dashboard']['total_revenue_today'] = (float) $dashboard['revenueSuperlinkToday'] + $data['payoutToday'] ?? $data['payoutToday'];
        $data['dashboard']['total_revenue_yesterday'] = (float) $dashboard['revenueSuperlinkYesterday'] + $data['payoutYesterday'] ?? $data['payoutYesterday'];
        $data['dashboard']['total_revenue_last_week'] = (float) $dashboard['revenueSuperlinkLastSevenDays'] + $data['payoutLastSevenDays'] ?? $data['payoutLastSevenDays'];
        $data['dashboard']['total_revenue_this_month'] = (float) $dashboard['revenueSuperlinkThisMonth'] + $data['payoutThisMonth'] ?? $data['payoutThisMonth'];
        $data['dashboard']['total_revenue_last_month'] = (float) $dashboard['revenueSuperlinkLastMonth'] + $data['payoutLastMonth'] ?? $data['payoutLastMonth'];
        $data['dashboard']['total_revenue_all_time'] = (float) $dashboard['revenueSuperlinkAllTime'] + $data['payoutAllTime'] ?? $data['payoutAllTime'];

        $data['dashboard']['offers_active'] = (int) $dashboard['offersActive'] ?? 0;
        $data['dashboard']['offers_private'] = (int) $dashboard['offersPrivate'] ?? 0;
        $data['dashboard']['offers_suspended'] = (int) $dashboard['offersSuspended'] ?? 0;
        $data['dashboard']['offers_incent'] = (int) $dashboard['offersIncent'] ?? 0;
        $data['dashboard']['offers_non_incent'] = (int) $dashboard['offersNonIncent'] ?? 0;
        $data['dashboard']['offers_total'] = (int) $dashboard['offersTotal'] ?? 0;
        $data['dashboard']['offers_per_day'] = (int) $dashboard['offersPerDay'] ?? 0;

        return $data;
    }
}