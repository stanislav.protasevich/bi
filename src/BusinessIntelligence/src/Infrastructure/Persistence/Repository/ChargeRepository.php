<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository;

use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeInterface;
use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeMapperInterface;
use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeRepositoryInterface;
use Ramsey\Uuid\Uuid;

/**
 * Class ChargeRepository
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository
 */
final class ChargeRepository implements ChargeRepositoryInterface
{
    /** @var ChargeMapperInterface */
    private $mapper;

    /**
     * ChargeRepository constructor
     *
     * @param ChargeMapperInterface $mapper
     */
    public function __construct(ChargeMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * {@inheritdoc}
     */
    public function findById(ChargeIdentifierInterface $ChargeIdentifier):? ChargeInterface
    {
        return $this->mapper->fetchById($ChargeIdentifier);
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(array $conditions = [])
    {
        return $this->mapper->fetchAll($conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function findOneBy(array $conditions = [])
    {
        return $this->mapper->fetchOneBy($conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function create(ChargeInterface $Charge)
    {
        $this->mapper->create($Charge);
    }

    /**
     * {@inheritdoc}
     */
    public function update(ChargeInterface $Charge)
    {
        $this->mapper->update($Charge);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(ChargeInterface $Charge)
    {
        $this->mapper->delete($Charge);
    }

    /**
     * {@inheritdoc}
     */
    public function nextIdentifier(): ChargeIdentifierInterface
    {
        return  ChargeIdentifier::fromString(Uuid::uuid4()->toString());
    }
}
