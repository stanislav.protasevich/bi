<?php

declare(strict_types=1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository;

use Orangear\BusinessIntelligence\Domain\Model\Dashboard\DashboardId;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\DashboardIdInterface;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\DashboardInterface;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\DashboardMapperInterface;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\DashboardRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\IdentifierInterface;

/**
 * Class DashboardRepository
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository
 */
class DashboardRepository implements DashboardRepositoryInterface
{
    /** @var DashboardMapperInterface */
    private $mapper;

    /**
     * DashboardRepository constructor
     *
     * @param DashboardMapperInterface $mapper
     */
    public function __construct(DashboardMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * {@inheritdoc}
     */
    public function findById(IdentifierInterface $identifier)
    {
        /** @var $identifier DashboardIdInterface */

        return $this->mapper->fetchById( $identifier );
    }

    /**
     * {@inheritdoc}
     */
    public function findOneBy(array $parameters): ?DashboardInterface
    {
        return $this->mapper->fetchOneBy( $parameters );
    }

    /**
     * {@inheritdoc}
     */
    public function create(DashboardInterface $dashboard)
    {
        $this->mapper->create( $dashboard );
    }

    /**
     * {@inheritdoc}
     */
    public function nextIdentifier(): DashboardIdInterface
    {
        return DashboardId::fromString();
    }
}
