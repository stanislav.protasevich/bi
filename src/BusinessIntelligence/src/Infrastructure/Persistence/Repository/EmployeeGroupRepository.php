<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository;

use Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\EmployeeGroupIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\EmployeeGroupIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\EmployeeGroupInterface;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\EmployeeGroupRepositoryInterface;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper\DoctrineEmployeeGroupMapper;
use Ramsey\Uuid\Uuid;

/**
 * Class EmployeeGroupRepository
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository
 */
class EmployeeGroupRepository implements EmployeeGroupRepositoryInterface
{
    /** @var DoctrineEmployeeGroupMapper */
    private $mapper;

    /**
     * EmployeeGroupRepository constructor
     *
     * @param DoctrineEmployeeGroupMapper $mapper
     */
    public function __construct(DoctrineEmployeeGroupMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * {@inheritdoc}
     */
    public function findById(EmployeeGroupIdentifierInterface $EmployeeGroupIdentifier):? EmployeeGroupInterface
    {
        return $this->mapper->fetchById($EmployeeGroupIdentifier);
    }

    /**
     * {@inheritdoc}
     */
    public function findByName(string $name):? EmployeeGroupInterface
    {
        return $this->mapper->fetchByName($name);
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(array $conditions = [])
    {
        return $this->mapper->fetchAll($conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function create(EmployeeGroupInterface $EmployeeGroup)
    {
        $this->mapper->create($EmployeeGroup);
    }

    /**
     * {@inheritdoc}
     */
    public function update(EmployeeGroupInterface $EmployeeGroup)
    {
        $this->mapper->update($EmployeeGroup);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(EmployeeGroupInterface $EmployeeGroup)
    {
        $this->mapper->delete($EmployeeGroup);
    }

    /**
     * {@inheritdoc}
     */
    public function nextIdentifier(): EmployeeGroupIdentifierInterface
    {
        return  EmployeeGroupIdentifier::fromString(Uuid::uuid4()->toString());
    }
}
