<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository;

use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployeeIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployeeIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployeeInterface;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployeeRepositoryInterface;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper\DoctrineRemoteEmployeeMapper;
use Ramsey\Uuid\Uuid;

/**
 * Class RemoteEmployeeRepository
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository
 */
class RemoteEmployeeRepository implements RemoteEmployeeRepositoryInterface
{
    /** @var DoctrineRemoteEmployeeMapper */
    private $mapper;

    /**
     * RemoteEmployeeRepository constructor
     *
     * @param DoctrineRemoteEmployeeMapper $mapper
     */
    public function __construct(DoctrineRemoteEmployeeMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * {@inheritdoc}
     */
    public function findById(RemoteEmployeeIdentifierInterface $RemoteEmployeeIdentifier):? RemoteEmployeeInterface
    {
        return $this->mapper->fetchById($RemoteEmployeeIdentifier);
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(array $conditions = [])
    {
        return $this->mapper->fetchAll($conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function create(RemoteEmployeeInterface $RemoteEmployee)
    {
        $this->mapper->create($RemoteEmployee);
    }

    /**
     * {@inheritdoc}
     */
    public function update(RemoteEmployeeInterface $RemoteEmployee)
    {
        $this->mapper->update($RemoteEmployee);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(RemoteEmployeeInterface $RemoteEmployee)
    {
        $this->mapper->delete($RemoteEmployee);
    }

    /**
     * {@inheritdoc}
     */
    public function nextIdentifier(): RemoteEmployeeIdentifierInterface
    {
        return RemoteEmployeeIdentifier::fromString(Uuid::uuid4()->toString());
    }
}
