<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository;

use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeInterface;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeRepositoryInterface;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper\DoctrineEmployeeMapper;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper\DoctrineRemoteEmployeeMapper;
use Ramsey\Uuid\Uuid;

/**
 * Class EmployeeRepository
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository
 */
class EmployeeRepository implements EmployeeRepositoryInterface
{
    /** @var DoctrineEmployeeMapper */
    private $mapper;

    /**
     * EmployeeRepository constructor
     *
     * @param DoctrineEmployeeMapper $mapper
     */
    public function __construct(DoctrineEmployeeMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * {@inheritdoc}
     */
    public function findById(EmployeeIdentifierInterface $identifier):? EmployeeInterface
    {
        return $this->mapper->fetchById($identifier);
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(array $conditions = [])
    {
        return $this->mapper->fetchAll($conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function create(EmployeeInterface $employee)
    {
        $this->mapper->create($employee);
    }

    /**
     * {@inheritdoc}
     */
    public function update(EmployeeInterface $employee)
    {
        $this->mapper->update($employee);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(EmployeeInterface $employee)
    {
        $this->mapper->delete($employee);
    }

    /**
     * {@inheritdoc}
     */
    public function nextIdentifier(): EmployeeIdentifierInterface
    {
        return EmployeeIdentifier::fromString(Uuid::uuid4()->toString());
    }
}
