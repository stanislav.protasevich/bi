<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository;

use Orangear\BusinessIntelligence\Domain\Model\Admin\AdminInterface;
use Orangear\BusinessIntelligence\Domain\Model\Admin\AdminRepositoryInterface;
use Orangear\BusinessIntelligence\src\Domain\Model\Admin\Exception\AdminNotFound;
use Orangear\MembershipBundle\Domain\Model\Member\MemberIdentifierInterface;
use Orangear\MembershipBundle\Infrastructure\Persistence\Repository\MemberRepository;

/**
 * Class AdminRepository
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository
 */
final class AdminRepository extends MemberRepository implements AdminRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAdminByIdOrFail(MemberIdentifierInterface $identifier) : AdminInterface
    {
        /** @var AdminInterface $admin */
        $admin = $this->mapper->fetchById($identifier);

        if ($admin === null) {
            throw AdminNotFound::withMemberId($identifier);
        }

        return $admin;
    }
}