<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository;

use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeInterface;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeRepositoryInterface;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper\DoctrineChargeTypeMapper;
use Ramsey\Uuid\Uuid;

/**
 * Class ChargeTypeRepository
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository
 */
class ChargeTypeRepository implements ChargeTypeRepositoryInterface
{
    /** @var DoctrineChargeTypeMapper */
    private $mapper;

    /**
     * ChargeTypeRepository constructor
     *
     * @param DoctrineChargeTypeMapper $mapper
     */
    public function __construct(DoctrineChargeTypeMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * {@inheritdoc}
     */
    public function findById(ChargeTypeIdentifierInterface $chargeTypeIdentifier):? ChargeTypeInterface
    {
        return $this->mapper->fetchById($chargeTypeIdentifier);
    }

    /**
     * {@inheritdoc}
     */
    public function findByName(string $name):? ChargeTypeInterface
    {
        return $this->mapper->fetchByName($name);
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(array $conditions = [])
    {
        return $this->mapper->fetchAll($conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function findOneBy(array $conditions = [])
    {
        return $this->mapper->fetchOneBy($conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function create(ChargeTypeInterface $chargeType)
    {
        $this->mapper->create($chargeType);
    }

    /**
     * {@inheritdoc}
     */
    public function update(ChargeTypeInterface $chargeType)
    {
        $this->mapper->update($chargeType);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(ChargeTypeInterface $chargeType)
    {
        $this->mapper->delete($chargeType);
    }

    /**
     * {@inheritdoc}
     */
    public function nextIdentifier(): ChargeTypeIdentifierInterface
    {
        return  ChargeTypeIdentifier::fromString(Uuid::uuid4()->toString());
    }
}
