<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository;

use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeChargeIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeChargeIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeChargeInterface;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeChargeRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeChargeMapperInterface;
use Ramsey\Uuid\Uuid;

/**
 * Class EmployeeChargeRepository
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository
 */
final class EmployeeChargeRepository implements EmployeeChargeRepositoryInterface
{
    /** @var EmployeeChargeMapperInterface */
    private $mapper;

    /**
     * ChargeRepository constructor
     *
     * @param EmployeeChargeMapperInterface $mapper
     */
    public function __construct(EmployeeChargeMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * {@inheritdoc}
     */
    public function findById(EmployeeChargeIdentifierInterface $employeeChargeIdentifier):? EmployeeChargeInterface
    {
        return $this->mapper->fetchById($employeeChargeIdentifier);
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(array $conditions = [])
    {
        return $this->mapper->fetchAll($conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function create(EmployeeChargeInterface $employeeCharge)
    {
        $this->mapper->create($employeeCharge);
    }

    /**
     * {@inheritdoc}
     */
    public function update(EmployeeChargeInterface $employeeCharge)
    {
        $this->mapper->update($employeeCharge);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(EmployeeChargeInterface $employeeCharge)
    {
        $this->mapper->delete($employeeCharge);
    }

    /**
     * {@inheritdoc}
     */
    public function nextIdentifier(): EmployeeChargeIdentifierInterface
    {
        return  EmployeeChargeIdentifier::fromString(Uuid::uuid4()->toString());
    }
}
