<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository;

use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\ManagerSaleInterface;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\ManagerSaleStatisticsRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployeeIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployeeIdentifierInterface;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper\DoctrineManagerSaleStatisticsMapper;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper\DoctrineRemoteEmployeeMapper;
use Ramsey\Uuid\Uuid;

/**
 * Class ManagerSaleStatisticsRepository
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository
 */
class ManagerSaleStatisticsRepository implements ManagerSaleStatisticsRepositoryInterface
{
    /** @var DoctrineManagerSaleStatisticsMapper */
    private $mapper;

    /**
     * ManagerSaleStatisticsRepository constructor
     *
     * @param DoctrineManagerSaleStatisticsMapper $mapper
     */
    public function __construct(DoctrineManagerSaleStatisticsMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * {@inheritdoc}
     */
    public function create(ManagerSaleInterface $managerSale)
    {
        $this->mapper->create($managerSale);
    }

    /**
     * {@inheritdoc}
     */
    public function update(ManagerSaleInterface $managerSale)
    {
        $this->mapper->update($managerSale);
    }

    /**
     * {@inheritdoc}
     */
    public function findOneBy($criteria = [])
    {
        return $this->mapper->fetchOneBy($criteria);
    }

    /**
     * {@inheritdoc}
     */
    public function nextIdentifier(): RemoteEmployeeIdentifierInterface
    {
        return RemoteEmployeeIdentifier::fromString(Uuid::uuid4()->toString());
    }
}
