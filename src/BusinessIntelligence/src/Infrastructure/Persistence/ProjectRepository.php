<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence;

use Orangear\BusinessIntelligence\Domain\Model\Project\Exception\ProjectNotFound;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectMapperInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectRepositoryInterface;

/**
 * Class PlatformRepository
 *
 * @package BusinessIntelligence\Infrastructure\Persistence
 */
final class ProjectRepository implements ProjectRepositoryInterface
{
    /** @var ProjectMapperInterface */
    private $mapper;

    /**
     * PlatformRepository constructor
     *
     * @param ProjectMapperInterface $mapper
     */
    public function __construct(ProjectMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * {@inheritdoc}
     */
    public function findById(ProjectIdentifierInterface $identifier) :? ProjectInterface
    {
        return $this->mapper->fetchById($identifier);
    }

    /**
     * {@inheritdoc}
     */
    public function findByName(string $name): array
    {
        return $this->mapper->fetchAll(['name' => $name]);
    }

    /**
     * {@inheritdoc}
     */
    public function create(ProjectInterface $project)
    {
        $this->mapper->create($project);
    }

    /**
     * {@inheritdoc}
     */
    public function findByToken(string $token) :? ProjectInterface
    {
        return $this->mapper->fetchByToken($token);
    }

    public function findAll()
    {
        return $this->mapper->fetchAll();
    }

    /**
     * {@inheritdoc}
     */
    public function findByTokenOrFail(string $token) : ProjectInterface
    {
        $project = $this->findByToken($token);

        if ($project === null) {
            throw ProjectNotFound::withProjectToken($token);
        }

        return $project;
    }

    /**
     * {@inheritdoc}
     */
    public function findByIdOrFail(ProjectIdentifierInterface $id) :? ProjectInterface
    {
        $project = $this->findById($id);

        if ($project === null) {
            throw ProjectNotFound::withProjectId($id);
        }

        return $project;
    }
}
