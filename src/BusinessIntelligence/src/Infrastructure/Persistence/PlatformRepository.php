<?php

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence;

use Orangear\BusinessIntelligence\Domain\Model\Platform\Exception\PlatformAlreadyExists;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformMapperInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformId;
use Orangear\Common\Identifier\IdentifierInterface;
use Ramsey\Uuid\Uuid;

/**
 * Class PlatformRepository
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence
 */
final class PlatformRepository implements PlatformRepositoryInterface
{
    /** @var PlatformMapperInterface */
    private $mapper;

    /**
     * PlatformRepository constructor
     *
     * @param PlatformMapperInterface $mapper
     */
    public function __construct(PlatformMapperInterface $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * {@inheritdoc}
     */
    public function findById(PlatformId $identifier) :? PlatformInterface
    {
        return $this->mapper->fetchById($identifier);
}

    /**
     * {@inheritdoc}
     */
    public function findByName(string $name): array
    {
        return $this->mapper->fetchAll(['name' => $name]);
    }

    /**
     * {@inheritdoc}
     */
    public function create(PlatformInterface $platform)
    {
        $this->mapper->create($platform);
    }

    /**
     * {@inheritdoc}
     */
    public function findPlatformByNameAndFail(string $name)
    {
        $platforms = $this->findByName($name);

        if (count($platforms) > 0) {
            throw PlatformAlreadyExists::withPlatformName($name);
        }
    }

    /**
     * @inheritdoc
     */
    public function findBy(array $conditions): ?PlatformInterface
    {
        return $this->mapper->fetchOne($conditions);
    }

    /**
     * @return IdentifierInterface
     */
    public function nextIdentifier(): IdentifierInterface
    {
        return PlatformId::fromString(Uuid::uuid4()->toString());
    }
}
