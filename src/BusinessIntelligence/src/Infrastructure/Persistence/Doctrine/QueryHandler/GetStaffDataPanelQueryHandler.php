<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\QueryHandler;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Orangear\BusinessIntelligence\Domain\Model\Employee\Employee;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\ManagerSale;
use Orangear\BusinessIntelligence\Domain\Model\Panel\Query\GetStaffDataPanelQueryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployee;
use React\Promise\Deferred;

/**
 * Class GetStaffDataPanelQueryHandler
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\QueryHandler
 */
class GetStaffDataPanelQueryHandler
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(GetStaffDataPanelQueryInterface $query, Deferred $deferred = null)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb
            ->select(['DISTINCT IDENTITY(' . 'ms' . '.employee)'])
            ->from(ManagerSale::class, 'ms');

        $qb2 = $this->entityManager->createQueryBuilder();

        $query2 = $qb2->select([
            'p' . '.id AS project_id',
            'p' . '.name AS project_name',
            'p' . '.logo AS project_logo',

            'e' . '.id AS employee_id',
            'e' . '.firstName AS employee_first_name',
            'e' . '.lastName AS employee_last_name',
        ])
            ->from(RemoteEmployee::class, 're')
            ->join(
                Employee::class,
                'e',
                Join::WITH,
                're' . '.employee' . '=' . 'e'  . '.id')
            ->join(
                Project::class,
                'p',
                Join::WITH,
                'p' . '.id' . '=' . 're' . '.project')
            ->where($qb2->expr()->in('IDENTITY('. 're' . '.employee)', $query->getDQL()))
            ->orderBy('p' . '.name');

        $data = [];

        $results = $query2->getQuery()->getResult();

        $projects = [];
        $employees = [];
        foreach ($results as $row) {
            $employee = [];
            $employee['id'] = $row['employee_id']->toString();
            $employee['first_name'] = $row['employee_first_name'];
            $employee['last_name'] = $row['employee_last_name'];

            if (!isset($employees[$employee['id']])) {
                $employees[$employee['id']] = $employee;
            }

            $project = [];
            $project['id'] = $row['project_id']->toString();

            $employees[$employee['id']]['projects'][$project['id']] = $project;
            $projects[$project['id']]['managers'][] = $employee;
        }

        foreach ($results as $row) {
            $project = [];
            $project['id'] = $row['project_id']->toString();
            $project['name'] = $row['project_name'];
            $project['logo'] = $row['project_logo'];

            foreach ($projects[$project['id']]['managers'] as $manager) {
                $project['managers'][] = $employees[$manager['id']];
            }

            $data[$project['id']] = $project;
        }

        $results = [];
        foreach ($data as $project) {

            $managers = [];
            foreach ($project['managers'] as $manager) {
                $managerProjects = array_values($manager['projects']);
                $manager['projects'] = $managerProjects;
                $managers[] = $manager;
            }

            $project['managers'] = $managers;

            $results[] = $project;
        }

        $deferred->resolve($results);
    }
}
