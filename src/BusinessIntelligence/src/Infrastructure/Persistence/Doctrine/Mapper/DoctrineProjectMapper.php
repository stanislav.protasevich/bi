<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper;

use Doctrine\ORM\EntityManagerInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectMapperInterface;

/**
 * Class DoctrineProjectMapper
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper
 */
final class DoctrineProjectMapper implements ProjectMapperInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * DoctrinePlatformMapper constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function fetchById(ProjectIdentifierInterface $id)
    {
        /** @var $id ProjectIdentifierInterface */

        $repository = $this->entityManager->getRepository(Project::class);

        return $repository->findOneBy(
            [
                'id' => $id
            ]
        );
    }

    public function fetchByToken(string $token)
    {
        $repository = $this->entityManager->getRepository(Project::class);

        return $repository->findOneBy(
            [
                'token' => $token
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function fetchAll(array $conditions = array())
    {
        $repository = $this->entityManager->getRepository(Project::class);

        $data = $repository->findBy($conditions);

        return $data;
    }

    /**
     * @param ProjectInterface $project
     */
    public function create(ProjectInterface $project)
    {
        $this->entityManager->persist($project);
        $this->entityManager->flush();
    }
}
