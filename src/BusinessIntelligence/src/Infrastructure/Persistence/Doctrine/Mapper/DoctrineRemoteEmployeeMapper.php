<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployee;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployeeIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployeeInterface;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployeeMapperInterface;

/**
 * Class DoctrineRemoteEmployeeMapper
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper
 */
class DoctrineRemoteEmployeeMapper implements RemoteEmployeeMapperInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ObjectRepository */
    private $repository;

    /**
     * DoctrinePlatformMapper constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->repository = $this->entityManager->getRepository(RemoteEmployee::class);
    }

    /**
     * {@inheritdoc}
     */
    public function fetchById(RemoteEmployeeIdentifierInterface $RemoteEmployeeIdentifier):? RemoteEmployeeInterface
    {
        return $this->repository->findOneBy(
            [
                'id' => $RemoteEmployeeIdentifier
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function fetchAll(array $conditions = [])
    {
        return $this->repository->findBy($conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function create(RemoteEmployeeInterface $RemoteEmployee)
    {
        $this->entityManager->persist($RemoteEmployee);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function update(RemoteEmployeeInterface $RemoteEmployee)
    {
        $this->entityManager->persist($RemoteEmployee);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function delete(RemoteEmployeeInterface $RemoteEmployee)
    {
        $this->entityManager->remove($RemoteEmployee);
        $this->entityManager->flush();
    }
}
