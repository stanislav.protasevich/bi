<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeCharge;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeChargeIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeChargeInterface;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeChargeMapperInterface;

/**
 * Class DoctrineEmployeeChargeMapper
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper
 */
final class DoctrineEmployeeChargeMapper implements EmployeeChargeMapperInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ObjectRepository */
    private $repository;

    /**
     * DoctrineEmployeeChargeMapper constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->repository = $this->entityManager->getRepository(EmployeeCharge::class);
    }

    /**
     * {@inheritdoc}
     */
    public function fetchById(EmployeeChargeIdentifierInterface $employeeChargeIdentifier):? EmployeeChargeInterface
    {
        return $this->repository->findOneBy(
            [
                'id' => $employeeChargeIdentifier
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function fetchAll(array $conditions = [])
    {
        return $this->repository->findBy($conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function create(EmployeeChargeInterface $employeeCharge)
    {
        $this->entityManager->persist($employeeCharge);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function update(EmployeeChargeInterface $employeeCharge)
    {
        $this->entityManager->persist($employeeCharge);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function delete(EmployeeChargeInterface $employeeCharge)
    {
        $this->entityManager->remove($employeeCharge);
        $this->entityManager->flush();
    }
}
