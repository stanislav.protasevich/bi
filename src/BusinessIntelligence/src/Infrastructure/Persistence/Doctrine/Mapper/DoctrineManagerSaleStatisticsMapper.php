<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\ManagerSale;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\ManagerSaleInterface;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\ManagerSaleStatisticsMapperInterface;

/**
 * Class DoctrineManagerSaleStatisticsMapper
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper
 */
class DoctrineManagerSaleStatisticsMapper implements ManagerSaleStatisticsMapperInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ObjectRepository */
    private $repository;

    /**
     * DoctrineManagerSaleStatisticsMapper constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->repository = $this->entityManager->getRepository(ManagerSale::class);
    }

    /**
     * {@inheritdoc}
     */
    public function fetchOneBy(array $conditions = [])
    {
        return $this->repository->findOneBy($conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function create(ManagerSaleInterface $managerSale)
    {
        $this->entityManager->persist($managerSale);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function update(ManagerSaleInterface $managerSale)
    {
        $this->entityManager->persist($managerSale);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function delete(ManagerSaleInterface $managerSale)
    {
        $this->entityManager->remove($managerSale);
        $this->entityManager->flush();
    }
}
