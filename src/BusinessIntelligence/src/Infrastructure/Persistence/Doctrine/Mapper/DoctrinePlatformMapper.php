<?php

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformMapperInterface;
use Orangear\BusinessIntelligence\Domain\Model\IdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\Platform;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformId;

/**
 * Class DoctrinePlatformMapper
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper
 */
final class DoctrinePlatformMapper implements PlatformMapperInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ObjectRepository */
    private $repository;

    /**
     * DoctrinePlatformMapper constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->repository = $this->entityManager->getRepository(Platform::class);
    }

    /**
     * {@inheritdoc}
     */
    public function fetchById(PlatformIdentifierInterface $id) :? PlatformInterface
    {
        $repository = $this->entityManager->getRepository(Platform::class);

        return $repository->findOneBy(
            [
                'id' => $id
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function fetchAll(array $conditions = array())
    {
        $repository = $this->entityManager->getRepository(Platform::class);

        $data = $repository->findBy($conditions);

        return $data;
    }

    /**
     * @param PlatformInterface $platform
     */
    public function create(PlatformInterface $platform)
    {
        $this->entityManager->persist($platform);
        $this->entityManager->flush();
    }

    /**
     * @param array $conditions
     * @return null|object|Platform
     */
    public function fetchOne(array $conditions)
    {
        return $this->repository->findOneBy($conditions);
    }
}