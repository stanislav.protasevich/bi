<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper;

use Doctrine\ORM\EntityManagerInterface;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\Dashboard;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\DashboardId;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\DashboardIdInterface;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\DashboardInterface;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\DashboardMapperInterface;
use Orangear\BusinessIntelligence\Domain\Model\IdentifierInterface;

/**
 * Class DoctrineDashboardMapper
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper
 */
class DoctrineDashboardMapper implements DashboardMapperInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * DoctrinePlatformMapper constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function fetchById(DashboardIdInterface $id) :? DashboardInterface
    {
        $repository = $this->entityManager->getRepository(Dashboard::class);

        return $repository->findOneBy(
            [
                'id' => $id
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function fetchOneBy(array $parameters): ?DashboardInterface
    {
        $repository = $this->entityManager->getRepository(Dashboard::class);

        return $repository->findOneBy(
            $parameters,
            [
                'synchronizedAt' => 'DESC'
            ]
        );
    }

    /**
     * @param DashboardInterface $dashboard
     */
    public function create(DashboardInterface $dashboard)
    {
        $this->entityManager->persist($dashboard);
        $this->entityManager->flush();
    }

    /**
     * @return IdentifierInterface
     */
    public function nextIdentifier() : IdentifierInterface
    {
        return DashboardId::fromString();
    }
}
