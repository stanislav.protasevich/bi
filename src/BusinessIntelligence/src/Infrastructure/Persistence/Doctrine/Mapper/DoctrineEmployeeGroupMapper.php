<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\EmployeeGroup;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\EmployeeGroupIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\EmployeeGroupInterface;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeGroup\EmployeeGroupMapperInterface;

/**
 * Class DoctrineEmployeeGroupMapper
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper
 */
class DoctrineEmployeeGroupMapper implements EmployeeGroupMapperInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ObjectRepository */
    private $repository;

    /**
     * DoctrinePlatformMapper constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->repository = $this->entityManager->getRepository(EmployeeGroup::class);
    }

    /**
     * {@inheritdoc}
     */
    public function fetchById(EmployeeGroupIdentifierInterface $chargeTypeIdentifier):? EmployeeGroupInterface
    {
        return $this->repository->findOneBy(
            [
                'id' => $chargeTypeIdentifier
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function fetchByName(string $name):? EmployeeGroupInterface
    {
        return $this->repository->findOneBy(
            [
                'name' => $name
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function fetchAll(array $conditions = [])
    {
        return $this->repository->findBy($conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function create(EmployeeGroupInterface $chargeType)
    {
        $this->entityManager->persist($chargeType);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function update(EmployeeGroupInterface $chargeType)
    {
        $this->entityManager->persist($chargeType);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function delete(EmployeeGroupInterface $chargeType)
    {
        $this->entityManager->remove($chargeType);
        $this->entityManager->flush();
    }
}
