<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeType;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeInterface;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeMapperInterface;

/**
 * Class DoctrineChargeTypeMapper
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper
 */
class DoctrineChargeTypeMapper implements ChargeTypeMapperInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ObjectRepository */
    private $repository;

    /**
     * DoctrinePlatformMapper constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->repository = $this->entityManager->getRepository(ChargeType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function fetchById(ChargeTypeIdentifierInterface $chargeTypeIdentifier):? ChargeTypeInterface
    {
        return $this->repository->findOneBy(
            [
                'id' => $chargeTypeIdentifier
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function fetchByName(string $name):? ChargeTypeInterface
    {
        return $this->repository->findOneBy(
            [
                'name' => $name
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function fetchOneBy($conditions = []):? ChargeTypeInterface
    {
        return $this->repository->findOneBy($conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function fetchAll(array $conditions = [])
    {
        return $this->repository->findBy($conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function create(ChargeTypeInterface $chargeType)
    {
        $this->entityManager->persist($chargeType);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function update(ChargeTypeInterface $chargeType)
    {
        $this->entityManager->persist($chargeType);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function delete(ChargeTypeInterface $chargeType)
    {
        $this->entityManager->remove($chargeType);
        $this->entityManager->flush();
    }
}
