<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Orangear\BusinessIntelligence\Domain\Model\Employee\Employee;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeInterface;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeMapperInterface;

/**
 * Class DoctrineEmployeeMapper
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper
 */
class DoctrineEmployeeMapper implements EmployeeMapperInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ObjectRepository */
    private $repository;

    /**
     * DoctrinePlatformMapper constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->repository = $this->entityManager->getRepository(Employee::class);
    }

    /**
     * {@inheritdoc}
     */
    public function fetchById(EmployeeIdentifierInterface $identifier):? EmployeeInterface
    {
        return $this->repository->findOneBy(
            [
                'id' => $identifier
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function fetchAll(array $conditions = [])
    {
        return $this->repository->findBy($conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function create(EmployeeInterface $Employee)
    {
        $this->entityManager->persist($Employee);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function update(EmployeeInterface $Employee)
    {
        $this->entityManager->persist($Employee);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function delete(EmployeeInterface $Employee)
    {
        $this->entityManager->remove($Employee);
        $this->entityManager->flush();
    }
}
