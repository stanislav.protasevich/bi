<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeInterface;
use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeMapperInterface;

/**
 * Class DoctrineChargeMapper
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\Mapper
 */
final class DoctrineChargeMapper implements ChargeMapperInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ObjectRepository */
    private $repository;

    /**
     * DoctrinePlatformMapper constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->repository = $this->entityManager->getRepository(Charge::class);
    }

    /**
     * {@inheritdoc}
     */
    public function fetchById(ChargeIdentifierInterface $chargeTypeIdentifier):? ChargeInterface
    {
        return $this->repository->findOneBy(
            [
                'id' => $chargeTypeIdentifier
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function fetchOneBy($conditions = []):? ChargeInterface
    {
        return $this->repository->findOneBy($conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function fetchAll(array $conditions = [])
    {
        return $this->repository->findBy($conditions);
    }

    /**
     * {@inheritdoc}
     */
    public function create(ChargeInterface $charge)
    {
        $this->entityManager->persist($charge);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function update(ChargeInterface $charge)
    {
        $this->entityManager->persist($charge);
        $this->entityManager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function delete(ChargeInterface $charge)
    {
        $this->entityManager->remove($charge);
        $this->entityManager->flush();
    }
}
