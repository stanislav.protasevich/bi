<?php

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine;

use Doctrine\ORM\EntityManager;

/**
 * Class TransactionManager
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine
 */
final class TransactionManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $anEntityManager
     */
    public function __construct(EntityManager $anEntityManager)
    {
        $this->entityManager = $anEntityManager;
    }

    /**
     * @return void
     */
    public function beginTransaction()
    {
        $this->entityManager->beginTransaction();
    }

    /**
     * @return void
     */
    public function commit()
    {
        $this->entityManager->commit();
    }

    /**
     * @return void
     */
    public function rollback()
    {
        $this->entityManager->rollback();
    }
}
