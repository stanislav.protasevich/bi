<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeChargeIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeChargeIdentifierInterface;

/**
 * Class EmployeeChargeIdType
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\DBAL\Types
 */
final class EmployeeChargeIdType extends GuidType
{
    const EMPLOYEE_CHARGE_ID = 'employee_charge_id';

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return EmployeeChargeIdentifier::fromString($value);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var EmployeeChargeIdentifierInterface $value */

        return $value->toString();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::EMPLOYEE_CHARGE_ID;
    }
}
