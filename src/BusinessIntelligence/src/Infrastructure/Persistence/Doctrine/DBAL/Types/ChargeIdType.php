<?php

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeIdentifierInterface;

/**
 * Class ChargeIdType
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\DBAL\Types
 */
final class ChargeIdType extends GuidType
{
    const CHARGE_ID = 'charge_id';

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return ChargeIdentifier::fromString($value);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var ChargeIdentifierInterface $value */

        return $value->toString();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::CHARGE_ID;
    }
}
