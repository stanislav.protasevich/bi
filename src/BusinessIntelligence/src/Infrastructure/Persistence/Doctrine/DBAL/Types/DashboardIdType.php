<?php

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\DashboardId;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformId;

/**
 * Class DashboardIdType
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\DBAL\Types
 */
final class DashboardIdType extends GuidType
{
    const DASHBOARD_ID = 'dashboard_id';

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return DashboardId::fromString($value);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var PlatformId $value */

        return $value->toString();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::DASHBOARD_ID;
    }
}
