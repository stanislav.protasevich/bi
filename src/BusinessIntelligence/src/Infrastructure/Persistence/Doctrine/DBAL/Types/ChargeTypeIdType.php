<?php

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformId;

/**
 * Class ChargeIdType
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\DBAL\Types
 */
final class ChargeTypeIdType extends GuidType
{
    const CHARGE_TYPE_ID = 'charge_type_id';

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return ChargeTypeIdentifier::fromString($value);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var PlatformId $value */

        return $value->toString();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::CHARGE_TYPE_ID;
    }
}
