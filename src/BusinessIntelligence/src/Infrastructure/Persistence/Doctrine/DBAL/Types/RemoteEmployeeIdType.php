<?php

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployeeIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\SuperEmployeeId;

/**
 * Class ProjectIdType
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\DBAL\Types
 */
final class RemoteEmployeeIdType extends GuidType
{
    const REMOTE_EMPLOYEE_ID = 'remote_employee_id';

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return RemoteEmployeeIdentifier::fromString($value);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var RemoteEmployeeIdentifier $value */

        return $value->toString();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::REMOTE_EMPLOYEE_ID;
    }
}
