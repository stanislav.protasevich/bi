<?php

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\SuperEmployeeId;

/**
 * Class ProjectIdType
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\DBAL\Types
 */
final class EmployeeIdType extends GuidType
{
    const EMPLOYEE_ID = 'employee_id';

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return EmployeeIdentifier::fromString($value);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var EmployeeIdentifier $value */

        if ($value !== null) {
            return $value->toString();
        }

        return $value;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::EMPLOYEE_ID;
    }
}
