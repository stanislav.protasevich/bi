<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformId;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformIdentifierInterface;

/**
 * Class PlatformIdType
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\DBAL\Types
 */
final class PlatformIdType extends GuidType
{
    const PLATFORM_ID = 'platform_id';

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return PlatformId::fromString($value);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var PlatformIdentifierInterface $value */

        return (string) $value;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::PLATFORM_ID;
    }
}
