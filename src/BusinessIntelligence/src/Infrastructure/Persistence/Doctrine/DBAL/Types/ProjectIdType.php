<?php

namespace Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;

/**
 * Class ProjectIdType
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\Persistence\Doctrine\DBAL\Types
 */
final class ProjectIdType extends GuidType
{
    const PROJECT_ID = 'project_id';

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return ProjectId::fromString($value);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var ProjectId $value */

        return $value->toString();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::PROJECT_ID;
    }
}
