<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\EventHandler\Project;

use Orangear\BusinessIntelligence\Domain\Model\Project\Event\ProjectWasCreated;
use Orangear\BusinessIntelligence\Domain\Model\Project\Exception\ProjectAlreadyExists;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectRepositoryInterface;

/**
 * Class OnProjectWasCreated
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\EventHandler\Project
 */
class OnProjectWasCreated
{
    /** @var ProjectRepositoryInterface */
    private $projectRepository;

    /**
     * OnProjectWasCreated constructor
     *
     * @param ProjectRepositoryInterface $projectRepository
     */
    public function __construct(ProjectRepositoryInterface $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    /**
     * @param ProjectWasCreated $event
     */
    public function __invoke(ProjectWasCreated $event)
    {
        $projects = $this->projectRepository->findByName($event->projectName());

        if (count($projects) > 0) {
            throw ProjectAlreadyExists::withProjectName($event->projectName());
        }

        $project = Project::withData(
            $event->projectId(),
            $event->platform(),
            $event->projectName(),
            $event->projectToken(),
            $event->projectLogo(),
            $event->projectOrder(),
            $event->projectApiUrl()
        );

        $this->projectRepository->create($project);
    }
}
