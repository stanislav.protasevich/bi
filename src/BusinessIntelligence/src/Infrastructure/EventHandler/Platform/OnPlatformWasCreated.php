<?php

namespace Orangear\BusinessIntelligence\Infrastructure\EventHandler\Platform;

use Orangear\BusinessIntelligence\Domain\Model\Platform\Exception\PlatformAlreadyExists;
use Orangear\BusinessIntelligence\Domain\Model\Platform\Event\PlatformWasCreated;
use Orangear\BusinessIntelligence\Domain\Model\Platform\Platform;
use Orangear\BusinessIntelligence\Domain\Model\Platform\PlatformRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Platform\ProjectRepositoryInterface;

/**
 * Class OnPlatformWasCreated
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\EventHandler\Platform
 */
final class OnPlatformWasCreated
{
    private $logger;

    /**
     * OnPlatformWasCreated constructor
     *
     * @param $logger
     */
    public function __construct($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param PlatformWasCreated $event
     */
    public function __invoke(PlatformWasCreated $event)
    {
        $platforms = $this->platformRepository->findByName($event->name());

        if (count($platforms) > 0) {
            throw PlatformAlreadyExists::withPlatformName($event->name());
        }

        $platform = Platform::withData(
            $event->platformId(),
            $event->name(),
            $event->logo()
        );

        $this->platformRepository->create($platform);
    }
}
