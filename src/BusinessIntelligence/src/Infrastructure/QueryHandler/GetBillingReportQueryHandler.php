<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\QueryHandler;

use Doctrine\ORM\EntityManagerInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\ProjectDashboardResponse;
use Orangear\Admin\Core\Domain\Model\BillingReport\BillingReport;
use Orangear\BusinessIntelligence\Domain\Model\BillingReport\Query\GetBillingReportQueryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeType;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\MembershipBundle\Domain\Model\Member\Member;

/**
 * Class GetBillingReportQueryHandler
 * @package Orangear\BusinessIntelligence\Infrastructure\QueryHandler
 */
final class GetBillingReportQueryHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $memberRepository;

    private $chargeTypeRepository;

    private $billingReportRepository;

    /**
     * GetDashboardQueryHandler constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->memberRepository = $this->entityManager->getRepository(Member::class);
        $this->chargeTypeRepository = $this->entityManager->getRepository(ChargeType::class);
        $this->billingReportRepository = $this->entityManager->getRepository(BillingReport::class);
    }

    /**
     * @param GetBillingReportQueryInterface $query
     * @return array
     */
    public function __invoke(GetBillingReportQueryInterface $query)
    {
        $params = [];

        $memberId = $query->memberId();

        if (!($member = $this->memberRepository->findOneBy(['id' => $memberId]))) {
            throw new \InvalidArgumentException('Member not found.');
        }

        $ids = [];
        /** @var ProjectInterface $project */
        foreach ($member->getProjects() as $project) {
            $ids[] = (string) $project->projectId();
        }

        $params['project'] = $member->getProjects();

        $projectId = $query->projectId();

        if ($projectId !== null) {
            if (!in_array($projectId, $ids)) {
                throw new \InvalidArgumentException('Corruption');
            } else {
                $ids = [$projectId];
                $params['project'] = $project;
            }
        }

        if (!is_null($query->startDate())) {
            $params['start_date'] = $query->startDate();
        }
        $params['ids'] = $ids;
        if (!is_null($query->endDate())) {
            $params['end_date'] = $query->endDate();
        }

        $params['not_charge_type'] = $this->findSalaryChargeOrFail('salary');


        $expensesTable = [];
        foreach ($this->findExpenses($params) as $charge) {
            $expensesTable[$charge['project_id']][$charge['year']][$charge['month']] = $charge['salaries'];
        }

        unset($params['not_charge_type']);
        $params['charge_type'] = $this->findSalaryChargeOrFail('salary');

        $salariesTable = [];
        foreach ($this->findExpenses($params) as $charge) {
            $salariesTable[$charge['project_id']][$charge['year']][$charge['month']] = $charge['salaries'];
        }

        $qb = $this->entityManager->createQueryBuilder();
        $qb->select(['br'])
            ->from(BillingReport::class, 'br')
            ->where('br' . '.project IN (:ids)')
            ->andWhere('br' . '.date BETWEEN :start AND :end')
            ->setParameter('ids', $ids)
            ->setParameter('start', $params['start_date'])
            ->setParameter('end', $params['end_date'])
            ->orderBy('br' . '.date');

        $data = [];
        /** @var BillingReport $billingReport */
        foreach ($qb->getQuery()->execute() as $billingReport) {
            $temp['date'] = $billingReport->getDate()->format('Y-m');
            $temp['revenue'] = $billingReport->getRevenue();
            $temp['revenueReceived'] = $billingReport->getRevenueReceived();
            $temp['crossPayment'] = $billingReport->getCrossPayment();
            $temp['deduction'] = $billingReport->getDeduction();
            $temp['revenueUnpaid'] = $billingReport->getRevenueUnpaid();
            $temp['publisherAmount'] = $billingReport->getPublisherAmount();
            $temp['publisherPaid'] = $billingReport->getPublisherPaid();
            $temp['publisherUnpaid'] = $billingReport->getPublisherUnpaid();
            $temp['grossIncome'] = $billingReport->getGrossIncome();

            $temp['netRevenue'] = $temp['revenue'] - $temp['crossPayment'] - $temp['deduction'];
            $temp['netIncome'] = $temp['revenueReceived'] - $temp['publisherPaid'];

            $salaries = 0;
            if (isset($salariesTable[$billingReport->getProject()->projectId()->toString()][$billingReport->getDate()->format('Y')][intval($billingReport->getDate()->format('m'))])) {
                $salaries = (float) $salariesTable[$billingReport->getProject()->projectId()->toString()][$billingReport->getDate()->format('Y')][intval($billingReport->getDate()->format('m'))];
            }
//
            $otherExpenses = 0;
            if (isset($expensesTable[$billingReport->getProject()->projectId()->toString()][$billingReport->getDate()->format('Y')][intval($billingReport->getDate()->format('m'))])) {
                $otherExpenses = (float) $expensesTable[$billingReport->getProject()->projectId()->toString()][$billingReport->getDate()->format('Y')][intval($billingReport->getDate()->format('m'))];
            }

            $temp['totalExpenses'] = $salaries + $otherExpenses;

//            $temp['grossProfit'] = 0;
            $temp['netProfit'] = $temp['netIncome'] - $temp['totalExpenses'];
//            $temp['discrepancy'] = 0;
//
//            if ($temp['netRevenue'] != 0) {
//                $temp['netMargin'] = $temp['netIncome'] / $temp['netRevenue'] * 100;
//            } else {
//                $temp['netMargin'] = 0;
//            }

            /** @todo Our amount */
//            $temp['our_amount'] = 0;
//            $temp['discrepancy'] = $temp['revenue'] - $temp['our_amount'];

            $temp['project']['id'] = $billingReport->getProject()->projectId()->toString();
            $temp['project']['name'] = $billingReport->getProject()->name();

            $data['data'][] = $temp;
        }

        return $data['data'];
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getSummary(array $ids = [])
    {
        $sql = "
            SELECT
              SUM(dashboard.income_today) AS incomeToday,
              SUM(dashboard.income_yesterday) AS incomeYesterday,
              SUM(dashboard.income_last_seven_days) AS incomeLastSevenDays,
              SUM(dashboard.income_this_month) AS incomeThisMonth,
              SUM(dashboard.income_last_month) AS incomeLastMonth,
              SUM(dashboard.income_all_time) AS incomeAllTime,
              
              SUM(dashboard.income_superlink_today) AS incomeSuperlinkToday,
              SUM(dashboard.income_superlink_yesterday) AS incomeSuperlinkYesterday,
              SUM(dashboard.income_superlink_last_seven_days) AS incomeSuperlinkLastSevenDays,
              SUM(dashboard.income_superlink_this_month) AS incomeSuperlinkThisMonth,
              SUM(dashboard.income_superlink_last_month) AS incomeSuperlinkLastMonth,
              SUM(dashboard.income_superlink_all_time) AS incomeSuperlinkAllTime,
              
              SUM(dashboard.payout_today) AS payoutToday,
              SUM(dashboard.payout_yesterday) AS payoutYesterday,
              SUM(dashboard.payout_last_seven_days) AS payoutLastSevenDays,
              SUM(dashboard.payout_this_month) AS payoutThisMonth,
              SUM(dashboard.payout_last_month) AS payoutLastMonth,
              SUM(dashboard.payout_all_time) AS payoutAllTime,
              
              SUM(dashboard.offers_active) AS offersActive,
              SUM(dashboard.offers_private) AS offersPrivate,
              SUM(dashboard.offers_suspended) AS offersSuspended,
              SUM(dashboard.offers_incent) AS offersIncent,
              SUM(dashboard.offers_non_incent) AS offersNonIncent,
              SUM(dashboard.offers_total) AS offersTotal,
              SUM(dashboard.offers_per_day) AS offersPerDay,
              
              SUM(dashboard.revenue_superlink_today) AS revenueSuperlinkToday,
              SUM(dashboard.revenue_superlink_yesterday) AS revenueSuperlinkYesterday,
              SUM(dashboard.revenue_superlink_last_seven_days) AS revenueSuperlinkLastSevenDays,
              SUM(dashboard.revenue_superlink_this_month) AS revenueSuperlinkThisMonth,
              SUM(dashboard.revenue_superlink_last_month) AS revenueSuperlinkLastMonth,
              SUM(dashboard.revenue_superlink_all_time) AS revenueSuperlinkAllTime
            FROM project
              JOIN dashboard ON dashboard.project_id = project.id            
        ";

        $where = [];
        $where[] = "(dashboard.project_id, UNIX_TIMESTAMP(dashboard.synchronized_at)) IN (SELECT project_id, MAX(UNIX_TIMESTAMP(dashboard.synchronized_at)) AS synchronized_at FROM dashboard GROUP BY dashboard.project_id)";

        if (!empty($ids)) {
            $where[] = "project.id IN ('" . implode('\',\'', $ids) . "')";
        }

        $sql .= ' WHERE ' . implode(' AND ', $where);

        $sql .= ' ORDER BY project.name ASC';

        return $this->entityManager->getConnection()->executeQuery($sql)->fetchAll();
    }

    /**
     * @param $key
     *
     * @return array|mixed|null|\Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeInterface
     *
     * @throws \Exception
     */
    private function findSalaryChargeOrFail($key)
    {
        $salary = $this->chargeTypeRepository->findOneBy(['key' => $key]);

        if ($salary === null) {
            throw new \Exception('Can not find "' . $key . '" charge type');
        }

        return $salary;
    }

    private function findExpenses($params)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select([
            'IDENTITY(' . 'c' . '.project) AS project_id',
            'SUM(' . 'c' . '.amount) AS salaries',
            'YEAR(' . 'c' . '.startDate) AS year',
            'MONTH(' . 'c' . '.startDate) AS month',
        ])
            ->from(Charge::class, 'c')
            ->groupBy('project_id')
            ->addGroupBy('year')
            ->addGroupBy('month');

        if (isset($params['ids'])) {
            $qb->andWhere('c' . '.project IN (:ids)')
                ->setParameter('ids', $params['ids']);
        }

        if (isset($params['charge_type'])) {
            $qb->andWhere('c' . '.chargeType = :chargeType')
                ->setParameter('chargeType', $params['charge_type']);
        }

        if (isset($params['not_charge_type'])) {
            $qb->andWhere('c' . '.chargeType != :chargeType')
                ->setParameter('chargeType', $params['not_charge_type']);
        }

        if (isset($params['start_date']) && isset($params['end_date'])) {
            $qb->andWhere('c' . '.startDate >= :start_date AND ' . 'c' . '.endDate <= :end_date');
            $qb->setParameter('start_date', $params['start_date'])
                ->setParameter('end_date', $params['end_date']);
        }

        $query = $qb->getQuery()->getResult();

        return $query;
    }
}