<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\QueryHandler;

use Orangear\BusinessIntelligence\Domain\Model\Employee\Employee;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query\GetManagerDataSummaryQueryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployee;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository\EmployeeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Orangear\MembershipBundle\Domain\Model\Member\Member;

/**
 * Class GetHeadManagerDataSummaryQueryHandler
 * @package Orangear\BusinessIntelligence\Infrastructure\QueryHandler
 */
final class GetHeadManagerDataSummaryQueryHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * GetHeadManagerDataSummaryQueryHandler constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param GetManagerDataSummaryQueryInterface $query
     * @return array
     */
    public function __invoke(GetManagerDataSummaryQueryInterface $query)
    {

        $memberId = $query->memberId();

        $qb = $this->entityManager->createQueryBuilder();
        $qb->select(['m'])
            ->from(Member::class,'m')
            ->where('m' . '.id = :id')
            ->setParameter('id', $memberId);

        if (!($member = $qb->getQuery()->getOneOrNullResult())) {
            throw new \InvalidArgumentException('Member not found.');
        }

        $employeeId = $query->employeeId();

        $qb = $this->entityManager->createQueryBuilder();
        $qb->select(['e'])
            ->from(Employee::class,'e')
            ->where('e' . '.id = :id')
            ->setParameter('id', $employeeId);

        if (!($employee = $qb->getQuery()->getOneOrNullResult())) {
            throw new \InvalidArgumentException('Employee not found.');
        }

        $ids = [];
        /** @var ProjectInterface $project */
        foreach ($member->getProjects() as $project) {
            $ids[] = (string) $project->projectId();
        }

        $qb = $this->entityManager->createQueryBuilder();
        $qb->select(['re'])
            ->from(RemoteEmployee::class,'re')
            ->where('IDENTITY(' . 're' . '.employee) = :id')
            ->andWhere('IDENTITY(' . 're' . '.project) IN (:ids)')
            ->setParameter('id', $employee)
            ->setParameter('ids', $ids);

        if (!$qb->getQuery()->getOneOrNullResult()) {
            throw new \InvalidArgumentException('Corruption');
        }

        /** @var Employee $manager */
        $manager = $this->getManager($query->employeeId());
        $managerStatistics =$this->getManagerStatistics($query->employeeId());
        $managerProjects = $this->getManagerProjects($query->employeeId());

        $response = [];
        $response['id'] = (string) $manager->id();
        $response['firstName'] = $manager->firstName();
        $response['lastName'] = $manager->lastName();
        $response['position'] = 0;
        $response['employedSince'] = 0;

        $response['wage'] = 0;
        $response['totalGrossSales'] = $managerStatistics['gross_sale'];
        $response['totalGrossProfit'] = $managerStatistics['gross_profit'];
        $response['totalNetProfit'] = 0;

        /** @var Project $project */
        foreach ($managerProjects as $project) {
            $temp = [];
            $temp['id'] = $project['id'];
            $temp['name'] = $project['name'];

            $response['projects'][] = $temp;
        }

        return $response;
    }

    /**
     * @param $managerId
     * @return array
     */
    private function getManager($managerId)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select(['e'])
            ->from(Employee::class, 'e')
            ->where('e' . '.id = :id')
            ->setParameter('id', $managerId);

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @param string $managerId
     * @return array
     */
    private function getManagerProjects($managerId)
    {
        $sql = "
            SELECT 
              project.*
            FROM remote_employee
            JOIN employee ON remote_employee.employee_id = employee.id
            JOIN project ON project.id = remote_employee.project_id
            WHERE employee.id = '" . $managerId ."'
        ";

        return $this->entityManager->getConnection()->executeQuery($sql)->fetchAll();
    }

    /**
     * @param string $managerId
     * @return array
     */
    private function getManagerStatistics($managerId)
    {
        $sql = "
            SELECT 
              SUM(manager_sale_statistics.gross_sale) AS gross_sale,
              SUM(manager_sale_statistics.gross_profit) AS gross_profit
            FROM manager_sale_statistics
            WHERE manager_sale_statistics.employee_id = '" . $managerId ."'
        ";

        return $this->entityManager->getConnection()->executeQuery($sql)->fetch();
    }
}
