<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\QueryHandler;

use Doctrine\ORM\EntityManagerInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\ProjectDashboardResponse;
use Orangear\Admin\Core\Domain\Model\BillingReport\BillingReport;
use Orangear\Admin\Core\Domain\Model\BillingReport\BillingReportRepositoryInterface;
use Orangear\Admin\Core\InfrastructureBundle\Repository\ProjectEmployeeRepository;
use Orangear\Admin\Model\Entity;
use Orangear\BusinessIntelligence\Domain\Model\BillingReport\Query\GetBillingReportQueryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeType;
use Orangear\BusinessIntelligence\Domain\Model\Employee\Employee;
use Orangear\BusinessIntelligence\Domain\Model\FinancialData\Query\GetFinancialDataQueryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectRepositoryInterface;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository\ChargeRepository;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository\ChargeTypeRepository;
use Orangear\MembershipBundle\Domain\Model\Member\Member;

/**
 * Class GetFinancialDataQueryHandler
 * @package Orangear\BusinessIntelligence\Infrastructure\QueryHandler
 */
final class GetFinancialDataQueryHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    private $billingReportRepository;
    private $projectRepository;
    private $chargeRepository;
    private $chargeTypeRepository;
    private $projectEmployeeRepository;
    private $memberRepository;

    /**
     * GetFinancialDataQueryHandler constructor.
     * @param EntityManagerInterface $entityManager
     * @param BillingReportRepositoryInterface $billingReportRepository
     * @param ProjectRepositoryInterface $projectRepository
     * @param ChargeRepository $chargeRepository
     * @param ChargeTypeRepository $chargeTypeRepository
     * @param ProjectEmployeeRepository $projectEmployeeRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        BillingReportRepositoryInterface $billingReportRepository,
        ProjectRepositoryInterface $projectRepository,
        ChargeRepository $chargeRepository,
        ChargeTypeRepository $chargeTypeRepository,
        ProjectEmployeeRepository $projectEmployeeRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->billingReportRepository = $billingReportRepository;
        $this->projectRepository = $projectRepository;
        $this->chargeRepository = $chargeRepository;
        $this->chargeTypeRepository = $chargeTypeRepository;
        $this->projectEmployeeRepository = $projectEmployeeRepository;

        $this->memberRepository = $this->entityManager->getRepository(Member::class);
    }

    /**
     * @param GetFinancialDataQueryInterface $query
     *
     * @return array
     */
    public function __invoke(GetFinancialDataQueryInterface $query)
    {
        $memberId = $query->memberId();

        if (!($member = $this->memberRepository->findOneBy(['id' => $memberId]))) {
            throw new \InvalidArgumentException('Member not found.');
        }

        $ids = [];
        /** @var ProjectInterface $project */
        foreach ($member->getProjects() as $project) {
            $ids[] = (string) $project->projectId();
        }

        $projectId = $query->projectId();
        if ($projectId !== null) {
            if (!in_array($projectId, $ids)) {
                throw new \InvalidArgumentException('Corruption');
            } else {
                $ids = [$projectId];
            }
        }

        $params = [];

        if (!is_null($query->startDate())) {
            $params['start_date'] = $query->startDate();
        }

        if (!is_null($query->endDate())) {
            $params['end_date'] = $query->endDate();
        }

        $params['not_charge_type'] = $this->findSalaryChargeOrFail('salary');

        $expensesTable = [];
        foreach ($this->findExpenses($params) as $charge) {
            $expensesTable[$charge['project_id']][$charge['year']][$charge['month']] = $charge['salaries'];
        }

        unset($params['not_charge_type']);
        $params['charge_type'] = $this->findSalaryChargeOrFail('salary');

        $salariesTable = [];
        foreach ($this->findExpenses($params) as $charge) {
            $salariesTable[$charge['project_id']][$charge['year']][$charge['month']] = $charge['salaries'];
        }

        $employees = [];
        foreach ($this->projectEmployeeRepository->countEmployeesPerProject($params['start_date'], $params['end_date']) as $cnt) {
            $employees[$cnt['project_id']][date('Y-m', strtotime($cnt['period']))] = $cnt['amount'];
            //$employees[$cnt['project_id']] = $cnt['amount'];
        }

        $interval = new \DateInterval('P1M');
        $dateRange = new \DatePeriod($params['start_date'], $interval, $params['end_date']);

        $data = [];

        $memberProjects = $member->getProjects();

        foreach ($dateRange as $date) {
            $summaries = [];
            $perManagers = [];
            $summaryEfficiencies = [];

            /** @var ProjectInterface $project */
            foreach ($memberProjects as $project) {
                $temp = [];
                $temp['date'] = $date->format('Y-m');
                $temp['revenue'] = 0;
//                $temp['discrepancy'] = 0;
                $temp['net_revenue'] =  0;
//                $temp['revenue_received'] = 0;
                $temp['gross_income'] = 0;
//                $temp['salaries'] = 0;
                $temp['other_expenses'] = 0;
//                $temp['total_expenses'] = 0;
                $temp['net_income'] = 0;
                $temp['gross_profit'] = 0;
                $temp['net_profit'] = 0;
//                $temp['gross_income_margin'] = 0;
//                $temp['net_profit_margin'] = 0;
                $temp['romi'] = 0;
//                $temp['cost_to_income'] = 0;
//                $temp['net_income_weight'] = 0;
                $temp['project']['id'] = (string) $project->projectId();
                $temp['project']['name'] = $project->name();

                $summaryEfficiency = [];
                $summaryEfficiency['date'] = $date->format('Y-m');
                $summaryEfficiency['deductions_rate'] = 0;
//                $summaryEfficiency['salary_efficiency'] = 0;
                $summaryEfficiency['adv_unpaid_rate'] = 0;
                $summaryEfficiency['pub_unpaid_rate'] = 0;
                $summaryEfficiency['project']['id'] = (string) $project->projectId();
                $summaryEfficiency['project']['name'] = $project->name();

                $summaries[(string) $project->projectId()] = $temp;
                $perManagers[(string) $project->projectId()] = $temp;
                $summaryEfficiencies[(string) $project->projectId()] = $summaryEfficiency;
            }

            $data[$date->format('Y-m')] = [
                'date' => $date->format('Y-m'),
                'summary' => $summaries,
                'per_manager' => $perManagers,
                'summary_efficiency' => $summaryEfficiencies,
            ];
        }

//        var_dump($data);
//        exit;

        $qb = $this->entityManager->createQueryBuilder();
        $qb->select(['br'])
            ->from(BillingReport::class, 'br')
            ->where('br' . '.project IN (:ids)')
            ->andWhere('br' . '.date BETWEEN :start AND :end')
            ->setParameter('ids', $ids)
            ->setParameter('start', $query->startDate())
            ->setParameter('end', $query->endDate());

        $billingReports = $qb->getQuery()->execute();

        foreach ($billingReports as $billingReport) {
            $temp['date'] = $billingReport->getDate()->format('Y-m');

            $temp['revenue'] = $billingReport->getRevenue();

            $ourAmount = 0;
//            $temp['discrepancy'] = $temp['revenue'] - $ourAmount;

            $temp['net_revenue'] = $temp['revenue'] - $billingReport->getCrossPayment() - $billingReport->getDeduction();


            $revenueReceived = $temp['net_revenue'] - $billingReport->getRevenueUnpaid();
            $temp['gross_income'] = $temp['revenue'] - $billingReport->getPublisherAmount();

            $salaries = 0;
            if (isset($salariesTable[$billingReport->getProject()->projectId()->toString()][$billingReport->getDate()->format('Y')][intval($billingReport->getDate()->format('m'))])) {
                $salaries = (float) $salariesTable[$billingReport->getProject()->projectId()->toString()][$billingReport->getDate()->format('Y')][intval($billingReport->getDate()->format('m'))];
            }

            $temp['other_expenses'] = 0;
            if (isset($expensesTable[$billingReport->getProject()->projectId()->toString()][$billingReport->getDate()->format('Y')][intval($billingReport->getDate()->format('m'))])) {
                $temp['other_expenses'] = (float) $expensesTable[$billingReport->getProject()->projectId()->toString()][$billingReport->getDate()->format('Y')][intval($billingReport->getDate()->format('m'))];
            }

            $totalExpenses = $salaries + $temp['other_expenses'];

            $temp['net_income'] = $revenueReceived - $billingReport->getPublisherPaid();

            $temp['gross_profit'] = $temp['net_income'] - $salaries;
            $temp['net_profit'] = $temp['gross_profit'] - $temp['other_expenses'];

            if ($temp['revenue'] != 0) {
                $grossIncomeMargin = $temp['gross_income'] / $temp['revenue'] * 100;
            } else {
                $grossIncomeMargin = 0;
            }

            if ($revenueReceived != 0) {
                $netProfitMargin = $temp['net_profit'] / $revenueReceived * 100;
            } else {
                $netProfitMargin = 0;
            }

            if (($billingReport->getPublisherPaid() + $totalExpenses) != 0) {
                $temp['romi'] = $temp['net_profit'] / ($billingReport->getPublisherPaid() + $totalExpenses);
            } else {
                $temp['romi'] = $temp['net_profit'] / 1;
            }

            if ($revenueReceived != 0) {
                $costToIncome = ($billingReport->getPublisherPaid() + $totalExpenses) / $revenueReceived;
            } else {
                $costToIncome = 0;
            }

            if ($temp['gross_income'] != 0) {
                $netIncomeWeight = $temp['net_income'] / $temp['gross_income'];
            } else {
                $netIncomeWeight = 0;
            }

            $temp['project']['id'] = $billingReport->getProject()->projectId()->toString();
            $temp['project']['name'] = $billingReport->getProject()->name();

            $data[$billingReport->getDate()->format('Y-m')]['summary'][$billingReport->getProject()->projectId()->toString()] = $temp;

            /** PER MANAGER */
            $perManagerCount = 1;
            if (isset($employees[$billingReport->getProject()->projectId()->toString()]) && isset($employees[$billingReport->getProject()->projectId()->toString()][$temp['date']])) {
                $perManagerCount = $employees[$billingReport->getProject()->projectId()->toString()][$temp['date']];
            }

            $perManager = [];
            $perManager['date'] = $temp['date'];
            $perManager['revenue'] = $temp['revenue'] / $perManagerCount;
//            $perManager['discrepancy'] = $temp['discrepancy'] / $perManagerCount;
            $perManager['net_revenue'] = $temp['net_revenue'] / $perManagerCount;
//            $perManager['revenue_received'] = $temp['revenue_received'] / $perManagerCount;
            $perManager['gross_income'] = $temp['gross_income'] / $perManagerCount;
            $salaries = $salaries / $perManagerCount;
            $perManager['other_expenses'] = $temp['other_expenses'] / $perManagerCount;
//            $perManager['total_expenses'] = $temp['total_expenses'] / $perManagerCount;
            $perManager['net_income'] = $temp['net_income'] / $perManagerCount;
            $perManager['gross_profit'] = $temp['gross_profit'] / $perManagerCount;
            $perManager['net_profit'] = $temp['net_profit'] / $perManagerCount;
//            $perManager['gross_income_margin'] = $grossIncomeMargin / $perManagerCount;
//            $perManager['net_profit_margin'] = $netProfitMargin / $perManagerCount;
            $perManager['romi'] = ($temp['romi'] / $perManagerCount) * 100;
//            $perManager['cost_to_income'] = $costToIncome / $perManagerCount;
//            $perManager['net_income_weight'] = $netIncomeWeight / $perManagerCount;

            $perManager['project']['id'] = $billingReport->getProject()->projectId()->toString();
            $perManager['project']['name'] = $billingReport->getProject()->name();

            $data[$billingReport->getDate()->format('Y-m')]['per_manager'][$billingReport->getProject()->projectId()->toString()] = $perManager;

            /** SUMMARY EFFICIENCY */
            $summaryEfficiency = [];
            $summaryEfficiency['date'] = $billingReport->getDate()->format('Y-m');

            if ($temp['revenue'] != 0) {
                $summaryEfficiency['deductions_rate'] = $billingReport->getDeduction() / $temp['revenue'] * 100;
            } else {
                $summaryEfficiency['deductions_rate'] = 0;
            }

            if ($perManager['net_income'] != 0) {
                $salaryEfficiency = $salaries / $perManager['net_income'] * 100;
            } else {
                $salaryEfficiency = 0;
            }

            if ($temp['net_revenue'] != 0) {
                $summaryEfficiency['adv_unpaid_rate'] = ($temp['net_revenue'] - $revenueReceived) / $temp['net_revenue'] * 100;
            } else {
                $summaryEfficiency['adv_unpaid_rate'] = 0;
            }

            if ($billingReport->getPublisherAmount() != 0) {
                $summaryEfficiency['pub_unpaid_rate'] = ($billingReport->getPublisherAmount() - $billingReport->getPublisherPaid()) / $billingReport->getPublisherAmount() * 100;
            } else {
                $summaryEfficiency['pub_unpaid_rate'] = 0;
            }

            if ($billingReport->getPublisherAmount() != 0) {
                $summaryEfficiency['pub_unpaid_rate'] = ($billingReport->getPublisherAmount() - $billingReport->getPublisherPaid()) / $billingReport->getPublisherAmount() * 100;
            } else {
                $summaryEfficiency['pub_unpaid_rate'] = 0;
            }

            $summaryEfficiency['project']['id'] = $billingReport->getProject()->projectId()->toString();
            $summaryEfficiency['project']['name'] = $billingReport->getProject()->name();

            /** @todo calculate efficiency */
            $data[$billingReport->getDate()->format('Y-m')]['summary_efficiency'][$billingReport->getProject()->projectId()->toString()] = $summaryEfficiency;
        }

        $newData = [];
        foreach ($data as $date) {
//            var_dump($date); exit;

            $row = [];
            $row['date'] = $date['date'];

            $row['summary'] = array_values($date['summary']);
            $row['per_manager'] = array_values($date['per_manager']);
            $row['summary_efficiency'] = array_values($date['summary_efficiency']);

            $newData[] = $row;
        }

        return $newData;
    }

    /**
     * @param $key
     *
     * @return null|object
     *
     * @throws \Exception
     */
    private function findSalaryChargeOrFail($key)
    {
        $salary = $this->chargeTypeRepository->findOneBy(['key' => $key]);

        if ($salary === null) {
            throw new \Exception('Can not find "' . $key . '" charge type');
        }

        return $salary;
    }

    private function findExpenses($params)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select([
            'IDENTITY(' . 'c' . '.project) AS project_id',
            'SUM(' . 'c' . '.amount) AS salaries',
            'YEAR(' . 'c' . '.startDate) AS year',
            'MONTH(' . 'c' . '.startDate) AS month',
        ])
            ->from(Charge::class, 'c')
            ->groupBy('project_id')
            ->addGroupBy('year')
            ->addGroupBy('month');

        if (isset($params['project'])) {
            $qb->andWhere('c' . '.project = :project')
                ->setParameter('project', $params['project']);
        }

        if (isset($params['charge_type'])) {
            $qb->andWhere('c' . '.chargeType = :chargeType')
                ->setParameter('chargeType', $params['charge_type']);
        }

        if (isset($params['not_charge_type'])) {
            $qb->andWhere('c' . '.chargeType != :chargeType')
                ->setParameter('chargeType', $params['not_charge_type']);
        }

        if (isset($params['start_date']) && isset($params['end_date'])) {
            $qb->andWhere('c' . '.startDate >= :start_date AND ' . 'c' . '.endDate <= :end_date');
            $qb->setParameter('start_date', $params['start_date'])
                ->setParameter('end_date', $params['end_date']);
        }

        $query = $qb->getQuery()->getResult();

        return $query;
    }
}