<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\QueryHandler;

use Doctrine\ORM\EntityManagerInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\ProjectDashboardResponse;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\Query\GetDashboardQueryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\MembershipBundle\Domain\Model\Member\Member;

/**
 * Class GetDashboardQueryHandler
 * @package Orangear\BusinessIntelligence\Infrastructure\QueryHandler
 */
final class GetDashboardQueryHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $memberRepository;

    /**
     * GetDashboardQueryHandler constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->memberRepository = $this->entityManager->getRepository(Member::class);
    }

    /**
     * @param GetDashboardQueryInterface $query
     * @return array
     */
    public function __invoke(GetDashboardQueryInterface $query)
    {
        $memberId = $query->memberId();

        if (!($member = $this->memberRepository->findOneBy(['id' => $memberId]))) {
            throw new \InvalidArgumentException('Member not found.');
        }

        $ids = [];
        /** @var ProjectInterface $project */
        foreach ($member->getProjects() as $project) {
            $ids[] = (string) $project->projectId();
        }

        $projectId = $query->projectId();
        if ($projectId !== null) {
            if (!in_array($projectId, $ids)) {
                throw new \InvalidArgumentException('Corruption');
            } else {
                $ids = [$projectId];
            }
        }

        return (new ProjectDashboardResponse($this->getSummary($ids)))->getResult();
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getSummary(array $ids = [])
    {
        $sql = "
            SELECT
              SUM(dashboard.income_today) AS incomeToday,
              SUM(dashboard.income_yesterday) AS incomeYesterday,
              SUM(dashboard.income_last_seven_days) AS incomeLastSevenDays,
              SUM(dashboard.income_this_month) AS incomeThisMonth,
              SUM(dashboard.income_last_month) AS incomeLastMonth,
              SUM(dashboard.income_all_time) AS incomeAllTime,
              
              SUM(dashboard.income_superlink_today) AS incomeSuperlinkToday,
              SUM(dashboard.income_superlink_yesterday) AS incomeSuperlinkYesterday,
              SUM(dashboard.income_superlink_last_seven_days) AS incomeSuperlinkLastSevenDays,
              SUM(dashboard.income_superlink_this_month) AS incomeSuperlinkThisMonth,
              SUM(dashboard.income_superlink_last_month) AS incomeSuperlinkLastMonth,
              SUM(dashboard.income_superlink_all_time) AS incomeSuperlinkAllTime,
              
              SUM(dashboard.payout_today) AS payoutToday,
              SUM(dashboard.payout_yesterday) AS payoutYesterday,
              SUM(dashboard.payout_last_seven_days) AS payoutLastSevenDays,
              SUM(dashboard.payout_this_month) AS payoutThisMonth,
              SUM(dashboard.payout_last_month) AS payoutLastMonth,
              SUM(dashboard.payout_all_time) AS payoutAllTime,
              
              SUM(dashboard.offers_active) AS offersActive,
              SUM(dashboard.offers_private) AS offersPrivate,
              SUM(dashboard.offers_suspended) AS offersSuspended,
              SUM(dashboard.offers_incent) AS offersIncent,
              SUM(dashboard.offers_non_incent) AS offersNonIncent,
              SUM(dashboard.offers_total) AS offersTotal,
              SUM(dashboard.offers_per_day) AS offersPerDay,
              
              SUM(dashboard.revenue_superlink_today) AS revenueSuperlinkToday,
              SUM(dashboard.revenue_superlink_yesterday) AS revenueSuperlinkYesterday,
              SUM(dashboard.revenue_superlink_last_seven_days) AS revenueSuperlinkLastSevenDays,
              SUM(dashboard.revenue_superlink_this_month) AS revenueSuperlinkThisMonth,
              SUM(dashboard.revenue_superlink_last_month) AS revenueSuperlinkLastMonth,
              SUM(dashboard.revenue_superlink_all_time) AS revenueSuperlinkAllTime
            FROM project
              JOIN dashboard ON dashboard.project_id = project.id            
        ";

        $where = [];
        $where[] = "(dashboard.project_id, UNIX_TIMESTAMP(dashboard.synchronized_at)) IN (SELECT project_id, MAX(UNIX_TIMESTAMP(dashboard.synchronized_at)) AS synchronized_at FROM dashboard GROUP BY dashboard.project_id)";

        if (!empty($ids)) {
            $where[] = "project.id IN ('" . implode('\',\'', $ids) . "')";
        }

        $sql .= ' WHERE ' . implode(' AND ', $where);

        $sql .= ' ORDER BY project.name ASC';

        return $this->entityManager->getConnection()->executeQuery($sql)->fetchAll();
    }
}