<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\QueryHandler\ChargeType;

use Doctrine\ORM\EntityManagerInterface;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeType;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeInterface;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\Query\ListChargeTypeQuery;
use React\Promise\Deferred;

/**
 * Class ListChargeTypeQueryHandler
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\QueryHandler\ChargeType
 */
final class ListChargeTypeQueryHandler
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * ListChargeTypeQueryHandler constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param ListChargeTypeQuery $query
     *
     * @param Deferred|null $deferred
     */
    public function __invoke(ListChargeTypeQuery $query, Deferred $deferred = null)
    {
        $response = [];

        /** @var ChargeTypeInterface $chargeType */
        foreach ($this->getChargeTypes() as $chargeType){
            $response[] = [
                'id'          => $chargeType['id']->toString(),
                'name'        => $chargeType['name'],
                'key'         => $chargeType['key'],
                'description' => $chargeType['description'],
            ];
        }

        $deferred->resolve($response);
    }

    /**
     * @return array
     */
    private function getChargeTypes()
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb
            ->select([
                'ct' . '.id',
                'ct' . '.name',
                'ct' . '.key',
                'ct' . '.description',
            ])
            ->from(ChargeType::class, 'ct');

        return $qb->getQuery()->getArrayResult();
    }
}
