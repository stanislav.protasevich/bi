<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\QueryHandler;

use Doctrine\ORM\EntityManagerInterface;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\Query\GetDashboardPanelQueryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\MembershipBundle\Domain\Model\Member\Member;

/**
 * Class GetDashboardPanelQueryHandler
 * @package Orangear\BusinessIntelligence\Infrastructure\QueryHandler
 */
final class GetDashboardPanelQueryHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $memberRepository;

    /**
     * GetDashboardPanelQueryHandler constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->memberRepository = $this->entityManager->getRepository(Member::class);
    }

    /**
     * @param GetDashboardPanelQueryInterface $query
     * @return array
     */
    public function __invoke(GetDashboardPanelQueryInterface $query)
    {
        $memberId = $query->memberId();


        if (!($member = $this->memberRepository->findOneBy(['id' => $memberId]))) {
            throw new \InvalidArgumentException('Member not found.');
        }

        $ids = [];
        /** @var ProjectInterface $project */
        foreach ($member->getProjects() as $project) {
            $ids[] = (string) $project->projectId();
        }

        $data = [];
        foreach ($this->getSummary($ids) as $item) {
            $value['id']     = $item['id'];
            $value['name']   = $item['name'];
            $value['logo']   = $item['logo'];
            $value['token']  = $item['token'];
            $value['trend']  = (bool) $item['trend'];
            $value['income'] = $item['income'];

            $data[] = $value;
        }

        return $data;
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getSummary(array $ids = [])
    {
        $sql = "
            SELECT DISTINCT
              project.id,
              project.name,
              project.logo,
              project.token,
              ABS(dashboard.income_yesterday - dashboard.income_today) AS income,
              CASE WHEN (dashboard.income_yesterday - dashboard.income_today >= 0) THEN 0 ELSE 1 END AS trend
            FROM dashboard
              JOIN project ON project.id = dashboard.project_id
            WHERE ((dashboard.project_id, UNIX_TIMESTAMP(dashboard.synchronized_at)) IN (SELECT DISTINCT project_id, MAX(UNIX_TIMESTAMP(dashboard.synchronized_at)) AS synchronized_at FROM dashboard GROUP BY dashboard.project_id))";

        if (!empty($ids)) {
            $sql .= "
                AND project.id IN ('" . implode('\',\'', $ids) . "')
            ";
        }

        $sql .= "
            GROUP BY project.id, dashboard.income_today, dashboard.income_yesterday
            ORDER BY project.name ASC
        ";

        return $this->entityManager->getConnection()->executeQuery($sql)->fetchAll();
    }
}