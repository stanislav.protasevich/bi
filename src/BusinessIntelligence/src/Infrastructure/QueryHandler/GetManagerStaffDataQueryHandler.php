<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\QueryHandler;

use Doctrine\ORM\EntityManagerInterface;
use Orangear\Admin\Core\Domain\Model\BillingReport\BillingReport;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query\GetStaffDataByManagerQueryInterface;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query\GetStaffDataByProjectQueryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeType;
use Orangear\BusinessIntelligence\Domain\Model\Employee\Employee;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeCharge;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\ManagerSale;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployee;
use Doctrine\ORM\Query\Expr\Join;
use Orangear\MembershipBundle\Domain\Model\Member\Member;

/**
 * Class GetManagerStaffDataQueryHandler
 * @package Orangear\BusinessIntelligence\Infrastructure\QueryHandler
 */
final class GetManagerStaffDataQueryHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function __invoke(GetStaffDataByManagerQueryInterface $query)
    {
        $memberId = $query->memberId();

        $qb = $this->entityManager->createQueryBuilder();
        $qb->select(['m'])
            ->from(Member::class,'m')
            ->where('m' . '.id = :id')
            ->setParameter('id', $memberId);

        if (!($member = $qb->getQuery()->getOneOrNullResult())) {
            throw new \InvalidArgumentException('Member not found.');
        }

        $employeeId = $query->employeeId();

        $qb = $this->entityManager->createQueryBuilder();
        $qb->select(['e'])
            ->from(Employee::class,'e')
            ->where('e' . '.id = :id')
            ->setParameter('id', $employeeId);

        if (!($employee = $qb->getQuery()->getOneOrNullResult())) {
            throw new \InvalidArgumentException('Employee not found.');
        }

        $ids = [];
        /** @var ProjectInterface $project */
        foreach ($member->getProjects() as $project) {
            $ids[] = (string) $project->projectId();
        }

        $qb = $this->entityManager->createQueryBuilder();
        $qb->select(['re'])
            ->from(RemoteEmployee::class,'re')
            ->where('IDENTITY(' . 're' . '.employee) = :id')
            ->andWhere('IDENTITY(' . 're' . '.project) IN (:ids)')
            ->setParameter('id', $employee)
            ->setParameter('ids', $ids);

        if (!$qb->getQuery()->getResult()) {
            throw new \InvalidArgumentException('Corruption');
        }

        //@todo rewrite query

        $startDate = $query->startDate()->format('Y-m-d');
        $endDate = $query->endDate()->format('Y-m-d');
        $months = $this->getMonthRange($startDate, $endDate);

        $sql = "
            SELECT 
              employee.id AS employee_id,
              employee.first_name AS first_name,
              employee.last_name AS last_name,
              
              DAY(manager_sale_statistics.period) AS day,
              MONTH(manager_sale_statistics.period) AS month,
              YEAR(manager_sale_statistics.period) AS year,
              SUM(manager_sale_statistics.gross_sale) AS grossSale,
              SUM(manager_sale_statistics.gross_profit) AS grossProfit,
              SUM(manager_sale_statistics.scrub) AS scrub,
              manager_sale_statistics.period
            FROM employee
            JOIN manager_sale_statistics ON employee.id = manager_sale_statistics.employee_id
        ";

        $where = [];

        $employeeId = $query->employeeId();
        if ($employeeId !== null) {
            $where []= "employee.id = '" . $employeeId . "'";
        }

        $where[] = "manager_sale_statistics.period BETWEEN '" . $months[0] . "' AND '" . $endDate. "'";
        $where[] = "manager_sale_statistics.project_id IN ('" . implode("','", $ids) . "')";

        $sql .= ' WHERE ' . implode(' AND ', $where);

        $sql .= ' GROUP BY period, employee.id';

        $employee = $this->entityManager->getConnection()->executeQuery(
            'SELECT employee.*, project.id AS project_id, project.name as project_name FROM employee JOIN remote_employee ON employee.id = remote_employee.employee_id JOIN project ON remote_employee.project_id = project.id WHERE employee.id = "' . $employeeId . '"'
        )->fetch();

//        var_dump($employee); exit;

        $wages = $this->getWages([$employeeId], $months[0], $endDate);
        $wagesWithZeros = [];
        foreach ($months as $month) {
            if (key_exists($month, $wages) === false) {
                $wagesWithZeros[$month] = (float) 0;
            } else {
                $wagesWithZeros[$month] = (float) array_shift($wages);
            }
        }

        /***************************************************************************************/
        /*******************************TOTAL SALES*********************************************/
        /***************************************************************************************/
        $projects = $this->getTotalManagerData($employeeId, $months[0], $endDate);
        $employees = $this->getProjectEmployeeIdentifiers($projects);
        $totalSales = $this->getTotalSales($employees, $months[0], $endDate);

        $totalSalesWithZeros = [];
        foreach ($months as $month) {
            if (key_exists($month, $totalSales) === false) {
                $totalSalesWithZeros[$month] = [
                    'grossSale' => (float) 0,
                    'grossProfit' => (float) 0,
                    'scrub' => (float) 0
                ];
            } else {
                $totalSalesWithZeros[$month] = array_shift($totalSales);
                $totalSalesWithZeros[$month]['grossSale'] /= 2;
                $totalSalesWithZeros[$month]['grossProfit'] /= 2;
            }
        }
        /***************************************************************************************/

        /***************************************************************************************/
        /*******************************TOTAL WAGES*********************************************/
        /***************************************************************************************/
        $totalWages = $this->getWages($employees, $months[0], $endDate);
        $totalWagesWithZeros = [];
        foreach ($months as $month) {
            if (key_exists($month, $totalWages) === false) {
                $totalWagesWithZeros[$month] = (float) 0;
            } else {
                $totalWagesWithZeros[$month] = (float) array_shift($totalWages);
            }
        }
        /*************************************************************************************/

        $sales = [];
        foreach ($this->entityManager->getConnection()->executeQuery($sql)->fetchAll() as $sale) {
            $sales[$sale['period']] = [
                'grossSale' => (float) $sale['grossSale'],
                'grossProfit' => (float) $sale['grossProfit'],
                'scrub' => (float) $sale['scrub']
            ];
        }

        $salesWithZeros = [];
        foreach ($months as $month) {
            if (key_exists($month, $sales) === false) {
                $salesWithZeros[$month] = [
                    'grossSale' => (float) 0,
                    'grossProfit' => (float) 0,
                    'scrub' => (float) 0
                ];
            } else {
                $salesWithZeros[$month] = array_shift($sales);
            }
        }

        $response = [];
        $response['id'] = $employee['id'];
        $response['firstName'] = $employee['first_name'];
        $response['lastName'] = $employee['last_name'];
        $response['start_date'] = $startDate;
        $response['end_date'] = $endDate;

        $response['project']['id'] = $employee['project_id'];
        $response['project']['name'] = $employee['project_name'];
        $response['data'] = [];

        foreach ($salesWithZeros as $date => $saleWithZero) {
            $c = count($response['data']);
            if ($c <= 0) {
                $c = 1;
            }

            $data = [];

            $tmp['date'] = date('Y-m-01', strtotime($date));

            $data['date'] = $tmp['date'];

            $data['statistics']['grossSales'] = $saleWithZero['grossSale'];
            $data['statistics']['grossProfit'] = $saleWithZero['grossProfit'];
//            $data['statistics']['netProfit'] = $data['statistics']['grossProfit'] - $wagesWithZeros[$date];

//            $effectiveness = 0;
//            if ($wagesWithZeros[$date] != 0) {
//                $effectiveness = $data['statistics']['netProfit'] / $wagesWithZeros[$date];
//            }

//            $data['indexes']['netProfitDynamics'] = 0;
//            if (isset($response['data'][$c - 1])) {
//                $data['indexes']['netProfitDynamics'] = $data['statistics']['netProfit'] - $response['data'][$c - 1]['statistics']['netProfit'];
//            }

//            $effectivenessDynamics = 0;
//            if (isset($response['data'][$c - 1])) {
//                $effectivenessDynamics = $effectiveness - $response['data'][$c - 1]['indexes']['effectiveness'];
//            }

            $data['indexes']['problemSolving'] = 0;
            if ($saleWithZero['grossSale'] != 0) {
                $data['indexes']['problemSolving'] = (($saleWithZero['grossSale'] - $saleWithZero['scrub']) / $saleWithZero['grossSale'] * 100);
            }

//            $data['comparison']['relativeEffectiveness'] = 0;
//            if ($wagesWithZeros[$date] != 0 && $totalSalesWithZeros[$date]['grossProfit'] != 0) {
//                $data['comparison']['relativeEffectiveness'] =
//                    (($data['statistics']['netProfit'] / $wagesWithZeros[$date]) - (($totalSalesWithZeros[$date]['grossProfit'] - $totalWagesWithZeros[$date]) / $totalWagesWithZeros[$date])) / (($totalSalesWithZeros[$date]['grossProfit'] - $totalWagesWithZeros[$date]) / $totalWagesWithZeros[$date]) * 100;
//            }
//
//            $data['comparison']['dti'] = 0;
//            if ($data['statistics']['netProfit'] != 0) {
//                $data['comparison']['dti'] = ($wagesWithZeros[$date] / $data['statistics']['netProfit']) * 100;
//            }
//
//            $data['comparison']['scrubs'] = $saleWithZero['scrub'];
//            $data['comparison']['revenueReceivedPerManager'] = 0;
//            $data['comparison']['unpaidRevenuePerManager'] = 0;

            $response['data'][] = $data;
        }

        array_shift($response['data']);

        return $response;
    }

    /**
     * Get range of months
     *
     * @param $startDate
     * @param $endDate
     *
     * @return array
     */
    private function getMonthRange($startDate, $endDate)
    {
        $months = [];
        $month = strtotime('-1 month',strtotime($startDate));
        while($month < strtotime($endDate))
        {
            $months[] = date('Y-m-01', $month);
            $month = strtotime("+1 month", $month);
        }

        return $months;
    }

    /**
     * @param $employeeIdentifiers
     * @param $startDate
     * @param $endDate
     *
     * @return array
     */
    public function getWages($employeeIdentifiers, $startDate, $endDate)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb->select([
            'YEAR(' . 'c' . '.startDate' . ') AS year',
            'MONTH(' . 'c' . '.startDate' . ') AS month',
            'SUM(' . 'c' . '.amount' . ') AS wages',
        ])
            ->from(Charge::class, 'c')
            ->join(ChargeType::class, 'ct', Join::WITH, 'c' . '.chargeType = ' . 'ct' . '.id')
            ->join(EmployeeCharge::class, 'ec', Join::WITH, 'c' . '.id = ' . 'ec' . '.charge')
            ->where('IDENTITY(' . 'ec' . '.employee) IN (:in)')
            ->andWhere('c' . '.startDate >= :startDate AND ' . 'c' . '.endDate <= :endDate')
            ->andWhere('ct' . ".key = 'salary'")
            ->groupBy('year, month');

        $query->setParameters([
            ':in' => $employeeIdentifiers,
            ':startDate' => $startDate,
            ':endDate' => $endDate,
        ]);

        $wages = [];
        foreach ($query->getQuery()->getResult() as $wage) {
            $date = date('Y-m-d', strtotime($wage['year'] . '-' . $wage['month'] . '-01'));
            $wages[$date] = $wage['wages'];
        }

        return $wages;
    }

    /**
     * @param $employeeId
     * @param $startDate
     * @param $endDate
     *
     * @return array
     */
    private function getTotalManagerData($employeeId, $startDate, $endDate)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb
            ->select([
                'IDENTITY(' . 're' . '.project) as projectId'
            ])
            ->from(RemoteEmployee::class, 're')
            ->where('IDENTITY(' . 're' . '.employee) = :employeeId')
            ->setParameters([
                'employeeId' => $employeeId
            ]);

        $projectIds = [];
        foreach ($query->getQuery()->getResult() as $remoteEmployee) {
            $projectIds[] = $remoteEmployee['projectId'];
        }

        return $projectIds;
    }

    /**
     * @param array $projectIds
     *
     * @return array
     */
    private function getProjectEmployeeIdentifiers($projectIds = [])
    {
        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb->select(['IDENTITY(' . 're' . '.employee) AS employeeId'])
            ->from(RemoteEmployee::class, 're')
            ->where('IDENTITY(' . 're' . '.project) in (:in)')
            ->setParameters([
                'in' => $projectIds
            ]);

        $employeeIds = [];
        foreach ($query->getQuery()->getResult() as $employee) {
            $employeeIds[] = $employee['employeeId'];
        }

        return $employeeIds;
    }

    /**
     * @param $employees
     * @param $startDate
     * @param $endDate
     *
     * @return array
     */
    private function getTotalSales($employees, $startDate, $endDate)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb->select([
            'ms' . '.period',
            'SUM(' . 'ms' . '.grossSale) as grossSale',
            'SUM(' . 'ms' . '.grossProfit) as grossProfit',
            'SUM(' . 'ms' . '.scrub) as scrub',

            'DAY(' . 'ms' . '.period) as day',
            'MONTH(' . 'ms' . '.period) as month',
            'YEAR(' . 'ms' . '.period) as year',
        ])
            ->from(ManagerSale::class, 'ms')
            ->where('ms' . '.employee IN (:in)')
            ->andWhere('ms' . '.period BETWEEN :startDate AND :endDate')
            ->groupBy('ms' . '.period')
            ->setParameters([
                ':in' => $employees,
                ':startDate' => $startDate,
                ':endDate' => $endDate,
            ]);

        $sales = [];
        foreach ($query->getQuery()->getResult() as $sale) {
            $sales[$sale['period']->format('Y-m-d')] = [
                'grossSale' => (float) $sale['grossSale'],
                'grossProfit' => (float) $sale['grossProfit'],
                'scrub' => (float) $sale['scrub']
            ];
        }

        return $sales;
    }
}
