<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\QueryHandler;

use Doctrine\ORM\EntityManagerInterface;
use Orangear\BusinessIntelligence\Domain\Model\Panel\Query\GetStaffDataPanelQueryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\MembershipBundle\Domain\Model\Member\Member;
use Orangear\BusinessIntelligence\Domain\Model\Employee\Employee;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\ManagerSale;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployee;
use Doctrine\ORM\Query\Expr\Join;
/**
 * Class GetStaffDataPanelQueryHandler
 * @package Orangear\BusinessIntelligence\Infrastructure\QueryHandler
 */
final class GetStaffDataPanelQueryHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    private $memberRepository;

    /**
     * GetStaffDataPanelQueryHandler constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        $this->memberRepository = $this->entityManager->getRepository(Member::class);
    }

    /**
     * @param GetStaffDataPanelQueryInterface $query
     * @return array
     */
    public function __invoke(GetStaffDataPanelQueryInterface $query)
    {
        $memberId = $query->memberId();

        if (!($member = $this->memberRepository->findOneBy(['id' => $memberId]))) {
            throw new \InvalidArgumentException('Member not found.');
        }

        $ids = [];
        /** @var ProjectInterface $project */
        foreach ($member->getProjects() as $project) {
            $ids[] = (string) $project->projectId();
        }

        $data = [];
        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb
            ->select(['DISTINCT IDENTITY(' . 'ms' . '.employee)'])
            ->from(ManagerSale::class, 'ms');

        $qb2 = $this->entityManager->createQueryBuilder();

        $query2 = $qb2->select([
            'p' . '.id AS project_id',
            'p' . '.name AS project_name',
            'p' . '.logo AS project_logo',

            'e' . '.id AS employee_id',
            'e' . '.firstName AS employee_first_name',
            'e' . '.lastName AS employee_last_name',
        ])
            ->from(RemoteEmployee::class, 're')
            ->join(
                Employee::class,
                'e',
                Join::WITH,
                're' . '.employee' . '=' . 'e' . '.id')
            ->join(
                Project::class,
                'p',
                Join::WITH,
                'p' . '.id' . '=' . 're' . '.project')
            ->where($qb2->expr()->in('IDENTITY('. 're' . '.employee)', $query->getDQL()))
            ->andWhere('p' . '.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->orderBy('p' . '.name');

        $results = $query2->getQuery()->getResult();

        $projects = [];
        $employees = [];
        foreach ($results as $row) {
            $employee = [];
            $employee['id'] = $row['employee_id']->toString();
            $employee['first_name'] = $row['employee_first_name'];
            $employee['last_name'] = $row['employee_last_name'];

            if (!isset($employees[$employee['id']])) {
                $employees[$employee['id']] = $employee;
            }

            $project = [];
            $project['id'] = $row['project_id']->toString();

            $employees[$employee['id']]['projects'][$project['id']] = $project;
            $projects[$project['id']]['managers'][] = $employee;
        }

        foreach ($results as $row) {
            $project = [];
            $project['id'] = $row['project_id']->toString();
            $project['name'] = $row['project_name'];
            $project['logo'] = $row['project_logo'];

            foreach ($projects[$project['id']]['managers'] as $manager) {
                $project['managers'][] = $employees[$manager['id']];
            }

            $data[$project['id']] = $project;
        }

        $results = [];
        foreach ($data as $project) {

            $managers = [];
            foreach ($project['managers'] as $manager) {
                $managerProjects = array_values($manager['projects']);
                $manager['projects'] = $managerProjects;
                $managers[] = $manager;
            }

            $project['managers'] = $managers;

            $results[] = $project;
        }

        return $results;
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getSummary(array $ids = [])
    {
        $sql = "
            SELECT DISTINCT
              project.id,
              project.name,
              project.logo,
              project.token,
              ABS(dashboard.income_yesterday - dashboard.income_today) AS income,
              CASE WHEN (dashboard.income_yesterday - dashboard.income_today >= 0) THEN 0 ELSE 1 END AS trend
            FROM dashboard
              JOIN project ON project.id = dashboard.project_id
            WHERE ((dashboard.project_id, UNIX_TIMESTAMP(dashboard.synchronized_at)) IN (SELECT DISTINCT project_id, MAX(UNIX_TIMESTAMP(dashboard.synchronized_at)) AS synchronized_at FROM dashboard GROUP BY dashboard.project_id))";

        if (!empty($ids)) {
            $sql .= "
                AND project.id IN ('" . implode('\',\'', $ids) . "')
            ";
        }

        $sql .= "
            GROUP BY project.id, dashboard.income_today, dashboard.income_yesterday
            ORDER BY project.name ASC
        ";

        return $this->entityManager->getConnection()->executeQuery($sql)->fetchAll();
    }
}