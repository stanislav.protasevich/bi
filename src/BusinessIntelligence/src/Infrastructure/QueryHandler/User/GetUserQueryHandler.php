<?php

namespace Orangear\BusinessIntelligence\Infrastructure\QueryHandler\User;

use Orangear\BusinessIntelligence\Model\User\Exception\UserNotFound;
use Orangear\BusinessIntelligence\Model\User\Query\GetUserQueryInterface;
use Orangear\BusinessIntelligence\Model\User\Response\GetUserResponse;
use Orangear\BusinessIntelligence\Model\User\Response\GetUserResponseInterface;
use Orangear\BusinessIntelligence\Model\User\UserRepositoryInterface;
use React\Promise\Deferred;

/**
 * Class GetUserQuery
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\QueryHandler\User
 */
final class GetUserQueryHandler
{
    /** @var UserRepositoryInterface */
    private $userRepository;

    /**
     * GetUserQueryHandler constructor
     *
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param GetUserQueryInterface $query
     * @param Deferred|null $deferred
     *
     * @return GetUserResponseInterface
     */
    public function __invoke(GetUserQueryInterface $query, Deferred $deferred = null) : GetUserResponseInterface
    {
        $user = $this->userRepository->findByUsername($query->username());

        if ($user === null) {
            throw UserNotFound::withUsername($query->username());
        }

        $response = GetUserResponse::withUser($user);

        if (null === $deferred) {
            return $response;
        }

        $deferred->resolve($response);
    }
}
