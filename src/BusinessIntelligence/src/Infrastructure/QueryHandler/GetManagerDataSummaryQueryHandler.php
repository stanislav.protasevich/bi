<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\QueryHandler;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Orangear\BusinessIntelligence\Domain\Model\Employee\Employee;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query\GetManagerDataSummaryQueryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\ProjectRepository;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository\EmployeeRepository;
use React\Promise\Deferred;

/**
 * Class GetManagerDataSummaryQueryHandler
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\QueryHandler\User
 */
final class GetManagerDataSummaryQueryHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * GetManagerDataSummaryQueryHandler constructor
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param GetManagerDataSummaryQueryInterface $query
     * @param Deferred|null $deferred
     *
     * @return null
     */
    public function __invoke(GetManagerDataSummaryQueryInterface $query, Deferred $deferred = null)
    {
        /** @var Employee $manager */
        $manager = $this->getManager($query->employeeId());
        $managerStatistics =$this->getManagerStatistics($query->employeeId());
        $managerProjects = $this->getManagerProjects($query->employeeId());

        $response = [];
        $response['id'] = $manager->id()->toString();
        $response['firstName'] = $manager->firstName();
        $response['lastName'] = $manager->lastName();
        $response['position'] = 0;
        $response['employedSince'] = 0;

        $response['wage'] = 0;
        $response['totalGrossSales'] = $managerStatistics['gross_sale'];
        $response['totalGrossProfit'] = $managerStatistics['gross_profit'];
        $response['totalNetProfit'] = 0;

        /** @var Project $project */
        foreach ($managerProjects as $project) {
            $temp = [];
            $temp['id'] = $project['id'];
            $temp['name'] = $project['name'];

            $response['projects'][] = $temp;
        }

        if (null === $deferred) {
            return $response;
        }

        $deferred->resolve($response);
    }

    /**
     * @param $managerId
     * @return array
     */
    private function getManager($managerId)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select(['e'])
            ->from(Employee::class, 'e')
            ->where('e' . '.id = :id')
            ->setParameter('id', $managerId);

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @param string $managerId
     * @return array
     */
    private function getManagerProjects($managerId)
    {
        $sql = "
            SELECT 
              project.*
            FROM remote_employee
            JOIN employee ON remote_employee.employee_id = employee.id
            JOIN project ON project.id = remote_employee.project_id
            WHERE employee.id = '" . $managerId ."'
        ";

        return $this->entityManager->getConnection()->executeQuery($sql)->fetchAll();
    }

    /**
     * @param string $managerId
     * @return array
     */
    private function getManagerStatistics($managerId)
    {
        $sql = "
            SELECT 
              SUM(manager_sale_statistics.gross_sale) AS gross_sale,
              SUM(manager_sale_statistics.gross_profit) AS gross_profit
            FROM manager_sale_statistics
            WHERE manager_sale_statistics.employee_id = '" . $managerId ."'
        ";

        return $this->entityManager->getConnection()->executeQuery($sql)->fetch();
    }
}
