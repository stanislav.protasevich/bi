<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\QueryHandler\StaffData;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Orangear\BusinessIntelligence\Domain\Model\Employee\Employee;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\ManagerSale;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployee;
use Orangear\BusinessIntelligence\Domain\Model\StaffData\Query\ManagerKpiStatisticsQueryInterface;
use React\Promise\Deferred;
use DateTime;

/**
 * Class ManagerKpiStatisticsQueryHandler
 * @package Orangear\BusinessIntelligence\Infrastructure\QueryHandler\StaffData
 */
class ManagerKpiStatisticsQueryHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ManagerKpiStatisticsQueryHandler constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param ManagerKpiStatisticsQueryInterface $query
     * @param Deferred|null $deferred
     */
    public function __invoke(ManagerKpiStatisticsQueryInterface $query, Deferred $deferred = null)
    {
        $startDate = $query->startDate();
        $endDate   = $query->endDate();

        list($previousStartDate, $previousEndDate) = $this->getPreviousDates($startDate, $endDate);

        $previousStatistics = [];
        foreach ($this->getStatistics($previousStartDate, $previousEndDate) as $statistics) {
            $previousStatistics[$statistics['employee']['id']]['grossSale'] = $statistics['grossSale']['amount'];
            $previousStatistics[$statistics['employee']['id']]['grossProfit'] = $statistics['grossProfit']['amount'];
        }

        $currentStatistics = $this->getStatistics($startDate, $endDate);
        foreach ($currentStatistics as &$statistics) {
            if (!isset($previousStatistics[$statistics['employee']['id']])) {
                $statistics['grossSale']['trend'] = true;
                $statistics['grossProfit']['trend'] = true;

                $statistics['efficiency'] = 0;
                if ($statistics['grossSale']['amount'] != 0) {
                    $statistics['efficiency'] = ($statistics['grossProfit']['amount'] * 100) / $statistics['grossSale']['amount'];
                }

                continue;
            }

            $statistics['grossSale']['trend'] = true;
            if ($previousStatistics[$statistics['employee']['id']]['grossSale'] >= $statistics['grossSale']['amount']) {
                $statistics['grossSale']['trend'] = false;
            }

            $statistics['grossProfit']['trend'] = true;
            if ($previousStatistics[$statistics['employee']['id']]['grossProfit'] >= $statistics['grossProfit']['amount']) {
                $statistics['grossProfit']['trend'] = false;
            }

            $statistics['efficiency'] = 0;
            if ($statistics['grossSale']['amount'] != 0) {
                $statistics['efficiency'] = ($statistics['grossProfit']['amount'] * 100) / $statistics['grossSale']['amount'];
            }
        }

        $data = $currentStatistics;

        $deferred->resolve($data);
    }

    /**
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @return array
     */
    private function getPreviousDates(DateTime $startDate, DateTime $endDate) : array
    {
        $months = $endDate->diff($startDate)->m + 1;

        $temp = clone $startDate;
        $prevStartDate = $temp->modify('-' . $months . ' month');

        $temp = clone $startDate;
        $prevEndDate = $temp->modify('-1 day');

        return [$prevStartDate, $prevEndDate];
    }

    private function getStatistics(DateTime $startDate, DateTime $endDate)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb->select([
            'p' . '.id as projectId',
            'p' . '.name as projectName',
            'e' . '.id as employeeId',
            'e' . '.firstName as employeeFirstName',
            'e' . '.lastName as employeeLastName',
            'SUM(' . 'ms' . '.grossSale) as grossSale',
            'SUM(' . 'ms' . '.grossProfit) as grossProfit',
            'SUM(' . 'ms' . '.superlinkGrossSale) as superlinkGrossSale',
            'SUM(' . 'ms' . '.superlinkGrossProfit) as superlinkGrossProfit',
        ])
            ->from(ManagerSale::class, 'ms')
            ->join(
                Project::class,
                'p',
                Join::WITH,
                'ms' . '.project = ' . 'p' . '.id'
            )
            ->join(
                Employee::class,
                'e',
                Join::WITH,
                'ms' . '.employee = ' . 'e' . '.id'
            )
            ->where('ms' . '.period BETWEEN :startDate AND :endDate')
            ->groupBy('e' . '.id')
            ->addGroupBy('p' . '.id')
            ->setParameters([
                ':startDate' => $startDate,
                ':endDate' => $endDate,
            ]);

        $response = [];
        foreach ($query->getQuery()->getResult() as $sale) {
            $response[] = [
                'employee' => [
                    'id'          => (string) $sale['employeeId'],
                    'firstName'   => $sale['employeeFirstName'],
                    'lastName'    => $sale['employeeLastName'],
                ],
                'company' => [
                    'id'   => (string) $sale['projectId'],
                    'name' => $sale['projectName']
                ],
                'grossSale' => [
                    'amount' => (float) $sale['grossSale'] + (float) $sale['superlinkGrossSale'],
                    'trend' => null
                ],
                'grossProfit' => [
                    'amount' => (float) $sale['grossProfit'] + (float) $sale['superlinkGrossProfit'],
                    'trend' => null
                ],
                'tier' => [
                    'id' => md5((string)rand(1, 100)),
                    'name' => 'Tier #'
                ],
                'efficiency' => 0
            ];
        }

        return $response;
    }
}