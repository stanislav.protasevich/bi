<?php

declare(strict_types=1);

namespace Orangear\BusinessIntelligence\Infrastructure\QueryHandler\StaffData;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Orangear\BusinessIntelligence\Domain\Model\Employee\Employee;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\ManagerSale;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployee;
use Orangear\BusinessIntelligence\Domain\Model\StaffData\Query\CompanyKpiChartQuery;
use Orangear\BusinessIntelligence\Domain\Model\StaffData\Query\CompanyKpiChartQueryInterface;
use Orangear\BusinessIntelligence\Domain\Model\StaffData\Query\ManagerKpiChartQueryInterface;
use React\Promise\Deferred;

/**
 * Class ManagerKpiChartQueryHandler
 * @package Orangear\BusinessIntelligence\Infrastructure\QueryHandler\StaffData
 */
final class ManagerKpiChartQueryHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CompanyKpiChartQueryHandler constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param ManagerKpiChartQueryInterface $query
     * @param Deferred|null $deferred
     */
    public function __invoke(ManagerKpiChartQueryInterface $query, Deferred $deferred = null)
    {
        $startDate = $query->startDate();
        $endDate = $query->endDate();
        $direction = $query->direction();

        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb->select( [
            'e' . '.id as employeeId',
            'e' . '.firstName as employeeFirstName',
            'e' . '.lastName as employeeLastName',
            '(SUM(' . 'ms' . '.grossSale) + SUM(' . 'ms' . '.superlinkGrossSale)) as grossSale',
            '(SUM(' . 'ms' . '.grossProfit) + SUM(' . 'ms' . '.superlinkGrossProfit)) as grossProfit',
        ] )
            ->from( ManagerSale::class, 'ms' )
            ->join(
                Employee::class,
                'e',
                Join::WITH,
                'ms' . '.employee = ' . 'e' . '.id'
            )
            ->where( 'ms' . '.period BETWEEN :startDate AND :endDate' )
            ->having( 'grossProfit > 0' )
            ->groupBy( 'e' . '.id' )
            ->setMaxResults( 10 )
            ->setParameters( [
                ':startDate' => $startDate,
                ':endDate' => $endDate,
            ] );

        if ($direction == CompanyKpiChartQuery::DIRECTION_BEST) {
            $query->orderBy( 'grossProfit', 'DESC' );
        } else {
            $query->orderBy( 'grossProfit', 'ASC' );
        }

        $ids = [];
        $response = [];
        foreach ($query->getQuery()->getResult() as $sale) {
            $ids[] = (string)$sale['employeeId'];

            $response[(string)$sale['employeeId']] = [
                'company' => [
                    'id' => '-',
                    'name' => '-'
                ],
                'employee' => [
                    'id' => (string)$sale['employeeId'],
                    'firstName' => $sale['employeeFirstName'],
                    'lastName' => $sale['employeeLastName'],
                ],
                'amount' => (float)$sale['grossProfit'],
            ];
        }

        $db = $this->entityManager->getConnection();

        $sql = <<<SQL
SELECT mss.employee_id, p.name
FROM manager_sale_statistics AS mss
INNER JOIN project AS p ON mss.project_id = p.id
WHERE mss.employee_id IN (:IN)
GROUP BY mss.employee_id, p.name
SQL;

        $sql = str_replace(':IN', "'" . implode("','", $ids) . "'", $sql);
        $result = $db->query($sql);

        $companies = [];
        foreach ($result->fetchAll() as $sale) {
            $companies[$sale['employee_id']][] = $sale['name'];
        }

        foreach ($companies as $id => $_companies) {
            $response[$id]['company']['name'] = implode(', ', $_companies);
        }

        $deferred->resolve( $response );
    }
}
