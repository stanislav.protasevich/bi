<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\QueryHandler\StaffData;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\ManagerSale;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\StaffData\Query\CompanyKpiChartQuery;
use Orangear\BusinessIntelligence\Domain\Model\StaffData\Query\CompanyKpiChartQueryInterface;
use React\Promise\Deferred;

/**
 * Class CompanyKpiChartQueryHandler
 * @package Orangear\BusinessIntelligence\Infrastructure\QueryHandler\StaffData
 */
final class CompanyKpiChartQueryHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CompanyKpiChartQueryHandler constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param CompanyKpiChartQueryInterface $query
     * @param Deferred|null $deferred
     */
    public function __invoke(CompanyKpiChartQueryInterface $query, Deferred $deferred = null)
    {
        $startDate = $query->startDate();
        $endDate   = $query->endDate();
        $direction = $query->direction();

        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb->select([
            'p' . '.id as projectId',
            'p' . '.name as projectName',
            '((SUM(' . 'ms' . '.grossSale)/2) + SUM(' . 'ms' . '.superlinkGrossSale)) as grossSale',
            '((SUM(' . 'ms' . '.grossProfit)/2) + SUM(' . 'ms' . '.superlinkGrossProfit)) as grossProfit',
        ])
            ->from(ManagerSale::class, 'ms')
            ->join(
                Project::class,
                'p',
                Join::WITH,
                'ms' . '.project = ' . 'p' . '.id'
            )
            ->where('ms' . '.period BETWEEN :startDate AND :endDate')
            ->groupBy('p' . '.id')
            ->setMaxResults(10)
            ->setParameters([
                ':startDate' => $startDate,
                ':endDate' => $endDate,
            ]);

        if ($direction == CompanyKpiChartQuery::DIRECTION_BEST) {
            $query->orderBy('grossProfit', 'DESC');
        } else {
            $query->orderBy('grossProfit', 'ASC');
        }

        $response = [];
        foreach ($query->getQuery()->getResult() as $sale) {
            $response[] = [
                'company' => [
                    'id'   => (string) $sale['projectId'],
                    'name' => $sale['projectName']
                ],
                'amount' => (float) $sale['grossProfit']
            ];
        }

        $deferred->resolve($response);
    }
}
