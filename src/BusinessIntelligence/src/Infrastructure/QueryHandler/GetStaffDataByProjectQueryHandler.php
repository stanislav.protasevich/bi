<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Infrastructure\QueryHandler;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use MongoDB\Driver\Manager;
use Orangear\Admin\Core\Domain\Model\BillingReport\BillingReport;
use Orangear\Admin\Core\InfrastructureBundle\Repository\EmployeeRepository;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeType;
use Orangear\BusinessIntelligence\Domain\Model\Employee\Employee;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeCharge;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\ManagerSale;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query\GetStaffDataByManagerQueryInterface;
use Orangear\BusinessIntelligence\Domain\Model\ManagerSale\Query\GetStaffDataByProjectQueryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployee;
use React\Promise\Deferred;

/**
 * Class GetStaffDataByProjectQueryHandler
 *
 * @package Orangear\BusinessIntelligence\Infrastructure\QueryHandler
 */
final class GetStaffDataByProjectQueryHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * GetStaffDataByProjectQueryHandler constructor
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param GetStaffDataByProjectQueryInterface $query
     * @param Deferred|null $deferred
     *
     * @return null
     */
    public function __invoke(GetStaffDataByProjectQueryInterface $query, Deferred $deferred = null)
    {
        //@todo rewrite query

        $projectId = $query->projectId();

        $projectIds = [];
        if (!$projectId->isNull()) {
            $projectIds[] = $projectId->toString();
        }

        if (!$projects = $this->getProjects($projectIds)) {
            $projects = [];
        }

        $startDate = $query->startDate()->format('Y-m-d');
        $endDate = $query->endDate()->format('Y-m-d');
        $months = $this->getMonthRange($startDate, $endDate);

        $billingReport = $this->getBillingReport($projectId, $months[0], $endDate);

        $billingReportZeros = [];
        foreach ($months as $month) {
            if (key_exists($month, $billingReport) === false) {
                $billingReportZeros[$month]['revenueReceived'] = (float) 0;
                $billingReportZeros[$month]['revenueUnpaid'] = (float) 0;
            } else {
                $billingReportZeros[$month] = array_shift($billingReport);
            }
        }

        $projectEmployeeIdentifiers = $this->getProjectEmployeeIdentifiers($projects);

        $wages = $this->getWages($projectEmployeeIdentifiers, $months[0], $endDate);

        $wagesWithZeros = [];
        foreach ($months as $month) {
            if (key_exists($month, $wages) === false) {
                $wagesWithZeros[$month] = (float) 0;
            } else {
                $wagesWithZeros[$month] = (float) array_shift($wages);
            }
        }

        $qb = $this->entityManager->createQueryBuilder();
        $query = $qb->select([
            'ms' . '.period',
            'SUM(' . 'ms' . '.grossSale) as grossSale',
            'SUM(' . 'ms' . '.grossProfit) as grossProfit',
            'SUM(' . 'ms' . '.scrub) as scrub',

            'DAY(' . 'ms' . '.period) as day',
            'MONTH(' . 'ms' . '.period) as month',
            'YEAR(' . 'ms' . '.period) as year',
        ])
            ->from(ManagerSale::class, 'ms')
            ->where('ms' . '.employee IN (:in)')
            ->andWhere('ms' . '.period BETWEEN :start AND :end')
            ->groupBy('ms' . '.period')
            ->setParameters([
                ':in' => $projectEmployeeIdentifiers,
                ':start' => $months[0],
                ':end' => $endDate,
            ]);

        $sales = [];
        foreach ($query->getQuery()->getResult() as $sale) {
            $sales[$sale['period']->format('Y-m-d')] = [
                'grossSale' => (float) $sale['grossSale'],
                'grossProfit' => (float) $sale['grossProfit'],
                'scrub' => (float) $sale['scrub']
            ];
        }

        $salesWithZeros = [];
        foreach ($months as $month) {
            if (key_exists($month, $sales) === false) {
                $salesWithZeros[$month] = [
                    'grossSale' => (float) 0,
                    'grossProfit' => (float) 0,
                    'scrub' => (float) 0
                ];
            } else {
                $salesWithZeros[$month] = array_shift($sales);
                $salesWithZeros[$month]['grossSale'] /= 2;
                $salesWithZeros[$month]['grossProfit'] /= 2;
            }
        }

        /* COUNTS */
        $qb = $this->entityManager->createQueryBuilder();
        $query = $qb->select([
            'ms' . '.period',
            'COUNT (DISTINCT(' . 'ms' . '.employee' . ')) AS counter',
        ])
            ->from(ManagerSale::class, 'ms')
            ->join(Employee::class, 'e', Join::WITH, 'e' . '.id = ' . 'ms' . '.employee')
            ->join(RemoteEmployee::class, 're', Join::WITH, 'e' . '.id = ' . 're' . '.employee')
            ->join(Project::class, 'p', Join::WITH, 're' . '.project = ' . 'p' . '.id')
            ->where('IDENTITY(' . 're' . '.project) IN (:in)')
            ->andWhere('ms' . '.period BETWEEN :startDate AND :endDate')
            ->groupBy('ms' . '.period')
            ->setParameters([
                ':in' => $projects,
                ':startDate' => $months[0],
                ':endDate' => $endDate,
            ]);

        $counts = [];
        foreach ($query->getQuery()->getResult() as $result) {
            $counts[$result['period']->format('Y-m-d')] = (int) $result['counter'];
        }

        $countsZeros = [];
        foreach ($months as $month) {
            if (key_exists($month, $counts) === false) {
                $countsZeros[$month] = 0;
            } else {
                $countsZeros[$month] = array_shift($counts);
            }
        }

        $totalSales = $this->getTotalSales($months[0], $endDate);

        $totalSalesWithZeros = [];
        foreach ($months as $month) {
            if (key_exists($month, $totalSales) === false) {
                $totalSalesWithZeros[$month] = [
                    'grossSale' => (float) 0,
                    'grossProfit' => (float) 0,
                    'scrub' => (float) 0,
                ];
            } else {
                $totalSalesWithZeros[$month] = array_shift($totalSales);
                $totalSalesWithZeros[$month]['grossSale'] /= 2;
                $totalSalesWithZeros[$month]['grossProfit'] /= 2;
            }
        }

        $totalWages = $this->getTotalWages($months[0], $endDate);
        $totalWagesWithZeros = [];
        foreach ($months as $month) {
            if (key_exists($month, $totalWages) === false) {
                $totalWagesWithZeros[$month] = 0;
            } else {
                $totalWagesWithZeros[$month] = (float) array_shift($totalWages);
            }
        }

//        var_dump($salesWithZeros);
//        var_dump($wagesWithZeros);
//        var_dump("@@@");
//        var_dump($totalSalesWithZeros);
//        var_dump($totalWagesWithZeros);
//exit;

//        var_dump($salesWithZeros);
//        var_dump($wagesWithZeros);
//        var_dump($totalSalesWithZeros);
//        var_dump($totalWagesWithZeros);
//        exit;

        $response = [];

        if (!$projectId->isNull()) {
            $response['id'] = $projects[0]['id']->toString();
            $response['name'] = $projects[0]['name'];
        }

        $response['startDate'] = $startDate;
        $response['endDate'] = $endDate;
        $response['data'] = [];

        foreach ($salesWithZeros as $date => $saleWithZero) {
            $c = count($response['data']);
            if ($c <= 0) {
                $c = 1;
            }

            $data = [];

            $tmp['date'] = date('Y-m-01', strtotime($date));

            $divider = 1;
            if ($countsZeros[$tmp['date']] != 0) {
                $divider = $countsZeros[$tmp['date']];
            }

            $data['date'] = $tmp['date'];

            $data['statistics']['grossSales'] = $saleWithZero['grossSale'];
            $data['statistics']['grossSales'] /= $divider;

            $data['statistics']['grossProfit'] = $saleWithZero['grossProfit'];
            $data['statistics']['grossProfit'] /= $divider;

            $data['statistics']['netProfit'] = $saleWithZero['grossProfit'] - $wagesWithZeros[$date];
            $data['statistics']['netProfit'] /= $divider;

            /********************************************************************************************/
            $data['indexes']['effectiveness'] = 0;
            if ($wagesWithZeros[$date] != 0) {
                $data['indexes']['effectiveness'] = ($saleWithZero['grossProfit'] - $wagesWithZeros[$date]) / $wagesWithZeros[$date];
            }

            $data['indexes']['netProfitDynamics'] = 0;
            if (isset($response['data'][$c - 1])) {
                $data['indexes']['netProfitDynamics'] = $data['statistics']['netProfit'] - $response['data'][$c - 1]['statistics']['netProfit'];
            }

            $data['indexes']['effectivenessDynamics'] = 0;
            if (isset($response['data'][$c - 1])) {
                $data['indexes']['effectivenessDynamics'] = $data['indexes']['effectiveness'] - $response['data'][$c - 1]['indexes']['effectiveness'];
            }
            $data['indexes']['effectivenessDynamics'] /= $divider;

            $data['indexes']['problemSolving'] = 0;
            if ($data['statistics']['grossSales'] != 0) {
                $data['indexes']['problemSolving'] = (($saleWithZero['grossSale'] - $saleWithZero['scrub']) / $saleWithZero['grossSale']) * 100;
            }
            /****************************************************************************************************/

            $data['comparison']['relativeEffectiveness'] = 0;
            if ($wagesWithZeros[$date] != 0 && $totalWagesWithZeros[$date] != 0) {

                $projectNetProfit = ($saleWithZero['grossProfit'] - $wagesWithZeros[$date]) / $wagesWithZeros[$date];

                $totalProjectNetProfit = ($totalSalesWithZeros[$date]['grossProfit'] - $totalWagesWithZeros[$date]) / $totalWagesWithZeros[$date];

//                var_dump("Month: " .$date);
//                var_dump("---");
//                var_dump("Profit: " . ($saleWithZero['grossProfit']));
//                var_dump("Wage: " . ($wagesWithZeros[$date]));
//                var_dump("Net profit: " . ($saleWithZero['grossProfit'] - $wagesWithZeros[$date]));
//                var_dump("---");
//                var_dump("Total Profit: " . ($totalSalesWithZeros[$date]['grossProfit']));
//                var_dump("Total Wage: " . ($totalWagesWithZeros[$date]));
//                var_dump("Total Net Profit: " . ($totalSalesWithZeros[$date]['grossProfit'] - $totalWagesWithZeros[$date]));
//                var_dump("@@@");
//                var_dump($projectNetProfit);
//                var_dump($totalProjectNetProfit);
//                var_dump(($projectNetProfit - $totalProjectNetProfit) / $totalProjectNetProfit * 100);
//                var_dump("!!!");

                $data['comparison']['relativeEffectiveness'] = ($projectNetProfit - $totalProjectNetProfit) / $totalProjectNetProfit * 100;
            }

            $data['comparison']['dti'] = 0;
            if ($data['statistics']['netProfit'] != 0) {

                $data['comparison']['dti'] = (($wagesWithZeros[$date] / ($saleWithZero['grossProfit'] - $wagesWithZeros[$date])) * 100) * 100;
            }

            $data['comparison']['scrubs'] = 0;
            if ($countsZeros[$date] != 0) {
                $data['comparison']['scrubs'] = $saleWithZero['scrub'] / $divider;
            }

            $data['comparison']['revenueReceivedPerManager'] = 0;
            if ($countsZeros[$date] != 0) {
                $data['comparison']['revenueReceivedPerManager'] = $billingReportZeros[$date]['revenueReceived'] / $countsZeros[$date];
            }

            $data['comparison']['unpaidRevenuePerManager'] = 0;
            if ($countsZeros[$date] != 0) {
                $data['comparison']['unpaidRevenuePerManager'] = $billingReportZeros[$date]['revenueUnpaid'] / $countsZeros[$date];
            }

            $response['data'][] = $data;

//        $resultWithZeros = [];
//        foreach ($months as $month) {
//            if (!in_array($month, $existingMoths)) {
//                $tmp = [];
//
//                $tmp['date'] = $month;
//
//                $tmp['statistics']['grossSales'] = 0;
//                $tmp['statistics']['grossProfit'] = 0;
//                $tmp['statistics']['netProfit'] = 0;
//
//                $tmp['indexes']['effectiveness'] = 0;
//                $tmp['indexes']['netProfitDynamics'] = 0;
//                $tmp['indexes']['effectivenessDynamics'] = 0;
//                $tmp['indexes']['problemSolving'] = 0;
//
//                $tmp['comparison']['relativeEffectiveness'] = 0;
//                $tmp['comparison']['dti'] = 0;
//                $tmp['comparison']['relativeProblemSolving'] = 0;
//                $tmp['comparison']['newNonZeroCustomers'] = 0;
//                $tmp['comparison']['netIncomePerManager'] = 0;
//                $tmp['comparison']['revenueReceivedPerManager'] = 0;
//                $tmp['comparison']['unpaidRevenuePerManager'] = 0;
//                $tmp['comparison']['rejectionsPerManager'] = 0;
//
//                $resultWithZeros[] = $tmp;
//            } else {
//                $resultWithZeros[] = array_shift($response['data']);
//            }

        }

        array_shift($response['data']);

        $deferred->resolve($response);
    }

    /**
     * @param $employeeIdentifiers
     * @param $startDate
     * @param $endDate
     *
     * @return array
     */
    public function getWages($employeeIdentifiers, $startDate, $endDate)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb->select([
            'YEAR(' . 'c' . '.startDate' . ') AS year',
            'MONTH(' . 'c' . '.startDate' . ') AS month',
            'SUM(' . 'c' . '.amount' . ') AS wages',
        ])
            ->from(Charge::class, 'c')
            ->join(ChargeType::class, 'ct', Join::WITH, 'c'  . '.chargeType = ' . 'ct' . '.id')
            ->join(EmployeeCharge::class, 'ec', Join::WITH, 'c'  . '.id = ' . 'ec' . '.charge')
            ->where('IDENTITY(' . 'ec' . '.employee) IN (:in)')
            ->andWhere('c'  . '.startDate >= :startDate AND ' . 'c'  . '.endDate <= :endDate')
            ->andWhere('ct' . ".key = 'salary'")
            ->groupBy('year, month');

        $query->setParameters([
            ':in' => $employeeIdentifiers,
            ':startDate' => $startDate,
            ':endDate' => $endDate,
        ]);

        $wages = [];
        foreach ($query->getQuery()->getResult() as $wage) {
            $date = date('Y-m-d', strtotime($wage['year'] . '-' . $wage['month'] . '-01'));
            $wages[$date] = $wage['wages'];
        }

        return $wages;
    }

    /**
     * Get range of months
     *
     * @param $startDate
     * @param $endDate
     *
     * @return array
     */
    private function getMonthRange($startDate, $endDate)
    {
        $months = [];
        $month = strtotime('-1 month',strtotime($startDate));
        while($month < strtotime($endDate))
        {
            $months[] = date('Y-m-01', $month);
            $month = strtotime("+1 month", $month);
        }

        return $months;
    }

    /**
     * @param array $projectIdentifiers
     *
     * @return array
     */
    private function getProjects($projectIdentifiers = [])
    {
        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb->select(['p' . '.id', 'p' . '.name'])
            ->from(Project::class, 'p');

        if (count($projectIdentifiers) > 0) {
            $query->where('p' . '.id IN (:in)')
                ->setParameters([':in' => $projectIdentifiers]);
        }

        return $query->getQuery()->getResult();
    }

    /**
     * @param array $projectIds
     *
     * @return array
     */
    private function getProjectEmployeeIdentifiers($projectIds = [])
    {
        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb->select(['IDENTITY(' . 're' . '.employee) AS employeeId'])
            ->from(RemoteEmployee::class, 're')
            ->where('IDENTITY(' . 're' . '.project) in (:in)')
            ->setParameters([
                'in' => $projectIds
            ]);

        $employeeIds = [];
        foreach ($query->getQuery()->getResult() as $employee) {
            $employeeIds[] = $employee['employeeId'];
        }

        return $employeeIds;
    }

    private function getBillingReport($projectIds = [], $startDate, $endDate)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb
            ->select([
                'br' . '.date',
                'SUM(' . 'br' . '.revenueReceived) as revenueReceived',
                'SUM(' . 'br' . '.revenueUnpaid) as revenueUnpaid',
            ])
            ->from(BillingReport::class, 'br')
            ->where('IDENTITY(' . 'br' . '.project) in (:in)')
            ->andWhere('br' . '.date >= :startDate AND ' . 'br' . '.date <= :endDate')
            ->groupBy('br' . '.date')
            ->setParameters([
                ':in' => $projectIds,
                ':startDate' => $startDate,
                ':endDate' => $endDate,
            ]);

        $reports = [];
        foreach ($query->getQuery()->getResult() as $report) {
            $reports[$report['date']->format('Y-m-d')]['revenueReceived'] = (float) $report['revenueReceived'];
            $reports[$report['date']->format('Y-m-d')]['revenueUnpaid'] = (float) $report['revenueUnpaid'];
        }

        return $reports;
    }

    /**
     * @param $startDate
     * @param $endDate
     *
     * @return array
     */
    public function getTotalSales($startDate, $endDate)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb->select([
            'ms' . '.period',
            'SUM(' . 'ms' . '.grossSale) as grossSale',
            'SUM(' . 'ms' . '.grossProfit) as grossProfit',
            'SUM(' . 'ms' . '.scrub) as scrub',

            'DAY(' . 'ms' . '.period) as day',
            'MONTH(' . 'ms' . '.period) as month',
            'YEAR(' . 'ms' . '.period) as year',
        ])
            ->from(ManagerSale::class, 'ms')
            ->where('ms' . '.period BETWEEN :startDate AND :endDate')
            ->groupBy('ms' . '.period')
            ->setParameters([
                ':startDate' => $startDate,
                ':endDate' => $endDate,
            ]);

        $sales = [];
        foreach ($query->getQuery()->getResult() as $sale) {
            $sales[$sale['period']->format('Y-m-d')] = [
                'grossSale' => (float) $sale['grossSale'],
                'grossProfit' => (float) $sale['grossProfit'],
                'scrub' => (float) $sale['scrub']
            ];
        }

        return $sales;
    }

    public function getTotalWages($startDate, $endDate)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $query = $qb->select([
            'YEAR(' . 'c' . '.startDate' . ') AS year',
            'MONTH(' . 'c' . '.startDate' . ') AS month',
            'SUM(' . 'c' . '.amount' . ') AS wages',
        ])
            ->from(Charge::class, 'c')
            ->join(ChargeType::class, 'ct', Join::WITH, 'c' . '.chargeType = ' . 'ct' . '.id')
            ->join(EmployeeCharge::class, 'ec', Join::WITH, 'c' . '.id = ' . 'ec' . '.charge')
            ->where('c' . '.startDate >= :startDate AND ' . 'c' . '.endDate <= :endDate')
            ->andWhere('ct' . ".key = 'salary'")
            ->groupBy('year, month');

        $query->setParameters([
            ':startDate' => $startDate,
            ':endDate' => $endDate,
        ]);

        $wages = [];
        foreach ($query->getQuery()->getResult() as $wage) {
            $date = date('Y-m-d', strtotime($wage['year'] . '-' . $wage['month'] . '-01'));
            $wages[$date] = $wage['wages'];
        }

        return $wages;
    }
}
