<?php

namespace Orangear\Admin\OwnerContext\Domain\Model\Owner;

/**
 * Class Owner
 *
 * @package Orangear\Admin\OwnerContext\Domain\Model\Owner
 */
class Owner
{
    /** @var int Owner identifier */
    private $id;

    /** @var string Owner name */
    private $name;

    /**
     * Owner constructor
     *
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
}