<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\Charge;

use Orangear\Admin\Core\ApplicationBundle\Command\Charge\CreateChargeCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\Admin\Core\Domain\Model\Charge\Command\DeleteChargeCommand;
use Orangear\Admin\Core\Domain\Model\Charge\Command\UpdateChargeCommand;
use Prooph\Common\Messaging\MessageFactory;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\Exception\CommandDispatchException;
use Psr\Http\Message\ServerRequestInterface;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UpdateChargeController
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Controller\Charge
 */
final class UpdateChargeController
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * DeleteChargeController constructor
     *
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @ApiDoc(
     *   section = "Charges",
     *   description = "Update charge",
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   requirements = {
     *     {"name" = "id", "dataType" = "string", "required" = true, "description" = "Charge identifier"},
     *   },
     *   parameters = {
     *     {"name" = "start_date", "dataType" = "string", "required" = true, "description" = "Charge start date"},
     *     {"name" = "end_date", "dataType" = "string", "required" = true, "description" = "Charge end date"},
     *     {"name" = "amount", "dataType" = "string", "required" = true, "description" = "Charge amount"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        $payload = $this->getPayloadFromRequest($request);

        $command = UpdateChargeCommand::withData(
            $payload['charge_id'],
            $payload['start_date'],
            $payload['end_date'],
            $payload['amount']
        );

        try {
            $this->commandBus->dispatch($command);
        } catch (CommandDispatchException $ex) {

            return JsonResponse::create(
                ['message' => $ex->getPrevious()->getPrevious()->getMessage()],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        } catch (\Throwable $error) {
            return JsonResponse::create(['message' => $error->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return JsonResponse::create(null, Response::HTTP_OK);
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return array
     *
     * @throws \Exception
     */
    public function getPayloadFromRequest(ServerRequestInterface $request)
    {
        $payload = null;

        if (($chargeId = $request->getAttribute('id'))) {
            $payload['charge_id'] = $chargeId;
        }

        $queryParams = $request->getParsedBody();

        $payload['start_date'] = $queryParams['start_date'] ?? null;
        $payload['end_date'] = $queryParams['end_date'] ?? null;
        $payload['amount'] = $queryParams['amount'] ?? null;

        return $payload;
    }
}