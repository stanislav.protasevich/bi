<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\Charge;

use Orangear\Admin\Core\ApplicationBundle\Command\Charge\CreateChargeCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Psr\Http\Message\ServerRequestInterface;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class ChargeCreateController
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Controller\ChargeType
 */
class ChargeCreateController
{
    /** @var MessageBus */
    private $commandBus;

    /**
     * ChargeTypeCreateController constructor
     *
     * @param MessageBus $commandBus
     */
    public function __construct(MessageBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @ApiDoc(
     *   section = "Charges",
     *   description = "Make charge",
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   parameters = {
     *     {"name" = "project_id", "dataType" = "string", "required" = true, "description" = "Charge project identifier"},
     *     {"name" = "charge_type_id", "dataType" = "string", "required" = true, "description" = "Charge type identifier"},
     *     {"name" = "employee_id", "dataType" = "string", "required" = false, "description" = "Charge employee"},
     *     {"name" = "start_date", "dataType" = "string", "required" = true, "description" = "Charge start date"},
     *     {"name" = "end_date", "dataType" = "string", "required" = true, "description" = "Charge end date"},
     *     {"name" = "type", "dataType" = "string", "required" = true, "description" = "Charge type"},
     *     {"name" = "amount", "dataType" = "string", "required" = true, "description" = "Charge amount"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $command = CreateChargeCommand::handleRequest($request);

            $this->commandBus->handle($command);

            return new JsonResponse(['success'], 200);
        }
        catch (CommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}