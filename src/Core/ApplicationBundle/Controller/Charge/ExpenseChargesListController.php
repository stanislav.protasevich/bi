<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\Charge;

use Orangear\Admin\Core\ApplicationBundle\Command\ChargeType\CreateChargeTypeCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\Admin\Core\ApplicationBundle\Query\Charge\FindExpenseCharges;
use Orangear\Admin\Core\ApplicationBundle\Query\ChargeType\AllChargeType;
use Prooph\ServiceBus\QueryBus;
use Psr\Http\Message\ServerRequestInterface;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class ExpenseChargesListController
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Controller\Charge
 */
final class ExpenseChargesListController
{
    /** @var QueryBus */
    private $queryBus;

    /**
     * ChargeListController constructor
     *
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @ApiDoc(
     *   section = "Charges",
     *   description = "Get list of charge expenses",
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   parameters = {
     *     {"name" = "project_id", "dataType" = "string", "required" = true, "description" = "Charge project identifier"},
     *     {"name" = "start_date", "dataType" = "string", "required" = true, "description" = "Charge start date"},
     *     {"name" = "end_date", "dataType" = "string", "required" = true, "description" = "Charge end date"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        $parameters = $request->getQueryParams();

        try {
            $data = [];

            $query = new FindExpenseCharges(
                $parameters['project_id'] ?? null,
                $parameters['start_date'] ?? null,
                $parameters['end_date'] ?? null
            );

            $response = $this->queryBus->dispatch($query);
            $response->then(function($result) use (&$data) { $data = $result; })
                ->otherwise(function($result){var_dump($result->getPrevious());});

            return new JsonResponse(['data' => $data], 200);
        }
        catch (CommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}