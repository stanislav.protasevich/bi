<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\Charge;

use Orangear\Admin\Core\ApplicationBundle\Command\ChargeType\CreateChargeTypeCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\Admin\Core\ApplicationBundle\Query\Charge\FindExpenseCharges;
use Orangear\Admin\Core\ApplicationBundle\Query\ChargeType\AllChargeType;
use Orangear\Admin\Core\Domain\Model\Charge\Query\GetExpenseChargeById;
use Orangear\Admin\Core\Domain\Model\Charge\Query\GetSalaryChargeById;
use Prooph\ServiceBus\QueryBus;
use Psr\Http\Message\ServerRequestInterface;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class GetSalaryController
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Controller\Charge
 */
final class GetSalaryController
{
    /** @var QueryBus */
    private $queryBus;

    /**
     * ChargeListController constructor
     *
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @ApiDoc(
     *   section = "Charges",
     *   description = "Get salary charge",
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   requirements = {
     *     {"name" = "id", "dataType" = "string", "required" = true, "description" = "Charge identifier"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        $payload = $this->getPayloadFromRequest($request);

        try {
            $data = [];

            $query = GetSalaryChargeById::withData(
                $payload['charge_id'] ?? null
            );

            $response = $this->queryBus->dispatch($query);

            $response->then(function($result) use (&$data) {
                $data = $result;
            })->otherwise(function($result){
                //@todo throw exception

                var_dump($result->getPrevious());
            });

            return new JsonResponse(['data' => $data], 200);
        }
        catch (CommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return array
     *
     * @throws \Exception
     */
    protected function getPayloadFromRequest(ServerRequestInterface $request)
    {
        $payload = null;

        if (($chargeId = $request->getAttribute('id'))) {
            $payload['charge_id'] = $chargeId;
        }

        return $payload;
    }
}