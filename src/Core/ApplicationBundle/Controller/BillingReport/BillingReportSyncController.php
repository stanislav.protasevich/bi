<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\BillingReport;

use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\BillingReport\BillingReportListCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\BillingReport\BillingReportsSyncCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\BillingReport\BillingReportSyncCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\BillingReportResponse;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\Admin\Core\ApplicationBundle\Command\ListResponse;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\ProjectListCommand;
use Orangear\Admin\Core\Domain\Model\BillingReport\BillingReport;
use Orangear\Admin\Core\Domain\Model\BillingReport\BillingReportSyncCommandCollection;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\Email;
use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\SuperAdmin;
use Pagerfanta\Pagerfanta;
use Psr\Http\Message\ServerRequestInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use SimpleBus\Message\Bus\MessageBus;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Class BillingReportSyncController
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Controller\BillingReport
 */
class BillingReportSyncController
{
    /** @var MessageBus */
    private $commandBus;

    /**
     * BillingReportSyncController constructor
     *
     * @@param MessageBus $commandBus
     */
    public function __construct(MessageBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @ApiDoc(
     *   section = "Billing Report",
     *   description = "Sync project's billing report",
     *   requirements = {
     *     {
     *       "name"        = "reports",
     *       "dataType"    = "string",
     *       "description" = "Billing report"
     *     },
     *   },
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $command = BillingReportsSyncCommand::handleRequest($request);

            $this->commandBus->handle($command);

            return new JsonResponse(['success'], 200);
        }
        catch (CommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}