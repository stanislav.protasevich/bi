<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\BillingReport;

use Orangear\Admin\Core\ApplicationBundle\Command\BillingReport\BillingReportListCommand;
use Orangear\Admin\Core\Domain\ServiceInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Psr\Http\Message\ServerRequestInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Zend\Diactoros\Response\JsonResponse;

class BillingReportExportController
{
    /**
     * @const int
     */
    const DEFAULT_PAGE = 1;

    /**
     * @const int
     */
    const DEFAULT_SIZE = 1000;

    /** @var ServiceInterface */
    private $service;

    /**
     * ProjectListController constructor
     *
     * @param ServiceInterface $service
     */
    public function __construct(ServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @ApiDoc(
     *   section = "Billing Report",
     *   description = "Export billing reports",
     *   requirements = {
     *     {
     *       "name"        = "project_id",
     *       "dataType"    = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Project identifier"
     *     },
     *     {
     *       "name"        = "start_date",
     *       "dataType"    = "string",
     *       "requirement" = "\d+",
     *       "description" = "Start date"
     *     },
     *     {
     *       "name"        = "end_date",
     *       "dataType"    = "string",
     *       "requirement" = "\d+",
     *       "description" = "End date"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $command = BillingReportListCommand::handleRequest($request);
            try {
                $data = $this->service->execute($command);
            } catch (\InvalidArgumentException $exception) {
                return new JsonResponse([], 404);
            }

            $arrayData = [
                ['Project', 'Date', 'Revenue', 'Deduction', 'Cross Payment', 'Net Revenue', 'Revenue Received', 'Revenue Unpaid', 'Pub Amount', 'Pub paid', 'Pub unpaid', 'Gross Income', 'Net Income', 'Total Expenses', 'Net Profit']
            ];

            foreach ($data['data'] as $d) {
                $arrayData[] = [
                    $d['project']['name'],
                    $d['date'],
                    number_format($d['revenue'], 2, '.', ''),
                    number_format($d['deduction'],2, '.', ''),
                    number_format($d['crossPayment'],2, '.', ''),

                    number_format($d['revenue'],2, '.', ''),
                    number_format($d['revenueReceived'],2, '.', ''),
                    number_format($d['revenueUnpaid'],2, '.', ''),
                    number_format($d['publisherAmount'],2, '.', ''),
                    number_format($d['publisherPaid'],2, '.', ''),

                    number_format($d['publisherUnpaid'],2, '.', ''),
                    number_format($d['grossIncome'],2, '.', ''),
                    number_format($d['netIncome'],2, '.', ''),
                    number_format($d['totalExpenses'],2, '.', ''),
                    number_format($d['netProfit'],2, '.', ''),
                ];
            }

            if(count($arrayData) < 0) {
                return new JsonResponse(['errors' => 'No data'], 400);
            }

            $columns = array_slice(range('A', 'Z'), 0, count($arrayData[0]));

            $spreadsheet = new Spreadsheet();
            $spreadsheet->getActiveSheet()->freezePane('A2');
            $spreadsheet->getActiveSheet()->fromArray($arrayData, null, 'A1');

            foreach ($columns as $column) {
                $spreadsheet->getActiveSheet()->getColumnDimension($column)->setAutoSize(true);
            }

            $spreadsheet->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true);
            $spreadsheet->getActiveSheet()->getStyle('A1:O1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $count = count($data['data']);
            foreach ($columns as $column) {
                $spreadsheet->getActiveSheet()
                    ->setCellValueExplicit(
                        $column . ($count + 2),
                        '=SUM(' . $column . '2:' . $column . ($count + 1) .')',
                        \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_FORMULA
                    );
            }

            $spreadsheet->getActiveSheet()->getStyle('A' . ($count + 2) . ':O' . ($count + 2))->getFont()->setBold(true);

            $spreadsheet->getActiveSheet()->mergeCells('A' . ($count + 2) . ':B' . ($count + 2));
            $spreadsheet->getActiveSheet()->getCell('A' . ($count + 2))->setValue('TOTAL');
            $spreadsheet->getActiveSheet()->getStyle('A' . ($count + 2))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

            $spreadsheet->getActiveSheet()->getStyle('A1:O' . ($count + 2))->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $spreadsheet->getActiveSheet()->getPageSetup()->setHorizontalCentered(true);

            $spreadsheet->getProperties()
                ->setCreator('Tapmedia Business Intelligence')
                ->setTitle('Billing Report');

            header('Access-Control-Allow-Origin: *');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8');
            header('Content-Disposition: attachment;filename="billing-report.xlsx"');
            header('Cache-Control: max-age=0');

            $writer = new Xlsx($spreadsheet);
//            $writer->save('report_admin.xlsx');
            $writer->save('php://output');

            return new JsonResponse('', 200);
        } catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}