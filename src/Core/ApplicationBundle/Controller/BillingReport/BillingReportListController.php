<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\BillingReport;

use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\BillingReport\BillingReportListCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\BillingReportResponse;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\Admin\Core\ApplicationBundle\Command\ListResponse;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\ProjectListCommand;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\Email;
use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\SuperAdmin;
use Pagerfanta\Pagerfanta;
use Psr\Http\Message\ServerRequestInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Zend\Diactoros\Response\JsonResponse;

class BillingReportListController
{
    /**
     * @const int
     */
    const DEFAULT_PAGE = 1;

    /**
     * @const int
     */
    const DEFAULT_SIZE = 1000;

    /** @var ServiceInterface */
    private $service;

    /**
     * ProjectListController constructor
     *
     * @param ServiceInterface $service
     */
    public function __construct(ServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @ApiDoc(
     *   section = "Billing Report",
     *   description = "Get list of billing reports",
     *   requirements = {
     *     {
     *       "name"        = "project_id",
     *       "dataType"    = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Project identifier"
     *     },
     *     {
     *       "name"        = "start_date",
     *       "dataType"    = "string",
     *       "requirement" = "\d+",
     *       "description" = "Start date"
     *     },
     *     {
     *       "name"        = "end_date",
     *       "dataType"    = "string",
     *       "requirement" = "\d+",
     *       "description" = "End date"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $command = BillingReportListCommand::handleRequest($request);
            try {
                $data = $this->service->execute($command);
            } catch (\InvalidArgumentException $exception) {
                return new JsonResponse([], 404);
            }

            return new JsonResponse($data, 200);
        } catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}