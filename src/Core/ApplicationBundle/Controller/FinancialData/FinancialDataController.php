<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\FinancialData;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Orangear\Admin\Core\ApplicationBundle\Command\FinancialData\FinancialDataListCommand;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class FinancialData
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Controller\FinancialData
 */
class FinancialDataController
{
    /** @var ServiceInterface */
    private $service;

    /**
     * FinancialDataController constructor
     *
     * @param ServiceInterface $service
     */
    public function __construct(ServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @ApiDoc(
     *   section = "Financial Data",
     *   description = "Get financial data",
     *   parameters = {
     *     {
     *       "name"        = "start_date",
     *       "dataType"    = "string",
     *       "required"    = "true",
     *       "description" = "Start date"
     *     },
     *     {
     *       "name"        = "end_date",
     *       "dataType"    = "string",
     *       "required"    = "true",
     *       "description" = "End date"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $command = FinancialDataListCommand::handleRequest($request);
            try {
                $data = $this->service->execute($command);
            } catch (\InvalidArgumentException $exception) {
                return new JsonResponse(['errors' => $exception->getMessage()], Response::HTTP_BAD_REQUEST);
            }

            return new JsonResponse($data, Response::HTTP_OK);
        } catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }
    }
}
