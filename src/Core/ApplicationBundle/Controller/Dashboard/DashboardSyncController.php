<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\Dashboard;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\Admin\Core\ApplicationBundle\Command\Dashboard\DashboardSyncCommand;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Psr\Http\Message\ServerRequestInterface;
use SimpleBus\Message\Bus\MessageBus;
use Zend\Diactoros\Response\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class DashboardSyncController
{
    /** @var MessageBus */
    private $commandBus;

    /**
     * ProjectListController constructor
     *
     * @@param MessageBus $commandBus
     */
    public function __construct(MessageBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @ApiDoc(
     *   section = "Dashboard",
     *   description = "Sync project's dashboard",
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   filters = {
     *     {"name" = "period", "dataType" = "string", "required" = true, "description" = "Dashboard sync time"},
     *     {"name" = "timezone", "dataType" = "string", "required" = true, "description" = "Project timezone"},
     *     {"name" = "incomeToday", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "incomeYesterday", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "incomeLastSevenDays", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "incomeThisMonth", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "incomeLastMonth", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "incomeAllTime", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "incomeSuperlinkToday", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "incomeSuperlinkYesterday", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "incomeSuperlinkLastSevenDays", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "incomeSuperlinkThisMonth", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "incomeSuperlinkLastMonth", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "incomeSuperlinkAllTime", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "payoutToday", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "payoutYesterday", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "payoutLastSevenDays", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "payoutThisMonth", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "payoutLastMonth", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "payoutAllTime", "dataType" = "float", "required" = true, "description" = ""},
     *     {"name" = "offersActive", "dataType" = "int", "required" = true, "description" = ""},
     *     {"name" = "offersPublic", "dataType" = "int", "required" = true, "description" = ""},
     *     {"name" = "offersSuspended", "dataType" = "int", "required" = true, "description" = ""},
     *     {"name" = "offersIncent", "dataType" = "int", "required" = true, "description" = ""},
     *     {"name" = "offersNonIncent", "dataType" = "int", "required" = true, "description" = ""},
     *     {"name" = "offersTotal", "dataType" = "int", "required" = true, "description" = ""},
     *     {"name" = "offersPerDay", "dataType" = "int", "required" = true, "description" = ""},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $command = DashboardSyncCommand::handleRequest($request);

            $this->commandBus->handle($command);

            return new JsonResponse(['success'], 200);
        }
        catch (CommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}