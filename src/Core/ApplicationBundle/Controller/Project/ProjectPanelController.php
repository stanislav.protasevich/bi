<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\Project;

use InvalidArgumentException;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\ProjectListCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\ProjectPanelCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\ProjectPanelResponse;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Pagerfanta\Pagerfanta;
use Psr\Http\Message\ServerRequestInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Zend\Diactoros\Response\JsonResponse;

class ProjectPanelController
{
    /** @var ServiceInterface */
    private $service;

    /**
     * ProjectPanelController constructor
     *
     * @param ServiceInterface $service
     */
    public function __construct(ServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @ApiDoc(
     *   section = "Panels",
     *   description = "Get projects panel",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $command = ProjectPanelCommand::handleRequest($request);

            $adapter = $this->service->execute($command);

            try {
                $response = new ProjectPanelResponse(
                    $adapter
                );
            } catch (InvalidArgumentException $e) {
                return new JsonResponse(['errors' => $e->getMessage()], 400);
            }

            return new JsonResponse($response->getResult(), 200);
        }
        catch (CommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}