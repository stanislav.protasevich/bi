<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\Project\Exception;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;

/**
 * Class ProjectCreateCommandException
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Controller\Project\Exception
 */
class ProjectCreateCommandException extends CommandValidationException {}