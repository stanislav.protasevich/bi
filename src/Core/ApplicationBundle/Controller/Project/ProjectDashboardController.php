<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\Project;

use InvalidArgumentException;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\ProjectDashboardCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\ProjectListCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\ProjectPanelCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\ProjectDashboardResponse;
use Orangear\Admin\Core\ApplicationBundle\Command\ProjectPanelResponse;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Pagerfanta\Pagerfanta;
use Psr\Http\Message\ServerRequestInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Class ProjectDashboardController
 * @package Orangear\Admin\Core\ApplicationBundle\Controller\Project
 */
class ProjectDashboardController
{
    /** @var ServiceInterface */
    private $service;

    /**
     * ProjectDashboardController constructor
     *
     * @param ServiceInterface $service
     */
    public function __construct(ServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @ApiDoc(
     *   section = "Dashboard",
     *   description = "Get project dashboard",
     *   requirements = {
     *     {
     *       "name"        = "id",
     *       "dataType"    = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Project identifier"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $command = ProjectDashboardCommand::handleRequest($request);

            $data = $this->service->execute($command);

            try {
                $response = new ProjectDashboardResponse(
                    $data
                );
            } catch (InvalidArgumentException $e) {
                return new JsonResponse(['errors' => $e->getMessage()], 400);
            }

            return new JsonResponse($response->getResult(), 200);
        }
        catch (CommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}