<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\Project;

use Assert\InvalidArgumentException;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\Admin\Core\ApplicationBundle\Command\ListResponse;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\ProjectListCommand;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\Email;
use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\SuperAdmin;
use Pagerfanta\Pagerfanta;
use Psr\Http\Message\ServerRequestInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use SimpleBus\Message\Bus\MessageBus;
use Zend\Diactoros\Response\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class ProjectListController
{
    /** @var ServiceInterface */
    private $service;

    /**
     * ProjectListController constructor
     *
     * @param ServiceInterface $service
     */
    public function __construct(ServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @ApiDoc(
     *   section = "Project",
     *   description = "Get list of projects",
     *   parameters = {
     *     {"name" = "project_id", "dataType" = "string", "required" = true, "description" = "Project identifier"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $command = ProjectListCommand::handleRequest($request);

            $adapter = $this->service->execute($command);

            try {
                $response = new ListResponse(
                    $adapter,
                    1,
                    100
                );
            } catch (InvalidArgumentException $e) {
                return new JsonResponse(['errors' => $e->getMessage()], 400);
            }

            return new JsonResponse($response->getResult(), 200);
        }
        catch (CommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}