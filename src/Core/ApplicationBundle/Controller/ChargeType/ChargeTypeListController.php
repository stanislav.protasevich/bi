<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\ChargeType;

use Orangear\Admin\Core\ApplicationBundle\Command\ChargeType\CreateChargeTypeCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\Admin\Core\ApplicationBundle\Query\ChargeType\AllChargeType;
use Prooph\ServiceBus\QueryBus;
use Psr\Http\Message\ServerRequestInterface;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class ChargeTypeListController
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Controller\ChargeType
 */
final class ChargeTypeListController
{
    /** @var QueryBus */
    private $queryBus;

    /**
     * ChargeTypeCreateController constructor
     *
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @ApiDoc(
     *   section = "Charge types",
     *   description = "Get list of charge types",
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $data = [];

            $query = new AllChargeType();

            $response = $this->queryBus->dispatch($query);
            $response->then(function($result) use (&$data) { $data = $result; })
            ->otherwise(function($data){
                var_dump($data->getPrevious()->getMessage());
            });

            return new JsonResponse(['data' => $data], 200);
        }
        catch (CommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}