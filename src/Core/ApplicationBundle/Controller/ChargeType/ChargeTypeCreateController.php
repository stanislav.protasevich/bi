<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\ChargeType;

use Orangear\Admin\Core\ApplicationBundle\Command\ChargeType\CreateChargeTypeCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Psr\Http\Message\ServerRequestInterface;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class ChargeTypeCreateController
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Controller\ChargeType
 */
class ChargeTypeCreateController
{
    /** @var MessageBus */
    private $commandBus;

    /**
     * ChargeTypeCreateController constructor
     *
     * @@param MessageBus $commandBus
     */
    public function __construct(MessageBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @ApiDoc(
     *   section = "Charge types",
     *   description = "Create charge type",
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   parameters = {
     *     {"name" = "name", "dataType" = "string", "required" = true, "description" = "Charge type name"},
     *     {"name" = "description", "dataType" = "string", "required" = false, "description" = "Charge type description"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $command = CreateChargeTypeCommand::handleRequest($request);

            $this->commandBus->handle($command);

            return new JsonResponse(['success'], 200);
        }
        catch (CommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}