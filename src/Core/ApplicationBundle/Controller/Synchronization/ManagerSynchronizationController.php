<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\Synchronization;

use Orangear\Admin\Core\ApplicationBundle\Command\Synchronization\ManagerSynchronizationCommand;
use Orangear\Admin\Core\ApplicationBundle\Controller\Project\Exception\ProjectCreateCommandException;
use Psr\Http\Message\ServerRequestInterface;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ManagerSynchronizationController
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Controller\Synchronization
 */
class ManagerSynchronizationController extends Controller
{
    /** @var MessageBus  */
    private $commandBus;

    /**
     * ManagerSynchronizationController constructor
     *
     * @param MessageBus $commandBus
     */
    public function __construct(MessageBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @ApiDoc(
     *   section = "Synchronization",
     *   description = "Synchronize managers",
     *   parameters = {
     *     {"name" = "project_id", "dataType" = "int", "required" = true, "description" = "Project identifier"},
     *   },
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $command = ManagerSynchronizationCommand::handleRequest($request);

            $this->commandBus->handle($command);

            return new JsonResponse(['success'], 200);
        }
        catch (ProjectCreateCommandException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}
