<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\Employee;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\Admin\Core\ApplicationBundle\Command\Employee\CreateEmployeeCommand;
use Orangear\Admin\Core\ApplicationBundle\Query\Charge\FindExpenseSalaries;
use Orangear\Admin\Core\ApplicationBundle\Query\Employee\FindEmployees;
use Prooph\ServiceBus\QueryBus;
use Psr\Http\Message\ServerRequestInterface;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class EmployeeListController
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Controller\Employee
 */
final class EmployeeListController
{
    /** @var QueryBus */
    private $queryBus;

    /**
     * EmployeeCreateController constructor
     *
     * @param QueryBus $queryBus
     */
    public function __construct(QueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /**
     * @ApiDoc(
     *   section = "Employee",
     *   description = "Get list of employees",
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   parameters = {
     *     {"name" = "project_id", "dataType" = "string", "required" = true, "description" = "Employee project identifier"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        $parameters = $request->getQueryParams();

        try {
            $data = [];

            $query = new FindEmployees(
                $parameters['project_id'] ?? null
            );

            $response = $this->queryBus->dispatch($query);
            $response->then(function($result) use (&$data) { $data = $result; });

            return new JsonResponse(['data' => $data], 200);
        }
        catch (CommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}