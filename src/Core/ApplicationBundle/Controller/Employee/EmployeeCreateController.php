<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\Employee;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\Admin\Core\ApplicationBundle\Command\Employee\CreateEmployeeCommand;
use Psr\Http\Message\ServerRequestInterface;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class EmployeeCreateController
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Controller\Employee
 */
class EmployeeCreateController
{
    /** @var MessageBus */
    private $commandBus;

    /**
     * EmployeeCreateController constructor
     *
     * @param MessageBus $commandBus
     */
    public function __construct(MessageBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @ApiDoc(
     *   section = "Employee",
     *   description = "Create employee",
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   parameters = {
     *     {"name" = "employee_group_id", "dataType" = "string", "required" = true, "description" = "Employee group identifier"},
     *     {"name" = "project_id[]", "dataType" = "string", "required" = true, "description" = "Employee project identifier"},
     *     {"name" = "remote_id", "dataType" = "string", "required" = false, "description" = "Employee remote identifier"},
     *     {"name" = "first_name", "dataType" = "string", "required" = true, "description" = "Employee first name"},
     *     {"name" = "last_name", "dataType" = "string", "required" = true, "description" = "Employee last name"},
     *     {"name" = "notes", "dataType" = "string", "required" = false, "description" = "Noted about employee"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $command = CreateEmployeeCommand::handleRequest($request);

            $this->commandBus->handle($command);

            return new JsonResponse(['success'], 200);
        }
        catch (CommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}