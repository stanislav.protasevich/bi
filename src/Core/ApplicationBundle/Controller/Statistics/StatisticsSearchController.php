<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\Statistics;

use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\BillingReport\BillingReportListCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\BillingReport\BillingReportsSyncCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\BillingReport\BillingReportSyncCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\BillingReportResponse;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\Admin\Core\ApplicationBundle\Command\ListResponse;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\ProjectListCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\Statistics\StatisticsFindCommand;
use Orangear\Admin\Core\Domain\Model\BillingReport\BillingReport;
use Orangear\Admin\Core\Domain\Model\BillingReport\BillingReportSyncCommandCollection;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\Email;
use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\SuperAdmin;
use Pagerfanta\Pagerfanta;
use Psr\Http\Message\ServerRequestInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use SimpleBus\Message\Bus\MessageBus;
use Zend\Diactoros\Response\JsonResponse;

/**
 * Class StatisticsSearchController
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Controller\BillingReport
 */
class StatisticsSearchController
{
    /** @var ServiceInterface */
    private $service;

    public function __construct(ServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @ApiDoc(
     *   section = "Statistics",
     *   description = "Statistics search",
     *   requirements = {
     *     {
     *       "name"        = "project_id",
     *       "dataType"    = "int",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "start_date",
     *       "dataType"    = "string",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "end_date",
     *       "dataType"    = "string",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "timezone",
     *       "dataType"    = "string",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "breakdown_date",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "breakdown_geography",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "breakdown_platforms",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "breakdown_offer_id",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "breakdown_offer_name",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "breakdown_advertiser_id",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "breakdown_real_pub_id",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "breakdown_pub_id",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "breakdown_sub_aff_id",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "breakdown_publisher_name",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "breakdown_affiliate_manager",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "breakdown_account_manager",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "breakdown_non_zero_revenuer",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "breakdown_secret_hash",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "data_clicks",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "data_back_traffic",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "data_conversions",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "data_pubs_conversions",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "data_cpa",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "data_pubs_cpa",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "data_cpc",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "data_cr",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "data_pubs_cr",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "data_rpa",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "data_rpc",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "data_costs",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "data_revenues",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "data_extra_revenues",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "data_profit",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "fraud_duplicates",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "fraud_duplicates_percentage",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "fraud_min_fraud",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "fraud_min_fraud_percentage",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "fraud_hdr_proxy_clicks",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "fraud_hdr_proxy_clicks_percentage",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "fraud_hdr_proxy_conversions",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "fraud_hdr_proxy_conversions_percentage",
     *       "dataType"    = "bool",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "filter_countries",
     *       "dataType"    = "array",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "filter_sub_aff_id",
     *       "dataType"    = "array",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "filter_platforms",
     *       "dataType"    = "array",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "filter_offers",
     *       "dataType"    = "array",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "filter_offer_types",
     *       "dataType"    = "array",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "filter_advertisers",
     *       "dataType"    = "array",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "filter_publishers",
     *       "dataType"    = "array",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "filter_managers",
     *       "dataType"    = "array",
     *       "description" = ""
     *     },
     *     {
     *       "name"        = "filter_hash",
     *       "dataType"    = "array",
     *       "description" = ""
     *     },
     *   },
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $command = StatisticsFindCommand::handleRequest($request);

            $data = $this->service->execute($command);

            return new JsonResponse($data, 200);
        }
        catch (CommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}