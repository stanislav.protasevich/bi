<?php

namespace Orangear\Admin\Core\ApplicationBundle\Controller\Company;

use InvalidArgumentException;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandValidationException;
use Orangear\Admin\Core\ApplicationBundle\Command\Company\CompanyListCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\ListResponse;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\ProjectListCommand;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Pagerfanta\Pagerfanta;
use Psr\Http\Message\ServerRequestInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Zend\Diactoros\Response\JsonResponse;

class CompanyListController
{
    /**
     * @const int
     */
    const DEFAULT_PAGE = 1;

    /**
     * @const int
     */
    const DEFAULT_SIZE = 100;

    /** @var ServiceInterface */
    private $service;

    /**
     * ProjectListController constructor
     *
     * @param ServiceInterface $service
     */
    public function __construct(ServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @ApiDoc(
     *   section = "Platform",
     *   description = "Get list of platforms",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        $params = $request->getQueryParams();

        $page  = $params['page'] ?? self::DEFAULT_PAGE;
        $limit = $params['limit'] ?? self::DEFAULT_SIZE;

        try {
            $command = CompanyListCommand::handleRequest($request);

            $adapter = $this->service->execute($command);

            try {
                $response = new ListResponse(
                    $adapter,
                    $page,
                    $limit
                );
            } catch (InvalidArgumentException $e) {
                return new JsonResponse(['errors' => $e->getMessage()], 400);
            }

            $encoders = array(new JsonEncoder());
            $normalizers = array(new ObjectNormalizer());

            $serializer = new Serializer($normalizers, $encoders);
            $data = $serializer->normalize($response->getResult(), 'json', array_merge(array(
                'json_encode_options' => 15,
            )));

            return new JsonResponse($data, 200);
        }
        catch (CommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}