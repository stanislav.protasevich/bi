<?php

namespace Orangear\Admin\Core\ApplicationBundle\Query\Charge;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\Exception\ProjectDoesNotExistException;
use Orangear\Admin\Core\Domain\Model\ChargeType\Service\Exception\ChargeTypeDoesNotExistException;
use Orangear\Admin\Core\InfrastructureBundle\Repository\ChargeRepository;
use Orangear\Admin\Core\InfrastructureBundle\Repository\ChargeTypeRepository;
use Orangear\Admin\Core\InfrastructureBundle\Repository\EmployeeChargeRepository;
use Orangear\Admin\Core\InfrastructureBundle\Repository\EmployeeRepository;
use Orangear\Admin\Core\InfrastructureBundle\Repository\ProjectRepository;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeType;
use Orangear\BusinessIntelligence\Domain\Model\Employee\Employee;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeCharge;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use React\Promise\Deferred;
use \DateTime;

class FindExpenseSalariesHandler
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(FindExpenseSalaries $query, Deferred $deferred = null)
    {
        $data = [];

        $chargeType = $this->findSalaryChargeTypeOrFail();
        $project = $this->findProjectOrFail($query->getProjectId());

        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select([
            'c' . '.id AS charge_id',
            'c' . '.amount AS amount',
            'c' . '.startDate AS start_date',
            'c' . '.endDate AS end_date',

            'e' . '.id AS employee_id',
            'e' . '.firstName AS employee_first_name',
            'e' . '.lastName AS employee_last_name',
        ])
            ->from(Charge::class, 'c')
            ->join(
                EmployeeCharge::class,
                'ec',
                Join::WITH,
                'c' . '.id = ' . 'ec' . '.charge')
            ->join(
                Employee::class,
                'e',
                Join::WITH,
                'ec' . '.employee = ' . 'e' . '.id')
            ->where('c' . '.project = :project')
            ->andWhere('c' . '.chargeType = :charge_type')
            ->andWhere('c' . '.startDate >= :start_date AND ' . 'c' . '.endDate <= :end_date')
            ->setParameter('project', $project)
            ->setParameter('charge_type', $chargeType)
            ->setParameter('start_date', new DateTime($query->getStartDate()))
            ->setParameter('end_date', new DateTime($query->getEndDate()));

        /** @var Charge $charge */
        foreach ($queryBuilder->getQuery()->getResult() as $charge) {
            $row['id'] = $charge['charge_id']->toString();
            $row['amount'] = floatval($charge['amount']);
            $row['start_date'] = $charge['start_date']->format('Y-m-d');
            $row['end_date'] = $charge['end_date']->format('Y-m-d');

            $row['employee']['id'] = $charge['employee_id']->toString();
            $row['employee']['full_name'] = implode(' ', [$charge['employee_first_name'], $charge['employee_last_name']]);

            $row['employee']['first_name'] = $charge['employee_first_name'];
            $row['employee']['last_name'] = $charge['employee_last_name'];

            $data[] = $row;
        }

        if (null === $deferred) {
            return $data;
        }

        $deferred->resolve($data);
    }

    /**
     * @return mixed
     *
     * @throws ChargeTypeDoesNotExistException
     */
    protected function findSalaryChargeTypeOrFail()
    {
        $chargesTypeRepository = $this->entityManager->getRepository(ChargeType::class);

        $chargeType = $chargesTypeRepository->findOneBy(['key' => 'salary']);

        if ($chargeType === null) {
            throw new ChargeTypeDoesNotExistException('Salary charge type does not exists');
        }

        return $chargeType;
    }

    protected function findProjectOrFail($projectId)
    {
        $projectRepository = $this->entityManager->getRepository(Project::class);

        $project = $projectRepository->findOneBy(['id' => $projectId]);

        if (!$project) {
            throw new ProjectDoesNotExistException('Project does not exist');
        }

        return $project;
    }
}