<?php

namespace Orangear\Admin\Core\ApplicationBundle\Query\Charge;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\Exception\ProjectDoesNotExistException;
use Orangear\Admin\Core\Domain\Model\Charge\ChargeFrequency;
use Orangear\Admin\Core\Domain\Model\ChargeType\Service\Exception\ChargeTypeDoesNotExistException;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeType;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository\ChargeRepository;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository\ChargeTypeRepository;
use React\Promise\Deferred;
use \DateTime;

class FindExpenseChargesHandler
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(FindExpenseCharges $query, Deferred $deferred = null)
    {
        $data = [];

        $chargeType = $this->findSalaryChargeTypeOrFail();
        $project = $this->findProjectOrFail($query->getProjectId());

        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select([
            'c' . '.id AS charge_id',
            'c' . '.amount AS amount',
            'c' . '.frequency AS frequency',
            'c' . '.startDate AS start_date',
            'c' . '.endDate AS end_date',

            'ct' . '.id AS charge_type_id',
            'ct' . '.name AS charge_name',
            'ct' . '.description AS charge_description',
        ])
            ->from(Charge::class, 'c')
            ->join(
                ChargeType::class,
                'ct',
                Join::WITH,
                'ct' . '.id = ' . 'c' . '.chargeType')
            ->where('c' . '.project = :project')
            ->andWhere('c' . '.chargeType != :charge_type')
            ->andWhere('c' . '.startDate >= :start_date AND ' . 'c' . '.endDate <= :end_date')
            ->setParameters([
                'project' => $project,
                'charge_type' => $chargeType,
                'start_date' => new DateTime($query->getStartDate()),
                'end_date' => new DateTime($query->getEndDate()),
            ]);

        /** @var Charge $charge */
        foreach ($queryBuilder->getQuery()->getResult() as $charge) {
            $row['id'] = $charge['charge_id']->toString();
            $row['amount'] = $charge['amount'];
            $row['start_date'] = $charge['start_date']->format('Y-m-d');
            $row['end_date'] = $charge['end_date']->format('Y-m-d');

            $row['frequency']['id'] = $charge['frequency'];
            $row['frequency']['name'] = ChargeFrequency::$frequencies[$charge['frequency']];

            $row['charge_type']['id'] = $charge['charge_type_id']->toString();
            $row['charge_type']['name'] = $charge['charge_name'];
            $row['charge_type']['description'] = $charge['charge_description'];

            $data[] = $row;
        }

        if (null === $deferred) {
            return $data;
        }

        $deferred->resolve($data);
    }

    /**
     * @return mixed
     *
     * @throws ChargeTypeDoesNotExistException
     */
    protected function findSalaryChargeTypeOrFail()
    {
        $chargesTypeRepository = $this->entityManager->getRepository(ChargeType::class);

        $chargeType = $chargesTypeRepository->findOneBy(['key' => 'salary']);

        if ($chargeType === null) {
            throw new ChargeTypeDoesNotExistException('Salary charge type does not exists');
        }

        return $chargeType;
    }

    protected function findProjectOrFail($projectId)
    {
        $projectRepository = $this->entityManager->getRepository(Project::class);

        $project = $projectRepository->findOneBy(['id' => $projectId]);

        if (!$project) {
            throw new ProjectDoesNotExistException('Project does not exist');
        }

        return $project;
    }
}