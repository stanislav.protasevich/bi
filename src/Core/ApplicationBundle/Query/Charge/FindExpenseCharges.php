<?php

namespace Orangear\Admin\Core\ApplicationBundle\Query\Charge;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;

/**
 * Class FindChargeExpenses
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Query\Charge
 */
class FindExpenseCharges
{
    /** @var string */
    protected $projectId;

    /** @var string */
    protected $startDate;

    /** @var string */
    protected $endDate;

    /**
     * FindChargeExpenses constructor
     *
     * @param string $projectId
     * @param string $startDate
     * @param string $endDate
     */
    public function __construct(string $projectId, string $startDate, string $endDate)
    {
        $this->projectId = $projectId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * @return ProjectIdentifierInterface
     */
    public function getProjectId() : ProjectIdentifierInterface
    {
        return ProjectId::fromString($this->projectId);
    }

    /**
     * @param string $projectId
     */
    public function setProjectId(string $projectId)
    {
        $this->projectId = $projectId;
    }

    /**
     * @return string
     */
    public function getStartDate(): string
    {
        return $this->startDate;
    }

    /**
     * @param string $startDate
     */
    public function setStartDate(string $startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return string
     */
    public function getEndDate(): string
    {
        return $this->endDate;
    }

    /**
     * @param string $endDate
     */
    public function setEndDate(string $endDate)
    {
        $this->endDate = $endDate;
    }
}