<?php

namespace Orangear\Admin\Core\ApplicationBundle\Query\Employee;

use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;

/**
 * Class AllEmployees
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Query\Employee
 */
class FindEmployees
{
    /** @var string */
    protected $projectId;

    /**
     * FindEmployees constructor
     *
     * @param $projectId
     */
    public function __construct($projectId)
    {
        $this->projectId = $projectId;
    }

    /**
     * @return mixed
     */
    public function getProjectId()
    {
        return ProjectId::fromString($this->projectId);
    }

    /**
     * @param mixed $projectId
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;
    }

}