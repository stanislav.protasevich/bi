<?php

namespace Orangear\Admin\Core\ApplicationBundle\Query\Employee;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\Exception\ProjectDoesNotExistException;
use Orangear\Admin\Core\InfrastructureBundle\Repository\EmployeeRepository;
use Orangear\BusinessIntelligence\Domain\Model\Employee\Employee;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\BusinessIntelligence\Domain\Model\RemoteEmployee\RemoteEmployee;
use React\Promise\Deferred;

/**
 * Class AllEmployeesHandler
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Query\Employee
 */
class FindEmployeesHandler
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * AllEmployeesHandler constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FindEmployees $query
     * @param Deferred|null $deferred
     *
     * @return array
     *
     * @throws ProjectDoesNotExistException
     */
    public function __invoke(FindEmployees $query, Deferred $deferred = null)
    {
        $data = [];

        $queryBuilder = $this->entityManager->createQueryBuilder();

        $projectRepository = $this->entityManager->getRepository(Project::class);

        /** @var ProjectInterface $project */
        $project = $projectRepository->findOneBy(['id' => $query->getProjectId()]);

        if (!$project) {
            throw new ProjectDoesNotExistException('Project does not exist');
        }

        $queryBuilder->select(['e'])
            ->from(Employee::class, 'e')
            ->leftJoin(
                RemoteEmployee::class,
                'r',
                Join::WITH,
                'e'. '.id = ' . 'r' . '.employee')
            ->leftJoin(
                Project::class,
                'p',
                Join::WITH,
                'r' . '.project = ' . 'p' . '.id')
            ->where('p' . '.id = :project')
            ->setParameter('project', $project->projectId());

        /** @var Employee $employee */
        foreach ($queryBuilder->getQuery()->getResult() as $employee) {
            $row['id'] = $employee->id()->toString();
            $row['first_name'] = $employee->firstName();
            $row['last_name'] = $employee->lastName();
            $row['notes'] = '';

            $data[] = $row;
        }

        if (null === $deferred) {
            return $data;
        }

        $deferred->resolve($data);
    }
}