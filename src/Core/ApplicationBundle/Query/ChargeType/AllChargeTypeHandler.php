<?php

namespace Orangear\Admin\Core\ApplicationBundle\Query\ChargeType;

use Doctrine\ORM\EntityManagerInterface;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeType;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository\ChargeTypeRepository;
use React\Promise\Deferred;

class AllChargeTypeHandler
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(AllChargeType $query, Deferred $deferred = null)
    {
        $data = [];

        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('ct')
            ->from(ChargeType::class, 'ct');

        /** @var ChargeType $chargeType */
        foreach ($queryBuilder->getQuery()->getResult() as $chargeType) {
            $row['id'] = $chargeType->id()->toString();
            $row['name'] = $chargeType->name();
            $row['key'] = $chargeType->key();
            $row['description'] = $chargeType->description();

            $data[] = $row;
        }

        if (null === $deferred) {
            return $data;
        }

        $deferred->resolve($data);
    }
}