<?php

namespace Orangear\Admin\Core\ApplicationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class OrangearAdminCoreApplicationBundle
 *
 * @package Orangear\Admin\Core\ApplicationBundle
 */
class OrangearAdminCoreApplicationBundle extends Bundle {}