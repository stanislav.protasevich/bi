<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command;

use Psr\Http\Message\ServerRequestInterface;
use Zend\Filter\FilterInterface;

/**
 * Interface CommandInterface
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command
 */
interface CommandInterface
{
    /**
     * Create command from array of data
     *
     * @param array $data
     * @param FilterInterface $filter
     *
     * @return mixed
     */
    static function hydrate(array $data, FilterInterface $filter);

    /**
     * Create command from request
     *
     * @param ServerRequestInterface $request
     *
     * @return mixed
     */
    static function handleRequest(ServerRequestInterface $request);
}