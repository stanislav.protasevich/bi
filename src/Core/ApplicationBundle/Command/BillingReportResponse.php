<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command;

use InvalidArgumentException;
use Orangear\Admin\Core\Domain\Model\BillingReport\BillingReport;
use Pagerfanta\Adapter\AdapterInterface;
use Pagerfanta\Exception\LessThan1CurrentPageException;
use Pagerfanta\Exception\NotIntegerMaxPerPageException;
use Pagerfanta\Exception\OutOfRangeCurrentPageException;
use Pagerfanta\Pagerfanta;

/**
 * Class BillingReportResponse
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command
 */
class BillingReportResponse
{
    private $result;

    /**
     * BillingReportResponse constructor
     *
     * @param $result
     */
    public function __construct($result)
    {
        $this->result = $result;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        $data = [];

        /** @var BillingReport $billingReport */
        foreach ($this->result as $billingReport) {
            $temp['date'] = $billingReport->getDate()->format('Y-m');
            $temp['revenue'] = $billingReport->getRevenue();
            $temp['revenueReceived'] = $billingReport->getRevenueReceived();
            $temp['crossPayment'] = $billingReport->getCrossPayment();
            $temp['deduction'] = $billingReport->getDeduction();
            $temp['revenueUnpaid'] = $billingReport->getRevenueUnpaid();
            $temp['publisherAmount'] = $billingReport->getPublisherAmount();
            $temp['publisherPaid'] = $billingReport->getPublisherPaid();
            $temp['publisherUnpaid'] = $billingReport->getPublisherUnpaid();
            $temp['grossIncome'] = $billingReport->getGrossIncome();

            $temp['netRevenue'] = $temp['revenue'] - $temp['crossPayment'] - $temp['deduction'];
            $temp['netIncome'] = $temp['revenueReceived'] - $temp['publisherPaid'];

            //@todo
            $temp['salary'] = 0;
            $temp['expenses'] = 0;
            $temp['totalExpenses'] = 0;
            $temp['grossProfit'] = 0;
            $temp['netProfit'] = 0;
            $temp['discrepancy'] = 0;

            $temp['project']['id'] = $billingReport->getProject()->getId();
            $temp['project']['name'] = $billingReport->getProject()->getName();

            $data['data'][] = $temp;
        }

        return $data;
    }
}