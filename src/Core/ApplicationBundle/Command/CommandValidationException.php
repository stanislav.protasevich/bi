<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Zend\Filter\Word\CamelCaseToUnderscore;

/**
 * Class CommandValidationException
 *
 * @package Orangear\Admin\Core\Exception
 */
class CommandValidationException extends BadRequestHttpException
{
    /** @var array Validation messages */
    protected $messages = [];

    /**
     * {@inheritdoc}
     */
    public function __construct($message, ConstraintViolationListInterface $validations, $code = 400, \Exception $previous = null)
    {
        $filter = new CamelCaseToUnderscore();
        $filter->setOptions([]);

        $messages = [];
        foreach ($validations as $validation) {
            $messages[strtolower($filter->filter($validation->getPropertyPath()))][] = $validation->getMessage();
        }

        $this->messages = $messages;

        parent::__construct($message, $previous, $code);
    }

    /**
     * Get array of validation messages
     *
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }
}