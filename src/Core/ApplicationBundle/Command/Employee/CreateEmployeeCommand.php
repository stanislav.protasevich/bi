<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command\Employee;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class CreateEmployeeCommand
 *
 * @property string $employeeGroupId
 * @property string $projectId
 * @property string $remoteId
 * @property string $firstName
 * @property string $lastName
 * @property string $notes
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command\Employee
 */
class CreateEmployeeCommand extends Command
{
    /** @var string */
    protected $employeeGroupId;

    /** @var string */
    protected $projectId;

    /** @var string */
    protected $remoteId;

    /** @var string */
    protected $firstName;

    /** @var string */
    protected $lastName;

    /** @var string */
    protected $notes;

    /**
     * CreateEmployeeCommand constructor
     *
     * @param string $employeeGroupId
     * @param string $projectId
     * @param string $remoteId
     * @param string $firstName
     * @param string $lastName
     * @param string $notes
     */
    public function __construct($employeeGroupId = null, $projectId = null, $remoteId = null, $firstName = null, $lastName = null, $notes = null)
    {
        $this->employeeGroupId = $employeeGroupId;
        $this->remoteId = $remoteId;
        $this->projectId = $projectId;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->notes = $notes;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return \Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface
     */
    static function handleRequest(ServerRequestInterface $request)
    {
        $data = array_merge([], $request->getParsedBody());

        return self::hydrate($data);
    }
}