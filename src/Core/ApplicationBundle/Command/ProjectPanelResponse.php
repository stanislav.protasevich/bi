<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command;

use InvalidArgumentException;
use Orangear\Admin\Core\Domain\Model\Project\Project;
use Pagerfanta\Adapter\AdapterInterface;
use Pagerfanta\Exception\LessThan1CurrentPageException;
use Pagerfanta\Exception\NotIntegerMaxPerPageException;
use Pagerfanta\Exception\OutOfRangeCurrentPageException;
use Pagerfanta\Pagerfanta;

/**
 * Class ProjectPanelResponse
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command
 */
class ProjectPanelResponse
{
    /** @var AdapterInterface  */
    private $adapter;

    /**
     * ListResponse constructor
     *
     * @param array $adapter
     */
    public function __construct(array $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        $data = [];

        /** @var Project $item */
        foreach ($this->adapter as $item) {


            $value['id']     = $item['id'];
            $value['name']   = $item['name'];
            $value['logo']   = $item['logo'];
            $value['token']  = $item['token'];
            $value['trend']  = (bool) $item['trend'];
            $value['income'] = $item['income'];

            $data['data'][] = $value;
        }

        return $data;
    }
}