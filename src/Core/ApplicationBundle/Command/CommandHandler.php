<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\Exception\ProjectCreateCommandException;

/**
 * Class CommandHandler
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command
 */
class CommandHandler
{
    /** @var ServiceInterface */
    private $service;

    /**
     * ProjectCreateCommandHandler constructor
     *
     * @param ServiceInterface $service
     */
    public function __construct(ServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Create project
     *
     * @param \Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface $command
     *
     * @return mixed
     */
    public function handle(CommandInterface $command)
    {
        return $this->service->execute($command);
    }
}