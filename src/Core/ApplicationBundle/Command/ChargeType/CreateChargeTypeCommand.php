<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command\ChargeType;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class CreateChargeTypeCommand
 *
 * @property string $name
 * @property string $key
 * @property string $description
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command\EmployeeGroup
 */
class CreateChargeTypeCommand extends Command
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $key;

    /** @var string */
    protected $description;

    /**
     * CreateChargeTypeCommand constructor
     *
     * @param null $name
     * @param null $key
     * @param null $description
     */
    public function __construct($name = null, $key = null, $description = null)
    {
        $this->name = $name;
        $this->key = $key;
        $this->description = $description;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return \Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface
     */
    static function handleRequest(ServerRequestInterface $request)
    {
        $data = array_merge([], $request->getParsedBody());

        return self::hydrate($data);
    }
}