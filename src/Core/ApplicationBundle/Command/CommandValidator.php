<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command;

use SimpleBus\Message\Bus\Middleware\MessageBusMiddleware;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class CommandValidator
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command
 */
class CommandValidator implements MessageBusMiddleware
{
    /** @var ValidatorInterface */
    protected $validator;

    /**
     * Dependency Injection constructor.
     *
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * {@inheritdoc}
     */
    public function handle($message, callable $next)
    {
        $violations = $this->validator->validate($message);

        if (count($violations) > 0) {
            throw new CommandValidationException(
                'Command violation detected',
                $violations
            );
        }

        $next($message);
    }
}