<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command\Dashboard;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Filter\FilterInterface;

/**
 * Class DashboardSync
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command\Dashboard
 */
class DashboardSyncCommand extends Command
{
    /** @var string */
    public $token = '';

    /** @var string */
    public $period = '';

    /** @var string */
    public $timezone = 0;

    /** @var float */
    public $incomeToday = 0;

    /** @var float */
    public $incomeYesterday = 0;

    /** @var float */
    public $incomeLast7Days = 0;

    /** @var float */
    public $incomeThisMonth = 0;

    /** @var float */
    public $incomeLastMonth = 0;

    /** @var float */
    public $incomeAllTime = 0;

    /** @var float */
    public $incomeSuperlinkToday = 0;

    /** @var float */
    public $incomeSuperlinkYesterday = 0;

    /** @var float */
    public $incomeSuperlinkLast7Days = 0;

    /** @var float */
    public $incomeSuperlinkThisMonth = 0;

    /** @var float */
    public $incomeSuperlinkLastMonth = 0;

    /** @var float */
    public $incomeSuperlinkAllTime = 0;

    /** @var float */
    public $payoutToday = 0;

    /** @var float */
    public $payoutYesterday = 0;

    /** @var float */
    public $payoutLast7Days = 0;

    /** @var float */
    public $payoutThisMonth = 0;

    /** @var float */
    public $payoutLastMonth = 0;

    /** @var float */
    public $payoutAllTime = 0;

    /** @var integer */
    public $offersActive = 0;

    /** @var integer */
    public $offersPrivate = 0;

    /** @var integer */
    public $offersSuspended = 0;

    /** @var integer */
    public $offersIncent = 0;

    /** @var integer */
    public $offersNonIncent = 0;

    /** @var integer */
    public $offersTotal = 0;

    /** @var integer */
    public $offersPerDay = 0;

    /**
     * DashboardSyncCommand constructor
     *
     * @param string $token
     * @param string $period
     * @param int $timezone
     * @param int $incomeToday
     * @param int $incomeYesterday
     * @param int $incomeLastSevenDays
     * @param int $incomeThisMonth
     * @param int $incomeLastMonth
     * @param int $incomeAllTime
     * @param int $incomeSuperlinkToday
     * @param int $incomeSuperlinkYesterday
     * @param int $incomeSuperlinkLastSevenDays
     * @param int $incomeSuperlinkThisMonth
     * @param int $incomeSuperlinkLastMonth
     * @param int $incomeSuperlinkAllTime
     * @param int $payoutToday
     * @param int $payoutYesterday
     * @param int $payoutLastSevenDays
     * @param int $payoutThisMonth
     * @param int $payoutLastMonth
     * @param int $payoutAllTime
     * @param int $offersActive
     * @param int $offersPrivate
     * @param int $offersSuspended
     * @param int $offersIncent
     * @param int $offersNonIncent
     * @param int $offersTotal
     * @param int $offersPerDay
     */
    public function __construct(
        $token = '',
        $period = '',
        $timezone = 0,
        $incomeToday = 0,
        $incomeYesterday = 0,
        $incomeLastSevenDays = 0,
        $incomeThisMonth = 0,
        $incomeLastMonth = 0,
        $incomeAllTime = 0,
        $incomeSuperlinkToday = 0,
        $incomeSuperlinkYesterday = 0,
        $incomeSuperlinkLastSevenDays = 0,
        $incomeSuperlinkThisMonth = 0,
        $incomeSuperlinkLastMonth = 0,
        $incomeSuperlinkAllTime = 0,
        $payoutToday = 0,
        $payoutYesterday = 0,
        $payoutLastSevenDays = 0,
        $payoutThisMonth = 0,
        $payoutLastMonth = 0,
        $payoutAllTime = 0,
        $offersActive = 0,
        $offersPrivate = 0,
        $offersSuspended = 0,
        $offersIncent = 0,
        $offersNonIncent = 0,
        $offersTotal = 0,
        $offersPerDay = 0
    ) {
        $this->token = $token;
        $this->period = $period;
        $this->timezone = $timezone;
        $this->incomeToday = $incomeToday;
        $this->incomeYesterday = $incomeYesterday;
        $this->incomeLast7Days = $incomeLastSevenDays;
        $this->incomeThisMonth = $incomeThisMonth;
        $this->incomeLastMonth = $incomeLastMonth;
        $this->incomeAllTime = $incomeAllTime;
        $this->incomeSuperlinkToday = $incomeSuperlinkToday;
        $this->incomeSuperlinkYesterday = $incomeSuperlinkYesterday;
        $this->incomeSuperlinkLast7Days = $incomeSuperlinkLastSevenDays;
        $this->incomeSuperlinkThisMonth = $incomeSuperlinkThisMonth;
        $this->incomeSuperlinkLastMonth = $incomeSuperlinkLastMonth;
        $this->incomeSuperlinkAllTime = $incomeSuperlinkAllTime;
        $this->payoutToday = $payoutToday;
        $this->payoutYesterday = $payoutYesterday;
        $this->payoutLast7Days = $payoutLastSevenDays;
        $this->payoutThisMonth = $payoutThisMonth;
        $this->payoutLastMonth = $payoutLastMonth;
        $this->payoutAllTime = $payoutAllTime;
        $this->offersActive = $offersActive;
        $this->offersPrivate = $offersPrivate;
        $this->offersSuspended = $offersSuspended;
        $this->offersIncent = $offersIncent;
        $this->offersNonIncent = $offersNonIncent;
        $this->offersTotal = $offersTotal;
        $this->offersPerDay = $offersPerDay;
    }

    /**
     * {@inheritdoc}
     */
    public static function handleRequest(ServerRequestInterface $request)
    {
        $body = $request->getParsedBody();
        $dashboard = $body['dashboard'] ?? json_encode([]);
        $dashboard = json_decode($dashboard, true);

        $data = array_merge(
            [
                'token' => $request->getHeader('X-Auth-Token')[0] ?? '',
            ],
            $dashboard
        );

        return self::hydrate($data);
    }
}