<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command;

use InvalidArgumentException;
use Orangear\Admin\Core\Domain\Model\Dashboard\Dashboard;
use Orangear\Admin\Core\Domain\Model\Project\Project;
use Pagerfanta\Adapter\AdapterInterface;
use Pagerfanta\Exception\LessThan1CurrentPageException;
use Pagerfanta\Exception\NotIntegerMaxPerPageException;
use Pagerfanta\Exception\OutOfRangeCurrentPageException;
use Pagerfanta\Pagerfanta;

/**
 * Class ProjectDashboardResponse
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command
 */
class ProjectDashboardResponse
{
    /** @var array */
    private $data;

    /**
     * ListResponse constructor
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = [];

        if (isset($data[0])) {
            $this->data = $data[0];
        }
    }

    /**
     * @return array
     */
    public function getResult()
    {
        $data = [];
        $data['incomeToday'] = (float) $this->data['incomeToday'] ?? 0;
        $data['incomeYesterday'] = (float) $this->data['incomeYesterday'] ?? 0;
        $data['incomeLastSevenDays'] = (float) $this->data['incomeLastSevenDays'] ?? 0;
        $data['incomeThisMonth'] = (float) $this->data['incomeThisMonth'] ?? 0;
        $data['incomeLastMonth'] = (float) $this->data['incomeLastMonth'] ?? 0;
        $data['incomeAllTime'] = (float) $this->data['incomeAllTime'] ?? 0;

        $data['incomeSuperlinkToday'] = (float) $this->data['incomeSuperlinkToday'] ?? 0;
        $data['incomeSuperlinkYesterday'] = (float) $this->data['incomeSuperlinkYesterday'] ?? 0;
        $data['incomeSuperlinkLastSevenDays'] = (float) $this->data['incomeSuperlinkLastSevenDays'] ?? 0;
        $data['incomeSuperlinkThisMonth'] = (float) $this->data['incomeSuperlinkThisMonth'] ?? 0;
        $data['incomeSuperlinkLastMonth'] = (float) $this->data['incomeSuperlinkLastMonth'] ?? 0;
        $data['incomeSuperlinkAllTime'] = (float) $this->data['incomeSuperlinkAllTime'] ?? 0;

        $data['payoutToday'] = (float) $this->data['payoutToday'] ?? 0;
        $data['payoutYesterday'] = (float) $this->data['payoutYesterday'] ?? 0;
        $data['payoutLastSevenDays'] = (float) $this->data['payoutLastSevenDays'] ?? 0;
        $data['payoutThisMonth'] = (float) $this->data['payoutThisMonth'] ?? 0;
        $data['payoutLastMonth'] = (float) $this->data['payoutLastMonth'] ?? 0;
        $data['payoutAllTime'] = (float) $this->data['payoutAllTime'] ?? 0;

        $data['totalRevenueToday'] = (float) $this->data['revenueSuperlinkToday'] + $data['payoutToday'] ?? $data['payoutToday'];
        $data['totalRevenueYesterday'] = (float) $this->data['revenueSuperlinkYesterday'] + $data['payoutYesterday'] ?? $data['payoutYesterday'];
        $data['totalRevenueLastSevenDays'] = (float) $this->data['revenueSuperlinkLastSevenDays'] + $data['payoutLastSevenDays'] ?? $data['payoutLastSevenDays'];
        $data['totalRevenueThisMonth'] = (float) $this->data['revenueSuperlinkThisMonth'] + $data['payoutThisMonth'] ?? $data['payoutThisMonth'];
        $data['totalRevenueLastMonth'] = (float) $this->data['revenueSuperlinkLastMonth'] + $data['payoutLastMonth'] ?? $data['payoutLastMonth'];
        $data['totalRevenueAllTime'] = (float) $this->data['revenueSuperlinkAllTime'] + $data['payoutAllTime'] ?? $data['payoutAllTime'];

        $data['offersActive'] = (int) $this->data['offersActive'] ?? 0;
        $data['offersPrivate'] = (int) $this->data['offersPrivate'] ?? 0;
        $data['offersSuspended'] = (int) $this->data['offersSuspended'] ?? 0;
        $data['offersIncent'] = (int) $this->data['offersIncent'] ?? 0;
        $data['offersNonIncent'] = (int) $this->data['offersNonIncent'] ?? 0;
        $data['offersTotal'] = (int) $this->data['offersTotal'] ?? 0;
        $data['offersPerDay'] = (int) $this->data['offersPerDay'] ?? 0;

        return $data;
    }
}