<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command\BillingReport;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class BillingReportListCommand
 *
 * @property int $projectId
 * @property string $startDate
 * @property string $endDate
 * @property int $page
 * @property int $limit
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command\BillingReport
 */
class BillingReportListCommand extends Command
{
    /**
     * Project identifier
     *
     * @var int
     */
    protected $projectId;

    /**
     * Start date
     *
     * @var string
     */
    protected $startDate;

    /**
     * End date
     *
     * @var string
     */
    protected $endDate;

    /**
     * @var int
     */
    protected $page;

    /**
     * @var int
     */
    protected $limit;

    /**
     * BillingReportListCommand constructor
     *
     * @param null $projectId
     * @param null $page
     * @param null $limit
     */
    public function __construct(
        $projectId = null,
        $page      = null,
        $limit     = null
    ) {
        $this->projectId = $projectId;
        $this->page = $page;
        $this->limit = $limit;
    }

    /**
     * Create command from request
     *
     * @param ServerRequestInterface $request
     *
     * @return mixed
     */
    public static function handleRequest(ServerRequestInterface $request)
    {
        $data = array_merge(
            $request->getQueryParams()
        );

        $data['projectId'] = $request->getAttribute('id',null);

        return self::hydrate($data);
    }
}