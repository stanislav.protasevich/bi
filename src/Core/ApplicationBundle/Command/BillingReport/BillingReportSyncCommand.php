<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command\BillingReport;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class BillingReportSyncCommand
 *
 * @property string $period
 * @property float $advertInvoices
 * @property float $advertPaid
 * @property float $advertUnpaid
 * @property float $advertScrubbed
 * @property float $advertCross
 * @property float $publisherInvoices
 * @property float $publisherPaid
 * @property float $publisherUnpaid
 * @property float $amountDiff
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command\BillingReport
 */
class BillingReportSyncCommand extends Command
{
    /** @var string */
    protected $period = '';

    /** @var int */
    protected $advertInvoices = 0;

    /** @var int */
    protected $advertPaid = 0;

    /** @var int */
    protected $advertUnpaid = 0;

    /** @var int */
    protected $advertScrubbed = 0;

    /** @var int */
    protected $advertCross = 0;

    /** @var int */
    protected $publisherInvoices = 0;

    /** @var int */
    protected $publisherPaid = 0;

    /** @var int */
    protected $publisherUnpaid = 0;

    /** @var int */
    protected $amountDiff = 0;

    /**
     * BillingReportSyncCommand constructor
     *
     * @param string $period
     * @param float $advertInvoices
     * @param float $advertPaid
     * @param float $advertUnpaid
     * @param float $advertScrubbed
     * @param float $advertCross
     * @param float $publisherInvoices
     * @param float $publisherPaid
     * @param float $publisherUnpaid
     * @param float $amountDiff
     */
    public function __construct(
        $period = '',
        $advertInvoices = 0.0,
        $advertPaid = 0.0,
        $advertUnpaid = 0.0,
        $advertScrubbed = 0.0,
        $advertCross = 0.0,
        $publisherInvoices = 0.0,
        $publisherPaid = 0.0,
        $publisherUnpaid = 0.0,
        $amountDiff = 0.0
    ) {
        $this->period = $period;
        $this->advertInvoices = $advertInvoices;
        $this->advertPaid = $advertPaid;
        $this->advertUnpaid = $advertUnpaid;
        $this->advertScrubbed = $advertScrubbed;
        $this->advertCross = $advertCross;
        $this->publisherInvoices = $publisherInvoices;
        $this->publisherPaid = $publisherPaid;
        $this->publisherUnpaid = $publisherUnpaid;
        $this->amountDiff = $amountDiff;
    }

    /**
     * Create command from request
     *
     * @param ServerRequestInterface $request
     *
     * @return mixed
     */
    public static function handleRequest(ServerRequestInterface $request)
    {
        $data = [];

        return self::hydrate($data);
    }
}