<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command\BillingReport;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Orangear\Admin\Core\Domain\Model\BillingReport\BillingReportSyncCommandCollection;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class BillingReportsSyncCommand
 *
 * @property string $token
 * @property BillingReportSyncCommandCollection $billingReportCollection
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command\BillingReport
 */
class BillingReportsSyncCommand extends Command
{
    /**
     * @var string
     */
    public $token;

    /**
     * @var BillingReportSyncCommandCollection
     */
    public $billingReportCollection;

    /**
     * BillingReportsSyncCommand constructor
     *
     * @param $token
     * @param BillingReportSyncCommandCollection $billingReportCollection
     */
    public function __construct(
        $token = '',
        BillingReportSyncCommandCollection $billingReportCollection = null)
    {
        $this->token = $token;
        $this->billingReportCollection = $billingReportCollection;
    }

    /**
     * Create command from request
     *
     * @param ServerRequestInterface $request
     *
     * @return mixed
     */
    public static function handleRequest(ServerRequestInterface $request)
    {
        $body = $request->getParsedBody();
        $billingReport = $body['reports'] ?? json_encode([]);
        $billingReport = json_decode($billingReport, true);

        $billingReportCollection = new BillingReportSyncCommandCollection();

        foreach ($billingReport as $br) {
            $billingReportCollection[] = BillingReportSyncCommand::hydrate($br);
        }

        $data = array_merge(
            [
                'token' => $request->getHeader('X-Auth-Token')[0] ?? '',
            ],
            [
                'billingReportCollection' => $billingReportCollection
            ]
        );

        return self::hydrate($data);
    }
}