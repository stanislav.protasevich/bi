<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command\FinancialData;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class FinancialDataListCommand
 *
 * @property string $startDate
 * @property string $endDate
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command\FinancialData
 */
class FinancialDataListCommand extends Command
{
    /**
     * Start date
     *
     * @var string
     */
    public $startDate;

    /**
     * End date
     *
     * @var string
     */
    public $endDate;

    /**
     * FinancialDataListCommand constructor
     *
     * @param null $startDate
     * @param null $endDate
     */
    public function __construct(
        $startDate = null,
        $endDate     = null
    ) {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    /**
     * Create command from request
     *
     * @param ServerRequestInterface $request
     *
     * @return mixed
     */
    public static function handleRequest(ServerRequestInterface $request)
    {
        $data = array_merge(
            $request->getAttributes(),
            $request->getQueryParams()
        );

        return self::hydrate($data);
    }
}