<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command\Statistics;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class StatisticsFindCommand
 *
 * @property $projectId
 * @property $startDate
 * @property $endDate
 * @property $timezone
 * @property $breakdownDate
 * @property $breakdownGeography
 * @property $breakdownPlatforms
 * @property $breakdownOfferId
 * @property $breakdownOfferName
 * @property $breakdownAdvertiserId
 * @property $breakdownRealPubId
 * @property $breakdownPubId
 * @property $breakdownSubAffId
 * @property $breakdownPublisherName
 * @property $breakdownAffiliateManager
 * @property $breakdownAccountManager
 * @property $breakdownNonZeroRevenuer
 * @property $breakdownSecretHash
 * @property $dataClicks
 * @property $dataBackTraffic
 * @property $dataConversions
 * @property $dataPubsConversions
 * @property $dataCpa
 * @property $dataPubsCpa
 * @property $dataCpc
 * @property $dataCr
 * @property $dataPubsCr
 * @property $dataRpa
 * @property $dataRpc
 * @property $dataCosts
 * @property $dataRevenues
 * @property $dataExtraRevenues
 * @property $dataProfit
 * @property $fraudDuplicates
 * @property $fraudDuplicatesPercentage
 * @property $fraudMinFraud
 * @property $fraudMinFraudPercentage
 * @property $fraudHdrProxyClicks
 * @property $fraudHdrProxyClicksPercentage
 * @property $fraudHdrProxyConversions
 * @property $fraudHdrProxyConversionsPercentage
 * @property $filterCountries
 * @property $filterSubAffId
 * @property $filterPlatforms
 * @property $filterOffers
 * @property $filterOfferTypes
 * @property $filterAdvertisers
 * @property $filterPublishers
 * @property $filterManagers
 * @property $filterHash
 * @property $size
 * @property $page
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command\Statistics
 */
class StatisticsFindCommand extends Command
{
    /** @var int */
    protected $projectId;

    /** @var string */
    protected $startDate;

    /** @var string */
    protected $endDate;

    /** @var string */
    protected $timezone;


    /** @var bool */
    protected $breakdownDate;

    /** @var bool */
    protected $breakdownGeography;

    /** @var bool */
    protected $breakdownPlatforms;

    /** @var bool */
    protected $breakdownOfferId;

    /** @var bool */
    protected $breakdownOfferName;

    /** @var bool */
    protected $breakdownAdvertiserId;

    /** @var bool */
    protected $breakdownRealPubId;

    /** @var bool */
    protected $breakdownPubId;

    /** @var bool */
    protected $breakdownSubAffId;

    /** @var bool */
    protected $breakdownPublisherName;

    /** @var bool */
    protected $breakdownAffiliateManager;

    /** @var bool */
    protected $breakdownAccountManager;

    /** @var bool */
    protected $breakdownNonZeroRevenuer;

    /** @var bool */
    protected $breakdownSecretHash;


    /** @var bool */
    protected $dataClicks;

    /** @var bool */
    protected $dataBackTraffic;

    /** @var bool */
    protected $dataConversions;

    /** @var bool */
    protected $dataPubsConversions;

    /** @var bool */
    protected $dataCpa;

    /** @var bool */
    protected $dataPubsCpa;

    /** @var bool */
    protected $dataCpc;

    /** @var bool */
    protected $dataCr;

    /** @var bool */
    protected $dataPubsCr;

    /** @var bool */
    protected $dataRpa;

    /** @var bool */
    protected $dataRpc;

    /** @var bool */
    protected $dataCosts;

    /** @var bool */
    protected $dataRevenues;

    /** @var bool */
    protected $dataExtraRevenues;

    /** @var bool */
    protected $dataProfit;


    /** @var bool */
    protected $fraudDuplicates;

    /** @var bool */
    protected $fraudDuplicatesPercentage;

    /** @var bool */
    protected $fraudMinFraud;

    /** @var bool */
    protected $fraudMinFraudPercentage;

    /** @var bool */
    protected $fraudHdrProxyClicks;

    /** @var bool */
    protected $fraudHdrProxyClicksPercentage;

    /** @var bool */
    protected $fraudHdrProxyConversions;

    /** @var bool */
    protected $fraudHdrProxyConversionsPercentage;


    /** @var array */
    protected $filterCountries;

    /** @var array */
    protected $filterSubAffId;

    /** @var array */
    protected $filterPlatforms;

    /** @var array */
    protected $filterOffers;

    /** @var array */
    protected $filterOfferTypes;

    /** @var array */
    protected $filterAdvertisers;

    /** @var array */
    protected $filterPublishers;

    /** @var array */
    protected $filterManagers;

    /** @var array */
    protected $filterHash;

    /** @var array */
    protected $size;

    /** @var array */
    protected $page;

    /**
     * @param ServerRequestInterface $request
     *
     * @return \Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface
     */
    static function handleRequest(ServerRequestInterface $request)
    {
        $data = [];

        $data = array_merge($data, $request->getQueryParams());

        return self::hydrate($data);
    }
}