<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command\EmployeeGroup;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class CreateEmployeeGroupCommand
 *
 * @property string $name
 * @property string $description
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command\EmployeeGroup
 */
class CreateEmployeeGroupCommand extends Command
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $description;

    /**
     * CreateEmployeeGroupCommand constructor
     *
     * @param null $name
     * @param null $description
     */
    public function __construct($name = null, $description = null)
    {
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return \Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface
     */
    static function handleRequest(ServerRequestInterface $request)
    {
        $data = array_merge([], $request->getParsedBody());

        return self::hydrate($data);
    }
}