<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command;

use InvalidArgumentException;
use Pagerfanta\Adapter\AdapterInterface;
use Pagerfanta\Exception\LessThan1CurrentPageException;
use Pagerfanta\Exception\NotIntegerMaxPerPageException;
use Pagerfanta\Exception\OutOfRangeCurrentPageException;
use Pagerfanta\Pagerfanta;

/**
 * Class ListResponse
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command
 */
class ListResponse
{
    /**
     * @var Pagerfanta
     */
    private $data;

    /**
     * ListResponse constructor
     *
     * @param $data
     * @param int $page
     * @param int $limit
     */
    public function __construct($data, int $page, int $limit)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        $data = [];

        foreach ($this->data as $project) {
            $qwe = [];
            $qwe['id'] = $project->projectId()->toString();
            $qwe['name'] = $project->name();
            $qwe['logo'] = $project->logo();

            $data[] = $qwe;
        }

        return [
            'data' => $data,
        ];
    }
}