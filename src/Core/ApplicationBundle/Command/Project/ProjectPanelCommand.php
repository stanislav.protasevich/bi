<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command\Project;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Filter\FilterInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ProjectPanelCommand
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command\Project
 */
class ProjectPanelCommand extends Command
{
    /**
     * ProjectListCommand constructor
     */
    public function __construct() {}

    /**
     * Create command from request
     *
     * @param ServerRequestInterface $request
     * @param FilterInterface|null $filter
     *
     * @return mixed
     */
    public static function handleRequest(ServerRequestInterface $request, FilterInterface $filter = null)
    {
        $data = array_merge([]);

        return self::hydrate($data, $filter);
    }
}