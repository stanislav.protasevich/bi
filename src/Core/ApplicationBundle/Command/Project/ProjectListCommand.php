<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command\Project;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Filter\FilterInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ProjectListCommand
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command\Project
 */
class ProjectListCommand extends Command
{
    /**
     * Company identifier
     *
     * @var int
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(
     *  type="digit",
     *  message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public $companyId;

    /**
     * Project identifier
     *
     * @var int
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(
     *  type="digit",
     *  message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public $projectId;

    /**
     * ProjectListCommand constructor
     *
     * @param null $projectId
     * @param null $companyId
     */
    public function __construct($projectId = null, $companyId = null)
    {
        $this->companyId = $companyId;
        $this->projectId = $projectId;
    }

    /**
     * Create command from request
     *
     * @param ServerRequestInterface $request
     * @param FilterInterface|null $filter
     *
     * @return mixed
     */
    public static function handleRequest(ServerRequestInterface $request, FilterInterface $filter = null)
    {
        $data = array_merge($request->getQueryParams());

        return self::hydrate($data, $filter);
    }
}