<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command\Project;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Filter\FilterInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ProjectCreateCommand
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command\Project
 */
class ProjectCreateCommand extends Command
{
    /**
     * Platform identifier
     *
     * @var string
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(
     *  type="digit",
     *  message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public $platformId = null;

    /**
     * Project name
     *
     * @var int
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public $name = null;

    /**
     * Project logo
     *
     * @var string
     *
     * @Assert\NotBlank()
     */
    public $logo = null;

    /**
     * ProjectCreateCommand constructor
     *
     * @param string $platformId
     * @param string $name
     * @param string $logo
     */
    public function __construct($platformId = null, $name = null, $logo = null)
    {
        $this->platformId = $platformId;
        $this->name       = $name;
        $this->logo       = $logo;
    }

    /**
     * {@inheritdoc}
     */
    public static function handleRequest(ServerRequestInterface $request, FilterInterface $filter = null)
    {
        $data = array_merge($request->getParsedBody());

        return self::hydrate($data, $filter);
    }
}