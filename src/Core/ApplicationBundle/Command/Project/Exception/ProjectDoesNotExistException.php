<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command\Project\Exception;

/**
 * Class ProjectDoesNotExistException
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command\Project\Exception
 */
class ProjectDoesNotExistException extends \Exception {}