<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command\Project;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Filter\FilterInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ProjectDashboardCommand
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command\Project
 */
class ProjectDashboardCommand extends Command
{
    /**
     * Project identifier
     *
     * @var int
     */
    public $projectId;

    /**
     * ProjectListCommand constructor
     *
     * @param null $projectId
     */
    public function __construct($projectId = null)
    {
        $this->projectId = $projectId;
    }

    /**
     * Get project dashboard from request
     *
     * @param ServerRequestInterface $request
     * @param FilterInterface|null $filter
     *
     * @return mixed
     */
    public static function handleRequest(ServerRequestInterface $request, FilterInterface $filter = null)
    {
        $data = array_merge($request->getAttributes());
        $data['project_id'] = $request->getAttribute('id', null);

        return self::hydrate($data, $filter);
    }
}