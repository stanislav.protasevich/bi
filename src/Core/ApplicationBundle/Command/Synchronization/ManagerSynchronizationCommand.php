<?php
/**
 * Created by PhpStorm.
 * User: stanislas
 * Date: 11.07.17
 * Time: 16:03
 */

namespace Orangear\Admin\Core\ApplicationBundle\Command\Synchronization;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Filter\FilterInterface;

/**
 * Class ManagerSynchronization
 *
 * @property $projectId
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command\Synchronization
 */
class ManagerSynchronizationCommand extends Command
{
    /** @var int */
    protected $projectId;

    /**
     * {@inheritdoc}
     */
    public static function handleRequest(ServerRequestInterface $request, FilterInterface $filter = null)
    {
        $data = array_merge($request->getQueryParams());

        return self::hydrate($data, $filter);
    }
}