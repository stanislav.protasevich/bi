<?php

namespace Orangear\Admin\Core\ApplicationBundle\Command\Charge;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class CreateEmployeeCommand
 *
 * @property string $projectId
 * @property string $chargeTypeId
 * @property string $employeeId
 * @property string $startDate
 * @property string $endDate
 * @property int $type
 * @property float $amount
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Command\Employee
 */
class CreateChargeCommand extends Command
{
    /** @var string */
    protected $projectId;

    /** @var string */
    protected $chargeTypeId;

    /** @var string */
    protected $employeeId;

    /** @var string */
    protected $startDate;

    /** @var string */
    protected $endDate;

    /** @var int */
    protected $type;

    /** @var float */
    protected $amount;

    /**
     * CreateChargeCommand constructor
     *
     * @param string $projectId
     * @param string $chargeTypeId
     * @param string $employeeId
     * @param string $startDate
     * @param string $endDate
     * @param int $type
     * @param float $amount
     */
    public function __construct($projectId = null, $chargeTypeId = null, $employeeId = null, $startDate = null, $endDate = null, $type = null, $amount = null)
    {
        $this->projectId = $projectId;
        $this->chargeTypeId = $chargeTypeId;
        $this->employeeId = $employeeId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->type = $type;
        $this->amount = $amount;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return \Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface
     */
    static function handleRequest(ServerRequestInterface $request)
    {
        $data = array_merge([], $request->getParsedBody());

        return self::hydrate($data);
    }
}