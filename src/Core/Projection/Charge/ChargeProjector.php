<?php

namespace Orangear\Admin\Core\Projection\Charge;

use Doctrine\ORM\EntityManagerInterface;
use Orangear\Admin\Core\Domain\Model\Charge\Event\ChargeWasDeleted;
use Orangear\Admin\Core\Domain\Model\Charge\Exception\ChargeNotFound;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeCharge;

/**
 * Class ChargeProjector
 *
 * @package Orangear\Admin\Core\Projection\Charge
 */
final class ChargeProjector
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * ChargeProjector constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param ChargeWasDeleted $event
     */
    public function onEvent(ChargeWasDeleted $event)
    {
        $chargeRepository = $this->entityManager->getRepository(Charge::class);
        $employeeChargeRepository = $this->entityManager->getRepository(EmployeeCharge::class);

        $charge = $chargeRepository->findOneBy(['id' => $event->chargeId()]);
        if (!$charge) {
            throw ChargeNotFound::withChargeId($event->chargeId()->toString());
        }

        $this->entityManager->remove($charge);

        $employeeCharge = $employeeChargeRepository->findOneBy(['charge' => $event->chargeId()]);

        if ($employeeCharge) {
            $this->entityManager->remove($employeeCharge);
        }

        $this->entityManager->flush();
    }
}