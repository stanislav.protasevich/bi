<?php

namespace Orangear\Admin\Core\Projection\Charge;

use Doctrine\ORM\EntityManagerInterface;
use Orangear\Admin\Core\Domain\Model\Charge\Event\ChargeWasUpdated;
use Orangear\Admin\Core\Domain\Model\Charge\Exception\ChargeNotFound;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;

/**
 * Class OnChargeWasUpdatedProjector
 *
 * @package Orangear\Admin\Core\Projection\Charge
 */
final class OnChargeWasUpdatedProjector
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * ChargeProjector constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param ChargeWasUpdated $event
     */
    public function onEvent(ChargeWasUpdated $event)
    {
        $chargeRepository = $this->entityManager->getRepository(Charge::class);

        /** @var Charge $charge */
        $charge = $chargeRepository->findOneBy(['id' => $event->chargeId()]);

        if ($charge === null) {
            throw ChargeNotFound::withChargeId($event->chargeId()->toString());
        }

        $charge
            ->setStartDate($event->chargeStartDate())
            ->setEndDate($event->chargeEndDate())
            ->setAmount($event->chargeAmount());

        $this->entityManager->persist($charge);
        $this->entityManager->flush();
    }
}