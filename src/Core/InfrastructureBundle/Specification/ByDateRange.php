<?php

namespace Orangear\Admin\Core\InfrastructureBundle\Specification;

use Rb\Specification\Doctrine\Condition\Between;
use Rb\Specification\Doctrine\Specification;

/**
 * Class ByProjectId
 *
 * @package Orangear\Admin\Core\InfrastructureBundle\Specification
 */
class ByDateRange extends Specification
{
    /**
     * ByDateRange constructor
     *
     * @param array|\Rb\Specification\Doctrine\SpecificationInterface[] $startDate
     * @param $endDate
     * @param $alias
     */
    public function __construct($startDate, $endDate, $alias)
    {
        $specs = [
            new Between('date', $startDate, $endDate, $alias),
        ];

        parent::__construct($specs);
    }
}