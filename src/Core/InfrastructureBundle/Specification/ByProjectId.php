<?php

namespace Orangear\Admin\Core\InfrastructureBundle\Specification;

use Rb\Specification\Doctrine\Condition\Equals;
use Rb\Specification\Doctrine\Specification;

/**
 * Class ByProjectId
 *
 * @package Orangear\Admin\Core\InfrastructureBundle\Specification
 */
class ByProjectId extends Specification
{
    /**
     * ByCompanyId constructor.
     *
     * @param array|\Rb\Specification\Doctrine\SpecificationInterface[] $projectId
     * @param $alias
     */
    public function __construct($projectId, $alias)
    {
        if (!is_null($projectId)) {
            $specs = [
                new Equals('id', $projectId, $alias),
            ];
        } else {
            $specs = [];
        }

        parent::__construct($specs);
    }
}