<?php

namespace Orangear\Admin\Core\InfrastructureBundle\Specification;

use Rb\Specification\Doctrine\Condition\Between;
use Rb\Specification\Doctrine\Specification;

/**
 * Class ByDate
 *
 * @package Orangear\Admin\Core\InfrastructureBundle\Specification
 */
class ByDate extends Specification
{
    /**
     * ByDate constructor
     *
     * @param array|\Rb\Specification\Doctrine\SpecificationInterface[] $date
     * @param $alias
     */
    public function __construct($date, $alias)
    {
        $specs = [
            new Equ('date', $startDate, $endDate, $alias),
        ];

        parent::__construct($specs);
    }
}