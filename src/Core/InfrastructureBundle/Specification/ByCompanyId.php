<?php

namespace Orangear\Admin\Core\InfrastructureBundle\Specification;

use Rb\Specification\Doctrine\Condition\Equals;
use Rb\Specification\Doctrine\Specification;

/**
 * Class ByCompanyId
 *
 * @package Orangear\Admin\Core\InfrastructureBundle\Specification
 */
class ByCompanyId extends Specification
{
    /**
     * ByCompanyId constructor.
     *
     * @param array|\Rb\Specification\Doctrine\SpecificationInterface[] $companyId
     * @param $alias
     */
    public function __construct($companyId, $alias)
    {
        if (!is_null($companyId)) {
            $specs = [
                new Equals('company', $companyId, $alias),
            ];
        } else {
            $specs = [];
        }

        parent::__construct($specs);
    }
}