<?php

namespace Orangear\Admin\Core\InfrastructureBundle\Repository;

use Orangear\Admin\Core\Domain\Model\ChargeType\ChargeType;

/**
 * Class ChargeTypeRepository
 *
 * @package Orangear\Admin\Core\InfrastructureBundle\Repository
 */
class ChargeTypeRepository extends Repository
{
    const ALIAS = 'ct';

    /**
     * @param string $id
     *
     * @return null|ChargeType
     */
    public function byId(string $id)
    {
        return $this->entityManager
            ->getRepository($this->getEntityName())
            ->findOneBy(['id' => $id]);
    }

    /**
     * @param string $name
     *
     * @return null|object
     */
    public function byName(string $name)
    {
        return $this->entityManager
            ->getRepository($this->getEntityName())
            ->findOneBy(['name' => $name]);
    }

    /**
     * @param string $key
     *
     * @return null|object
     */
    public function byKey(string $key)
    {
        return $this->entityManager
            ->getRepository($this->getEntityName())
            ->findOneBy(['key' => $key]);
    }

    /**
     * @param ChargeType $chargeType
     */
    public function create(ChargeType $chargeType)
    {
        $this->entityManager->persist($chargeType);
        $this->entityManager->flush();
    }

    /**
     * @return string
     */
    protected function getAlias()
    {
        return self::ALIAS;
    }

    /**
     * @return string
     */
    protected function getEntityName()
    {
        return ChargeType::class;
    }
}