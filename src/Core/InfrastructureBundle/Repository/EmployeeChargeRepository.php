<?php

namespace Orangear\Admin\Core\InfrastructureBundle\Repository;

use Orangear\Admin\Core\Domain\Model\EmployeeCharge\EmployeeCharge;

/**
 * Class EmployeeChargeRepository
 *
 * @package Orangear\Admin\Core\InfrastructureBundle\Repository
 */
class EmployeeChargeRepository extends Repository
{
    const ALIAS = 'ec';

    /**
     * @param EmployeeCharge $employeeCharge
     */
    public function create(EmployeeCharge $employeeCharge)
    {
        $this->entityManager->persist($employeeCharge);
        $this->entityManager->flush();
    }

    /**
     * @return string
     */
    protected function getAlias()
    {
        return self::ALIAS;
    }

    /**
     * @return string
     */
    protected function getEntityName()
    {
        return EmployeeCharge::class;
    }
}