<?php

namespace Orangear\Admin\Core\InfrastructureBundle\Repository;

use Orangear\Admin\Core\Domain\Model\Company\Company;
use Orangear\Admin\Core\Domain\Model\Company\CompanyRepositoryInterface;
use Orangear\Admin\Core\Domain\Model\Dashboard\Dashboard;
use Orangear\Admin\Core\Domain\Model\Dashboard\DashboardRepositoryInterface;
use Orangear\Admin\Core\Domain\Model\Project\Project;
use Orangear\Admin\Core\InfrastructureBundle\Specification\ByCompanyId;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Rb\Specification\AndX;
use Rb\Specification\Doctrine\Specification;
use Symfony\Component\Cache\Adapter\DoctrineAdapter;
use Symfony\Component\Validator\Constraints\DateTime;


/**
 * Class CompanyRepository
 *
 * @package Orangear\Admin\Core\InfrastructureBundle\Repository
 */
class DashboardRepository extends Repository implements DashboardRepositoryInterface
{
    const ALIAS = 'd';

    /**
     * @param array $filters
     *
     * @return DoctrineORMAdapter
     */
    public function find(array $filters)
    {
        $specification = new Specification([]);

        $query = $this->match($specification);

        return new DoctrineORMAdapter($query);
    }

    /**
     * @param ProjectInterface $project
     * @return null|Dashboard
     */
    public function getByProject(ProjectInterface $project): ?Dashboard
    {
        $project = $this->entityManager->createQueryBuilder()
            ->select(self::ALIAS)
            ->from($this->getEntityName(), self::ALIAS)
            ->where(self::ALIAS . '.project = :project')
            ->setParameter('project', $project)
            ->getQuery()
            ->getOneOrNullResult();

        return $project;
    }

    /**
     * @param array $filters
     *
     * @return DoctrineORMAdapter
     */
    public function getDashboard(array $filters)
    {
        $sql = "
            SELECT
              SUM(dashboard.income_today) AS incomeToday,
              SUM(dashboard.income_yesterday) AS incomeYesterday,
              SUM(dashboard.income_last_seven_days) AS incomeLastSevenDays,
              SUM(dashboard.income_this_month) AS incomeThisMonth,
              SUM(dashboard.income_last_month) AS incomeLastMonth,
              SUM(dashboard.income_all_time) AS incomeAllTime,
              
              SUM(dashboard.income_superlink_today) AS incomeSuperlinkToday,
              SUM(dashboard.income_superlink_yesterday) AS incomeSuperlinkYesterday,
              SUM(dashboard.income_superlink_last_seven_days) AS incomeSuperlinkLastSevenDays,
              SUM(dashboard.income_superlink_this_month) AS incomeSuperlinkThisMonth,
              SUM(dashboard.income_superlink_last_month) AS incomeSuperlinkLastMonth,
              SUM(dashboard.income_superlink_all_time) AS incomeSuperlinkAllTime,
              
              SUM(dashboard.payout_today) AS payoutToday,
              SUM(dashboard.payout_yesterday) AS payoutYesterday,
              SUM(dashboard.payout_last_seven_days) AS payoutLastSevenDays,
              SUM(dashboard.payout_this_month) AS payoutThisMonth,
              SUM(dashboard.payout_last_month) AS payoutLastMonth,
              SUM(dashboard.payout_all_time) AS payoutAllTime,
              
              SUM(dashboard.offers_active) AS offersActive,
              SUM(dashboard.offers_private) AS offersPrivate,
              SUM(dashboard.offers_suspended) AS offersSuspended,
              SUM(dashboard.offers_incent) AS offersIncent,
              SUM(dashboard.offers_non_incent) AS offersNonIncent,
              SUM(dashboard.offers_total) AS offersTotal,
              SUM(dashboard.offers_per_day) AS offersPerDay,
              
              SUM(dashboard.revenue_superlink_today) AS revenueSuperlinkToday,
              SUM(dashboard.revenue_superlink_yesterday) AS revenueSuperlinkYesterday,
              SUM(dashboard.revenue_superlink_last_seven_days) AS revenueSuperlinkLastSevenDays,
              SUM(dashboard.revenue_superlink_this_month) AS revenueSuperlinkThisMonth,
              SUM(dashboard.revenue_superlink_last_month) AS revenueSuperlinkLastMonth,
              SUM(dashboard.revenue_superlink_all_time) AS revenueSuperlinkAllTime
            FROM project
              JOIN dashboard ON dashboard.project_id = project.id            
        ";

        $where = [];
        $where[] = "(dashboard.project_id, UNIX_TIMESTAMP(dashboard.synchronized_at)) IN (SELECT project_id, MAX(UNIX_TIMESTAMP(dashboard.synchronized_at)) AS synchronized_at FROM dashboard GROUP BY dashboard.project_id)";

        if (isset($filters['project']) && !is_null($filters['project'])) {
            $where[] = "project.id = '" . $filters['project']->projectId() . "'";
        }

        $sql .= ' WHERE ' . implode(' AND ', $where);

        $sql .= ' ORDER BY project.name ASC';

        return $this->entityManager->getConnection()->executeQuery($sql)->fetchAll();
    }

    public function delete(Dashboard $company)
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param Dashboard $dashboard
     * @return mixed|void
     */
    public function update(Dashboard $dashboard)
    {
        $this->entityManager->persist($dashboard);
        $this->entityManager->flush();
    }

    public function getById(int $id): ? Dashboard
    {
        /** @var Dashboard $project */

        $project = $this->entityManager
            ->getRepository(Dashboard::class)
            ->find($id);

        return $project;
    }

    public function create(Dashboard $dashboard)
    {
        $this->entityManager->persist($dashboard);
        $this->entityManager->flush();
    }

    /**
     * @return string
     */
    protected function getEntityName()
    {
        return Dashboard::class;
    }

    /**
     * @return string
     */
    protected function getAlias()
    {
        return self::ALIAS;
    }
}