<?php

namespace Orangear\Admin\Core\InfrastructureBundle\Repository;

use Doctrine\ORM\Query\Expr\Join;
use Orangear\Admin\Core\Domain\Model\Employee\Employee;
use Orangear\Admin\Core\Domain\Model\ProjectEmployee\ProjectEmployee;

/**
 * Class EmployeeRepository
 *
 * @package Orangear\Admin\Core\InfrastructureBundle\Repository
 */
class EmployeeRepository extends Repository
{
    const ALIAS = 'e';

    /**
     * @param string $id
     *
     * @return null|Employee
     */
    public function byId(string $id)
    {
        return $this->entityManager
            ->getRepository($this->getEntityName())
            ->findOneBy(['id' => $id]);
    }

    /**
     * @param $projectId
     * @param string $remoteId
     *
     * @return null|Employee
     */
    public function byRemoteId($projectId, string $remoteId)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select(self::ALIAS)
            ->from($this->getEntityName(), self::ALIAS)
            ->join(ProjectEmployee::class, 'pe', Join::WITH, 'pe.employee = e.id')
            ->where(self::ALIAS . '.remoteId = :remote_id')
            ->andWhere('pe.project = :project');

        $qb->setParameters(['remote_id' => $remoteId, 'project' => $projectId]);

        $query = $qb->getQuery()->getOneOrNullResult();

        return $query;
    }

    /**
     * @param Employee $employee
     */
    public function create(Employee $employee)
    {
        $this->entityManager->persist($employee);
        $this->entityManager->flush();
    }

    /**
     * @return string
     */
    protected function getAlias()
    {
        return self::ALIAS;
    }

    /**
     * @return string
     */
    protected function getEntityName()
    {
        return Employee::class;
    }
}