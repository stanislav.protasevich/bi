<?php

namespace Orangear\Admin\Core\InfrastructureBundle\Repository;

use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Pagerfanta\Adapter\DoctrineORMAdapter;

/**
 * Class ChargeRepository
 *
 * @package Orangear\Admin\Core\InfrastructureBundle\Repository
 */
class ChargeRepository extends Repository
{
    const ALIAS = 'c';

    /**
     * @return DoctrineORMAdapter
     */
    public function find(array $filters)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select([
            'IDENTITY(' . self::ALIAS . '.project) AS project_id',
            'SUM(' . self::ALIAS . '.amount) AS salaries',
            'YEAR(' . self::ALIAS . '.startDate) AS year',
            'MONTH(' . self::ALIAS . '.startDate) AS month',
            ])
            ->from($this->getEntityName(), self::ALIAS)
            ->groupBy('project_id')
            ->addGroupBy('year')
            ->addGroupBy('month');

        if (isset($filters['project'])) {
            $qb->andWhere(self::ALIAS . '.project = :project')
                ->setParameter('project', $filters['project']);
        }

        if (isset($filters['charge_type'])) {
            $qb->andWhere(self::ALIAS . '.chargeType = :chargeType')
                ->setParameter('chargeType', $filters['charge_type']);
        }

        if (isset($filters['not_charge_type'])) {
            $qb->andWhere(self::ALIAS . '.chargeType != :chargeType')
                ->setParameter('chargeType', $filters['not_charge_type']);
        }

        if (isset($filters['start_date']) && isset($filters['end_date'])) {
            $qb->andWhere(self::ALIAS . '.startDate >= :start_date AND ' . self::ALIAS . '.endDate <= :end_date');
            $qb->setParameter('start_date', $filters['start_date'])
                ->setParameter('end_date', $filters['end_date']);
        }

        $query = $qb->getQuery()->execute();

        return $query;
    }

    /**
     * @param Charge $charge
     */
    public function delete(Charge $charge)
    {
        $this->entityManager->remove($charge);
        $this->entityManager->flush();
    }

    /**
     * @param string $id
     *
     * @return null|Charge
     */
    public function byId(string $id)
    {
        return $this->entityManager
            ->getRepository($this->getEntityName())
            ->findOneBy(['id' => $id]);
    }

    /**
     * @param Charge $charge
     */
    public function create(Charge $charge)
    {
        $this->entityManager->persist($charge);
        $this->entityManager->flush();
    }

    /**
     * @return string
     */
    protected function getAlias()
    {
        return self::ALIAS;
    }

    /**
     * @return string
     */
    protected function getEntityName()
    {
        return Charge::class;
    }
}