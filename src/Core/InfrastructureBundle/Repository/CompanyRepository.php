<?php

namespace Orangear\Admin\Core\InfrastructureBundle\Repository;

use Orangear\Admin\Core\Domain\Model\Company\Company;
use Orangear\Admin\Core\Domain\Model\Company\CompanyRepositoryInterface;
use Orangear\Admin\Core\InfrastructureBundle\Specification\ByCompanyId;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Rb\Specification\AndX;
use Rb\Specification\Doctrine\Specification;
use Symfony\Component\Cache\Adapter\DoctrineAdapter;

/**
 * Class CompanyRepository
 *
 * @package Orangear\Admin\Core\InfrastructureBundle\Repository
 */
class CompanyRepository extends Repository implements CompanyRepositoryInterface
{
    const ALIAS = 'c';

    /**
     * @param array $filters
     *
     * @return DoctrineORMAdapter
     */
    public function find(array $filters)
    {
        $specification = new Specification([]);

        $query = $this->match($specification);

        return new DoctrineORMAdapter($query);
    }

    public function delete(Company $company)
    {
        // TODO: Implement delete() method.
    }

    public function update(Company $company)
    {
        // TODO: Implement update() method.
    }

    public function getById(int $id): ? Company
    {
        /** @var Company $project */

        $project = $this->entityManager
            ->getRepository(Company::class)
            ->find($id);

        return $project;
    }

    public function create(Company $company)
    {
        // TODO: Implement create() method.
    }

    /**
     * @return string
     */
    protected function getEntityName()
    {
        return Company::class;
    }

    /**
     * @return string
     */
    protected function getAlias()
    {
        return self::ALIAS;
    }
}