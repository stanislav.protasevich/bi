<?php

namespace Orangear\Admin\Core\InfrastructureBundle\Repository;

use Doctrine\ORM\Query\Expr\Join;
use Orangear\Admin\Core\Domain\Model\Dashboard\Dashboard;
use Orangear\Admin\Core\Domain\Model\Project\Company;
use Orangear\Admin\Core\Domain\Model\Project\Project;
use Orangear\Admin\Core\Domain\Model\Project\ProjectRepositoryInterface;
use Orangear\Admin\Core\Domain\Model\ProjectEmployee\ProjectEmployee;
use Orangear\Admin\Core\InfrastructureBundle\Specification\ByCompanyId;
use Orangear\Admin\Core\InfrastructureBundle\Specification\ByProjectId;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Rb\Specification\Doctrine\Specification;
use Symfony\Component\Cache\Adapter\DoctrineAdapter;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class ProjectEmployeeRepository
 *
 * @package Orangear\Admin\Core\InfrastructureBundle\Repository
 */
class ProjectEmployeeRepository extends Repository
{
    const ALIAS = 'pe';

    /**
     * { @inheritdoc }
     */
    public function create(ProjectEmployee $projectEmployee)
    {
        $this->entityManager->persist($projectEmployee);
        $this->entityManager->flush();
    }

    /**
     * { @inheritdoc }
     */
    public function update(Project $project)
    {
        $this->entityManager->persist($project);
        $this->entityManager->flush();
    }

    /**
     * { @inheritdoc }
     */
    public function delete(Project $project)
    {
        $this->entityManager->remove($project);
        $this->entityManager->flush();
    }

    /**
     * @return string
     */
    protected function getEntityName()
    {
        return ProjectEmployee::class;
    }

    /**
     * @return string
     */
    protected function getAlias()
    {
        return self::ALIAS;
    }

    /**
     * @return mixed
     */
    public function countEmployeesPerProject($start, $end)
    {
        $sql = '
SELECT
  manager_sale_statistics.period AS period,
  manager_sale_statistics.project_id as project_id,
  project.name as project_name,
  COUNT(DISTINCT(manager_sale_statistics.employee_id)) AS amount
FROM manager_sale_statistics
  JOIN project ON project.id = manager_sale_statistics.project_id
  WHERE manager_sale_statistics.period BETWEEN \'' . $start->format('Y-m-d') . '\' AND \'' . $end->format('Y-m-d') . '\'
GROUP BY manager_sale_statistics.project_id, manager_sale_statistics.period;
';

        return $this->entityManager->getConnection()->executeQuery($sql)->fetchAll();
    }
}