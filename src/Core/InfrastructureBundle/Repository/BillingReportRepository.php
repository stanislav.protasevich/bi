<?php

namespace Orangear\Admin\Core\InfrastructureBundle\Repository;

use Orangear\Admin\Core\Domain\Model\BillingReport\BillingReport;
use Orangear\Admin\Core\Domain\Model\BillingReport\BillingReportRepositoryInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Ramsey\Uuid\Uuid;

/**
 * Class BillingReportRepository
 *
 * @package Orangear\Admin\Core\InfrastructureBundle\Repository
 */
class BillingReportRepository extends Repository implements BillingReportRepositoryInterface
{
    const ALIAS = 'br';

    /**
     * @param array $filters
     *
     * @return DoctrineORMAdapter
     */
    public function find(array $filters)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select(self::ALIAS)
            ->from($this->getEntityName(), self::ALIAS)
            ->orderBy(self::ALIAS . '.date', 'ASC');

        if (isset($filters['project'])) {
            $qb->andWhere(self::ALIAS . '.project = :project')
                ->setParameter('project', $filters['project']);
        }

        if (isset($filters['start_date']) && isset($filters['end_date'])) {
            $qb->andWhere(self::ALIAS . '.date BETWEEN :start_date AND :end_date')
                ->setParameter('start_date', $filters['start_date']->format('Y-m-d'))
                ->setParameter('end_date', $filters['end_date']->format('Y-m-d'));
        } else if (isset($filters['start_date'])) {
            $qb->andWhere(self::ALIAS . '.date = :date')
                ->setParameter('date', $filters['start_date']->format('Y-m-d'));
        }

        //$query = $qb->getQuery()->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

        $query = $qb->getQuery()->execute();

        return $query;
    }

    /**
     * {@inheritdoc}
     */
    public function getByDate(ProjectInterface $project, \DateTime $date): ? BillingReport
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select(self::ALIAS)
            ->from($this->getEntityName(), self::ALIAS)
            ->where(self::ALIAS . '.date = :date')
            ->andWhere(self::ALIAS . '.project = :project')
            ->setParameters(['date' => $date->format('Y-m-01'),
                'project' => $project]);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function delete(BillingReport $billingReport)
    {
        // TODO: Implement delete() method.
    }

    public function sync(BillingReport $billingReport)
    {
        $this->entityManager->persist($billingReport);
    }

    public function update(BillingReport $billingReport)
    {
        // TODO: Implement update() method.
    }

    public function getById(Uuid $uuid): ? BillingReport
    {
        /** @var BillingReport $billingReport */

        $billingReport = $this->entityManager
            ->getRepository(BillingReport::class)
            ->find($uuid);

        return $billingReport;
    }

    public function create(BillingReport $billingReport)
    {
        // TODO: Implement create() method.
    }

    /**
     * @return string
     */
    protected function getEntityName()
    {
        return BillingReport::class;
    }

    /**
     * @return string
     */
    protected function getAlias()
    {
        return self::ALIAS;
    }

    /**
     * return void
     */
    public function flush()
    {
        $this->entityManager->flush();
    }
}