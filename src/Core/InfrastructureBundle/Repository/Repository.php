<?php

namespace Orangear\Admin\Core\InfrastructureBundle\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Rb\Specification\Doctrine\Exception\LogicException;
use Rb\Specification\Doctrine\Result\ModifierInterface;
use Rb\Specification\SpecificationInterface;

/**
 * Class Repository
 *
 * @package Orangear\Admin\Core\InfrastructureBundle\Repository\OwnerRepository
 */
abstract class Repository
{
    /** @var EntityManager */
    protected $entityManager;

    /**
     * Repository constructor
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get the query after matching with given specification.
     *
     * @param SpecificationInterface $specification
     * @param ModifierInterface $resultModifier
     *
     * @throws LogicException
     *
     * @return Query
     */
    public function match(SpecificationInterface $specification, ModifierInterface $resultModifier = null, QueryBuilder $queryBuilder = null)
    {
        if (!$specification->isSatisfiedBy($this->getEntityName())) {
            throw new LogicException(sprintf(
                'Specification "%s" not supported by this repository!',
                get_class($specification)
            ));
        }

        $dqlAlias = $this->getAlias();
        if (is_null($queryBuilder)) {
            $queryBuilder = $this->entityManager->getRepository($this->getEntityName())->createQueryBuilder($dqlAlias);
        }
        $condition = $specification->modify($queryBuilder, $dqlAlias);

        if (!empty($condition)) {
            $queryBuilder->where($condition);
        }

        $query = $queryBuilder->getQuery();
        if ($resultModifier) {
            $resultModifier->modify($query);
        }

        return $query;
    }

    /**
     * @return string
     */
    abstract protected function getEntityName();

    /**
     * @return string
     */
    abstract protected function getAlias();
}