<?php

namespace Orangear\Admin\Core\InfrastructureBundle\Repository;

use Orangear\Admin\Core\Domain\Model\EmployeeGroup\EmployeeGroup;

/**
 * Class EmployeeGroupRepository
 *
 * @package Orangear\Admin\Core\InfrastructureBundle\Repository
 */
class EmployeeGroupRepository extends Repository
{
    const ALIAS = 'eg';

    /**
     * @param string $id
     *
     * @return null|EmployeeGroup
     */
    public function byId(string $id)
    {
        return $this->entityManager
            ->getRepository($this->getEntityName())
            ->findOneBy(['id' => $id]);
    }

    /**
     * @param string $name
     *
     * @return null|object
     */
    public function byName(string $name)
    {
        return $this->entityManager
            ->getRepository($this->getEntityName())
            ->findOneBy(['name' => $name]);
    }

    /**
     * @param EmployeeGroup $employeeGroup
     */
    public function create(EmployeeGroup $employeeGroup)
    {
        $this->entityManager->persist($employeeGroup);
        $this->entityManager->flush();
    }

    /**
     * @return string
     */
    protected function getAlias()
    {
        return self::ALIAS;
    }

    /**
     * @return string
     */
    protected function getEntityName()
    {
        return EmployeeGroup::class;
    }
}