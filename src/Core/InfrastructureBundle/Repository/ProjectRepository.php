<?php

namespace Orangear\Admin\Core\InfrastructureBundle\Repository;

use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Orangear\Admin\Core\Domain\Model\Dashboard\Dashboard;
use Orangear\Admin\Core\Domain\Model\Project\Company;
use Orangear\Admin\Core\Domain\Model\Project\ProjectRepositoryInterface;
use Orangear\Admin\Core\InfrastructureBundle\Specification\ByCompanyId;
use Orangear\Admin\Core\InfrastructureBundle\Specification\ByProjectId;
use Orangear\BusinessIntelligence\Domain\Model\Dashboard\Dashboard as DashboardQ;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Rb\Specification\AndX;
use Rb\Specification\Doctrine\Specification;
use Symfony\Component\Cache\Adapter\DoctrineAdapter;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class ProjectRepository
 *
 * @package Orangear\Admin\Core\InfrastructureBundle\Repository
 */
class ProjectRepository extends Repository implements ProjectRepositoryInterface
{
    const ALIAS = 'p';

    /**
     * @param string $id
     *
     * @return null|Project
     */
    public function byId(string $id)
    {
        return $this->entityManager
            ->getRepository($this->getEntityName())
            ->findOneBy(['id' => $id]);
    }

    /**
     * @param array $filters
     *
     * @return DoctrineORMAdapter
     */
    public function find(array $filters)
    {
        $specification = new Specification([]);

        if (isset($filters['project_id'])) {
            $specification->add(new ByProjectId($filters['project_id'], self::ALIAS));
        }

        if (isset($filters['company_id'])) {
            $specification->add(new ByCompanyId($filters['company_id'], self::ALIAS));
        }

        $query = $this->match($specification);

        return new DoctrineORMAdapter($query);
    }

    /**
     * @return DoctrineORMAdapter
     */
    public function fetchAll()
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select([
            self::ALIAS . '.id',
            self::ALIAS . '.name',
            self::ALIAS . '.token',
            self::ALIAS . '.logo',
        ])
            ->from($this->getEntityName(), self::ALIAS)
            ->orderBy(self::ALIAS . '.order', 'ASC');

        return $qb->getQuery()->execute();
    }

    /**
     * Get projects summary
     *
     * @param array $filters
     * @return DoctrineORMAdapter
     */
    public function getSummary(array $filters)
    {
        $sql = "
            SELECT DISTINCT
              project.id,
              project.name,
              project.logo,
              project.token,
              ABS(dashboard.income_yesterday - dashboard.income_today) AS income,
              CASE WHEN (dashboard.income_yesterday - dashboard.income_today >= 0) THEN 0 ELSE 1 END AS trend
            FROM dashboard
              JOIN project ON project.id = dashboard.project_id
            WHERE (dashboard.project_id, UNIX_TIMESTAMP(dashboard.synchronized_at)) IN (SELECT DISTINCT project_id, MAX(UNIX_TIMESTAMP(dashboard.synchronized_at)) AS synchronized_at FROM dashboard GROUP BY dashboard.project_id)
            GROUP BY project.id, dashboard.income_today, dashboard.income_yesterday
            ORDER BY project.name ASC
        ";

        return $this->entityManager->getConnection()->executeQuery($sql)->fetchAll();
    }

    /**
     * { @inheritdoc }
     */
    public function create(Project $project)
    {
        $this->entityManager->persist($project);
        $this->entityManager->flush();
    }

    /**
     * { @inheritdoc }
     */
    public function update(Project $project)
    {
        $this->entityManager->persist($project);
        $this->entityManager->flush();
    }

    /**
     * { @inheritdoc }
     */
    public function delete(Project $project)
    {
        $this->entityManager->remove($project);
        $this->entityManager->flush();
    }

    /**
     * { @inheritdoc }
     */
    public function getById(int $id): ? Project
    {
        /** @var Project $project */

        $project = $this->entityManager
            ->getRepository(Project::class)
            ->find($id);

        return $project;
    }

    /**
     * { @inheritdoc }
     */
    public function getByToken(string $token): ? Project
    {
        /** @var Project $project */

        $project = $this->entityManager->createQueryBuilder()
            ->select(self::ALIAS)
            ->from($this->getEntityName(), self::ALIAS)
            ->where(self::ALIAS . '.token = :token')
            ->setParameter('token', $token)
            ->getQuery()
            ->getOneOrNullResult();

        return $project;
    }

    /**
     * @return string
     */
    protected function getEntityName()
    {
        return Project::class;
    }

    /**
     * @return string
     */
    protected function getAlias()
    {
        return self::ALIAS;
    }
}