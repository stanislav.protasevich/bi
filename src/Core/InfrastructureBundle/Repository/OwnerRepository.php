<?php

namespace Orangear\Admin\Core\InfrastructureBundle\Repository\OwnerRepository;

use Orangear\Admin\Core\Domain\Owner\OwnerRepositoryInterface;
use Orangear\Admin\Core\Domain\Owner\OwnerId;
use Orangear\Admin\Core\Domain\Owner\Owner;

/**
 * Class OwnerRepository
 *
 * @package Orangear\Admin\Core\InfrastructureBundle\Repository\OwnerRepository
 */
class OwnerRepository extends Repository implements OwnerRepositoryInterface
{
    /**
     * { @inheritdoc }
     */
    public function getByOwnerId(OwnerId $ownerId) : Owner
    {
        /** @var Owner $owner */
        $owner = $this->entityManager->getRepository(Owner::class)->find($ownerId);

        return $owner;
    }
}