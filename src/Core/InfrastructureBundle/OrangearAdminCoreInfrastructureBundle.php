<?php

namespace Orangear\Admin\Core\InfrastructureBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class OrangearAdminCoreInfrastructureBundle
 *
 * @package Orangear\Admin\Core\InfrastructureBundle
 */
class OrangearAdminCoreInfrastructureBundle extends Bundle {}