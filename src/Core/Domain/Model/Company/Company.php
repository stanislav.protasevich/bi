<?php

namespace Orangear\Admin\Core\Domain\Model\Company;

/**
 * Class Company
 *
 * @package Orangear\Admin\Core\Domain\Model\Company
 */
class Company
{
    /** @var int Company id */
    private $id;

    /** @var string Company name */
    private $name;

    /** @var string Company logo */
    private $logo;

    public function __construct(string $name, string $logo)
    {
        $this->name = $name;
        $this->logo = $logo;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     */
    public function setLogo(string $logo)
    {
        $this->logo = $logo;
    }

    /**
     * Create new platform
     *
     * @param CompanyId $companyId
     * @param string $name
     * @param string $logo
     *
     * @return Company
     */
    public static function create(CompanyId $companyId, string $name, string $logo)
    {
        return new self(
            $companyId,
            $name,
            $logo
        );
    }
}