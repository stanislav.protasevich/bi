<?php

namespace Orangear\Admin\Core\Domain\Model\Company;

use Symfony\Component\Ldap\Adapter\AdapterInterface;

/**
 * Interface CompanyRepositoryInterface
 * 
 * @package Orangear\Admin\Core\Domain\Model\Company
 */
interface CompanyRepositoryInterface
{
    /**
     * Get company by identifier
     *
     * @param int $id
     *
     * @return null|Company
     */
    public function getById(int $id): ? Company;

    /**
     * Get list of company
     *
     * @param array $filters
     * @return AdapterInterface
     */
    public function find(array $filters);

    /**
     * Create new company
     *
     * @param Company $company
     *
     * @return mixed
     */
    public function create(Company $company);

    /**
     * Update existing company
     *
     * @param Company $company
     *
     * @return mixed
     */
    public function update(Company $company);

    /**
     * Delete existing company
     *
     * @param Company $company
     *
     * @return mixed
     */
    public function delete(Company $company);
}