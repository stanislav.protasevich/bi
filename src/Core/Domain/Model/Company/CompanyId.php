<?php

namespace Orangear\Admin\Core\Domain\Model\Company;

/**
 * Class CompanyId
 *
 * @package Orangear\Admin\Core\Domain\Model\Company
 */
class CompanyId
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->id;
    }
}