<?php

namespace Orangear\Admin\Core\Domain\Model\Company\Service;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\ProjectListCommand;
use Orangear\Admin\Core\Domain\Model\Company\CompanyId;
use Orangear\Admin\Core\Domain\Model\Company\CompanyRepositoryInterface;
use Orangear\Admin\Core\Domain\Model\Project\ProjectRepositoryInterface;
use Orangear\Admin\Core\Domain\ServiceInterface;

/**
 * Class CompanyListService
 *
 * @package Orangear\Admin\Core\Domain\Model\Project\Service
 */
class CompanyListService implements ServiceInterface
{
    /** @var CompanyRepositoryInterface */
    private $repository;

    /**
     * CompanyListService constructor
     *
     * @param CompanyRepositoryInterface $companyRepository
     */
    public function __construct(CompanyRepositoryInterface $companyRepository)
    {
        $this->repository = $companyRepository;
    }

    /**
     * Get projects
     *
     * @param CommandInterface $command
     *
     * @return \Symfony\Component\Ldap\Adapter\AdapterInterface
     */
    public function execute(CommandInterface $command)
    {
        $params = [];

        return $this->repository->find($params);
    }
}