<?php

namespace Orangear\Admin\Core\Domain\Model\BillingReport\Service;

use Doctrine\ORM\EntityManagerInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\BillingReport\BillingReportListCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\Domain\Model\BillingReport\BillingReport;
use Orangear\Admin\Core\Domain\Model\BillingReport\BillingReportRepositoryInterface;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectRepositoryInterface;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository\ChargeRepository;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository\ChargeTypeRepository;

/**
 * Class BillingReportListService
 *
 * @package Orangear\Admin\Core\Domain\Model\BillingReport\Service
 */
class BillingReportListService implements ServiceInterface
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ProjectRepositoryInterface */
    private $projectRepository;

    /** @var BillingReportRepositoryInterface */
    private $billingReportRepository;

    /** @var ChargeRepository */
    private $chargeRepository;

    /** @var ChargeTypeRepository */
    private $chargeTypeRepository;

    /**
     * CompanyListService constructor
     *
     * @param EntityManagerInterface $entityManager
     * @param BillingReportRepositoryInterface $billingReportRepository
     * @param ProjectRepositoryInterface $projectRepository
     * @param ChargeRepository $chargeRepository
     * @param ChargeTypeRepository $chargeTypeRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        BillingReportRepositoryInterface $billingReportRepository,
        ProjectRepositoryInterface $projectRepository,
        ChargeRepository $chargeRepository,
        ChargeTypeRepository $chargeTypeRepository
    ) {
        $this->entityManager = $entityManager;
        $this->billingReportRepository = $billingReportRepository;
        $this->projectRepository = $projectRepository;
        $this->chargeRepository = $chargeRepository;
        $this->chargeTypeRepository = $chargeTypeRepository;
    }

    /**
     * @param CommandInterface $command
     *
     * @return array
     *
     * @throws \Exception
     */
    public function execute(CommandInterface $command)
    {
        /** @var BillingReportListCommand $command */

        $params = [];

        if (!is_null($command->projectId)) {
            $project = $this->projectRepository->findById(ProjectId::fromString($command->projectId));

            if (!$project) {
                throw new \Exception('No project with such token');
            }

            $params['project'] = $project;
        }

        if (!is_null($command->startDate)) {
            $params['start_date'] = new \DateTime($command->startDate);
        }

        if (!is_null($command->endDate)) {
            $params['end_date'] = new \DateTime($command->endDate);
        }

        $params['not_charge_type'] = $this->findSalaryChargeOrFail('salary');

        $expensesTable = [];
        foreach ($this->findExpenses($params) as $charge) {
            $expensesTable[$charge['project_id']][$charge['year']][$charge['month']] = $charge['salaries'];
        }

        unset($params['not_charge_type']);
        $params['charge_type'] = $this->findSalaryChargeOrFail('salary');

        $salariesTable = [];
        foreach ($this->findExpenses($params) as $charge) {
            $salariesTable[$charge['project_id']][$charge['year']][$charge['month']] = $charge['salaries'];
        }

        $billingReports = $this->billingReportRepository->find($params);

        $data = [];
        /** @var BillingReport $billingReport */
        foreach ($billingReports as $billingReport) {
            $temp['date'] = $billingReport->getDate()->format('Y-m');
            $temp['revenue'] = $billingReport->getRevenue();
            $temp['revenueReceived'] = $billingReport->getRevenueReceived();
            $temp['crossPayment'] = $billingReport->getCrossPayment();
            $temp['deduction'] = $billingReport->getDeduction();
            $temp['revenueUnpaid'] = $billingReport->getRevenueUnpaid();
            $temp['publisherAmount'] = $billingReport->getPublisherAmount();
            $temp['publisherPaid'] = $billingReport->getPublisherPaid();
            $temp['publisherUnpaid'] = $billingReport->getPublisherUnpaid();
            $temp['grossIncome'] = $billingReport->getGrossIncome();

            $temp['netRevenue'] = $temp['revenue'] - $temp['crossPayment'] - $temp['deduction'];
            $temp['netIncome'] = $temp['revenueReceived'] - $temp['publisherPaid'];

            $temp['salaries'] = 0;
            if (isset($salariesTable[$billingReport->getProject()->projectId()->toString()][$billingReport->getDate()->format('Y')][intval($billingReport->getDate()->format('m'))])) {
                $temp['salaries'] = (float) $salariesTable[$billingReport->getProject()->projectId()->toString()][$billingReport->getDate()->format('Y')][intval($billingReport->getDate()->format('m'))];
            }

            $temp['otherExpenses'] = 0;
            if (isset($expensesTable[$billingReport->getProject()->projectId()->toString()][$billingReport->getDate()->format('Y')][intval($billingReport->getDate()->format('m'))])) {
                $temp['otherExpenses'] = (float) $expensesTable[$billingReport->getProject()->projectId()->toString()][$billingReport->getDate()->format('Y')][intval($billingReport->getDate()->format('m'))];
            }

            $temp['totalExpenses'] = $temp['salaries'] + $temp['otherExpenses'];

            $temp['grossProfit'] = 0;
            $temp['netProfit'] = $temp['netIncome'] - $temp['totalExpenses'];
            $temp['discrepancy'] = 0;

            if ($temp['netRevenue'] != 0) {
                $temp['netMargin'] = $temp['netIncome'] / $temp['netRevenue'] * 100;
            } else {
                $temp['netMargin'] = 0;
            }

            /** @todo Our amount */
            $temp['our_amount'] = 0;
            $temp['discrepancy'] = $temp['revenue'] - $temp['our_amount'];

            $temp['project']['id'] = $billingReport->getProject()->projectId()->toString();
            $temp['project']['name'] = $billingReport->getProject()->name();

            $data['data'][] = $temp;
        }

        return $data;
    }

    /**
     * @param $key
     *
     * @return array|mixed|null|\Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeInterface
     *
     * @throws \Exception
     */
    private function findSalaryChargeOrFail($key)
    {
        $salary = $this->chargeTypeRepository->findOneBy(['key' => $key]);

        if ($salary === null) {
            throw new \Exception('Can not find "' . $key . '" charge type');
        }

        return $salary;
    }

    private function findExpenses($params)
    {
        $qb = $this->entityManager->createQueryBuilder();

        $qb->select([
            'IDENTITY(' . 'c' . '.project) AS project_id',
            'SUM(' . 'c' . '.amount) AS salaries',
            'YEAR(' . 'c' . '.startDate) AS year',
            'MONTH(' . 'c' . '.startDate) AS month',
        ])
            ->from(Charge::class, 'c')
            ->groupBy('project_id')
            ->addGroupBy('year')
            ->addGroupBy('month');

        if (isset($params['project'])) {
            $qb->andWhere('c' . '.project = :project')
                ->setParameter('project', $params['project']);
        }

        if (isset($params['charge_type'])) {
            $qb->andWhere('c' . '.chargeType = :chargeType')
                ->setParameter('chargeType', $params['charge_type']);
        }

        if (isset($params['not_charge_type'])) {
            $qb->andWhere('c' . '.chargeType != :chargeType')
                ->setParameter('chargeType', $params['not_charge_type']);
        }

        if (isset($params['start_date']) && isset($params['end_date'])) {
            $qb->andWhere('c' . '.startDate >= :start_date AND ' . 'c' . '.endDate <= :end_date');
            $qb->setParameter('start_date', $params['start_date'])
                ->setParameter('end_date', $params['end_date']);
        }

        $query = $qb->getQuery()->getResult();

        return $query;
    }
}