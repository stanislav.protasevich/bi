<?php

namespace Orangear\Admin\Core\Domain\Model\BillingReport\Service;

use Orangear\Admin\Core\ApplicationBundle\Command\BillingReport\BillingReportsSyncCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\BillingReport\BillingReportSyncCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\Domain\Model\BillingReport\BillingReport;
use Orangear\Admin\Core\Domain\Model\BillingReport\BillingReportRepositoryInterface;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectRepositoryInterface;
use Ramsey\Uuid\Uuid;

/**
 * Class BillingReportsSyncService
 *
 * @package Orangear\Admin\Core\Domain\Model\BillingReport\Service
 */
class BillingReportsSyncService implements ServiceInterface
{
    /** @var ProjectRepositoryInterface */
    private $projectRepository;

    /** @var BillingReportRepositoryInterface */
    private $billingReportRepository;

    /**
     * CompanyListService constructor
     *
     * @param BillingReportRepositoryInterface $billingReportRepository
     * @param ProjectRepositoryInterface $projectRepository
     */
    public function __construct(
        BillingReportRepositoryInterface $billingReportRepository,
        ProjectRepositoryInterface $projectRepository)
    {
        $this->billingReportRepository = $billingReportRepository;
        $this->projectRepository = $projectRepository;
    }

    /**
     * @param CommandInterface $command
     *
     * @return \Symfony\Component\Ldap\Adapter\AdapterInterface
     *
     * @throws \Exception
     */
    public function execute(CommandInterface $command)
    {
        /** @var BillingReportsSyncCommand $command */

        $project = $this->projectRepository->findByToken($command->token);

        if (!$project) {
            throw new \Exception('No project with such token');
        }

        /** @var BillingReportSyncCommand $billingReportSyncCommand */
        foreach ($command->billingReportCollection as $billingReportSyncCommand) {
            list($year, $month) = explode('-', $billingReportSyncCommand->period);
            $date = new \DateTime();
            $date->setDate($year, $month,1);

            /** @var BillingReport $billingReport */
            $billingReport = $this->billingReportRepository->getByDate($project, $date);

            if (!$billingReport) {
                $billingReport = BillingReport::create(
                    Uuid::uuid4(),
                    $project,
                    $date,
                    $billingReportSyncCommand->advertInvoices,
                    $billingReportSyncCommand->advertPaid,
                    $billingReportSyncCommand->advertCross,
                    $billingReportSyncCommand->advertScrubbed,
                    $billingReportSyncCommand->advertUnpaid,
                    $billingReportSyncCommand->publisherInvoices,
                    $billingReportSyncCommand->publisherPaid,
                    $billingReportSyncCommand->publisherUnpaid,
                    $billingReportSyncCommand->amountDiff
                );
            } else {
                $billingReport->update(
                    $billingReport->getProject(),
                    $billingReport->getDate(),
                    $billingReportSyncCommand->advertInvoices,
                    $billingReportSyncCommand->advertPaid,
                    $billingReportSyncCommand->advertCross,
                    $billingReportSyncCommand->advertScrubbed,
                    $billingReportSyncCommand->advertUnpaid,
                    $billingReportSyncCommand->publisherInvoices,
                    $billingReportSyncCommand->publisherPaid,
                    $billingReportSyncCommand->publisherUnpaid,
                    $billingReportSyncCommand->amountDiff
                );
            }

            $this->billingReportRepository->sync($billingReport);
        }

        $this->billingReportRepository->flush();
    }
}