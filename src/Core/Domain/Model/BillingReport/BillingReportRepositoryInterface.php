<?php

namespace Orangear\Admin\Core\Domain\Model\BillingReport;

use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Ldap\Adapter\AdapterInterface;

/**
 * Interface BillingReportRepositoryInterface
 * 
 * @package Orangear\Admin\Core\Domain\Model\Company
 */
interface BillingReportRepositoryInterface
{
    /**
     * Get billing report by identifier
     *
     * @param Uuid $uuid
     *
     * @return null|BillingReport
     */
    public function getById(Uuid $uuid): ? BillingReport;

    /**
     * Get billing report by date
     *
     * @param ProjectInterface $project
     * @param \DateTime $date
     * @return null|BillingReport
     */
    public function getByDate(ProjectInterface $project, \DateTime $date): ? BillingReport;

    /**
     * Get list of billing reports
     *
     * @param array $filters
     * @return AdapterInterface
     */
    public function find(array $filters);

    /**
     * Create new billing report
     *
     * @param BillingReport $billingReport
     *
     * @return mixed
     */
    public function create(BillingReport $billingReport);

    /**
     * Update existing billing report
     *
     * @param BillingReport $billingReport
     *
     * @return mixed
     */
    public function update(BillingReport $billingReport);

    /**
     * Delete existing billing report
     *
     * @param BillingReport $billingReport
     *
     * @return mixed
     */
    public function delete(BillingReport $billingReport);
}