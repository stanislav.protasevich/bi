<?php

namespace Orangear\Admin\Core\Domain\Model\BillingReport;

use Orangear\BusinessIntelligence\Domain\Model\Project\Project;

/**
 * Class BillingReport
 *
 * @package Orangear\Admin\Core\Domain\Model\BillingReport
 */
class BillingReport
{
    /** @var string */
    private $id;

    /** @var Project */
    private $project;

    /** @var \DateTime */
    private $date;

    /**
     * Advert amount
     *
     * @var float
     */
    private $revenue;

    /**
     * Received
     *
     * @var float
     */
    private $revenueReceived;

    /**
     * Cross payment
     *
     * @var float
     */
    private $crossPayment;

    /**
     * Deduction
     *
     * @var float
     */
    private $deduction;

    /**
     * Advert unpaid
     *
     * @var float
     */
    private $revenueUnpaid;

    /**
     * Publisher amount
     *
     * @var float
     */
    private $publisherAmount;

    /**
     * Publisher paid
     *
     * @var float
     */
    private $publisherPaid;

    /**
     * Publisher unpaid
     *
     * @var float
     */
    private $publisherUnpaid;

    /**
     * Amount diff
     *
     * @var float
     */
    private $grossIncome;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return Project
     */
    public function getProject(): Project
    {
        return $this->project;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @return float
     */
    public function getRevenue(): float
    {
        return $this->revenue;
    }

    /**
     * @return float
     */
    public function getRevenueReceived(): float
    {
        return $this->revenueReceived;
    }

    /**
     * @return float
     */
    public function getCrossPayment(): float
    {
        return $this->crossPayment;
    }

    /**
     * @return float
     */
    public function getDeduction(): float
    {
        return $this->deduction;
    }

    /**
     * @return float
     */
    public function getRevenueUnpaid(): float
    {
        return $this->revenueUnpaid;
    }

    /**
     * @return float
     */
    public function getPublisherAmount(): float
    {
        return $this->publisherAmount;
    }

    /**
     * @return float
     */
    public function getPublisherPaid(): float
    {
        return $this->publisherPaid;
    }

    /**
     * @return float
     */
    public function getPublisherUnpaid(): float
    {
        return $this->publisherUnpaid;
    }

    /**
     * @return float
     */
    public function getGrossIncome(): float
    {
        return $this->grossIncome;
    }

    /**
     * BillingReport constructor.
     * @param string $id
     * @param Project $project
     * @param \DateTime $date
     * @param float $revenue
     * @param float $revenueReceived
     * @param float $crossPayment
     * @param float $deduction
     * @param float $revenueUnpaid
     * @param float $publisherAmount
     * @param float $publisherPaid
     * @param float $publisherUnpaid
     * @param float $grossIncome
     */
    public function __construct(
        string $id,
        Project $project,
        \DateTime $date,
        $revenue,
        $revenueReceived,
        $crossPayment,
        $deduction,
        $revenueUnpaid,
        $publisherAmount,
        $publisherPaid,
        $publisherUnpaid,
        $grossIncome
    ) {
        $this->id = $id;
        $this->project = $project;
        $this->date = $date;
        $this->revenue = $revenue;
        $this->revenueReceived = $revenueReceived;
        $this->crossPayment = $crossPayment;
        $this->deduction = $deduction;
        $this->revenueUnpaid = $revenueUnpaid;
        $this->publisherAmount = $publisherAmount;
        $this->publisherPaid = $publisherPaid;
        $this->publisherUnpaid = $publisherUnpaid;
        $this->grossIncome = $grossIncome;
    }

    /**
     * Create new billing report
     *
     * @param string $id
     * @param Project $project
     * @param \DateTime $date
     * @param $revenue
     * @param $revenueReceived
     * @param $crossPayment
     * @param $deduction
     * @param $revenueUnpaid
     * @param $publisherAmount
     * @param $publisherPaid
     * @param $publisherUnpaid
     * @param $grossIncome
     *
     * @return BillingReport
     */
    public static function create(
        $id,
        Project $project,
        \DateTime $date,
        $revenue,
        $revenueReceived,
        $crossPayment,
        $deduction,
        $revenueUnpaid,
        $publisherAmount,
        $publisherPaid,
        $publisherUnpaid,
        $grossIncome
    ) {
        return new self(
            $id,
            $project,
            $date,
            $revenue,
            $revenueReceived,
            $crossPayment,
            $deduction,
            $revenueUnpaid,
            $publisherAmount,
            $publisherPaid,
            $publisherUnpaid,
            $grossIncome
        );
    }

    /**
     * Update billing report
     *
     * @param Project $project
     * @param \DateTime $date
     * @param $revenue
     * @param $revenueReceived
     * @param $crossPayment
     * @param $deduction
     * @param $revenueUnpaid
     * @param $publisherAmount
     * @param $publisherPaid
     * @param $publisherUnpaid
     * @param $grossIncome
     */
    public function update(
        Project $project,
        \DateTime $date,
        $revenue,
        $revenueReceived,
        $crossPayment,
        $deduction,
        $revenueUnpaid,
        $publisherAmount,
        $publisherPaid,
        $publisherUnpaid,
        $grossIncome
    ) {
        $this->project = $project;
        $this->date = $date;
        $this->revenue = $revenue;
        $this->revenueReceived = $revenueReceived;
        $this->crossPayment = $crossPayment;
        $this->deduction = $deduction;
        $this->revenueUnpaid = $revenueUnpaid;
        $this->publisherAmount = $publisherAmount;
        $this->publisherPaid = $publisherPaid;
        $this->publisherUnpaid = $publisherUnpaid;
        $this->grossIncome = $grossIncome;
    }
}