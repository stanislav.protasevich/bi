<?php

namespace Orangear\Admin\Core\Domain\Model\BillingReport;

use Orangear\Admin\Core\ApplicationBundle\Command\BillingReport\BillingReportSyncCommand;

/**
 * Class BillingReportCollection
 *
 * @package Orangear\Admin\Core\Domain\Model\BillingReport
 */
class BillingReportSyncCommandCollection implements \Iterator, \ArrayAccess, \Countable
{
    /**
     * @var int
     */
    private $position = 0;

    /**
     * @var array
     */
    private $billingReports = [];

    /**
     * @return mixed
     */
    public function current()
    {
        return $this->billingReports[$this->position];
    }

    /**
     * @return int
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * @return void
     */
    public function next()
    {
        ++$this->position;
    }

    /**
     * @return void
     */
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return isset($this->billingReports[$this->position]);
    }

    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->billingReports[$offset]);
    }

    /**
     * @param mixed $offset
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->offsetExists($offset) ? $this->billingReports[$offset] : null;
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     *
     * @throws \Exception
     */
    public function offsetSet($offset, $value)
    {
        if (!$value instanceof BillingReportSyncCommand) {
            throw new \Exception('Not BillingReport');
        }

        if (is_null($offset)) {
            $this->billingReports[] = $value;
        } else {
            $this->billingReports[$offset] = $value;
        }
    }

    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset)
    {
        unset($this->billingReports[$offset]);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->billingReports);
    }
}