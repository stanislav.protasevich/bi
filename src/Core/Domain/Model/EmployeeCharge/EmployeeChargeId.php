<?php

namespace Orangear\Admin\Core\Domain\Model\EmployeeCharge;

use Ramsey\Uuid\Uuid;

/**
 * Class EmployeeChargeId
 *
 * @package Orangear\Admin\Core\Domain\Model\EmployeeCharge
 */
class EmployeeChargeId
{
    /**
     * @var string
     */
    private $id;

    /**
     * ChargeTypeId constructor
     *
     * @param null $id
     */
    private function __construct($id = null)
    {
        $this->id = $id ?: Uuid::uuid4()->toString();
    }

    /**
     * @param null $anId
     *
     * @return static
     */
    public static function create($anId = null)
    {
        return new static($anId);
    }

    /**
     * @return string
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }

    /**
     * @param EmployeeChargeId $employeeChargeId
     *
     * @return bool
     */
    public function equals(EmployeeChargeId $employeeChargeId)
    {
        return $this->id === $employeeChargeId->id();
    }
}