<?php

namespace Orangear\Admin\Core\Domain\Model\EmployeeCharge;

use Orangear\Admin\Core\Domain\Model\Charge\Charge;
use Orangear\Admin\Core\Domain\Model\Employee\Employee;

/**
 * Class EmployeeCharge
 * @package Orangear\Admin\Core\Domain\Model\EmployeeCharge
 */
class EmployeeCharge
{
    /** @var EmployeeChargeId */
    private $id;

    /** @var Charge */
    private $charge;

    /** @var Employee */
    private $employee;

    /**
     * EmployeeCharge constructor
     *
     * @param EmployeeChargeId $id
     * @param Charge $charge
     * @param Employee $employee
     */
    public function __construct(EmployeeChargeId $id, Charge $charge, Employee $employee)
    {
        $this->id = $id;
        $this->charge = $charge;
        $this->employee = $employee;
    }

    /**
     * @return EmployeeChargeId
     */
    public function getId(): EmployeeChargeId
    {
        return $this->id;
    }

    /**
     * @param EmployeeChargeId $id
     */
    public function setId(EmployeeChargeId $id)
    {
        $this->id = $id;
    }

    /**
     * @return Charge
     */
    public function getCharge(): Charge
    {
        return $this->charge;
    }

    /**
     * @param Charge $charge
     */
    public function setCharge(Charge $charge)
    {
        $this->charge = $charge;
    }

    /**
     * @return Employee
     */
    public function getEmployee(): Employee
    {
        return $this->employee;
    }

    /**
     * @param Employee $employee
     */
    public function setEmployee(Employee $employee)
    {
        $this->employee = $employee;
    }
}