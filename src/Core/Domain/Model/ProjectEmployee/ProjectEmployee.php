<?php

namespace Orangear\Admin\Core\Domain\Model\ProjectEmployee;

use DateTime;
use Orangear\Admin\Core\Domain\Model\Employee\Employee;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;

/**
 * Class ProjectEmployee
 *
 * @package Orangear\Admin\Core\Domain\Model\ProjectEmployee
 */
class ProjectEmployee
{
    /** @var ProjectEmployeeId */
    private $id;

    /** @var Project */
    private $project;

    /** @var Employee */
    private $employee;

    /** @var DateTime */
    private $createdAt;

    /**
     * ProjectEmployee constructor
     *
     * @param ProjectEmployeeId $id
     * @param ProjectInterface $project
     * @param Employee $employee
     */
    public function __construct(ProjectEmployeeId $id, ProjectInterface $project, Employee $employee, DateTime $createdAt)
    {
        $this->id = $id;
        $this->project = $project;
        $this->employee = $employee;
        $this->createdAt = $createdAt;
    }

    /**
     * @return ProjectEmployeeId
     */
    public function getId(): ProjectEmployeeId
    {
        return $this->id;
    }

    /**
     * @param ProjectEmployeeId $id
     */
    public function setId(ProjectEmployeeId $id)
    {
        $this->id = $id;
    }

    /**
     * @return Project
     */
    public function getProject(): Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject(Project $project)
    {
        $this->project = $project;
    }

    /**
     * @return Employee
     */
    public function getEmployee(): Employee
    {
        return $this->employee;
    }

    /**
     * @param Employee $employee
     */
    public function setEmployee(Employee $employee)
    {
        $this->employee = $employee;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }
}