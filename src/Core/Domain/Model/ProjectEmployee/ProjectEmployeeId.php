<?php

namespace Orangear\Admin\Core\Domain\Model\ProjectEmployee;

use Ramsey\Uuid\Uuid;

/**
 * Class ProjectEmployeeId
 *
 * @package Orangear\Admin\Core\Domain\Model\ProjectEmployee
 */
class ProjectEmployeeId
{
    /**
     * @var string
     */
    private $id;

    /**
     * ChargeTypeId constructor
     *
     * @param null $id
     */
    private function __construct($id = null)
    {
        $this->id = $id ?: Uuid::uuid4()->toString();
    }

    /**
     * @param null $anId
     *
     * @return static
     */
    public static function create($anId = null)
    {
        return new static($anId);
    }

    /**
     * @return string
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }

    /**
     * @param ProjectEmployeeId $projectEmployeeId
     *
     * @return bool
     */
    public function equals(ProjectEmployeeId $projectEmployeeId)
    {
        return $this->id === $projectEmployeeId->id();
    }
}