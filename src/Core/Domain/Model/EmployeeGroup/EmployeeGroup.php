<?php

namespace Orangear\Admin\Core\Domain\Model\EmployeeGroup;

/**
 * Class EmployeeGroup
 *
 * @package Orangear\Admin\Core\Domain\Model\EmployeeGroup
 */
class EmployeeGroup
{
    /** @var EmployeeGroupId */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $description;

    /**
     * EmployeeGroup constructor
     *
     * @param EmployeeGroupId $id
     * @param string $name
     * @param string $description
     */
    public function __construct(EmployeeGroupId $id, $name, $description = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return EmployeeGroupId
     */
    public function getId(): EmployeeGroupId
    {
        return $this->id;
    }

    /**
     * @param EmployeeGroupId $id
     */
    public function setId(EmployeeGroupId $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }
}