<?php

namespace Orangear\Admin\Core\Domain\Model\EmployeeGroup;

use Ramsey\Uuid\Uuid;

/**
 * Class EmployeeGroupId
 *
 * @package Orangear\Admin\Core\Domain\Model\EmployeeGroup
 */
class EmployeeGroupId
{
    /**
     * @var string
     */
    private $id;

    /**
     * ChargeTypeId constructor
     *
     * @param null $id
     */
    private function __construct($id = null)
    {
        $this->id = $id ?: Uuid::uuid4()->toString();
    }

    /**
     * @param null $anId
     *
     * @return static
     */
    public static function create($anId = null)
    {
        return new static($anId);
    }

    /**
     * @return string
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }

    /**
     * @param EmployeeGroupId $employeeGroupId
     *
     * @return bool
     */
    public function equals(EmployeeGroupId $employeeGroupId)
    {
        return $this->id === $employeeGroupId->id();
    }
}