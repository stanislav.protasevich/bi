<?php

namespace Orangear\Admin\Core\Domain\Model\EmployeeGroup\Service\Exception;

/**
 * Class EmployeeGroupAlreadyExistsException
 *
 * @package Orangear\Admin\Core\Domain\Model\EmployeeGroup\Service\Exception
 */
class EmployeeGroupAlreadyExistsException extends \Exception {}