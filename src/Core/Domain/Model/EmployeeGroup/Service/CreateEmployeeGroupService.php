<?php

namespace Orangear\Admin\Core\Domain\Model\EmployeeGroup\Service;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\EmployeeGroup\CreateEmployeeGroupCommand;
use Orangear\Admin\Core\Domain\Model\EmployeeGroup\EmployeeGroup;
use Orangear\Admin\Core\Domain\Model\EmployeeGroup\EmployeeGroupId;
use Orangear\Admin\Core\Domain\Model\EmployeeGroup\Service\Exception\EmployeeGroupAlreadyExistsException;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\Admin\Core\InfrastructureBundle\Repository\EmployeeGroupRepository;

/**
 * Class CreateEmployeeGroupService
 *
 * @package Orangear\Admin\Core\Domain\Model\EmployeeGroup\Service
 */
class CreateEmployeeGroupService implements ServiceInterface
{
    /** @var EmployeeGroupRepository */
    private $employeeGroupRepository;

    /**
     * CreateEmployeeGroupService constructor
     *
     * @param $employeeGroupRepository
     */
    public function __construct($employeeGroupRepository)
    {
        $this->employeeGroupRepository = $employeeGroupRepository;
    }

    /**
     * @param CommandInterface $createEmployeeGroupCommand
     */
    public function execute(CommandInterface $createEmployeeGroupCommand)
    {
        /** @var CreateEmployeeGroupCommand $createEmployeeGroupCommand */

        $id = EmployeeGroupId::create();
        $name = $createEmployeeGroupCommand->name;
        $description = $createEmployeeGroupCommand->description;

        $this->dontFindEmployeeGroupOrFail($name);
        $employeeGroup = new EmployeeGroup(
            $id,
            $name,
            $description
        );

        $this->employeeGroupRepository->create($employeeGroup);
    }

    protected function dontFindEmployeeGroupOrFail($name)
    {
        $employeeGroup = $this->employeeGroupRepository->byName($name);

        if ($employeeGroup !== null) {
            throw new EmployeeGroupAlreadyExistsException('Employee group already exists');
        }
    }
}