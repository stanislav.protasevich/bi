<?php

namespace Orangear\Admin\Core\Domain\Model\Dashboard\Service;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\Dashboard\DashboardSyncCommand;
use Orangear\Admin\Core\Domain\Model\Dashboard\Dashboard;
use Orangear\Admin\Core\Domain\Model\Dashboard\DashboardRepositoryInterface;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectRepositoryInterface;

class DashboardSyncService implements ServiceInterface
{
    /** @var DashboardRepositoryInterface */
    private $dashboardRepository;

    /** @var ProjectRepositoryInterface */
    private $projectRepository;

    /**
     * DashboardSyncService constructor
     *
     * @param DashboardRepositoryInterface $dashboardRepository
     * @param ProjectRepositoryInterface $projectRepository
     */
    public function __construct(DashboardRepositoryInterface $dashboardRepository, ProjectRepositoryInterface $projectRepository)
    {
        $this->dashboardRepository = $dashboardRepository;
        $this->projectRepository = $projectRepository;
    }

    /**
     * @param CommandInterface $command
     * @throws \Exception
     */
    public function execute(CommandInterface $command)
    {
        /** @var DashboardSyncCommand $command */
        $project = $this->projectRepository->findByToken($command->token);

        if (!$project) {
            throw new \Exception('No project with such token');
        }

        $dashboard = $this->dashboardRepository->getByProject($project);
        if (!$dashboard) {
            $dashboard = Dashboard::create(
                null,
                $project,
                new \DateTime($command->period),
                $command->incomeToday,
                $command->incomeYesterday,
                $command->incomeLast7Days,
                $command->incomeThisMonth,
                $command->incomeLastMonth,
                $command->incomeAllTime,
                $command->incomeSuperlinkToday,
                $command->incomeSuperlinkYesterday,
                $command->incomeSuperlinkLast7Days,
                $command->incomeSuperlinkThisMonth,
                $command->incomeSuperlinkLastMonth,
                $command->incomeSuperlinkAllTime,
                $command->payoutToday,
                $command->payoutYesterday,
                $command->payoutLast7Days,
                $command->payoutThisMonth,
                $command->payoutLastMonth,
                $command->payoutAllTime,
                $command->offersActive,
                $command->offersPrivate,
                $command->offersSuspended,
                $command->offersIncent,
                $command->offersNonIncent,
                $command->offersTotal,
                $command->offersPerDay,
                $command->timezone
            );

            $this->dashboardRepository->create($dashboard);
        } else {
            $dashboard->update(
                new \DateTime($command->period),
                $command->incomeToday,
                $command->incomeYesterday,
                $command->incomeLast7Days,
                $command->incomeThisMonth,
                $command->incomeLastMonth,
                $command->incomeAllTime,
                $command->incomeSuperlinkToday,
                $command->incomeSuperlinkYesterday,
                $command->incomeSuperlinkLast7Days,
                $command->incomeSuperlinkThisMonth,
                $command->incomeSuperlinkLastMonth,
                $command->incomeSuperlinkAllTime,
                $command->payoutToday,
                $command->payoutYesterday,
                $command->payoutLast7Days,
                $command->payoutThisMonth,
                $command->payoutLastMonth,
                $command->payoutAllTime,
                $command->offersActive,
                $command->offersPrivate,
                $command->offersSuspended,
                $command->offersIncent,
                $command->offersNonIncent,
                $command->offersTotal,
                $command->offersPerDay,
                $command->timezone
            );

            $this->dashboardRepository->update($dashboard);
        }
    }
}