<?php

namespace Orangear\Admin\Core\Domain\Model\Dashboard;

use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;

/**
 * Interface DashboardRepositoryInterface
 *
 * @package Orangear\Admin\Core\Domain\Model\Dashboard
 */
interface DashboardRepositoryInterface
{
    /**
     * @param int $id
     * @return null|Dashboard
     */
    public function getById(int $id): ?Dashboard;

    /**
     * @param ProjectInterface $project
     * @return null|Dashboard
     */
    public function getByProject(ProjectInterface $project): ?Dashboard;

    /**
     * @param Dashboard $dashboard
     * @return mixed
     */
    public function create(Dashboard $dashboard);

    /**
     * @param Dashboard $dashboard
     * @return mixed
     */
    public function update(Dashboard $dashboard);

    /**
     * @param Dashboard $project
     * @return mixed
     */
    public function delete(Dashboard $project);

    public function getDashboard(array $filters);
}