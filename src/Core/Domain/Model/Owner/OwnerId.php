<?php

namespace Orangear\Admin\Core\Domain\Model\Owner;

/**
 * Class CompanyId
 *
 * @package Orangear\Admin\Core\Domain\Model\Owner
 */
class OwnerId
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @param $id
     */
    public function construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }
}