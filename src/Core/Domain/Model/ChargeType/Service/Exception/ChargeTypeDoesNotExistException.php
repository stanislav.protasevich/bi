<?php

namespace Orangear\Admin\Core\Domain\Model\ChargeType\Service\Exception;

/**
 * Class ChargeTypeDoesNotExistException
 *
 * @package Orangear\Admin\Core\Domain\Model\Employee\Service\Exception
 */
class ChargeTypeDoesNotExistException extends \Exception {}