<?php

namespace Orangear\Admin\Core\Domain\Model\ChargeType\Service\Exception;

/**
 * Class ChargeTypeAlreadyExistsException
 *
 * @package Orangear\Admin\Core\Domain\Model\EmployeeGroup\Service\Exception
 */
class ChargeTypeAlreadyExistsException extends \Exception {}