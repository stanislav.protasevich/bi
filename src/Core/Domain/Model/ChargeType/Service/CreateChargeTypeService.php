<?php

namespace Orangear\Admin\Core\Domain\Model\ChargeType\Service;

use Orangear\Admin\Core\ApplicationBundle\Command\ChargeType\CreateChargeTypeCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\Domain\Model\ChargeType\ChargeType;
use Orangear\Admin\Core\Domain\Model\ChargeType\ChargeTypeId;
use Orangear\Admin\Core\Domain\Model\ChargeType\Service\Exception\ChargeTypeAlreadyExistsException;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\Admin\Core\InfrastructureBundle\Repository\ChargeTypeRepository;

/**
 * Class CreateChargeTypeService
 *
 * @package Orangear\Admin\Core\Domain\Model\ChargeType\Service
 */
class CreateChargeTypeService implements ServiceInterface
{
    /** @var ChargeTypeRepository */
    private $chargeTypeRepository;

    /**
     * CreateEmployeeGroupService constructor
     *
     * @param $chargeTypeRepository
     */
    public function __construct($chargeTypeRepository)
    {
        $this->chargeTypeRepository = $chargeTypeRepository;
    }

    /**
     * @param CommandInterface $createChargeTypeCommand
     */
    public function execute(CommandInterface $createChargeTypeCommand)
    {
        /** @var CreateChargeTypeCommand $createChargeTypeCommand */

        $id = ChargeTypeId::create();
        $name = $createChargeTypeCommand->name;
        $key = str_replace(' ', '_', strtolower($name));
        $description = $createChargeTypeCommand->description;

        $this->doNotFindChargeGroupOrFail($name);

        $chargeType = new ChargeType(
            $id,
            $name,
            $key,
            $description
        );

        $this->chargeTypeRepository->create($chargeType);
    }

    /**
     * @param $name
     *
     * @throws ChargeTypeAlreadyExistsException
     */
    protected function doNotFindChargeGroupOrFail($name)
    {
        $chargeType = $this->chargeTypeRepository->byName($name);

        if ($chargeType !== null) {
            throw new ChargeTypeAlreadyExistsException('Charge type already exists');
        }
    }
}