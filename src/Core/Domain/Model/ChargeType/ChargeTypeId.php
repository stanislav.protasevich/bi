<?php

namespace Orangear\Admin\Core\Domain\Model\ChargeType;

use Ramsey\Uuid\Uuid;

/**
 * Class ChargeTypeId
 *
 * @package Orangear\Admin\Core\Domain\Model\ChargeType
 */
class ChargeTypeId
{
    /**
     * @var string
     */
    private $id;

    /**
     * ChargeTypeId constructor
     *
     * @param null $id
     */
    private function __construct($id = null)
    {
        $this->id = $id ?: Uuid::uuid4()->toString();
    }

    /**
     * @param null $anId
     *
     * @return static
     */
    public static function create($anId = null)
    {
        return new static($anId);
    }

    /**
     * @return string
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }

    /**
     * @param ChargeTypeId $chargeTypeId
     *
     * @return bool
     */
    public function equals(ChargeTypeId $chargeTypeId)
    {
        return $this->id === $chargeTypeId->id();
    }
}