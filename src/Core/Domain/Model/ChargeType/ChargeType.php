<?php

namespace Orangear\Admin\Core\Domain\Model\ChargeType;

/**
 * Class ChargeType
 *
 * @package Orangear\Admin\Core\Domain\Model\ChargeType
 */
class ChargeType
{
    /** @var ChargeTypeId */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $key;

    /** @var string */
    private $description;

    /**
     * ChargeType constructor
     *
     * @param ChargeTypeId $id
     * @param string $name
     * @param string $key
     * @param string $description
     */
    public function __construct(ChargeTypeId $id, string $name, string $key, string $description = null)
    {
        $this->id = $id;
        $this->key = $key;
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param ChargeTypeId $id
     */
    public function setId(ChargeTypeId $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey(string $key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }
}