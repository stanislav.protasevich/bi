<?php

namespace Orangear\Admin\Core\Domain\Model\Synchronization\Service;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\Synchronization\ManagerSynchronizationCommand;
use Orangear\Admin\Core\Domain\Model\Employee\Employee;
use Orangear\Admin\Core\Domain\Model\Employee\EmployeeId;
use Orangear\Admin\Core\Domain\Model\Project\Service\Exception\ProjectDoesNotExistException;
use Orangear\Admin\Core\Domain\Model\ProjectEmployee\ProjectEmployee;
use Orangear\Admin\Core\Domain\Model\ProjectEmployee\ProjectEmployeeId;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\Admin\Core\InfrastructureBundle\Repository\EmployeeGroupRepository;
use Orangear\Admin\Core\InfrastructureBundle\Repository\EmployeeRepository;
use Orangear\Admin\Core\InfrastructureBundle\Repository\ProjectEmployeeRepository;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\ProjectRepository;
use Zend\Http\Client;
use Zend\Http\Headers;
use Zend\Http\Request;
use Zend\Stdlib\Parameters;

/**
 * Class ManagerSynchronizationService
 *
 * @package Orangear\Admin\Core\Domain\Model\Synchronization\Service
 */
class ManagerSynchronizationService implements ServiceInterface
{
    /**
     * @var Client
     */
    private $client;

    /** @var EmployeeRepository */
    private $employeeRepository;

    /** @var ProjectEmployeeRepository */
    private $projectEmployeeRepository;

    /** @var ProjectRepository */
    private $projectRepository;

    /** @var EmployeeGroupRepository */
    private $employeeGroupRepository;

    /**
     * ManagerSynchronizationService constructor
     *
     * @param EmployeeRepository $employeeRepository
     * @param ProjectEmployeeRepository $projectEmployeeRepository
     * @param ProjectRepository $projectRepository
     * @param EmployeeGroupRepository $employeeGroupRepository
     */
    public function __construct(
        EmployeeRepository $employeeRepository,
        ProjectEmployeeRepository $projectEmployeeRepository,
        ProjectRepository $projectRepository,
        EmployeeGroupRepository $employeeGroupRepository
    ) {
        $this->client = new Client();

        $this->employeeRepository = $employeeRepository;
        $this->projectEmployeeRepository = $projectEmployeeRepository;
        $this->projectRepository = $projectRepository;
        $this->employeeGroupRepository = $employeeGroupRepository;
    }

    /**
     * @param CommandInterface $command
     *
     * @throws \Exception
     *
     * @return string
     */
    public function execute(CommandInterface $command)
    {
        /** @var ManagerSynchronizationCommand $command */
        $project = $this->findProjectOrFail(ProjectId::fromString($command->projectId));

        $managers = $this->getManagersOrFail($project);

        foreach ($managers['data'] as $manager) {
            if (empty($manager['first_name']) && empty($manager['last_name'])) {
                continue;
            }

            $employee = $this->employeeRepository->byRemoteId($project, $manager['id']);

            $update = false;
            if ($employee) {
                $update = true;
                $employee->setFirstName($manager['first_name']);
                $employee->setLastName($manager['last_name']);
            } else {
                $employee = new Employee(
                    EmployeeId::create(),
                    $manager['id'],
                    null,
                    null,
                    $manager['first_name'],
                    $manager['last_name'],
                    'imported manager'
                );
            }

            $this->employeeRepository->create($employee);

            if (!$update) {
                $projectEmployee = new ProjectEmployee(
                    ProjectEmployeeId::create(),
                    $project,
                    $employee,
                    new \DateTime()
                );

                $this->projectEmployeeRepository->create($projectEmployee);
            }
        }
    }

    /**
     * @param Project $project
     *
     * @return Project $project
     *
     * @throws \Exception
     */
    protected function getManagersOrFail(Project $project)
    {
        $params = [];
        $params['activated'] = 1;
        $params['size'] = 100;
        $params['token'] = $project->token();

        $request = new Request();
        $request->setUri($project->apiUrl() . 'bi/managers');
        $request->setHeaders((new Headers())->addHeaders(['X-AUTH-TOKEN' => $project->token()]));
        $request->setMethod(Request::METHOD_GET);
        $request->setQuery(new Parameters($params));

        $this->client->setRequest($request);

        $response = $this->client->send();


        if (!$response->isOk()) {
            throw new \Exception('No valid responce from ' . $project->apiUrl());
        }

        return json_decode($response->getBody(), true);
    }

    /**
     * @param $id
     *
     * @return null|ProjectInterface
     *
     * @throws ProjectDoesNotExistException
     */
    protected function findProjectOrFail($id)
    {
        $project = $this->projectRepository->findById($id);

        if ($project === null) {
            throw new ProjectDoesNotExistException('Project does not exists');
        }

        return $project;
    }
}