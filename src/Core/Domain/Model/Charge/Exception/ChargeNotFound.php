<?php

namespace Orangear\Admin\Core\Domain\Model\Charge\Exception;

/**
 * Class ChargeNotFound
 *
 * @package Orangear\Admin\Core\Domain\Model\Charge
 */
final class ChargeNotFound extends \InvalidArgumentException
{
    /**
     * @param string $chargeId
     *
     * @return ChargeNotFound
     */
    public static function withChargeId($chargeId)
    {
        return new self(sprintf('Charge with id %s cannot be found.', $chargeId));
    }
}