<?php

namespace Orangear\Admin\Core\Domain\Model\Charge;

use Ramsey\Uuid\Uuid;

/**
 * Class ChargeId
 *
 * @package Orangear\Admin\Core\Domain\Model\Charge
 */
class ChargeId
{
    /**
     * @var string
     */
    private $id;

    /**
     * ChargeTypeId constructor
     *
     * @param null $id
     */
    private function __construct($id = null)
    {
        $this->id = $id ?: Uuid::uuid4()->toString();
    }

    /**
     * @param null $anId
     *
     * @return static
     */
    public static function create($anId = null)
    {
        return new static($anId);
    }

    /**
     * @param string $chargeId
     *
     * @return static
     */
    public static function fromString(string $chargeId)
    {
        return new static($chargeId);
    }

    /**
     * @return string
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }

    /**
     * @param ChargeId $chargeId
     *
     * @return bool
     */
    public function equals(ChargeId $chargeId)
    {
        return $this->id === $chargeId->id();
    }
}