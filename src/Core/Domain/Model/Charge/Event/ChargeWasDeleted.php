<?php

namespace Orangear\Admin\Core\Domain\Model\Charge\Event;

use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeIdentifier;

/**
 * Class ChargeWasDeleted
 *
 * @package Orangear\Admin\Core\Domain\Model\Charge\Event
 */
final class ChargeWasDeleted
{
    private $chargeId;

    /**
     * ChargeWasDeleted constructor
     *
     * @param ChargeIdentifier $chargeId
     */
    private function __construct(ChargeIdentifier $chargeId)
    {
        $this->chargeId = $chargeId;
    }

    /**
     * @param ChargeIdentifier $chargeId
     *
     * @return ChargeWasDeleted
     */
    public static function withData(ChargeIdentifier $chargeId)
    {
        return new self(
            $chargeId
        );
    }

    /**
     * @return ChargeIdentifier
     */
    public function chargeId()
    {
        return $this->chargeId;
    }
}