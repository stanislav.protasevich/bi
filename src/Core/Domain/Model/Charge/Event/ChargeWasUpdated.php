<?php

namespace Orangear\Admin\Core\Domain\Model\Charge\Event;

use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeIdentifier;

/**
 * Class ChargeWasUpdated
 *
 * @package Orangear\Admin\Core\Domain\Model\Charge\Command
 */
final class ChargeWasUpdated
{
    /** @var ChargeIdentifier */
    private $chargeId;

    /** @var \DateTime */
    private $startDate;

    /** @var \DateTime */
    private $endDate;

    /** @var float */
    private $amount;

    /**
     * EditChargeCommand constructor
     *
     * @param string $chargeId
     * @param string $startDate
     * @param string $endDate
     * @param string $amount
     */
    public function __construct($chargeId, $startDate, $endDate, $amount)
    {
        $this->chargeId = $chargeId;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->amount = $amount;
    }

    /**
     * @param string $chargeId
     * @param string $startDate
     * @param string $endDate
     * @param string $amount
     *
     * @return ChargeWasUpdated
     */
    public static function withData($chargeId, $startDate, $endDate, $amount)
    {
        return new self(
            $chargeId,
            $startDate,
            $endDate,
            $amount
        );
    }

    /**
     * @return ChargeIdentifier
     */
    public function chargeId()
    {
        return $this->chargeId;
    }

    /**
     * @return \DateTime
     */
    public function chargeStartDate()
    {
        return $this->startDate;
    }

    /**
     * @return \DateTime
     */
    public function chargeEndDate()
    {
        return $this->endDate;
    }

    /**
     * @return float
     */
    public function chargeAmount()
    {
        return $this->amount;
    }
}