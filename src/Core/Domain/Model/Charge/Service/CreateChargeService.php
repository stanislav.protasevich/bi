<?php

namespace Orangear\Admin\Core\Domain\Model\Charge\Service;

use Orangear\Admin\Core\ApplicationBundle\Command\Charge\CreateChargeCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\Domain\Model\Charge\ChargeFrequency;
use Orangear\Admin\Core\Domain\Model\ChargeType\Service\Exception\ChargeTypeDoesNotExistException;
use Orangear\Admin\Core\Domain\Model\Employee\Service\Exception\EmployeeDoesNotExistException;
use Orangear\Admin\Core\Domain\Model\Project\Service\Exception\ProjectDoesNotExistException;
use Orangear\Admin\Core\Domain\ServiceInterface;
use \DateTime;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeType;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeTypeIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\Employee\EmployeeInterface;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeCharge;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\ProjectRepository;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository\ChargeRepository;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository\ChargeTypeRepository;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository\EmployeeChargeRepository;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository\EmployeeRepository;

/**
 * Class CreateChargeService
 *
 * @package Orangear\Admin\Core\Domain\Model\ChargeType\Service
 */
class CreateChargeService implements ServiceInterface
{
    /** @var ChargeRepository */
    private $chargeRepository;

    /** @var ProjectRepository */
    private $projectRepository;

    /** @var ChargeTypeRepository */
    private $chargeTypeRepository;

    /** @var EmployeeRepository */
    private $employeeRepository;

    /** @var EmployeeChargeRepository */
    private $employeeChargeRepository;

    /**
     * CreateChargeService constructor
     *
     * @param ChargeRepository $chargeRepository
     * @param ProjectRepository $projectRepository
     * @param ChargeTypeRepository $chargeTypeRepository
     * @param EmployeeRepository $employeeRepository
     * @param EmployeeChargeRepository $employeeChargeRepository
     */
    public function __construct(
        ChargeRepository $chargeRepository,
        ProjectRepository $projectRepository,
        ChargeTypeRepository $chargeTypeRepository,
        EmployeeRepository $employeeRepository,
        EmployeeChargeRepository $employeeChargeRepository
    ) {
        $this->chargeRepository = $chargeRepository;
        $this->projectRepository = $projectRepository;
        $this->chargeTypeRepository = $chargeTypeRepository;
        $this->employeeRepository = $employeeRepository;
        $this->employeeChargeRepository = $employeeChargeRepository;
    }


    /**
     * @param CommandInterface $createChargeCommand
     */
    public function execute(CommandInterface $createChargeCommand)
    {
        /** @var CreateChargeCommand $createChargeCommand */

        $projectId = ProjectId::fromString($createChargeCommand->projectId);
        $chargeTypeId = ChargeTypeIdentifier::fromString($createChargeCommand->chargeTypeId);
        $employeeId = EmployeeIdentifier::fromString($createChargeCommand->employeeId);
        $startDate = new DateTime($createChargeCommand->startDate);
        $endDate = new DateTime($createChargeCommand->endDate);
        $frequency = ChargeFrequency::create($createChargeCommand->type);
        $amount = $createChargeCommand->amount;

        $project = $this->findProjectOrFail($projectId);
        $chargeType = $this->findChargeTypeOrFail($chargeTypeId);

        $employee = null;
        if ($employeeId->toString() !== null) {
            $employee = $this->findEmployeeOrFail($employeeId);
        }

        $charge = Charge::withData(
            $this->chargeRepository->nextIdentifier(),
            $project,
            $chargeType,
            $startDate,
            $endDate,
            $frequency,
            $amount
        );

        $this->chargeRepository->create($charge);

        if ($employee !== null) {
            $employeeCharge = EmployeeCharge::withData(
                $this->employeeChargeRepository->nextIdentifier(),
                $charge,
                $employee
            );

            $this->employeeChargeRepository->create($employeeCharge);
        }
    }

    /**
     * @param $id
     *
     * @return null|ProjectInterface
     *
     * @throws ProjectDoesNotExistException
     */
    protected function findProjectOrFail($id)
    {
        $project = $this->projectRepository->findById($id);

        if ($project === null) {
            throw new ProjectDoesNotExistException('Project does not exists');
        }

        return $project;
    }

    /**
     * @param $chargeTypeId
     *
     * @return null|ChargeType
     *
     * @throws ChargeTypeDoesNotExistException
     */
    protected function findChargeTypeOrFail($chargeTypeId)
    {
        $chargeType = $this->chargeTypeRepository->findById($chargeTypeId);

        if ($chargeTypeId === null) {
            throw new ChargeTypeDoesNotExistException('Charge type does not exists');
        }

        return $chargeType;
    }

    /**
     * @param $employeeId
     *
     * @return null|EmployeeInterface
     *
     * @throws EmployeeDoesNotExistException
     */
    protected function findEmployeeOrFail($employeeId)
    {
        $employee = $this->employeeRepository->findById($employeeId);

        if ($employee === null) {
            throw new EmployeeDoesNotExistException('Employee does not exists');
        }

        return $employee;
    }
}