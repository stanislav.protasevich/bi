<?php

namespace Orangear\Admin\Core\Domain\Model\Charge;

use Orangear\Admin\Core\Domain\Model\Charge\Event\ChargeWasDeleted;
use Orangear\Admin\Core\Domain\Model\Charge\Event\ChargeWasUpdated;
use Orangear\Admin\Core\Domain\Model\ChargeType\ChargeType;
use DateTime;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;

/**
 * Class Charge
 *
 * @package Orangear\Admin\Core\Domain\Model\Charge
 */
class Charge
{
    /** @var ChargeId */
    private $id;

    /** @var Project */
    private $project;

    /** @var ChargeType */
    private $chargeType;

    /** @var DateTime */
    private $startDate;

    /** @var DateTime */
    private $endDate;

    /** @var ChargeFrequency */
    private $frequency;

    /** @var float */
    private $amount;

    /**
     * Charge constructor
     *
     * @param ChargeId $id
     * @param ProjectInterface $project
     * @param ChargeType $chargeType
     * @param $startDate
     * @param $endDate
     * @param ChargeFrequency $frequency
     * @param float $amount
     */
    public function __construct(
        ChargeId $id,
        ProjectInterface $project,
        ChargeType $chargeType,
        DateTime $startDate,
        DateTime $endDate,
        ChargeFrequency $frequency,
        float $amount)
    {
        $this->id = $id;
        $this->project = $project;
        $this->chargeType = $chargeType;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->frequency = $frequency;
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param ChargeId $id
     */
    public function setId(ChargeId $id)
    {
        $this->id = $id;
    }

    /**
     * @return Project
     */
    public function getProject(): Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     */
    public function setProject(Project $project)
    {
        $this->project = $project;
    }

    /**
     * @return ChargeType
     */
    public function getChargeType(): ChargeType
    {
        return $this->chargeType;
    }

    /**
     * @param ChargeType $chargeType
     */
    public function setChargeType(ChargeType $chargeType)
    {
        $this->chargeType = $chargeType;
    }

    /**
     * @return DateTime
     */
    public function getStartDate(): DateTime
    {
        return $this->startDate;
    }

    /**
     * @param DateTime $startDate
     *
     * @return $this
     */
    public function setStartDate(DateTime $startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getEndDate(): DateTime
    {
        return $this->endDate;
    }

    /**
     * @param DateTime $endDate
     *
     * @return $this
     */
    public function setEndDate(DateTime $endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return ChargeFrequency
     */
    public function getFrequency(): ChargeFrequency
    {
        return $this->frequency;
    }

    /**
     * @param ChargeFrequency $frequency
     */
    public function setType(ChargeFrequency $frequency)
    {
        $this->frequency = $frequency;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     *
     * @return $this
     */
    public function setAmount(float $amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @param ChargeId $chargeId
     * @param DateTime $startDate
     * @param DateTime $endDate
     * @param float $amount
     *
     * @return ChargeWasUpdated
     */
    public static function updateChargeById(ChargeId $chargeId, \DateTime $startDate, \DateTime $endDate, float $amount)
    {
        return ChargeWasUpdated::withData($chargeId, $startDate, $endDate, $amount);
    }

    /**
     * @param ChargeId $chargeId
     *
     * @return ChargeWasDeleted
     */
    public static function deleteByChargeId(ChargeId $chargeId)
    {
        return ChargeWasDeleted::withData($chargeId);
    }
}