<?php

namespace Orangear\Admin\Core\Domain\Model\Charge\Command;

use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeIdentifier;

/**
 * Class DeleteCharge
 *
 * @package Orangear\Admin\Core\Domain\Model\Charge\Command
 */
final class DeleteChargeCommand
{
    /** @var string */
    private $chargeId;

    /**
     * DeleteCharge constructor
     *
     * @param $chargeId
     */
    private function __construct($chargeId)
    {
        $this->chargeId = $chargeId;
    }

    /**
     * @param $chargeId
     *
     * @return DeleteChargeCommand
     */
    public static function withData($chargeId)
    {
        return new self(
            $chargeId
        );
    }

    /**
     * @return ChargeIdentifier
     */
    public function chargeId()
    {
        return ChargeIdentifier::fromString($this->chargeId);
    }
}