<?php

namespace Orangear\Admin\Core\Domain\Model\Charge\Query;

use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeIdentifier;
use Orangear\BusinessIntelligence\Domain\Model\Charge\ChargeIdentifierInterface;

/**
 * Class GetSalaryChargeById
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Query\Charge
 */
final class GetSalaryChargeById
{
    /** @var string */
    protected $chargeId;

    /**
     * GetExpenseChargeById constructor
     *
     * @param string $projectId
     */
    private function __construct($projectId)
    {
        $this->chargeId = $projectId;
    }

    /**
     * @param $chargeId
     *
     * @return GetSalaryChargeById
     */
    public static function withData($chargeId)
    {
        return new self(
            $chargeId
        );
    }

    /**
     * @return ChargeIdentifierInterface
     */
    public function chargeId(): ChargeIdentifierInterface
    {
        return ChargeIdentifier::fromString($this->chargeId);
    }
}