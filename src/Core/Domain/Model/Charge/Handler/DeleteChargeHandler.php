<?php

namespace Orangear\Admin\Core\Domain\Model\Charge\Handler;

use Orangear\Admin\Core\Domain\Model\Charge\Command\DeleteChargeCommand;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Prooph\ServiceBus\EventBus;

/**
 * Class DeleteChargeHandler
 *
 * @package Orangear\Admin\Core\Domain\Model\Charge\Handler
 */
final class DeleteChargeHandler
{
    /** @var EventBus */
    private $eventBus;

    /**
     * DeleteChargeHandler constructor
     *
     * @param EventBus $eventBus
     */
    public function __construct(EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    /**
     * @param DeleteChargeCommand $command
     * 
     * @return \Orangear\Admin\Core\Domain\Model\Charge\Event\ChargeWasDeleted
     */
    public function __invoke(DeleteChargeCommand $command)
    {
        $event = Charge::deleteByChargeId($command->chargeId());

        $this->eventBus->dispatch($event);
    }
}