<?php

namespace Orangear\Admin\Core\Domain\Model\Charge\Handler;

use Orangear\Admin\Core\Domain\Model\Charge\Command\UpdateChargeCommand;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Prooph\ServiceBus\EventBus;

/**
 * Class UpdateChargeHandler
 *
 * @package Orangear\Admin\Core\Domain\Model\Charge\Handler
 */
final class UpdateChargeHandler
{
    /** @var EventBus */
    private $eventBus;

    /**
     * DeleteChargeHandler constructor
     *
     * @param EventBus $eventBus
     */
    public function __construct(EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    /**
     * @param UpdateChargeCommand $command
     * 
     * @return \Orangear\Admin\Core\Domain\Model\Charge\Event\ChargeWasDeleted
     */
    public function __invoke(UpdateChargeCommand $command)
    {
        $event = Charge::updateChargeById(
            $command->chargeId(),
            $command->chargeStartDate(),
            $command->chargeEndDate(),
            $command->chargeAmount()
        );

        $this->eventBus->dispatch($event);
    }
}