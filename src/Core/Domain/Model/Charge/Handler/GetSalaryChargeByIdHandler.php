<?php

namespace Orangear\Admin\Core\Domain\Model\Charge\Handler;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Orangear\Admin\Core\Domain\Model\Charge\ChargeFrequency;
use Orangear\Admin\Core\Domain\Model\Charge\Exception\ChargeNotFound;
use Orangear\Admin\Core\Domain\Model\Charge\Query\GetSalaryChargeById;
use Orangear\BusinessIntelligence\Domain\Model\Charge\Charge;
use Orangear\BusinessIntelligence\Domain\Model\ChargeType\ChargeType;
use Orangear\BusinessIntelligence\Domain\Model\Employee\Employee;
use Orangear\BusinessIntelligence\Domain\Model\EmployeeCharge\EmployeeCharge;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository\ChargeRepository;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository\ChargeTypeRepository;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository\EmployeeChargeRepository;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\Repository\EmployeeRepository;
use React\Promise\Deferred;

/**
 * Class GetSalaryChargeByIdHandler
 *
 * @package Orangear\Admin\Core\ApplicationBundle\Query\Charge
 */
final class GetSalaryChargeByIdHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * GetSalaryChargeByIdHandler constructor
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param GetSalaryChargeById $query
     * @param Deferred|null $deferred
     *
     * @return array
     */
    public function __invoke(GetSalaryChargeById $query, Deferred $deferred = null)
    {
        $data = null;

        $charge = $this->findExpenseOrFail($query->chargeId());

        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select([
            'c' . '.id AS charge_id',
            'c' . '.amount AS amount',
            'c' . '.frequency AS frequency',
            'c' . '.startDate AS start_date',
            'c' . '.endDate AS end_date',

            'ct' . '.id AS charge_type_id',
            'ct' . '.name AS charge_name',
            'ct' . '.description AS charge_description',

            'e' . '.id AS employee_id',
            'e' . '.firstName AS employee_first_name',
            'e' . '.lastName AS employee_last_name',
        ])
            ->from(Charge::class, 'c')
            ->join(
                ChargeType::class,
                'ct',
                Join::WITH,
                'ct' . '.id = ' . 'c' . '.chargeType')
            ->join(
                EmployeeCharge::class,
                'ec',
                Join::WITH,
                'ec' . '.charge = ' . 'c' . '.id')
            ->join(
                Employee::class,
                'e',
                Join::WITH,
                'ec' . '.employee = ' . 'e' . '.id')
            ->where('c' . '.id = :charge_id')
            ->setParameters([
                'charge_id' => $charge,
            ]);

        /** @var Charge $charge */
        foreach ($queryBuilder->getQuery()->getResult() as $charge) {
            $row['id']         = $charge['charge_id']->toString();
            $row['amount']     = $charge['amount'];
            $row['start_date'] = $charge['start_date']->format('Y-m-d');
            $row['end_date']   = $charge['end_date']->format('Y-m-d');

            $row['frequency']['id']   = $charge['frequency'];
            $row['frequency']['name'] = ChargeFrequency::$frequencies[$charge['frequency']];

            $row['charge_type']['id']          = $charge['charge_type_id']->toString();
            $row['charge_type']['name']        = $charge['charge_name'];
            $row['charge_type']['description'] = $charge['charge_description'];

            $row['employee']['id']          = $charge['employee_id']->toString();
            $row['employee']['first_name']  = $charge['employee_first_name'];
            $row['employee']['last_name']   = $charge['employee_last_name'];
            $row['employee']['notes']       = '';

            $data = $row;
        }

        if (null === $deferred) {
            return $data;
        }

        $deferred->resolve($data);
    }

    /**
     * @param $charge
     *
     * @return null|object
     *
     * @throws ChargeNotFound
     */
    protected function findExpenseOrFail($charge)
    {
        $chargeRepository = $this->entityManager->getRepository(Charge::class);
        $charge = $chargeRepository->findOneBy(['id' => $charge]);

        if (!$charge) {
            throw new ChargeNotFound('Charge does not exist');
        }

        return $charge;
    }
}