<?php

namespace Orangear\Admin\Core\Domain\Model\Statistics\Service;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\Statistics\StatisticsFindCommand;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectRepositoryInterface;
use Zend\Http\Client;
use Zend\Http\Headers;
use Zend\Http\Request;
use Zend\Stdlib\Parameters;

/**
 * Class StatisticsSearchService
 *
 * @package Orangear\Admin\Core\Domain\Model\Statistics\Service
 */
class StatisticsSearchService implements ServiceInterface
{
    /**
     * @var Client
     */
    private $client;

    /** @var ProjectRepositoryInterface  */
    private $projectRepository;

    public function __construct(ProjectRepositoryInterface $projectRepository)
    {
        $this->client = new Client();

        $this->projectRepository = $projectRepository;
    }

    /**
     * @param CommandInterface $command
     *
     * @throws \Exception
     *
     * @return string
     */
    public function execute(CommandInterface $command)
    {
        /** @var StatisticsFindCommand $command */


        $project = $this->projectRepository->findById(ProjectId::fromString($command->projectId));

        if (is_null($command->projectId)) {
            throw new \Exception('No such project');
        }

        $params = [];

        $params['start_date'] = $command->startDate;
        $params['end_date'] = $command->endDate;
        $params['timezone'] = $command->timezone;

        $params['breakdown_date'] = $command->breakdownDate;
        $params['breakdown_geography'] = $command->breakdownGeography;
        $params['breakdown_platforms'] = $command->breakdownPlatforms;
        $params['breakdown_offer_id'] = $command->breakdownOfferId;
        $params['breakdown_offer_name'] = $command->breakdownOfferName;
        $params['breakdown_advertiser_id'] = $command->breakdownAdvertiserId;
        $params['breakdown_real_pub_id'] = $command->breakdownRealPubId;
        $params['breakdown_pub_id'] = $command->breakdownPubId;
        $params['breakdown_sub_aff_id'] = $command->breakdownSubAffId;
        $params['breakdown_publisher_name'] = $command->breakdownPublisherName;
        $params['breakdown_affiliate_manager'] = $command->breakdownAffiliateManager;
        $params['breakdown_account_manager'] = $command->breakdownAccountManager;
        $params['breakdown_non_zero_revenuer'] = 1;
        $params['breakdown_secret_hash'] = $command->breakdownSecretHash;

        $params['data_clicks'] = $command->dataClicks;
        $params['data_back_traffic'] = $command->dataBackTraffic;
        $params['data_conversions'] = $command->dataConversions;
        $params['data_pubs_conversions'] = $command->dataPubsConversions;
        $params['data_cpa'] = $command->dataCpa;
        $params['data_pubs_cpa'] = $command->dataPubsCpa;
        $params['data_cpc'] = $command->dataCpc;
        $params['data_cr'] = $command->dataCr;
        $params['data_pubs_cr'] = $command->dataPubsCr;
        $params['data_rpa'] = $command->dataRpa;
        $params['data_rpc'] = $command->dataRpc;
        $params['data_costs'] = $command->dataCosts;
        $params['data_revenues'] = $command->dataRevenues;
        $params['data_extra_revenues'] = $command->dataExtraRevenues;
        $params['data_profit'] = $command->dataProfit;

        $params['fraud_duplicates'] = $command->fraudDuplicates;
        $params['fraud_duplicates_percentage'] = $command->fraudDuplicatesPercentage;
        $params['fraud_min_fraud'] = $command->fraudMinFraud;
        $params['fraud_min_fraud_percentage'] = $command->fraudMinFraudPercentage;
        $params['fraud_hdr_proxy_clicks'] = $command->fraudHdrProxyClicks;
        $params['fraud_hdr_proxy_clicks_percentage'] = $command->fraudHdrProxyClicksPercentage;
        $params['fraud_hdr_proxy_conversions'] = $command->fraudHdrProxyConversions;
        $params['fraud_hdr_proxy_conversions_percentage'] = $command->fraudHdrProxyConversionsPercentage;
        $params['filter_countries'] = $command->filterCountries;
        $params['filter_sub_aff_id'] = $command->filterSubAffId;
        $params['filter_platforms'] = $command->filterPlatforms;
        $params['filter_offers'] = $command->filterOffers;
        $params['filter_offer_types'] = $command->filterOfferTypes;
        $params['filter_advertisers'] = $command->filterAdvertisers;
        $params['filter_publishers'] = $command->filterPublishers;
        $params['filter_managers'] = $command->filterManagers;
        $params['filter_hash'] = $command->filterHash;

        $params['page'] = $command->page ?? 1;
        $params['size'] = $command->size ?? 100;

        $request = new Request();
        $request->setUri($project->apiUrl() . 'bi/statistics');
        $request->setHeaders((new Headers())->addHeaders(['X-Auth-Token' => $project->token()]));
        $request->setMethod(Request::METHOD_GET);
        $request->setQuery(new Parameters($params));

        $this->client->setRequest($request);

        $response = $this->client->send();

        if (!$response->isOk()) {
            throw new \Exception('No valid responce from ' . $project->apiUrl());
        }

        return $response->getBody();
    }
}