<?php

namespace Orangear\Admin\Core\Domain\Model\Project;

use Orangear\Admin\Core\Domain\Model\Company\Company;
use Orangear\Admin\Core\Domain\Model\Company\CompanyId;

/**
 * Class Project
 *
 * @package Orangear\Admin\Core\Domain\Model\Project
 */
class Project
{
    /** @var int Project id */
    private $id;

    /** @var Company Company identifier */
    private $company;

    /** @var string Project name */
    private $name;

    /** @var string Project logo */
    private $logo;

    /** @var string Project url */
    private $url;

    /** @var string Project token */
    private $token;

    /** @var string Project order */
    private $order;

    /**
     * Project constructor
     *
     * @param Company $company
     * @param string $name
     * @param string $logo;
     * @param string $token
     */
    public function __construct(Company $company, string $name, string $token, string $logo, string $url, int $order)
    {
        $this->company = $company;
        $this->name = $name;
        $this->logo = $logo;
        $this->url = $url;
        $this->token = $token;
        $this->order = $order;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        return $this->logo;
    }

    /**
     * @param string $logo
     */
    public function setLogo(string $logo)
    {
        $this->logo = $logo;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getOrder(): string
    {
        return $this->order;
    }

    /**
     * @param string $order
     */
    public function setOrder(string $order)
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * Create new project
     *
     * @param Company $companyId
     * @param string $name
     * @param string $token
     *
     * @return Project
     */
    public static function create(Company $companyId, string $name, string $token, string $logo, string $url)
    {
        return new self(
            $companyId,
            $name,
            $token,
            $logo,
            $url
        );
    }
}