<?php

namespace Orangear\Admin\Core\Domain\Model\Project\Service\Exception;

/**
 * Class ProjectDoesNotExistException
 *
 * @package Orangear\Admin\Core\Domain\Model\Project\Service\Exception
 */
class ProjectDoesNotExistException extends \Exception {}