<?php

namespace Orangear\Admin\Core\Domain\Model\Project\Service;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\ProjectCreateCommand;
use Orangear\Admin\Core\Domain\Model\Company\Company;
use Orangear\Admin\Core\Domain\Model\Company\CompanyId;
use Orangear\Admin\Core\Domain\Model\Company\CompanyRepositoryInterface;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\Admin\Core\Domain\Model\Project\Project;
use Orangear\Admin\Core\Domain\Model\Project\ProjectRepositoryInterface;


/**
 * Class ProjectCreateService
 *
 * @package Orangear\Admin\Core\Domain\Model\Project\Service
 */
class ProjectCreateService implements ServiceInterface
{
    /** @var ProjectRepositoryInterface */
    private $repository;

    /** @var CompanyRepositoryInterface */
    private $companyRepository;

    /**
     * ProjectCreateService constructor
     *
     * @param ProjectRepositoryInterface $projectRepository
     */
    public function __construct(ProjectRepositoryInterface $projectRepository, CompanyRepositoryInterface $projectRepository2)
    {
        $this->repository = $projectRepository;
        $this->companyRepository = $projectRepository2;
    }

    /**
     * Create new project
     *
     * @param CommandInterface $command
     */
    public function execute(CommandInterface $command)
    {
        /** @var ProjectCreateCommand $command */

        $company = $this->companyRepository->getById($command->platformId);

        $project = Project::create(
            $company,
            $command->name,
            md5(time()),
            $command->logo
        );

        $this->repository->create($project);

        // @todo Send Event
    }
}