<?php

namespace Orangear\Admin\Core\Domain\Model\Project\Service;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\ProjectListCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\ProjectPanelCommand;
use Orangear\Admin\Core\Domain\Model\Company\CompanyId;
use Orangear\Admin\Core\Domain\Model\Project\ProjectRepositoryInterface;
use Orangear\Admin\Core\Domain\ServiceInterface;

/**
 * Class ProjectPanelService
 *
 * @package Orangear\Admin\Core\Domain\Model\Project\Service
 */
class ProjectPanelService implements ServiceInterface
{
    /** @var ProjectRepositoryInterface */
    private $repository;

    /**
     * ProjectUpdateService constructor
     *
     * @param ProjectRepositoryInterface $projectRepository
     */
    public function __construct(ProjectRepositoryInterface $projectRepository)
    {
        $this->repository = $projectRepository;
    }

    /**
     * Get projects
     *
     * @param CommandInterface $command
     *
     * @return \Symfony\Component\Ldap\Adapter\AdapterInterface
     */
    public function execute(CommandInterface $command)
    {
        /** @var ProjectPanelCommand $command */

        $params = [];

        return $this->repository->getSummary($params);
    }
}