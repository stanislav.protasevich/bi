<?php

namespace Orangear\Admin\Core\Domain\Model\Project\Service;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\ProjectListCommand;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectIdentifierInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectRepositoryInterface;

/**
 * Class ProjectListService
 *
 * @package Orangear\Admin\Core\Domain\Model\Project\Service
 */
class ProjectListService implements ServiceInterface
{
    /** @var ProjectRepositoryInterface */
    private $repository;

    /**
     * ProjectUpdateService constructor
     *
     * @param ProjectRepositoryInterface $projectRepository
     */
    public function __construct(ProjectRepositoryInterface $projectRepository)
    {
        $this->repository = $projectRepository;
    }

    /**
     * Get projects
     *
     * @param CommandInterface $command
     *
     * @return array|ProjectInterface
     */
    public function execute(CommandInterface $command)
    {
        /** @var ProjectListCommand $command */

        if (!is_null($command->projectId)) {
            /** @var ProjectIdentifierInterface $projectId */
            $projectId = ProjectId::fromString($command->projectId);

            return [$this->repository->findById($projectId)];
        }

        return $this->repository->findAll();
    }
}