<?php

namespace Orangear\Admin\Core\Domain\Model\Project\Service;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\ProjectDashboardCommand;
use Orangear\Admin\Core\Domain\Model\Dashboard\DashboardRepositoryInterface;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectRepositoryInterface;

/**
 * Class ProjectReadDashboardService
 *
 * @package Orangear\Admin\Core\Domain\Model\Project\Service
 */
class ProjectReadDashboardService implements ServiceInterface
{
    /** @var DashboardRepositoryInterface */
    private $repository;

    /** @var ProjectRepositoryInterface */
    private $projectRepository;

    /**
     * ProjectUpdateService constructor
     *
     * @param DashboardRepositoryInterface $repository
     * @param ProjectRepositoryInterface $projectRepository
     */
    public function __construct(DashboardRepositoryInterface $repository, ProjectRepositoryInterface $projectRepository)
    {
        $this->repository = $repository;
        $this->projectRepository = $projectRepository;
    }

    /**
     * Get projects
     *
     * @param CommandInterface $command
     *
     * @return \Symfony\Component\Ldap\Adapter\AdapterInterface
     */
    public function execute(CommandInterface $command)
    {
        /** @var ProjectDashboardCommand $command */

        $params = [];

        if (!is_null($command->projectId)) {
            $project = $this->projectRepository->findById(ProjectId::fromString($command->projectId));

            $params['project'] = $project;
        }

        return $this->repository->getDashboard($params);
    }
}