<?php

namespace Orangear\Admin\Core\Domain\Model\Project;

use Ramsey\Uuid\Uuid;

/**
 * Class ProjectId
 *
 * @package Orangear\Admin\Core\Domain\Model\Project
 */
class ProjectId
{
    /**
     * @var string
     */
    private $id;

    /**
     * ChargeTypeId constructor
     *
     * @param null $id
     */
    private function __construct($id = null)
    {
        $this->id = $id ?: Uuid::uuid4()->toString();
    }

    /**
     * @param null $anId
     *
     * @return static
     */
    public static function create($anId = null)
    {
        return new static($anId);
    }

    /**
     * @return string
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }

    /**
     * @param ProjectId $projectId
     *
     * @return bool
     */
    public function equals(ProjectId $projectId)
    {
        return $this->id === $projectId->id();
    }
}