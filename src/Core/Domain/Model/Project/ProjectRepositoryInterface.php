<?php

namespace Orangear\Admin\Core\Domain\Model\Project;

use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Symfony\Component\Ldap\Adapter\AdapterInterface;

/**
 * Interface ProjectRepositoryInterface
 * @package Orangear\Admin\Core\Domain\Model\Project
 */
interface ProjectRepositoryInterface
{
    /**
     * Get project by identifier
     *
     * @param int $id
     *
     * @return null|Project
     */
    public function getById(int $id): ? Project;

    /**
     * Get by token
     *
     * @param string $token
     *
     * @return null|Project
     */
    public function getByToken(string $token): ? Project;

    /**
     * Get projects summery
     *
     * @param array $filters
     * @return mixed
     */
    public function getSummary(array $filters);

    /**
     * Get list of projects
     *
     * @param array $filters
     * @return AdapterInterface
     */
    public function find(array $filters);

    /**
     * Create new project
     *
     * @param Project $project
     *
     * @return mixed
     */
    public function create(Project $project);

    /**
     * Update existing project
     *
     * @param Project $project
     *
     * @return mixed
     */
    public function update(Project $project);

    /**
     * Delete existing project
     *
     * @param Project $project
     *
     * @return mixed
     */
    public function delete(Project $project);
}