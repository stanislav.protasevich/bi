<?php

namespace Orangear\Admin\Core\Domain\Model\Employee;

use Orangear\Admin\Core\Domain\Model\EmployeeGroup\EmployeeGroup;
use Orangear\BusinessIntelligence\Domain\Model\SuperEmployee\SuperEmployeeInterface;

/**
 * Class Employee
 *
 * @package Orangear\Admin\Core\Domain\Model\Employee
 */
class Employee implements EmployeeInterface
{
    /** @var EmployeeId */
    private $id;

    /** @var string */
    private $remoteId;

    /** @var SuperEmployeeInterface */
    private $superEmployee;

    /** @var EmployeeGroup */
    private $employeeGroup;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var string */
    private $notes;

    /**
     * Employee constructor
     *
     * @param EmployeeId $id
     * @param string $remoteId
     * @param SuperEmployeeInterface $superEmployee
     * @param EmployeeGroup $employeeGroup
     * @param string $firstName
     * @param string $lastName
     * @param string $notes
     */
    public function __construct(EmployeeId $id, $remoteId = null, SuperEmployeeInterface $superEmployee = null, EmployeeGroup $employeeGroup = null, $firstName, $lastName, $notes = null)
    {
        $this->id = $id;
        $this->remoteId = $remoteId;
        $this->employeeGroup = $employeeGroup;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->notes = $notes;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param EmployeeId $id
     */
    public function setId(EmployeeId $id)
    {
        $this->id = $id;
    }

    /**
     * @return EmployeeGroup
     */
    public function getEmployeeGroupId(): EmployeeGroup
    {
        return $this->employeeGroup;
    }

    /**
     * @param EmployeeGroup $employeeGroup
     */
    public function setEmployeeGroupId(EmployeeGroup $employeeGroup)
    {
        $this->employeeGroup = $employeeGroup;
    }

    /**
     * {@inheritdoc}
     */
    public function superEmployee() :? SuperEmployeeInterface
    {
        return $this->superEmployee;
    }

    /**
     * {@inheritdoc}
     */
    public function setSuperEmployee(SuperEmployeeInterface $superEmployee = null)
    {
        $this->superEmployee = $superEmployee;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getNotes(): ?string
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     */
    public function setNotes(string $notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return string
     */
    public function getRemoteId(): ?string
    {
        return $this->remoteId;
    }

    /**
     * @param string $remoteId
     */
    public function setRemoteId(string $remoteId)
    {
        $this->remoteId = $remoteId;
    }

    /**
     * @return EmployeeGroup
     */
    public function getEmployeeGroup(): EmployeeGroup
    {
        return $this->employeeGroup;
    }

    /**
     * @param EmployeeGroup $employeeGroup
     */
    public function setEmployeeGroup(EmployeeGroup $employeeGroup)
    {
        $this->employeeGroup = $employeeGroup;
    }
}