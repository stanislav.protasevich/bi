<?php

declare(strict_types = 1);

namespace Orangear\Admin\Core\Domain\Model\Employee;

/**
 * Interface EmployeeIdInterface
 *
 * @package Orangear\Admin\Core\Domain\Model\Employee
 */
interface EmployeeIdInterface {}
