<?php

namespace Orangear\Admin\Core\Domain\Model\Employee\Service;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\ApplicationBundle\Command\Employee\CreateEmployeeCommand;
use Orangear\Admin\Core\ApplicationBundle\Command\Project\Exception\ProjectDoesNotExistException;
use Orangear\Admin\Core\Domain\Model\Employee\Employee;
use Orangear\Admin\Core\Domain\Model\Employee\EmployeeId;
use Orangear\Admin\Core\Domain\Model\Employee\Service\Exception\EmployeeGroupDoesNotExistException;
use Orangear\Admin\Core\Domain\Model\EmployeeGroup\EmployeeGroup;
use Orangear\Admin\Core\Domain\Model\ProjectEmployee\ProjectEmployee;
use Orangear\Admin\Core\Domain\Model\ProjectEmployee\ProjectEmployeeId;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\Admin\Core\InfrastructureBundle\Repository\EmployeeGroupRepository;
use Orangear\Admin\Core\InfrastructureBundle\Repository\EmployeeRepository;
use Orangear\Admin\Core\InfrastructureBundle\Repository\ProjectEmployeeRepository;
use Orangear\BusinessIntelligence\Domain\Model\Project\Project;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectId;
use Orangear\BusinessIntelligence\Domain\Model\Project\ProjectInterface;
use Orangear\BusinessIntelligence\Infrastructure\Persistence\ProjectRepository;

/**
 * Class CreateEmployeeService
 *
 * @package Orangear\Admin\Core\Domain\Model\Employee\Service
 */
class CreateEmployeeService implements ServiceInterface
{
    /** @var EmployeeRepository */
    private $employeeRepository;

    /** @var EmployeeGroupRepository */
    private $employeeGroupRepository;

    /** @var ProjectRepository */
    private $projectRepository;

    /** @var ProjectEmployeeRepository */
    private $projectEmployeeRepository;

    /**
     * CreateEmployeeService constructor
     *
     * @param EmployeeRepository $employeeRepository
     * @param EmployeeGroupRepository $employeeGroupRepository
     * @param ProjectRepository $projectRepository
     * @param ProjectEmployeeRepository $projectEmployeeRepository
     */
    public function __construct(
        EmployeeRepository $employeeRepository,
        EmployeeGroupRepository $employeeGroupRepository,
        ProjectRepository $projectRepository,
        ProjectEmployeeRepository $projectEmployeeRepository
    ) {
        $this->employeeRepository = $employeeRepository;
        $this->employeeGroupRepository = $employeeGroupRepository;
        $this->projectRepository = $projectRepository;
        $this->projectEmployeeRepository = $projectEmployeeRepository;
    }

    /**
     * @param CommandInterface $command
     */
    public function execute(CommandInterface $command)
    {
        /** @var CreateEmployeeCommand $command */

        $employeeId = EmployeeId::create();
        $employeeGroupId = $command->employeeGroupId;
        $projectIds = $command->projectId;
        $remoteId = $command->remoteId;
        $firstName = $command->firstName;
        $lastName = $command->lastName;
        $notes = $command->notes;
        $employeeGroup = $employeeGroupId;


        if ($employeeGroupId !== null) {
            $employeeGroup = $this->findEmployeeGroupOrFail($employeeGroupId);
        }

        $projects = [];
        foreach ($projectIds as $projectId) {
            $projects[] = $this->findProjectOrFail(ProjectId::fromString($projectId));
        }

        $employee = new Employee(
            $employeeId,
            $remoteId,
            $employeeGroup,
            $firstName,
            $lastName,
            $notes
        );

        $this->employeeRepository->create($employee);

        // Create project employee relation
        foreach ($projects as $project) {
            $projectEmployeeId = ProjectEmployeeId::create();
            $createdAt = new \DateTime();

            $projectEmployee = new ProjectEmployee(
                $projectEmployeeId,
                $project,
                $employee,
                $createdAt
            );

            $this->projectEmployeeRepository->create($projectEmployee);
        }
    }

    /**
     * @param $employeeGroupId
     *
     * @return null|EmployeeGroup
     *
     * @throws EmployeeGroupDoesNotExistException
     */
    protected function findEmployeeGroupOrFail($employeeGroupId)
    {
        $employeeGroup = $this->employeeGroupRepository->byId($employeeGroupId);

        if ($employeeGroup === null) {
            throw new EmployeeGroupDoesNotExistException('Employee group does not exist');
        }

        return $employeeGroup;
    }

    /**
     * @param $projectId
     *
     * @return null|ProjectInterface
     *
     * @throws ProjectDoesNotExistException
     */
    protected function findProjectOrFail($projectId)
    {
        $project = $this->projectRepository->findById($projectId);

        if ($project === null) {
            throw new ProjectDoesNotExistException('Project does not exist');
        }

        return $project;
    }
}