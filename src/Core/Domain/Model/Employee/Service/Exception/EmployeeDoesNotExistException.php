<?php

namespace Orangear\Admin\Core\Domain\Model\Employee\Service\Exception;

/**
 * Class EmployeeGroupDoesNotExistException
 *
 * @package Orangear\Admin\Core\Domain\Model\Employee\Service\Exception
 */
class EmployeeDoesNotExistException extends \Exception {}