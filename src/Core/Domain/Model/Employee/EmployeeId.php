<?php

namespace Orangear\Admin\Core\Domain\Model\Employee;

use Ramsey\Uuid\Uuid;

/**
 * Class EmployeeId
 *
 * @package Orangear\Admin\Core\Domain\Model\Employee
 */
class EmployeeId implements EmployeeIdInterface
{
    /**
     * @var string
     */
    private $id;

    /**
     * ChargeTypeId constructor
     *
     * @param null $id
     */
    private function __construct($id = null)
    {
        $this->id = $id ?: Uuid::uuid4()->toString();
    }

    /**
     * @param null $anId
     *
     * @return static
     */
    public static function create($anId = null)
    {
        return new static($anId);
    }

    /**
     * @return string
     */
    public function id()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->id;
    }

    /**
     * @param EmployeeId $employeeId
     *
     * @return bool
     */
    public function equals(EmployeeId $employeeId)
    {
        return $this->id === $employeeId->id();
    }
}