<?php

namespace Orangear\Admin\Core\Domain;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;

/**
 * Interface ServiceInterface
 *
 * @package Orangear\Admin\Core\Domain
 */
interface ServiceInterface
{
    /**
     * @param CommandInterface $command
     */
    public function execute(CommandInterface $command);
}