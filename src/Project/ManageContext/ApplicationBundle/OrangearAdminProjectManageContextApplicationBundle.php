<?php

namespace Orangear\Admin\Project\ManageContext\ApplicationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class OrangearAdminProjectManageContextApplicationBundle extends Bundle {}
