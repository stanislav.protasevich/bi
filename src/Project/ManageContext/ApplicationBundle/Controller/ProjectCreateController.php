<?php

namespace Orangear\Admin\Project\ManageContext\ApplicationBundle\Controller;

use Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\Exception\ProjectCreateCommandException;
use Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\ProjectCreateCommand;
use Psr\Http\Message\ServerRequestInterface;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ProjectCreateController
 *
 * @package Orangear\Admin\Project\ManageContext\ApplicationBundle\Controller
 */
class ProjectCreateController extends Controller
{
    /** @var MessageBus  */
    private $commandBus;

    /**
     * ProjectCreateController constructor
     *
     * @param MessageBus $commandBus
     */
    public function __construct(MessageBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @ApiDoc(
     *   section = "Project",
     *   description = "Create new project",
     *   filters = {
     *     {"name" = "owner_id", "dataType" = "int", "required" = true, "description" = "Project owner identifier"},
     *     {"name" = "name", "dataType" = "string", "required" = true, "description" = "Project name"},
     *   },
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect"
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $command = ProjectCreateCommand::handleRequest($request);

            $this->commandBus->handle($command);

            return new JsonResponse(['success'], 200);
        }
        catch (ProjectCreateCommandException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}
