<?php

namespace Orangear\Admin\Project\ManageContext\ApplicationBundle\Controller;

use Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\Exception\ProjectCreateCommandException;
use Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\ProjectUpdateCommand;
use Orangear\Admin\Project\ManageContext\Domain\Model\Project\Exception\ProjectDoesNotExistException;
use Psr\Http\Message\ServerRequestInterface;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ProjectUpdateController
 *
 * @package Orangear\Admin\Project\ManageContext\ApplicationBundle\Controller
 */
class ProjectUpdateController extends Controller
{
    /** @var MessageBus  */
    private $commandBus;

    /**
     * ProjectUpdateController constructor
     *
     * @param MessageBus $commandBus
     */
    public function __construct(MessageBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @ApiDoc(
     *   section="Project",
     *   description="Update existing project",
     *   requirements = {
     *     {
     *       "name"        = "id",
     *       "dataType"    = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Project identifier"
     *     }
     *   },
     *   filters={
     *     {"name" = "owner_id", "dataType" = "int", "required" = true, "description" = "Project owner identifier"},
     *     {"name" = "name", "dataType" = "string", "required" = true, "description" = "Project name"},
     *   },
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect",
     *     404 = {
     *       "Returned when the project is not found"
     *     }
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $command = ProjectUpdateCommand::handleRequest($request);

            $this->commandBus->handle($command);

            return new JsonResponse(['success'], 200);
        }
        catch (ProjectCreateCommandException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (ProjectDoesNotExistException $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 404);
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }
}
