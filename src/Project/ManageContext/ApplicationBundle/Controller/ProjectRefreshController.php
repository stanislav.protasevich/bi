<?php

namespace Orangear\Admin\Project\ManageContext\ApplicationBundle\Controller;

use Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\Exception\ProjectCreateCommandException;
use Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\ProjectRefreshCommand;
use Orangear\Admin\Project\ManageContext\Domain\Model\Project\Exception\ProjectDoesNotExistException;
use Psr\Http\Message\ServerRequestInterface;
use SimpleBus\Message\Bus\MessageBus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ProjectRefreshController
 *
 * @package Orangear\Admin\Project\ManageContext\ApplicationBundle\Controller
 */
class ProjectRefreshController extends Controller
{
    /** @var MessageBus  */
    private $commandBus;

    /**
     * ProjectRefreshController constructor
     *
     * @param MessageBus $commandBus
     */
    public function __construct(MessageBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @ApiDoc(
     *   section="Project",
     *   description="Refresh project's token",
     *   requirements = {
     *     {
     *       "name"        = "id",
     *       "dataType"    = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Project identifier"
     *     }
     *   },
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization token"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect",
     *     404 = {
     *       "Returned when the project is not found"
     *     }
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     *
     * @return JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $command = ProjectRefreshCommand::handleRequest($request);

            $this->commandBus->handle($command);

            return new JsonResponse(['success'], 200);
        }
        catch (ProjectCreateCommandException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (ProjectDoesNotExistException $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 404);
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }

//    /**
//     * @ApiDoc(
//     *  section="Project",
//     *  description="Get list of projects",
//     *  filters={
//     *     {"name"="page", "dataType"="integer", "required"=false, "description"="Page"},
//     *     {"name"="limit", "dataType"="integer", "required"=false, "description"="Limit"},
//     *  }
//     * )
//     *
//     * @param Request $request
//     * @return \Symfony\Component\HttpFoundation\JsonResponse
//     */
//    public function listAction(Request $request)
//    {
//        return $this->json([], 200, []);
//    }
//
//    /**
//     * @ApiDoc(
//     *  section="Project",
//     *  description="Update project",
//     *  filters={
//     *     {"name"="project_id", "dataType"="integer", "required"=true, "description"="Project id"},
//     *  }
//     * )
//     *
//     * @param Request $request
//     * @return \Symfony\Component\HttpFoundation\JsonResponse
//     */
//    public function editAction(Request $request)
//    {
//        return $this->json([], 200, []);
//    }
//
//    /**
//     * @ApiDoc(
//     *  section="Project",
//     *  description="Refresh project's token",
//     *  filters={
//     *     {"name"="project_id", "dataType"="integer", "required"=true, "description"="Project id"},
//     *     {"name"="project_id", "dataType"="integer", "required"=true, "description"="Project id"},
//     *  }
//     * )
//     *
//     * @param Request $request
//     * @return \Symfony\Component\HttpFoundation\JsonResponse
//     */
//    public function refreshTokenAction(Request $request)
//    {
//        return $this->json([], 200, []);
//    }
//
//    /**
//     * @ApiDoc(
//     *  section="Project",
//     *  description="Delete project",
//     *  filters={
//     *     {"name"="project_id", "dataType"="integer", "required"=true, "description"="Project id"},
//     *  }
//     * )
//     *
//     * @param Request $request
//     * @return \Symfony\Component\HttpFoundation\JsonResponse
//     */
//    public function deleteAction(Request $request)
//    {
//        return $this->json([], 200, []);
//    }
}
