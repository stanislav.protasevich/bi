<?php

namespace Orangear\Admin\Project\ManageContext\ApplicationBundle\Command;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Filter\FilterInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ProjectCreateCommand
 *
 * @package Orangear\Admin\Project\ManageContext\ApplicationBundle\Command
 */
class ProjectCreateCommand extends Command
{
    /**
     * Project owner identifier
     *
     * @var string
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(
     *  type="digit",
     *  message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public $ownerId = null;

    /**
     * Project name
     *
     * @var int
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public $name = null;

    /**
     * ProjectCreateCommand constructor
     *
     * @param null $ownerId
     * @param string $name
     */
    public function __construct($ownerId = null, $name = null)
    {
        $this->ownerId = $ownerId;
        $this->name    = $name;
    }

    /**
     * {@inheritdoc}
     */
    public static function handleRequest(ServerRequestInterface $request, FilterInterface $filter = null)
    {
        $data = array_merge($request->getParsedBody());

        return self::hydrate($data, $filter);
    }
}