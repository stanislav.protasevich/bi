<?php

namespace Orangear\Admin\Project\ManageContext\ApplicationBundle\Command;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpFoundation\Request;
use Zend\Filter\FilterInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ProjectDeleteCommand
 *
 * @package Orangear\Admin\Project\ManageContext\ApplicationBundle\Command
 */
class ProjectDeleteCommand extends Command
{
    /**
     * Project identifier
     *
     * @var string
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(
     *  type="digit",
     *  message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public $id = null;

    /**
     * ProjectUpdateCommand constructor
     *
     * @param int $id
     */
    public function __construct($id = null)
    {
        $this->id   = $id;
    }

    /**
     * {@inheritdoc}
     */
    public static function handleRequest(ServerRequestInterface $request, FilterInterface $filter = null)
    {
        $data = array_merge(
            ['id' => $request->getAttribute('id')]
        );

        return self::hydrate($data, $filter);
    }
}