<?php

namespace Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\Validator;

use Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\Exception\ProjectCreateCommandException;
use SimpleBus\Message\Bus\Middleware\MessageBusMiddleware;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ProjectCreateCommandValidator
 *
 * @package Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\Middleware
 */
class ProjectCreateCommandValidator implements MessageBusMiddleware
{
    /** @var ValidatorInterface */
    protected $validator;

    /**
     * Dependency Injection constructor.
     *
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * {@inheritdoc}
     */
    public function handle($message, callable $next)
    {
        $violations = $this->validator->validate($message);

        if (count($violations) > 0) {
            throw new ProjectCreateCommandException(
                'Project violation detected',
                $violations
            );
        }

        $next($message);
    }
}