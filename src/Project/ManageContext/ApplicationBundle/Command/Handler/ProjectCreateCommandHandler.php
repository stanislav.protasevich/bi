<?php

namespace Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\Handler;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\Exception\ProjectCreateCommandException;

/**
 * Class ProjectCreateCommandHandler
 *
 * @package Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\Handler
 */
class ProjectCreateCommandHandler
{
    /** @var ServiceInterface */
    private $service;

    /**
     * ProjectCreateCommandHandler constructor
     *
     * @param ServiceInterface $service
     */
    public function __construct(ServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * Create project
     *
     * @param  CommandInterface $command
     *
     * @throws ProjectCreateCommandException
     */
    public function handle(CommandInterface $command)
    {
        $this->service->execute($command);
    }
}