<?php

namespace Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\Exception;

use Orangear\Admin\Core\ApplicationBundle\Command\Exception\CommandValidationException;

/**
 * Class ProjectCreateCommandException
 * @package Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\Exception
 */
class ProjectCreateCommandException extends CommandValidationException {}