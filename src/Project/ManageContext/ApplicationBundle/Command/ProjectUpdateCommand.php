<?php

namespace Orangear\Admin\Project\ManageContext\ApplicationBundle\Command;

use Orangear\Admin\Core\ApplicationBundle\Command\Command;
use Orangear\Admin\Project\ManageContext\Domain\Model\Owner\OwnerId;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpFoundation\Request;
use Zend\Filter\FilterInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ProjectUpdateCommand
 *
 * @package Orangear\Admin\Project\ManageContext\ApplicationBundle\Command
 */
class ProjectUpdateCommand extends Command
{
    /**
     * Project identifier
     *
     * @var string
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(
     *  type="digit",
     *  message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public $id = null;

    /**
     * Project owner identifier
     *
     * @var int
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(
     *  type="digit",
     *  message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public $ownerId = null;

    /**
     * Project name
     *
     * @var string
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public $name = null;

    /**
     * ProjectUpdateCommand constructor
     *
     * @param int $id
     * @param OwnerId $ownerId
     * @param string $name
     */
    public function __construct($id = null, OwnerId $ownerId = null, $name = null)
    {
        $this->id   = $id;
        $this->name = $name;
    }

    /**
     * {@inheritdoc}
     */
    public static function handleRequest(ServerRequestInterface $request, FilterInterface $filter = null)
    {
        $data = array_merge(
            ['id' => $request->getAttribute('id')],
            $request->getParsedBody()
        );

        return self::hydrate($data, $filter);
    }
}