<?php

namespace Orangear\Admin\Project\ManageContext\InfrastructureBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class OrangearAdminProjectManageContextInfrastructureBundle
 *
 * @package Orangear\Admin\Project\ManageContext\InfrastructureBundle
 */
class OrangearAdminProjectManageContextInfrastructureBundle extends Bundle {}