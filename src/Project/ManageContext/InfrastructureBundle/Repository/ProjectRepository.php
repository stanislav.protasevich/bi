<?php

namespace Orangear\Admin\Project\ManageContext\InfrastructureBundle\Repository;

use Doctrine\ORM\EntityManager;
use Orangear\Admin\Project\ManageContext\Domain\Model\Project\Project;
use Orangear\Admin\Project\ManageContext\Domain\Model\Project\ProjectRepositoryInterface;

/**
 * Class ProjectRepository
 *
 * @package Orangear\Admin\Project\ManageContext\InfrastructureBundle\Repository
 */
class ProjectRepository implements ProjectRepositoryInterface
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * ProjectRepository constructor
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->entityManager = $em;
    }

    /**
     * { @inheritdoc }
     */
    public function getById(int $id): ? Project
    {
        /** @var Project $project */

        $project = $this->entityManager
            ->getRepository(Project::class)
            ->find($id);

        return $project;
    }

    /**
     * { @inheritdoc }
     */
    public function create(Project $project)
    {
        $this->entityManager->persist($project);
        $this->entityManager->flush();
    }

    /**
     * { @inheritdoc }
     */
    public function update(Project $project)
    {
        $this->entityManager->persist($project);
        $this->entityManager->flush();
    }

    /**
     * { @inheritdoc }
     */
    public function delete(Project $project)
    {
        $this->entityManager->remove($project);
        $this->entityManager->flush();
    }
}