<?php

namespace Orangear\Admin\Project\ManageContext\Domain\Model\Owner;

/**
 * Class OwnerId
 *
 * @package Orangear\Admin\Project\ManageContext\Domain\Model\Owner
 */
class OwnerId
{
    /** @var int Owner identifier */
    private $id;

    /**
     * Owner constructor
     *
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->setId($id);
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId() : int
    {
        return $this->id;
    }
}