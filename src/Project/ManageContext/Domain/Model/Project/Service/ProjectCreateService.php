<?php

namespace Orangear\Admin\Project\ManageContext\Domain\Model\Project\Service;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\ProjectCreateCommand;
use Orangear\Admin\Project\ManageContext\Domain\Model\Owner\OwnerId;
use Orangear\Admin\Project\ManageContext\Domain\Model\Project\Project;
use Orangear\Admin\Project\ManageContext\Domain\Model\Project\ProjectRepositoryInterface;

/**
 * Class ProjectCreateService
 *
 * @package Orangear\Admin\Project\ManageContext\Domain\Model\Project\Service
 */
class ProjectCreateService implements ServiceInterface
{
    /** @var ProjectRepositoryInterface */
    private $repository;

    /**
     * ProjectCreateService constructor
     *
     * @param ProjectRepositoryInterface $projectRepository
     */
    public function __construct(ProjectRepositoryInterface $projectRepository)
    {
        $this->repository = $projectRepository;
    }

    /**
     * Create new project
     *
     * @param CommandInterface $command
     */
    public function execute(CommandInterface $command)
    {
        /** @var ProjectCreateCommand $command */
        $ownerId = new OwnerId($command->ownerId);

        $project = Project::create(
            $ownerId,
            $command->name,
            md5(time())
        );

        $this->repository->create($project);

        // @todo Send Event
    }
}