<?php

namespace Orangear\Admin\Project\ManageContext\Domain\Model\Project\Service;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\ProjectUpdateCommand;
use Orangear\Admin\Project\ManageContext\Domain\Model\Owner\OwnerId;
use Orangear\Admin\Project\ManageContext\Domain\Model\Project\Exception\ProjectDoesNotExistException;
use Orangear\Admin\Project\ManageContext\Domain\Model\Project\Project;
use Orangear\Admin\Project\ManageContext\Domain\Model\Project\ProjectRepositoryInterface;

/**
 * Class ProjectUpdateService
 *
 * @package Orangear\Admin\Project\ManageContext\Domain\Model\Project\Service
 */
class ProjectUpdateService implements ServiceInterface
{
    /** @var ProjectRepositoryInterface */
    private $repository;

    /**
     * ProjectUpdateService constructor
     *
     * @param ProjectRepositoryInterface $projectRepository
     */
    public function __construct(ProjectRepositoryInterface $projectRepository)
    {
        $this->repository = $projectRepository;
    }

    /**
     * Create new project
     *
     * @param CommandInterface $command
     *
     * @throws ProjectDoesNotExistException
     */
    public function execute(CommandInterface $command)
    {
        /** @var ProjectUpdateCommand $command */
        $ownerId = new OwnerId($command->ownerId);

        $project = $this->repository->getById($command->id);

        if (!$project) {
            throw new ProjectDoesNotExistException("Project with identifier \"{$command->id}\" does not exist.");
        }

        $project->setName($command->name);
        $project->setOwnerId($ownerId);

        $this->repository->update($project);

        // @todo Send Event
    }
}