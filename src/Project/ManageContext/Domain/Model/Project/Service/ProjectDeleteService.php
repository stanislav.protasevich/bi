<?php

namespace Orangear\Admin\Project\ManageContext\Domain\Model\Project\Service;

use Orangear\Admin\Core\ApplicationBundle\Command\CommandInterface;
use Orangear\Admin\Core\Domain\ServiceInterface;
use Orangear\Admin\Project\ManageContext\ApplicationBundle\Command\ProjectUpdateCommand;
use Orangear\Admin\Project\ManageContext\Domain\Model\Project\Exception\ProjectDoesNotExistException;
use Orangear\Admin\Project\ManageContext\Domain\Model\Project\Project;
use Orangear\Admin\Project\ManageContext\Domain\Model\Project\ProjectRepositoryInterface;

/**
 * Class ProjectDeleteService
 *
 * @package Orangear\Admin\Project\ManageContext\Domain\Model\Project\Service
 */
class ProjectDeleteService implements ServiceInterface
{
    /** @var ProjectRepositoryInterface */
    private $repository;

    /**
     * ProjectDeleteService constructor
     *
     * @param ProjectRepositoryInterface $projectRepository
     */
    public function __construct(ProjectRepositoryInterface $projectRepository)
    {
        $this->repository = $projectRepository;
    }

    /**
     * Create new project
     *
     * @param CommandInterface $command
     *
     * @throws ProjectDoesNotExistException
     */
    public function execute(CommandInterface $command)
    {
        /** @var ProjectUpdateCommand $command */

        $project = $this->repository->getById($command->id);

        if (!$project) {
            throw new ProjectDoesNotExistException("Project with identifier \"{$command->id}\" does not exist.");
        }

        $this->repository->delete($project);

        // @todo Send Event
    }
}