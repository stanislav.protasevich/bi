<?php

namespace Orangear\Admin\Project\ManageContext\Domain\Model\Project;

/**
 * Interface ProjectRepositoryInterface
 *
 * @package Orangear\Admin\Project\ManageContext\Domain\Model\Project
 */
interface ProjectRepositoryInterface
{
    /**
     * Get project by identifier
     *
     * @param int $id
     *
     * @return null|Project
     */
    public function getById(int $id): ? Project;

    /**
     * Create new project
     *
     * @param Project $project
     *
     * @return mixed
     */
    public function create(Project $project);

    /**
     * Update existing project
     *
     * @param Project $project
     *
     * @return mixed
     */
    public function update(Project $project);

    /**
     * Delete existing project
     *
     * @param Project $project
     *
     * @return mixed
     */
    public function delete(Project $project);
}