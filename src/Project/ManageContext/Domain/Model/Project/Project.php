<?php

namespace Orangear\Admin\Project\ManageContext\Domain\Model\Project;

use Orangear\Admin\Project\ManageContext\Domain\Model\Owner\OwnerId;

/**
 * Class Project
 *
 * @package Orangear\Admin\Project\ManageContext\Domain\Model\Project
 */
class Project
{
    /** @var int Project id */
    private $id;

    /** @var string Project name */
    private $name;

    /** @var string Project token */
    private $token;

    /** @var OwnerId */
    private $ownerId;

    /**
     * Project constructor
     *
     * @param OwnerId $ownerId
     * @param string $name
     * @param string $token
     */
    public function __construct(OwnerId $ownerId, string $name, string $token)
    {
        $this->ownerId = $ownerId;

        $this->setName($name);

        $this->token = $token;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token)
    {
        $this->token = $token;
    }

    /**
     * @return OwnerId
     */
    public function getOwnerId(): OwnerId
    {
        return $this->ownerId;
    }

    /**
     * @param OwnerId $ownerId
     */
    public function setOwnerId(OwnerId $ownerId)
    {
        $this->ownerId = $ownerId;
    }

    /**
     * Create new project
     *
     * @param OwnerId $ownerId
     * @param string $name
     * @param string $token
     *
     * @return Project
     */
    public static function create(OwnerId $ownerId, string $name, string $token)
    {
        return new self(
            $ownerId,
            $name,
            $token
        );
    }
}