<?php

namespace Orangear\Admin\Project\ManageContext\Domain\Model\Project\Exception;

/**
 * Class ProjectDoesNotExistException
 *
 * @package Orangear\Admin\Project\ManageContext\Domain\Model\Project\Exception
 */
class ProjectDoesNotExistException extends \Exception {}