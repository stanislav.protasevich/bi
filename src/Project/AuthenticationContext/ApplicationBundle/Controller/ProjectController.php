<?php

namespace Orangear\Admin\Project\AuthenticationContext\ApplicationBundle\Controller;

use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\SuperAdmin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ProjectController
 *
 * @package Orangear\Admin\Project\AuthenticationContext\ApplicationBundle\Controller
 */
class ProjectController extends Controller
{
    /**
     * Ping project
     *
     * @return JsonResponse
     */
    public function pingAction()
    {
        return new JsonResponse(['ack' => time()]);
    }
}