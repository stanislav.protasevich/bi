<?php

namespace Orangear\Admin\Project\AuthenticationContext\ApplicationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class OrangearAdminProjectAuthenticationContextApplicationBundle extends Bundle
{
}
