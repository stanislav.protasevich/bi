<?php

namespace Orangear\Admin\Project\AuthenticationContext\InfrastructureBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class OrangearAdminProjectAuthenticationContextInfrastructureBundle
 *
 * @package Orangear\Admin\Project\AuthenticationContext\InfrastructureBundle
 */
class OrangearAdminProjectAuthenticationContextInfrastructureBundle extends Bundle {}