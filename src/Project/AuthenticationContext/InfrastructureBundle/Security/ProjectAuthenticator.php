<?php

namespace Orangear\Admin\Project\AuthenticationContext\InfrastructureBundle\Security;

use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;

/**
 * Class ProjectAuthenticator
 *
 * @package Orangear\Admin\Project\AuthenticationContext\InfrastructureBundle\Security
 */
class ProjectAuthenticator implements SimplePreAuthenticatorInterface, AuthenticationFailureHandlerInterface
{
    /**
     * Get authentication token
     *
     * @param TokenInterface $token
     * @param UserProviderInterface $userProvider
     * @param $providerKey
     * @return PreAuthenticatedToken
     * @throws \Exception
     */
    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        $token = $token->getCredentials();

        $project = $userProvider->loadUserByUsername($token);

        if (!$project) {
            throw new AuthenticationException(
                sprintf('An authentication exception occurred. Wrong project token "%s".', $token)
            );
        }

        return new PreAuthenticatedToken(
            $project,
            $token,
            $providerKey,
            $project->getRoles()
        );
    }

    /**
     * Create project API token
     *
     * @param Request $request
     * @param $providerKey
     * @return PreAuthenticatedToken
     */
    public function createToken(Request $request, $providerKey)
    {
        $authorizationHeader = $request->headers->get('Authorization');

        if (!$authorizationHeader) {
            throw new AuthenticationException('An authentication exception occurred. No Bearer token provided.');
        }

        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
        );

        $token = $extractor->extract($request);

        if(!$token) {
            throw new AuthenticationException('An authentication exception occurred. Empty token provided.');
        }

        return new PreAuthenticatedToken(
            'anonymous',
            $token,
            $providerKey
        );
    }

    /**
     * @param TokenInterface $token
     * @param $providerKey
     * @return bool
     */
    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return JsonResponse
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $responseData = [
            'status' => 'error',
            'error' => [
                'message' => $exception->getMessage(),
                'code' => $exception->getCode()
            ],
        ];

        return new JsonResponse(
            $responseData,
            401
        );
    }
}