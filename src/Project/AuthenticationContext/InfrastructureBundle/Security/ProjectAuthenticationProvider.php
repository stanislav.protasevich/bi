<?php

namespace Orangear\Admin\Project\AuthenticationContext\InfrastructureBundle\Security;

use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Orangear\Admin\Project\AuthenticationContext\Domain\Model\Project\Project;
use Orangear\Admin\Project\AuthenticationContext\Domain\Model\Project\ProjectRepositoryInterface;

/**
 * Class ProjectAuthenticationProvider
 *
 * @package Orangear\Admin\Project\AuthenticationContext\InfrastructureBundle\Security
 */
class ProjectAuthenticationProvider implements UserProviderInterface
{
    /**
     * @var ProjectRepositoryInterface
     */
    private $repository;

    /**
     * SuperAdminAuthProvider constructor.
     * @param ProjectRepositoryInterface $projectRepository
     */
    public function __construct(ProjectRepositoryInterface $projectRepository)
    {
        $this->repository= $projectRepository;
    }

    /**
     * @param string $token
     * @return null|Project
     */
    public function loadUserByUsername($token)
    {
        $project = $this->repository->getByToken($token);

        if ($project instanceof UserInterface) {
            return $project;
        }

        return null;
    }

    /**
     * @param UserInterface $user
     * @return UnsupportedUserException
     */
    public function refreshUser(UserInterface $user)
    {
        return new UnsupportedUserException();
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
    {
        return Project::class === $class;
    }
}