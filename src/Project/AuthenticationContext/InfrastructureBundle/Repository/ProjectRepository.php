<?php

namespace Orangear\Admin\Project\AuthenticationContext\InfrastructureBundle\Repository;

use Doctrine\ORM\EntityManager;
use Orangear\Admin\Project\AuthenticationContext\Domain\Model\Project\Project;
use Orangear\Admin\Project\AuthenticationContext\Domain\Model\Project\ProjectRepositoryInterface;

/**
 * Class ProjectRepository
 *
 * @package Orangear\Admin\Project\AuthenticationContext\InfrastructureBundle\Repository
 */
class ProjectRepository implements ProjectRepositoryInterface
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    public function __construct(EntityManager $em)
    {
        $this->entityManager = $em;
    }

    /**
     * Get project by token
     *
     * @param string $token
     * @return mixed
     */
    public function getByToken(string $token)
    {
        return $this->entityManager->createQueryBuilder()
            ->select('p')
            ->from(Project::class, 'p')
            ->where('p.token = :token')
            ->setParameter('token', $token)
            ->getQuery()
            ->getOneOrNullResult();
    }
}