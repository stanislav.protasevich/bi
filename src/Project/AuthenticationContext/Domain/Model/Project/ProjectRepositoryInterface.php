<?php

namespace Orangear\Admin\Project\AuthenticationContext\Domain\Model\Project;

interface ProjectRepositoryInterface
{
    /**
     * Get project by token
     *
     * @param string $token
     * @return Project|null
     */
    public function getByToken(string $token);
}