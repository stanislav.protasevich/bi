<?php

namespace Orangear\Admin\Project\AuthenticationContext\Domain\Model\Project;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class Project
 *
 * @package Orangear\Admin\Project\AuthenticationContext\Domain\Model\Project
 */
class Project implements UserInterface, \Serializable
{
    /** @var int Project id */
    private $id;

    /** @var string Project name */
    private $name;

    /** @var string Project token */
    private $token;

    /**
     * Project constructor
     *
     * @param int $id
     * @param string $name
     * @param string $token
     */
    public function __construct(int $id, string $name, string $token)
    {
        $this->id = $id;

        $this->name = $name;

        $this->token = $token;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     *
     */
    public function getUsername() : void
    {
        $this->name;
    }

    /**
     *
     */
    public function eraseCredentials() : void
    {
        $this->token = null;
    }

    /**
     * @return string
     */
    public function getPassword() : string
    {
        return $this->token;
    }

    /**
     * @return array
     */
    public function getRoles() : array
    {
        return ['ROLE_PROJECT'];
    }

    /**
     * @return null
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return string
     */
    public function serialize() : string
    {
        return serialize(array(
            $this->id,
            $this->name,
            $this->token,
        ));
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized) : void
    {
        list (
            $this->id,
            $this->name,
            $this->token,
        ) = unserialize($serialized);
    }
}