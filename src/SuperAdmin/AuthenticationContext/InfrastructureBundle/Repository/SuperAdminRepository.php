<?php

namespace Orangear\Admin\SuperAdmin\AuthenticationContext\InfrastructureBundle\Repository;

use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\Email;
use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\SuperAdmin;
use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\SuperAdminRepositoryInterface;
use Doctrine\ORM\EntityManager;

/**
 * Class SuperAdminRepository
 * @package Orangear\Admin\SuperAdmin\AuthenticationContext\InfrastructureBundle\Repository
 */
class SuperAdminRepository implements SuperAdminRepositoryInterface
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    public function __construct(EntityManager $em)
    {
        $this->entityManager = $em;
    }

    /**
     * @param string $email
     *
     * @return mixed
     */
    public function getByEmail(string $email)
    {
        return $this->entityManager->createQueryBuilder()
            ->select('u')
            ->from(SuperAdmin::class, 'u')
            ->where('u.email.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult();
    }
}