<?php

namespace Orangear\Admin\SuperAdmin\AuthenticationContext\InfrastructureBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class OrangearAdminSuperAdminAuthenticationContextInfrastructureBundle
 *
 * @package Orangear\Admin\SuperAdmin\AuthenticationContext\InfrastructureBundle
 */
class OrangearAdminSuperAdminAuthenticationContextInfrastructureBundle extends Bundle {}