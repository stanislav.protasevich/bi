<?php

namespace Orangear\Admin\SuperAdmin\AuthenticationContext\InfrastructureBundle\Auth;

use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\Email;
use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\SuperAdmin;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\SuperAdminRepositoryInterface;

/**
 * Class SuperAdminAuthProvider
 *
 * @package Orangear\Admin\SuperAdmin\AuthenticationContext\InfrastructureBundle\Auth
 */
class SuperAdminAuthProvider implements UserProviderInterface
{
    /**
     * @var SuperAdminRepositoryInterface
     */
    private $repository;

    /**
     * SuperAdminAuthProvider constructor.
     * @param SuperAdminRepositoryInterface $advertiserRepository
     */
    public function __construct(SuperAdminRepositoryInterface $advertiserRepository)
    {
        $this->repository= $advertiserRepository;
    }

    /**
     * @param string $username
     * @return null|SuperAdmin
     */
    public function loadUserByUsername($username)
    {
        $user = $this->repository->getByEmail($username);

        $user->qwe = 123;

        if ($user instanceof UserInterface) {
            return $user;
        }

        return null;
    }

    /**
     * @param UserInterface $user
     * @return UserInterface
     */
    public function refreshUser(UserInterface $user)
    {
        /** @var $user SuperAdmin */
        $user = $this->loadUserByUsername($user->getEmail()->getEmail());

        return $user;
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
    {
        return SuperAdmin::class === $class;
    }
}