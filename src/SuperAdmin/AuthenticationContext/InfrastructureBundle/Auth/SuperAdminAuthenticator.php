<?php

namespace Orangear\Admin\SuperAdmin\AuthenticationContext\InfrastructureBundle\Auth;

use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\SuperAdmin;
use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\SuperAdminRepositoryInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

use Doctrine\ORM\EntityManager;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class SuperAdminAuthenticator
 *
 * @package Orangear\Admin\SuperAdmin\AuthenticationContext\InfrastructureBundle\Auth
 */
class SuperAdminAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var SuperAdminRepositoryInterface
     */
    private $repository;

    /**
     * @var JWTEncoderInterface
     */
    private $jwtEncoder;

    /**
     * @var
     */
    private $user;

    /**
     * @param SuperAdminRepositoryInterface $repository
     * @param JWTEncoderInterface $jwtEncoder
     */
    public function __construct(SuperAdminRepositoryInterface $repository, JWTEncoderInterface $jwtEncoder)
    {
        $this->repository = $repository;
        $this->jwtEncoder = $jwtEncoder;
    }

    /**
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     * @return null|SuperAdmin
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $data = $this->jwtEncoder->decode($credentials);

        if(!$data){
            return null;
        }

        $username = $data['username'];

        $user = $this->repository->getByEmail($username);

        $this->user = $user;

        if(!$user){
            return null;
        }

        return $user;
    }

    /**
     * @param Request $request
     * @param AuthenticationException $authException
     * @return JsonResponse
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new JsonResponse(['error' => 'Auth header required'], 401);
    }

    /**
     * @param Request $request
     * @return null|string
     */
    public function getCredentials(Request $request)
    {
        if(!$request->headers->has('Authorization')) {
            return null;
        }

        $extractor = new AuthorizationHeaderTokenExtractor(
            'Bearer',
            'Authorization'
        );

        $token = $extractor->extract($request);

        if(!$token) {
            return null;
        }

        return $token;
    }

    /**
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return JsonResponse
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $responseData = [
            'status' => 'error',
            'error' => [
                'message' => $exception->getMessage(),
                'code' => $exception->getCode()
            ],
        ];
        return new JsonResponse($responseData, 401);
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     * @return array
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $request->attributes->set('user', $this->user);

        return null;
    }

    /**
     * @return bool
     */
    public function supportsRememberMe()
    {
        return false;
    }
}