<?php

namespace Orangear\Admin\SuperAdmin\AuthenticationContext\ApplicationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class OrangearAdminSuperAdminAuthenticationContextApplicationBundle extends Bundle
{
}
