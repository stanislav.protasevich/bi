<?php

namespace Orangear\Admin\SuperAdmin\AuthenticationContext\ApplicationBundle\Controller;

use Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin\SuperAdmin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AuthenticationController extends Controller
{
    public function loginAction()
    {
        return $this->render('OrangearAdminSuperAdminAuthenticationContextApplicationBundle:Authentication:login.html.twig');
    }
}
