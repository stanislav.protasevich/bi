<?php

namespace Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin;

/**
 * Class Email
 * @package Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model
 */
final class Email
{
    /**
     * @var string
     */
    public $email;

    /**
     * Email constructor
     *
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}