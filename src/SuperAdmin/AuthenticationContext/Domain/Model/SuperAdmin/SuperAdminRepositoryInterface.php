<?php

namespace Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin;

/**
 * Interface SuperAdminRepositoryInterface
 * @package Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model
 */
interface SuperAdminRepositoryInterface
{
    /**
     * @param string $email
     * @return mixed
     */
    public function getByEmail(string $email);
}