<?php

namespace Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model\SuperAdmin;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class SuperAdmin
 * @package Orangear\Admin\SuperAdmin\AuthenticationContext\Domain\Model
 */
class SuperAdmin implements UserInterface, \Serializable
{
    /**
     * @var int Unique identification
     */
    private $id;

    /**
     * @var string Encrypted password
     */
    private $password;

    /**
     * @var string Super admin email address
     */
    public $email;

    public function __construct(Email $email, string $password, array $roles)
    {
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        $this->password = null;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return ['ROLE_SUPER_ADMIN'];
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return null;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->getEmail()->getEmail();
    }

    /**
     * @return Email
     */
    public function getEmail() : Email
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
        ));
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            ) = unserialize($serialized);
    }
}