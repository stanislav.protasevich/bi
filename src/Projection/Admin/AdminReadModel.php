<?php

declare(strict_types=1);

namespace Orangear\Admin\Projection\Admin;

use Doctrine\DBAL\Connection;
use Orangear\Admin\Projection\Table;
use Prooph\EventStore\Projection\AbstractReadModel;

/**
 * Class AdminReadModel
 *
 * @package Orangear\Admin\Projection\Admin
 */
final class AdminReadModel extends AbstractReadModel
{
    /** @var Connection */
    private $connection;

    /**
     * AdminReadModel constructor
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return void
     */
    public function init(): void
    {
        $tableName = Table::ADMIN;
        $sql = <<<EOT
CREATE TABLE `$tableName` (
  `id` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
EOT;
        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }

    /**
     * @return bool
     */
    public function isInitialized(): bool
    {
        $tableName = Table::ADMIN;

        $sql = "SHOW TABLES LIKE '$tableName';";

        $statement = $this->connection->prepare($sql);
        $statement->execute();

        $result = $statement->fetch();

        if (false === $result) {
            return false;
        }

        return true;
    }

    /**
     * @return void
     */
    public function reset(): void
    {
        $tableName = Table::ADMIN;

        $sql = "TRUNCATE TABLE '$tableName';";

        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }

    /**
     * @return void
     */
    public function delete(): void
    {
        $tableName = Table::ADMIN;

        $sql = "DROP TABLE $tableName;";

        $statement = $this->connection->prepare($sql);
        $statement->execute();
    }
}