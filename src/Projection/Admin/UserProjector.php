<?php

declare(strict_types=1);

namespace Orangear\Admin\Projection\Admin;

use Doctrine\DBAL\Connection;
use Orangear\Admin\Model\Admin\Event\AdminWasCreated;
use Orangear\Admin\Projection\Table;

/**
 * Class UserProjector
 *
 * @package Orangear\Admin\Projection\Admin
 */
class UserProjector
{
    /** @var Connection */
    private $connection;

    /**
     * UserProjector constructor
     *
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function onAdminWasCreated(AdminWasCreated $event)
    {
        $this->connection->insert(
            Table::ADMIN,
            [
                'name' => $event->name()->toString()
            ]
        );
    }
}