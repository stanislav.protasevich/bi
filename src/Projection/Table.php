<?php

declare(strict_types=1);

namespace Orangear\Admin\Projection;

/**
 * Class Table
 *
 * @package Orangear\Admin\Projection
 */
class Table
{
    const ADMIN = 'read_admin';
}