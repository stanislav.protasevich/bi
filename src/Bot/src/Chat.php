<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Bot;

/**
 * Class Chat
 * @package Orangear\BusinessIntelligence\Bot
 */
final class Chat
{
    /** @var int */
    private $identifier;

    /**
     * Chat constructor
     *
     * @param int $identifier
     */
    private function __construct(int $identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * @param int $identifier
     * @return self
     */
    public static function withId(int $identifier): self
    {
        return new self($identifier);
    }

    /**
     * @return int
     */
    public function identifier(): int
    {
        return $this->identifier;
    }
}
