<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Bot;

/**
 * Class Message
 * @package Orangear\BusinessIntelligence\Bot
 */
final class Message
{
    /** @var string */
    private $heading;

    /** @var string */
    private $text;

    /**
     * Message constructor
     * @param string $heading
     * @param string $text
     */
    private function __construct(string $heading, string $text)
    {
        $this->heading = $heading;
        $this->text = $text;
    }

    /**
     * @param string $heading
     * @param string $text
     * @return self
     */
    public static function withData(string $heading, string $text): self
    {
        return new self($heading, $text);
    }

    /**
     * @return string
     */
    public function heading(): string
    {
        return $this->heading;
    }

    /**
     * @return string
     */
    public function text(): string
    {
        return $this->text;
    }
}
