<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Bot;

/**
 * Class Token
 * @package Orangear\BusinessIntelligence\Bot
 */
final class Token
{
    /** @var string */
    private $token;

    /**
     * Token constructor
     *
     * @param string $token
     */
    private function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * @param string $token
     * @return Token
     */
    public static function withToken(string $token): self
    {
        return new self($token);
    }

    /**
     * @return string
     */
    public function token(): string
    {
        return $this->token;
    }
}
