<?php

declare(strict_types = 1);

namespace Orangear\BusinessIntelligence\Bot;

/**
 * Class Bot
 * @package Orangear\BusinessIntelligence\Bot
 */
final class Bot
{
    /** @var Token */
    private $token;

    /**
     * Bot constructor
     * @param Token $token
     */
    private function __construct(Token $token)
    {
        $this->token = $token;
    }

    /**
     * @param Token $token
     * @return self
     */
    public static function withToken(Token $token): self
    {
        return new self($token);
    }

    /**
     * @param Chat $chat
     * @param Message $message
     */
    public function sendMessage(Chat $chat, Message $message)
    {
        $parameters = [
            'chat_id' => $chat->identifier(),
            'text' => $message->text()
        ];

        $url = implode(['https://api.telegram.org/bot', $this->token->token(), '/', 'sendMessage', '?'])
            . http_build_query($parameters);

        file_get_contents($url);
    }
}
