<?php

declare(strict_types=1);

namespace Orangear\Admin\Model;

/**
 * Interface ValueObject
 *
 * @package Orangear\Admin\Model
 */
interface ValueObject
{
    /**
     * @param ValueObject $object
     * @return bool
     */
    public function sameValueAs(ValueObject $object): bool;
}