<?php

declare(strict_types=1);

namespace Orangear\Admin\Model\Admin\Event;

use Orangear\Admin\Model\Admin\AdminId;
use Orangear\Admin\Model\Admin\AdminName;
use Prooph\EventSourcing\AggregateChanged;

/**
 * Class AdminWasCreated
 *
 * @package Orangear\Admin\Model\Admin\Event
 */
class AdminWasCreated extends AggregateChanged
{
    /** @var AdminId */
    private $id;

    /** @var AdminName */
    private $name;

    /**
     * @param AdminId $id
     * @param AdminName $name
     *
     * @return AdminWasCreated
     */
    public static function withData(AdminId $id, AdminName $name): self
    {
        /** @var self $event */
        $event = self::occur($id->toString(), [
            'name' => $name->toString(),
        ]);

        $event->id = $id;
        $event->name = $name;

        return $event;
    }

    /**
     * @return AdminId
     */
    public function id(): AdminId
    {
        if (null === $this->id) {
            $this->id = AdminId::fromString($this->aggregateId());
        }
        return $this->id;
    }

    /**
     * @return AdminName
     */
    public function name(): AdminName
    {
        if (null === $this->name) {
            $this->name = AdminName::fromString($this->payload['name']);
        }
        return $this->name;
    }
}