<?php

declare(strict_types=1);

namespace Orangear\Admin\Model\Admin;

use Orangear\Admin\Model\ValueObject;

/**
 * Class AdminId
 *
 * @package Orangear\Admin\Model\Admin
 */
final class AdminId implements ValueObject
{
    /** @var int */
    private $id;

    /**
     * AdminId constructor
     *
     * @param string $id
     */
    private function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @param string $id
     *
     * @return AdminId
     */
    public static function fromString(string $id): self
    {
        return new self($id);
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return (string) $this->id;
    }

    /**
     * @param ValueObject $object
     *
     * @return bool
     */
    public function sameValueAs(ValueObject $object): bool
    {
        /** @var $object self */
        return get_class($this) === get_class($object) && $this->id === $object->id;
    }
}