<?php

declare(strict_types=1);

namespace Orangear\Admin\Model\Admin;

use Orangear\Admin\Model\ValueObject;

/**
 * Class AdminName
 *
 * @package Orangear\Admin\Model\Admin
 */
final class AdminName implements ValueObject
{
    /** @var string */
    private $name;

    /**
     * AdminName constructor
     *
     * @param int $name
     */
    private function __construct(int $name)
    {
        $this->name = $name;
    }

    /**
     * @param int $name
     *
     * @return AdminName
     */
    public static function fromString(int $name): self
    {
        return new self($name);
    }

    /**
     * @return string
     */
    public function toString(): string
    {
        return (string) $this->name;
    }

    /**
     * @param ValueObject $object
     *
     * @return bool
     */
    public function sameValueAs(ValueObject $object): bool
    {
        /** @var $object self */
        return get_class($this) === get_class($object) && $this->name === $object->name;
    }
}