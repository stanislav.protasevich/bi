<?php

declare(strict_types=1);

namespace Orangear\Admin\Model\Admin;

use Orangear\Admin\Model\Admin\Event\AdminWasCreated;
use Prooph\EventSourcing\AggregateChanged;
use Prooph\EventSourcing\AggregateRoot;

/**
 * Class Admin
 *
 * @package Orangear\Admin\Model\Admin
 */
final class Admin extends AggregateRoot
{
    /** @var AdminId */
    private $id;

    /** @var AdminName */
    private $name;

    /**
     * @return AdminId
     */
    public function id(): AdminId
    {
        return $this->id;
    }

    /**
     * @return AdminName
     */
    public function name(): AdminName
    {
        return $this->name;
    }

    /**
     * @param AdminName $name
     *
     * @return Admin
     */
    public static function createWithData(AdminName $name) : self
    {
        $self = new self();

        $self->recordThat(AdminWasCreated::withData(rand(), $name));

        return $self;
    }

    /**
     * @return string
     */
    protected function aggregateId(): string
    {
        return $this->id->toString();
    }

    /**
     * @param AggregateChanged $event
     */
    protected function apply(AggregateChanged $event): void
    {
        $handler = $this->determineEventHandlerMethodFor($event);

        if (! method_exists($this, $handler)) {
            throw new \RuntimeException(sprintf(
                'Missing event handler method %s for aggregate root %s',
                $handler,
                get_class($this)
            ));
        }
        $this->{$handler}($event);
    }

    /**
     * @param AggregateChanged $event
     * @return string
     */
    protected function determineEventHandlerMethodFor(AggregateChanged $event): string
    {
        return 'when' . implode(array_slice(explode('\\', get_class($event)), -1));
    }
}