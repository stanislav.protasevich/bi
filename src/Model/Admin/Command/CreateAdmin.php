<?php

declare(strict_types=1);

namespace Orangear\Admin\Model\Admin\Command;

use Assert\Assertion;
use Orangear\Admin\Model\Admin\AdminId;
use Orangear\Admin\Model\Admin\AdminName;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;

/**
 * Class CreateAdmin
 *
 * @package Orangear\Admin\Model\Admin\Command
 */
class CreateAdmin extends Command implements PayloadConstructable
{
    use PayloadTrait;

    /**
     * CreateAdmin constructor
     *
     * @param array $payload
     */
    public function __construct(array $payload)
    {
        $this->setPayload($payload);
    }

    /**
     * @return AdminId
     */
    public function id(): AdminId
    {
        return AdminId::fromString($this->payload['id']);
    }

    /**
     * @return AdminName
     */
    public function name(): AdminName
    {
        return AdminName::fromString($this->payload['name']);
    }

    /**
     * @return array
     */
    public function payload(): array
    {
        return $this->payload;
    }

    /**
     * @param array $payload
     */
    protected function setPayload(array $payload): void
    {
        Assertion::keyExists($payload, 'id');
        Assertion::integer($payload, 'id');

        Assertion::keyExists($payload, 'name');
        Assertion::string($payload, 'name');

        $this->payload = $payload;
    }
}