<?php

declare(strict_types=1);

namespace Orangear\Admin\Model\Admin;

/**
 * Interface AdminCollection
 *
 * @package Orangear\Admin\Model\Admin
 */
interface AdminCollection
{
    /**
     * @param Admin $admin
     */
    public function save(Admin $admin): void;
}