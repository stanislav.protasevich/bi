<?php

declare(strict_types=1);

namespace Orangear\Admin\Model\Admin\Handler;

use Orangear\Admin\Model\Admin\Admin;
use Orangear\Admin\Model\Admin\AdminCollection;
use Orangear\Admin\Model\Admin\Command\CreateAdmin;

/**
 * Class CreateAdminHandler
 *
 * @package Orangear\Admin\Model\Admin\Handler
 */
class CreateAdminHandler
{
    /** @var AdminCollection */
    private $adminCollection;

    /**
     * CreateAdminHandler constructor
     *
     * @param AdminCollection $adminCollection
     */
    public function __construct(
        AdminCollection $adminCollection
    ) {
        $this->adminCollection = $adminCollection;
    }

    /**
     * @param CreateAdmin $command
     */
    public function __invoke(CreateAdmin $command): void
    {
        $admin = Admin::createWithData($command->name());

        $this->adminCollection->save($admin);
    }
}