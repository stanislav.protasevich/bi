<?php

declare(strict_types=1);

namespace Orangear\Admin\Model;

/**
 * Interface Entity
 *
 * @package Orangear\Admin\Model
 */
interface Entity
{
    /**
     * @param Entity $other
     *
     * @return bool
     */
    public function sameIdentityAs(Entity $other): bool;
}