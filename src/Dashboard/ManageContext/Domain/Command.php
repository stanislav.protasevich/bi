<?php

namespace Orangear\Admin\Dashboard\ManageContext\Domain;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use Zend\Filter\FilterInterface;
use Zend\Filter\Word\UnderscoreToCamelCase;
use Zend\Hydrator\Reflection;

/**
 * Class Command
 *
 * @package Orangear\Admin\Dashboard\ManageContext\Domain
 */
abstract class Command implements CommandInterface
{
    /**
     * Create command from array of data
     *
     * @param array $data
     * @param FilterInterface $filter
     *
     * @return CommandInterface
     */
    public static function hydrate(array $data, FilterInterface $filter = null)
    {
        $values = [];

        if (is_null($filter)) {
            $namingStrategy = new UnderscoreToCamelCase();

            foreach ($data as $key => $value) {
                $values[lcfirst($namingStrategy->filter($key))] = $value;
            }
        }

        $hydrator = new Reflection();

        /** @var CommandInterface $command */
        $command = $hydrator->hydrate($values, new static);

        return $command;
    }
}
