<?php

namespace Orangear\Admin\Dashboard\ManageContext\Domain;

use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Zend\Filter\FilterInterface;

/**
 * Interface CommandInterface
 *
 * @package Orangear\Admin\Dashboard\ManageContext\Domain
 */
interface CommandInterface
{
    /**
     * Create command from array of data
     *
     * @param array $data
     * @param FilterInterface $filter
     *
     * @return mixed
     */
    static function hydrate(array $data, FilterInterface $filter);

    /**
     * Create command from request
     *
     * @param ServerRequestInterface $request
     * @param FilterInterface|null $filter
     *
     * @return mixed
     */
    static function handleRequest(ServerRequestInterface $request, FilterInterface $filter = null);
}