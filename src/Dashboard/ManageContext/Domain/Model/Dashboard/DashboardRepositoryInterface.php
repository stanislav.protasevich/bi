<?php

namespace Orangear\Admin\Dashboard\ManageContext\Domain\Model\Project;

/**
 * Interface ProjectRepositoryInterface
 * @package Orangear\Admin\Project\ManageContext\Domain\Model\Project
 */
interface DashboardRepositoryInterface
{
    /**
     * @param int $id
     * @return null|Dashboard
     */
    public function getById(int $id): ?Dashboard;

    /**
     * @param Dashboard $project
     * @return mixed
     */
    public function create(Dashboard $project);

    /**
     * @param Dashboard $project
     * @return mixed
     */
    public function update(Dashboard $project);

    /**
     * @param Dashboard $project
     * @return mixed
     */
    public function delete(Dashboard $project);
}