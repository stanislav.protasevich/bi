<?php

namespace Orangear\Admin\Dashboard\ManageContext\Domain\Model\Dashboard;
use Orangear\Admin\Dashboard\ManageContext\Domain\Model\Project\Project;

/**
 * Class Dashboard
 *
 * @package Orangear\Admin\Dashboard\ManageContext\Domain\Model\Dashboard
 */
class Dashboard
{
    /** @var integer */
    private $id;

    /** @var Project */
    private $project;

    /** @var string */
    private $datetime;

    /** @var float */
    private $incomeToday;

    /** @var float */
    private $incomeYesterday;

    /** @var float */
    private $incomeLastSevenDays;

    /** @var float */
    private $incomeThisMonth;

    /** @var float */
    private $incomeLastMonth;

    /** @var float */
    private $incomeAllTime;

    /** @var float */
    private $incomeSuperlinkToday;

    /** @var float */
    private $incomeSuperlinkYesterday;

    /** @var float */
    private $incomeSuperlinkLastSevenDays;

    /** @var float */
    private $incomeSuperlinkThisMonth;

    /** @var float */
    private $incomeSuperlinkLastMonth;

    /** @var float */
    private $incomeSuperlinkAllTime;

    /** @var float */
    private $payoutToday;

    /** @var float */
    private $payoutYesterday;

    /** @var float */
    private $payoutLastSevenDays;

    /** @var float */
    private $payoutThisMonth;

    /** @var float */
    private $payoutLastMonth;

    /** @var float */
    private $payoutAllTime;

    /** @var integer */
    private $offersActive;

    /** @var integer */
    private $offersPrivate;

    /** @var integer */
    private $offersSuspended;

    /** @var integer */
    private $offersIncent;

    /** @var integer */
    private $offersNonIncent;

    /** @var integer */
    private $offersTotal;

    /** @var integer */
    private $offersPerDay;

    /** @var string */
    private $timezone;

    /**
     * Dashboard constructor.
     * @param int $id
     * @param Project $project
     * @param string $datetime
     * @param float $incomeToday
     * @param float $incomeYesterday
     * @param float $incomeLastSevenDays
     * @param float $incomeThisMonth
     * @param float $incomeLastMonth
     * @param float $incomeAllTime
     * @param float $incomeSuperlinkToday
     * @param float $incomeSuperlinkYesterday
     * @param float $incomeSuperlinkLastSevenDays
     * @param float $incomeSuperlinkThisMonth
     * @param float $incomeSuperlinkLastMonth
     * @param float $incomeSuperlinkAllTime
     * @param float $payoutToday
     * @param float $payoutYesterday
     * @param float $payoutLastSevenDays
     * @param float $payoutThisMonth
     * @param float $payoutLastMonth
     * @param float $payoutAllTime
     * @param int $offersActive
     * @param int $offersPrivate
     * @param int $offersSuspended
     * @param int $offersIncent
     * @param int $offersNonIncent
     * @param int $offersTotal
     * @param int $offersPerDay
     * @param string $timezone
     */
    public function __construct(
        $id,
        Project $project,
        $datetime,
        $incomeToday,
        $incomeYesterday,
        $incomeLastSevenDays,
        $incomeThisMonth,
        $incomeLastMonth,
        $incomeAllTime,
        $incomeSuperlinkToday,
        $incomeSuperlinkYesterday,
        $incomeSuperlinkLastSevenDays,
        $incomeSuperlinkThisMonth,
        $incomeSuperlinkLastMonth,
        $incomeSuperlinkAllTime,
        $payoutToday,
        $payoutYesterday,
        $payoutLastSevenDays,
        $payoutThisMonth,
        $payoutLastMonth,
        $payoutAllTime,
        $offersActive,
        $offersPrivate,
        $offersSuspended,
        $offersIncent,
        $offersNonIncent,
        $offersTotal,
        $offersPerDay,
        $timezone
    ) {
        $this->id = $id;
        $this->project = $project;
        $this->datetime = $datetime;
        $this->incomeToday = $incomeToday;
        $this->incomeYesterday = $incomeYesterday;
        $this->incomeLastSevenDays = $incomeLastSevenDays;
        $this->incomeThisMonth = $incomeThisMonth;
        $this->incomelastMonth = $incomeLastMonth;
        $this->incomeAllTime = $incomeAllTime;
        $this->incomeSuperlinkToday = $incomeSuperlinkToday;
        $this->incomeSuperlinkYesterday = $incomeSuperlinkYesterday;
        $this->incomeSuperlinkLastSevenDays = $incomeSuperlinkLastSevenDays;
        $this->incomeSuperlinkThisMonth = $incomeSuperlinkThisMonth;
        $this->incomeSuperlinkLastMonth = $incomeSuperlinkLastMonth;
        $this->incomeSuperlinkAllTime = $incomeSuperlinkAllTime;
        $this->payoutToday = $payoutToday;
        $this->payoutYesterday = $payoutYesterday;
        $this->payoutLastSevenDays = $payoutLastSevenDays;
        $this->payoutThisMonth = $payoutThisMonth;
        $this->payoutLastMonth = $payoutLastMonth;
        $this->payoutAllTime = $payoutAllTime;
        $this->offersActive = $offersActive;
        $this->offersPrivate = $offersPrivate;
        $this->offersSuspended = $offersSuspended;
        $this->offersIncent = $offersIncent;
        $this->offersNonIncent = $offersNonIncent;
        $this->offersTotal = $offersTotal;
        $this->offersPerDay = $offersPerDay;
        $this->timezone = $timezone;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Project
     */
    public function getProject(): Project
    {
        return $this->project;
    }

    /**
     * @return string
     */
    public function getDatetime(): string
    {
        return $this->datetime;
    }

    /**
     * @return float
     */
    public function getIncomeToday(): float
    {
        return $this->incomeToday;
    }

    /**
     * @return float
     */
    public function getIncomeYesterday(): float
    {
        return $this->incomeYesterday;
    }

    /**
     * @return float
     */
    public function getIncomeLastSevenDays(): float
    {
        return $this->incomeLastSevenDays;
    }

    /**
     * @return float
     */
    public function getIncomeThisMonth(): float
    {
        return $this->incomeThisMonth;
    }

    /**
     * @return float
     */
    public function getIncomeLastMonth(): float
    {
        return $this->incomeLastMonth;
    }

    /**
     * @return float
     */
    public function getIncomeAllTime(): float
    {
        return $this->incomeAllTime;
    }

    /**
     * @return float
     */
    public function getIncomeSuperlinkToday(): float
    {
        return $this->incomeSuperlinkToday;
    }

    /**
     * @return float
     */
    public function getIncomeSuperlinkYesterday(): float
    {
        return $this->incomeSuperlinkYesterday;
    }

    /**
     * @return float
     */
    public function getIncomeSuperlinkLastSevenDays(): float
    {
        return $this->incomeSuperlinkLastSevenDays;
    }

    /**
     * @return float
     */
    public function getIncomeSuperlinkThisMonth(): float
    {
        return $this->incomeSuperlinkThisMonth;
    }

    /**
     * @return float
     */
    public function getIncomeSuperlinkLastMonth(): float
    {
        return $this->incomeSuperlinkLastMonth;
    }

    /**
     * @return float
     */
    public function getIncomeSuperlinkAllTime(): float
    {
        return $this->incomeSuperlinkAllTime;
    }

    /**
     * @return float
     */
    public function getPayoutToday(): float
    {
        return $this->payoutToday;
    }

    /**
     * @return float
     */
    public function getPayoutYesterday(): float
    {
        return $this->payoutYesterday;
    }

    /**
     * @return float
     */
    public function getPayoutLastSevenDays(): float
    {
        return $this->payoutLastSevenDays;
    }

    /**
     * @return float
     */
    public function getPayoutThisMonth(): float
    {
        return $this->payoutThisMonth;
    }

    /**
     * @return float
     */
    public function getPayoutLastMonth(): float
    {
        return $this->payoutLastMonth;
    }

    /**
     * @return float
     */
    public function getPayoutAllTime(): float
    {
        return $this->payoutAllTime;
    }

    /**
     * @return int
     */
    public function getOffersActive(): int
    {
        return $this->offersActive;
    }

    /**
     * @return int
     */
    public function getOffersPrivate(): int
    {
        return $this->offersPrivate;
    }

    /**
     * @return int
     */
    public function getOffersSuspended(): int
    {
        return $this->offersSuspended;
    }

    /**
     * @return int
     */
    public function getOffersIncent(): int
    {
        return $this->offersIncent;
    }

    /**
     * @return int
     */
    public function getOffersNonIncent(): int
    {
        return $this->offersNonIncent;
    }

    /**
     * @return int
     */
    public function getOffersTotal(): int
    {
        return $this->offersTotal;
    }

    /**
     * @return int
     */
    public function getOffersPerDay(): int
    {
        return $this->offersPerDay;
    }

    /**
     * @return string
     */
    public function getTimezone(): string
    {
        return $this->timezone;
    }
}