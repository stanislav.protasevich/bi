<?php

namespace Orangear\Admin\Dashboard\ManageContext\Domain\Model\Project;

/**
 * Class Project
 * @package Orangear\Admin\Dashboard\ManageContext\Domain\Model\Project
 */
class Project
{
    /** @var int Project id */
    private $id;

    /**
     * Project constructor
     *
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}