<?php
namespace Orangear\Admin\Dashboard\ManageContext\Domain;

/**
 * Interface ServiceInterface
 * @package Orangear\Admin\Dashboard\ManageContext\Domain
 */
interface ServiceInterface
{
    /**
     * @param CommandInterface $command
     */
    public function execute(CommandInterface $command);
}