<?php

namespace Orangear\Admin\Dashboard\ManageContext\ApplicationBundle\Controller;

use Orangear\Admin\Dashboard\ManageContext\ApplicationBundle\Commands\DashboardGetByProjectIdCommand;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Psr\Http\Message\ServerRequestInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class UpdateDashboardController
 *
 * @package Orangear\Admin\Dashboard\ManageContext\ApplicationBundle\Controller
 */
final class UpdateDashboardController extends Controller
{
    /** @var  */
    private $validator;

    /**
     * DashboardController constructor
     *
     * @param ValidatorInterface $commandValidator
     */
    public function __construct(ValidatorInterface $commandValidator)
    {
        $this->validator = $commandValidator;
    }

    /**
     * @ApiDoc (
     *   section = "Dashboard",
     *   description = "Get dashboard by all owner's projects",
     *   requirements = {
     *     {
     *       "name"        = "owner_id",
     *       "dataType"    = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Owner identifier"
     *     }
     *   },
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization key"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Returned when api token is missing or incorrect",
     *     404 = {
     *       "Returned when the user is not found",
     *       "Returned when something else is not found"
     *     }
     *   }
     * )
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
//    public function getByOwnerIdAction(Request $request)
//    {
//        return $this->json([], 200, []);
//    }

    /**
     * @ApiDoc (
     *   section = "Dashboard",
     *   description = "Actualize data of project",
     *   requirements = {
     *     {
     *       "name"        = "project_id",
     *       "dataType"    = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Project identifier"
     *     }
     *   },
     *   parameters={
     *     {"name" = "offer", "dataType" = "int", "required" = false, "description" = "Offers income"},
     *     {"name" = "superlink", "dataType" = "int", "required" = false, "description" = "Superlink income"},
     *     {"name" = "payout", "dataType" = "int", "required" = false, "description" = "Payout"},
     *   },
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization key"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect",
     *     404 = {
     *       "Returned when the user is not found",
     *       "Returned when something else is not found"
     *     }
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        $command = DashboardGetByProjectIdCommand::hydrate($request->getAttributes());

//        var_dump($command); exit;

        if (count($errors = $this->validator->validate($command))) {
            return new JsonResponse([
                'errors' => $errors->get(1)
            ], 400);
        }

        return new JsonResponse([], 200, []);
    }





























    /**
     * @ApiDoc(
     *  section="Dashboard",
     *  description="Create new Dashboard",
     *  filters={
     *     {"name"="Dashboard_name", "dataType"="string", "required"=true, "description"="Dashboard name"},
     *  }
     * )
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
//    public function createAction(Request $request)
//    {
//        return $this->json([], 200, []);
//    }

    /**
     * @ApiDoc(
     *  section="Dashboard",
     *  description="Get list of Dashboards",
     *  filters={
     *     {"name"="page", "dataType"="integer", "required"=false, "description"="Page"},
     *     {"name"="limit", "dataType"="integer", "required"=false, "description"="Limit"},
     *  }
     * )
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
//    public function listAction(Request $request)
//    {
//        return $this->json([], 200, []);
//    }

    /**
     * @ApiDoc(
     *  section="Dashboard",
     *  description="Update Dashboard",
     *  filters={
     *     {"name"="Dashboard_id", "dataType"="integer", "required"=true, "description"="Dashboard id"},
     *  }
     * )
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
//    public function editAction(Request $request)
//    {
//        return $this->json([], 200, []);
//    }

    /**
     * @ApiDoc(
     *  section="Dashboard",
     *  description="Refresh Dashboard's token",
     *  filters={
     *     {"name"="Dashboard_id", "dataType"="integer", "required"=true, "description"="Dashboard id"},
     *     {"name"="Dashboard_id", "dataType"="integer", "required"=true, "description"="Dashboard id"},
     *  }
     * )
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
//    public function refreshTokenAction(Request $request)
//    {
//        return $this->json([], 200, []);
//    }

    /**
     * @ApiDoc(
     *  section="Dashboard",
     *  description="Delete Dashboard",
     *  filters={
     *     {"name"="Dashboard_id", "dataType"="integer", "required"=true, "description"="Dashboard id"},
     *  }
     * )
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
//    public function deleteAction(Request $request)
//    {
//        return $this->json([], 200, []);
//    }
}
