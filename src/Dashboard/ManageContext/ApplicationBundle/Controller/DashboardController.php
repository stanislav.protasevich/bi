<?php

namespace Orangear\Admin\Dashboard\ManageContext\ApplicationBundle\Controller;

use Orangear\Admin\Dashboard\ManageContext\ApplicationBundle\Commands\DashboardGetByProjectIdCommand;
use Orangear\Admin\Dashboard\ManageContext\ApplicationBundle\Exception\DashboardGetByProjectIdCommandValidationException;
use SimpleBus\Message\Bus\MessageBus;
use SimpleBus\Message\Bus\Middleware\MessageBusMiddleware;
use SimpleBus\Message\Bus\Middleware\MessageBusSupportingMiddleware;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Psr\Http\Message\ServerRequestInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class DashboardController
 *
 * @package Orangear\Admin\Dashboard\ManageContext\ApplicationBundle\Controller
 */
final class DashboardController extends Controller
{
    /** @var  */
    private $validator;

    /** @var MessageBus  */
    private $commandBus;

    /**
     * DashboardController constructor
     *
     * @param ValidatorInterface $commandValidator
     * @param MessageBus $commandBus
     */
    public function __construct(ValidatorInterface $commandValidator, MessageBus $commandBus)
    {
        $this->validator = $commandValidator;
        $this->commandBus = $commandBus;
    }

    /**
     * @ApiDoc (
     *   section = "Dashboard",
     *   description = "Get dashboard by all owner's projects",
     *   requirements = {
     *     {
     *       "name"        = "owner_id",
     *       "dataType"    = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Owner identifier"
     *     }
     *   },
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization key"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     403 = "Returned when api token is missing or incorrect",
     *     404 = {
     *       "Returned when the user is not found",
     *       "Returned when something else is not found"
     *     }
     *   }
     * )
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
//    public function getByOwnerIdAction(Request $request)
//    {
//        return $this->json([], 200, []);
//    }

    /**
     * @ApiDoc (
     *   section = "Dashboard",
     *   description = "Get dashboard of project",
     *   requirements = {
     *     {
     *       "name"        = "project_id",
     *       "dataType"    = "integer",
     *       "requirement" = "\d+",
     *       "description" = "Project identifier"
     *     }
     *   },
     *   parameters={
     *     {"name" = "start_date", "dataType" = "string", "required" = false, "description" = "Start date"},
     *     {"name" = "end_date", "dataType" = "string", "required" = false, "description" = "End date"},
     *   },
     *   headers = {
     *     {
     *       "name"        = "Authorization",
     *       "required"    = "true",
     *       "description" = "Bearer authorization key"
     *     }
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Bad Request",
     *     403 = "Returned when api token is missing or incorrect",
     *     404 = {
     *       "Returned when the user is not found",
     *       "Returned when something else is not found"
     *     }
     *   }
     * )
     *
     * @param ServerRequestInterface $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $command = DashboardGetByProjectIdCommand::handleRequest($request);

            $this->commandBus->handle($command);

            return new JsonResponse([], 200, []);
        }
        catch (DashboardGetByProjectIdCommandValidationException $e) {
            return new JsonResponse(['errors' => $e->getMessages()], $e->getStatusCode());
        }
        catch (\Exception $e) {
            return new JsonResponse(['errors' => $e->getMessage()], 400);
        }
    }





























    /**
     * @ApiDoc(
     *  section="Dashboard",
     *  description="Create new Dashboard",
     *  filters={
     *     {"name"="Dashboard_name", "dataType"="string", "required"=true, "description"="Dashboard name"},
     *  }
     * )
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
//    public function createAction(Request $request)
//    {
//        return $this->json([], 200, []);
//    }

    /**
     * @ApiDoc(
     *  section="Dashboard",
     *  description="Get list of Dashboards",
     *  filters={
     *     {"name"="page", "dataType"="integer", "required"=false, "description"="Page"},
     *     {"name"="limit", "dataType"="integer", "required"=false, "description"="Limit"},
     *  }
     * )
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
//    public function listAction(Request $request)
//    {
//        return $this->json([], 200, []);
//    }

    /**
     * @ApiDoc(
     *  section="Dashboard",
     *  description="Update Dashboard",
     *  filters={
     *     {"name"="Dashboard_id", "dataType"="integer", "required"=true, "description"="Dashboard id"},
     *  }
     * )
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
//    public function editAction(Request $request)
//    {
//        return $this->json([], 200, []);
//    }

    /**
     * @ApiDoc(
     *  section="Dashboard",
     *  description="Refresh Dashboard's token",
     *  filters={
     *     {"name"="Dashboard_id", "dataType"="integer", "required"=true, "description"="Dashboard id"},
     *     {"name"="Dashboard_id", "dataType"="integer", "required"=true, "description"="Dashboard id"},
     *  }
     * )
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
//    public function refreshTokenAction(Request $request)
//    {
//        return $this->json([], 200, []);
//    }

    /**
     * @ApiDoc(
     *  section="Dashboard",
     *  description="Delete Dashboard",
     *  filters={
     *     {"name"="Dashboard_id", "dataType"="integer", "required"=true, "description"="Dashboard id"},
     *  }
     * )
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
//    public function deleteAction(Request $request)
//    {
//        return $this->json([], 200, []);
//    }
}
