<?php

namespace Orangear\Admin\Dashboard\ManageContext\ApplicationBundle\Exception;

/**
 * Class DashboardGetByProjectIdCommandValidationException
 *
 * @package Orangear\Admin\Dashboard\ManageContext\ApplicationBundle\Exception
 */
class DashboardGetByProjectIdCommandValidationException extends \Exception {}