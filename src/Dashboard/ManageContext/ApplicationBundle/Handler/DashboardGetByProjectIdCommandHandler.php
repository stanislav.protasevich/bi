<?php

namespace Orangear\Admin\Dashboard\ManageContext\ApplicationBundle\Handler;

use Orangear\Admin\Dashboard\ManageContext\ApplicationBundle\Exception\DashboardGetByProjectIdCommandValidationException;
use Orangear\Admin\Dashboard\ManageContext\Domain\CommandInterface;

class DashboardGetByProjectIdCommandHandler
{
    public function __construct()
    {
    }

    /**
     * Get project
     *
     * @param  CommandInterface $command
     * @throws DashboardGetByProjectIdCommandValidationException
     */
    public function handle(CommandInterface $command)
    {

    }
}