<?php

namespace Orangear\Admin\Dashboard\ManageContext\ApplicationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class OrangearAdminDashboardManageContextApplicationBundle extends Bundle {}
