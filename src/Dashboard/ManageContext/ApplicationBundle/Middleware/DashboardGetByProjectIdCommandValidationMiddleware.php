<?php

namespace Orangear\Admin\Dashboard\ManageContext\ApplicationBundle\Middleware;

use Orangear\Admin\Dashboard\ManageContext\ApplicationBundle\Exception\DashboardGetByProjectIdCommandValidationException;
use SimpleBus\Message\Bus\Middleware\MessageBusMiddleware;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class DashboardGetByProjectIdCommandValidationMiddleware
 *
 * @package Orangear\Admin\Dashboard\ManageContext\ApplicationBundle\Middleware
 */
class DashboardGetByProjectIdCommandValidationMiddleware implements MessageBusMiddleware
{
    /** @var ValidatorInterface */
    protected $validator;

    /**
     * Dependency Injection constructor.
     *
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * {@inheritdoc}
     */
    public function handle($message, callable $next)
    {
        $violations = $this->validator->validate($message);

        if (count($violations) > 0) {
            throw new DashboardGetByProjectIdCommandValidationException(
                'Bad dashboard getByProjectId command',
                $violations
            );
        }

        $next($message);
    }
}