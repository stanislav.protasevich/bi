<?php

namespace Orangear\Admin\Dashboard\ManageContext\ApplicationBundle\Commands;

use Orangear\Admin\Dashboard\ManageContext\Domain\Command;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Filter\FilterInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class DashboardGetByProjectIdCommand
 *
 * @package Orangear\Admin\Dashboard\ManageContext\ApplicationBundle\Commands
 */
class DashboardGetByProjectIdCommand extends Command
{
    /**
     * Project identifier
     *
     * @var integer
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    public $projectId = null;

    /**
     * GetDashboardByProjectIdCommand constructor
     *
     * @param int $projectId
     */
    public function __construct($projectId = null)
    {
        $this->projectId = $projectId;
    }

    /**
     * Create command from request
     *
     * @param ServerRequestInterface $request
     * @param FilterInterface|null $filter
     *
     * @return mixed
     */
    public static function handleRequest(ServerRequestInterface $request, FilterInterface $filter = null)
    {
        $data = array_merge($request->getAttributes());

        return self::hydrate($data, $filter);
    }
}