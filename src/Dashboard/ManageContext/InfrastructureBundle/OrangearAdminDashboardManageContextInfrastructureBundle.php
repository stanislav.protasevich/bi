<?php

namespace Orangear\Admin\Dashboard\ManageContext\InfrastructureBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class OrangearAdminDashboardManageContextInfrastructureBundle
 * @package Orangear\Admin\Dashboard\ManageContext\InfrastructureBundle
 */
class OrangearAdminDashboardManageContextInfrastructureBundle extends Bundle {}