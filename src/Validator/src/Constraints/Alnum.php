<?php

declare(strict_types = 1);

namespace Orangear\Common\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class Alnum
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @package Orangear\Common\Validator\Constraints
 */
final class Alnum extends Constraint
{
    const ALNUM_FAILED_ERROR = '9d522ddf-2a4b-46bb-b5a4-c6520c44f4dd';

    protected static $errorNames = array(
        self::ALNUM_FAILED_ERROR => 'ALNUM_FAILED_ERROR',
    );

    public $message = 'This value is not valid.';
}
