<?php

declare(strict_types = 1);

namespace Orangear\Common\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class AlnumValidator
 * @package Orangear\Common\Validator\Constraints
 */
final class AlnumValidator extends ConstraintValidator
{
    const PATTERN = '/^[\p{L}\p{N} ’\'-]+$/u';

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof Alnum) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\Alnum');
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_scalar($value) && !(is_object($value) && method_exists($value, '__toString'))) {
            throw new UnexpectedTypeException($value, 'string');
        }

        $value = (string) $value;
        if ('' === $value) {
            return;
        }

        $pattern = static::PATTERN;

        if (!preg_match($pattern, implode(array_map('trim', explode(' ',$value))))) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $this->formatValue($value))
                ->addViolation();

            return;
        }
    }
}
