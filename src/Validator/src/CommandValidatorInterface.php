<?php

declare(strict_types = 1);

namespace Orangear\Common\Validator;

use Orangear\BusinessIntelligence\Domain\Model\CommandInterface;

/**
 * Interface ValidatorInterface
 * @package Orangear\Common\Validator
 */
interface CommandValidatorInterface
{
    /**
     * @param CommandInterface $command
     * @return array
     */
    public function validate(CommandInterface $command): array;
}
