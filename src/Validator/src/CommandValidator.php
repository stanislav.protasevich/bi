<?php

declare(strict_types = 1);

namespace Orangear\Common\Validator;

use Orangear\BusinessIntelligence\Domain\Model\CommandInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ValidatorBuilderInterface;

/**
 * Class Validator
 * @package Orangear\Common\Validator
 */
final class CommandValidator implements CommandValidatorInterface
{
    /** @var ValidatorBuilderInterface */
    private $validator;

    /**
     * CommandCommandValidator constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @inheritdoc
     */
    public function validate(CommandInterface $command): array
    {
        $errors = [];

        if (($violations = $this->validator->validate($command))) {
            /** @var ConstraintViolationInterface $violation */
            foreach ($violations as $violation) {
                $errors[$violation->getPropertyPath()][] = $violation->getMessage();
            }
        }

        return $errors;
    }
}
