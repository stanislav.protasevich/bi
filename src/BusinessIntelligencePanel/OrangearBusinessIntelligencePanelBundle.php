<?php

namespace Orangear\BusinessIntelligencePanel;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class OrangearBusinessIntelligencePanelBundle
 *
 * @package Orangear\BusinessIntelligencePanel
 */
class OrangearBusinessIntelligencePanelBundle extends Bundle {}
