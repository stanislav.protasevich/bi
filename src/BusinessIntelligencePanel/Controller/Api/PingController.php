<?php

namespace Orangear\BusinessIntelligencePanel\Controller\Api;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class PingController
 *
 * @package Orangear\BusinessIntelligencePanel\Controller\Api
 */
final class PingController
{
    /**
     * @ApiDoc(
     *   section = "API",
     *   description = "API ping",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Bad Request",
     *   }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        return new JsonResponse(['ack' => time()], JsonResponse::HTTP_OK);
    }
}
