<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Rezzza\DoctrineSchemaMultiMapping\App\Bundle\DoctrineSchemaMultiMappingBundle(),
            new Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle(),
            new SimpleBus\SymfonyBridge\SimpleBusCommandBusBundle(),
            new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
            new Nelmio\CorsBundle\NelmioCorsBundle(),
            new Prooph\Bundle\ServiceBus\ProophServiceBusBundle,

            // My bundles
            new Orangear\MembershipBundle\OrangearMembershipBundle(),

            // Orangear Business Intelligence API
            new Orangear\BusinessIntelligence\Api\OrangearBusinessIntelligenceApiBundle(),
            new \Orangear\BusinessIntelligencePanel\OrangearBusinessIntelligencePanelBundle(),

            // Core Context
            new Orangear\Admin\Core\ApplicationBundle\OrangearAdminCoreApplicationBundle(),
            new Orangear\Admin\Core\InfrastructureBundle\OrangearAdminCoreInfrastructureBundle(),

//            new Orangear\Admin\SuperAdmin\AuthenticationContext\ApplicationBundle\OrangearAdminSuperAdminAuthenticationContextApplicationBundle(),
//            new Orangear\Admin\SuperAdmin\AuthenticationContext\InfrastructureBundle\OrangearAdminSuperAdminAuthenticationContextInfrastructureBundle(),
//
//            new Orangear\Admin\Project\AuthenticationContext\ApplicationBundle\OrangearAdminProjectAuthenticationContextApplicationBundle(),
//            new Orangear\Admin\Project\AuthenticationContext\InfrastructureBundle\OrangearAdminProjectAuthenticationContextInfrastructureBundle(),
//
//            new Orangear\Admin\Project\ManageContext\ApplicationBundle\OrangearAdminProjectManageContextApplicationBundle(),
//            new Orangear\Admin\Project\ManageContext\InfrastructureBundle\OrangearAdminProjectManageContextInfrastructureBundle(),
//
//            new Orangear\Admin\Dashboard\ManageContext\ApplicationBundle\OrangearAdminDashboardManageContextApplicationBundle(),
//            new Orangear\Admin\Dashboard\ManageContext\InfrastructureBundle\OrangearAdminDashboardManageContextInfrastructureBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
