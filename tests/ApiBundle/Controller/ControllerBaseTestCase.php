<?php

namespace Orangear\BusinessIntelligencePanelTest\Api\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;

/**
 * Class ControllerBaseTestCase
 */
abstract class ControllerBaseTestCase extends WebTestCase
{
    /** @var Client */
    protected $client;

    /**
     * Initialization
     */
    public function setUp() {}

    /**
     * @return Client
     */
    private function client()
    {
        $client = static::createClient();

        return $client;
    }

    /**
     * @param string $type
     * @param string $url
     * @param array $payload
     *
     * @return Client
     */
    protected function request(string $type, string $url, array $payload)
    {
        $client = $this->client();

        $client->request(
            $type,
            $url,
            array(),
            array(),
            array(),
            http_build_query($payload)
        );

        return $client;
    }
}