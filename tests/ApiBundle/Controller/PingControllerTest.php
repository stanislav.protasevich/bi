<?php

namespace Orangear\BusinessIntelligencePanelTest\Api\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PingControllerTestCase
 */
class PingControllerTest extends ControllerBaseTestCase
{
    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();

        $this->client = $this->request(
            Request::METHOD_GET,
            '/api/v1/ping',
            []
        );
    }

    /**
     * Test ping Business Intelligence Panel API returns HTTP status 200
     */
    public function testPingReturnsHttpStatus200()
    {
        $this->assertEquals(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Test ping Business Intelligence Panel API returns ack parameter in response data
     */
    public function testPingReturnsAckParameterInResponseData()
    {
        $data = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('ack', $data);
    }
}
