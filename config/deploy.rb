lock '3.10.0'

set :application, 'orangear-business-intelligence'
set :repo_url, 'git@gitlab.com:stanislav.protasevich/bi.git'

set :scm, :git
set :format, :pretty
set :log_level, :debug
set :pty, true
set :use_sudo, false

set :linked_files, fetch(:linked_files, []).push('app/config/parameters.yml')
set :default_env, { path: "/opt/ruby/bin:$PATH" }
set :keep_releases, 1
set :symfony_env,  "prod"
set :app_path,              "app"
set :web_path,              "web"
set :log_path,              "var/logs"
set :cache_path,            "var/cache"
set :app_config_path,       "app/config"
set :controllers_to_clear, ["app_*.php"]
set :file_permissions_paths,         ["var"]
set :permission_method,     :chmod
set :use_set_permissions,   true
set :symfony_console_path, "bin/console"
set :symfony_console_flags, "--no-debug"
set :assets_install_path,   fetch(:web_path)
set :assets_install_flags,  '--symlink'
set :assetic_dump_flags,  ''
fetch(:default_env).merge!(symfony_env: fetch(:symfony_env))
set :composer_install_flags, '--prefer-dist --optimize-autoloader --ignore-platform-reqs --no-interaction'

after 'deploy:updated', 'symfony:clear_controllers' do
  on roles(:all) do
        #invoke 'deploy:set_permissions:chgrp'
        invoke 'deploy:set_permissions:chmod'
  end
end

before 'deploy:check', :touch_shared_files do
    on roles(:all) do
        execute("cd #{shared_path}/app/config && [ -e parameters.yml ] || touch parameters.yml")
    end
end

