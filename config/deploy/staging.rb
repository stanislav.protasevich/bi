server '138.201.60.158', user: 'finadmin', roles: %w{app db web}
set :deploy_to, '/home/finadmin/backend'
set :file_permissions_users, ['finadmin']
set :file_permissions_groups, ['finadmin']
set :webserver_user, "finadmin"
set :branch, 'master'
set :tmp_dir, "/home/finadmin/backend/tmp"