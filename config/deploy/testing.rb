server '138.201.60.158', user: 'ga', roles: %w{app db web}
set :deploy_to, '/home/ga'
set :file_permissions_users, ['ga']
set :file_permissions_groups, ['ga']
set :webserver_user, "ga"
set :branch, 'master'
set :tmp_dir, "/home/ga/tmp"