server '88.99.5.95', user: 'finadmin', roles: %w{app db web}
set :deploy_to, '/home/finadmin/backend'
set :file_permissions_users, ['finadmin:www-data']
set :file_permissions_groups, ['finadmin:www-data']
set :webserver_user, "finadmin"
set :branch, 'master'
set :tmp_dir, "/home/finadmin/backend/tmp"